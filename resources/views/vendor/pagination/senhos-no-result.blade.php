@if ($paginator->hasPages())
<div class="row pagination-around mt-2" role="navigation">
    <div class="col-12 pagination p1 p-2">
        <ul>
        @if (!$paginator->onFirstPage())
            <a href="{{ $paginator->previousPageUrl() }}" rel="prev" aria-label="@lang('pagination.previous')">
                <li aria-disabled="true" aria-label="@lang('pagination.previous')">«</li>
            </a>
        @endif

        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- "Three Dots" Separator --}}
            @if (is_string($element))
                <li class="page-item disabled" aria-disabled="true"><span class="page-link">{{ $element }}</span></li>
            @endif

            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <a href="javascript:;" class="is-active">
                            <li><span>{{ $page }}</span></li>
                        </a>
                    @else
                        <a href="{{ $url }}">
                            <li>{{ $page }}</li>
                        </a>
                    @endif
                @endforeach
            @endif
        @endforeach
        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <a href="{{ $paginator->nextPageUrl() }}" rel="prev" aria-label="@lang('pagination.previous')">
                <li>»</li>
            </a>
        @endif
        </ul>
    </div>
    
</div>
@endif