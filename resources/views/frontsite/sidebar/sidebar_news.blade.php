<aside class="col-12 col-xl-4">

    <!-- Widget Lasted News -->
    @include('frontsite.widgets.lasted_news')
    <!-- Widget Video -->
    @include('frontsite.widgets.gallery_video')    
    <!-- Widget Deal -->
    @include('frontsite.widgets.top_deal')

</aside>