<section class="mt-0 mt-sm-5" id="block-about">
    <div class="container">
        <div class="row">
            <div class="col-xl-6 col-12" data-animation="fadeInLeft">
                <a href="#">
                    @if($setting->get('theme_option::home::about_media_option') === 'image')
                    
                    <img src="{{$setting->get('theme_option::home::about_image', 'images/GT-senhos.png')}}">
                    @else
                    <video id="about_video" preload="auto" autoplay="" muted="" volume="1" playsinline="" data-vscid="4tl51qmzo">
                        <source src="{$setting->get('theme_option::home::about_video', '')}} " type="video/mp4"></video>
                    @endif
                   

                </a>
            </div>
            <div class="col-xl-6 col-12">
                <h4 class="title-about"><span>{{ __('frontsite.home.about_text') }}</span></h4>
                <div class="content-about" data-scrollbar>
                    {!! $setting->get('theme_option::home::about_content', '') !!}

                    </a>
                </div>
            </div>
        </div>
    </div>
</section>