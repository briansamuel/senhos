<section class="" id="block-project">
    <div class="container">
        <div class="row mt-4 mb-4 justify-content-center">
            <h3 class="text-center  title-block">{{ __('frontsite.home.project_feature') }}</h3>
        </div>
        <div class="row">
            @foreach($projects as $index => $project)
            <div class="col-12 col-sm-4 p-2 slideInRight animated" data-animation="slideInRight" data-delay="{{ $index*500 }}">
                <div class="project-item text-center">
                    <a href="du-an/{{ $project->project_slug }}.html" class="effect-zoom-in">
                        <img class="img-fluid" src="{{ $project->project_thumbnail }}" alt="{{ $project->project_title }}">
                    </a>
                    <a href="du-an/{{ $project->project_slug }}.html">
                        <h4 class="project-title mt-1 mb-0 mt-sm-4 mb-sm-1">{{ $project->project_title }}</h4>
                    </a>
                    
                    <span class="project-location ">{{ $project->project_location }}</span>
                </div>
            </div>
            @endforeach
        </div>

    </div>
</section>