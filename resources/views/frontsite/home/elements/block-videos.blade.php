<section class="" id="block-gallery-video">
    <div class="container">
        <div class="row mt-4 mb-4 justify-content-center">
            <h3 class="text-center  title-block">{{ __('frontsite.home.gallery_video') }}</h3>
        </div>
        <div class="row lastest-videos">
            @foreach($lasted_video as $index => $video)

            <div class="col-12 col-sm-4 p-2 slideInLeft animated" data-animation="slideInLeft" data-delay="{{ $index*500 }}">
                <div class="item-video text-center ">
                    <a href="#popup-block-video" class="various" iframe="{{ $video->post_content }}">
                        <img class="img-fluid" src="{{ $video->post_thumbnail }}" alt="{{ $video->post_title }}">

                    </a>

                    <h4 class="project-title mt-1 mb-0 mt-sm-4 mb-sm-1">{{ $video->post_title }}</h4>

                </div>
            </div>
            @endforeach



        </div>
    </div>
</section>
<!-- Popup Video -->
<div id="popup-block-video" class="popup-block-video">
    
</div>
<style>
    .popup-block-video {display: none;height: 100%;}
    .popup-block-video>p {height: 100%;margin: 0; padding: 0;}
    .popup-block-video iframe {width: 100%; height: 100% !important}
    @media (max-width: 576px) {
        #popup-block-video p {
            position: relative;
            padding-bottom: 56.25%;
            padding-top: 25px;
            height: 0;
        }
        #popup-block-video p iframe {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
        }
    }
</style>