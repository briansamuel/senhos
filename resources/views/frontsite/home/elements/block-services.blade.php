<section class="" id="block-service">
    <div class="container">
        <div class="row mt-3 mb-4 justify-content-center">
            <h3 class="text-center title-block">DỊCH VỤ</h3>
        </div>
        <div class="row">
            <div class="col-12 col-sm-4 p-2">
                <div class="service-item">
                    <img class="img-fluid " src="images/tu-van---van-hanh-hotel-ressort.jpg" alt="Card image cap">
                    <div class="block-outer-serivce">
                        <div class="block-inner-serivce">
                            <div class="baclground-inner-service">
                                <a href="tu-van-setup-quan-ly-va-van-hanh-hotel-resort/">
                                    <h4>TƯ VẤN<br>QUẢN LÝ & VẬN HÀNH <br>HOTEL & RESORT</h4>
                                </a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-4 p-2">
                <div class="service-item">
                    <img class="img-fluid" src="images/tu-van---setup--quan-li-nha-hang.jpg" alt="Card image cap">
                    <div class="block-outer-serivce">
                        <div class="block-inner-serivce">
                            <div class="baclground-inner-service">
                                <a href="tu-van-setup-quan-ly-van-hanh-nha-hang-cafe/">
                                    <h4>TƯ VẤN SETUP<br>QUẢN LÝ & VẬN HÀNH <br>NHÀ HÀNG, CAFE</h4>
                                </a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-4 p-2">
                <div class="service-item">
                    <img class="img-fluid" src="images/tu-van---set-up-qlvh-bar-louge.jpg" alt="Card image cap">
                    <div class="block-outer-serivce">
                        <div class="block-inner-serivce">
                            <div class="baclground-inner-service">
                                <a href="tu-van-quan-ly-van-hanh-bar-lounge/">
                                    <h4>TƯ VẤN<br>QUẢN LÝ & VẬN HÀNH <br>BAR - LOUNGE</h4>
                                </a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="row ">
            <div class="col-12 col-sm-4 p-2">
                <div class="service-item">
                    <img class="img-fluid" src="images/tu-van-plvh-spa.jpg" alt="Card image cap">
                    <div class="block-outer-serivce">
                        <div class="block-inner-serivce">
                            <div class="baclground-inner-service">
                                <a href="tu-van-quan-ly-van-hanh-spa-massage/">
                                    <h4>TƯ VẤN<br>QUẢN LÝ & VẬN HÀNH <br>SPA - MASSAGE</h4>
                                </a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-4 p-2">
                <div class="service-item">
                    <img class="img-fluid" src="images/tai-cau-truc-dinh-vi-thuong-hieu.jpg" alt="Card image cap">
                    <div class="block-outer-serivce">
                        <div class="block-inner-serivce">
                            <div class="baclground-inner-service">
                                <h4>ĐỊNH VỊ TÁI CẤU TRÚC<br> DOANH NGHIỆP</h4>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-4 p-2">
                <div class="service-item">
                    <img class="img-fluid" src="images/cung-ung-dao-tao-nhan-luc.jpg" alt="Card image cap">
                    <div class="block-outer-serivce">
                        <div class="block-inner-serivce">
                            <div class="baclground-inner-service">
                                <a href="cung-ung-dao-tao-nguon-nhan-luc/">
                                    <h4>CUNG ỨNG - ĐÀO TẠO<br>NGUỒN NHÂN LỰC</h4>
                                </a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>