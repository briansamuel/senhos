@extends('frontsite.index')
@section('page-header', 'Trang')
@section('page-sub_header', 'Danh sách trang')
@section('style')
<link rel="stylesheet" href="/fancybox/jquery.fancybox.css?v=2.1.7" type="text/css" media="screen" />
<style>
        body {
            @if($setting->get('theme_option::general::is_background') === 'on')
                @if($setting->get('theme_option::general::option_background', '') === 'color')
                    background-color: {{$setting->get('theme_option::general::color')}};
                @else
                    background-image: url("{{$setting->get('theme_option::general::image', __('frontsite.general.image'))}}");
                @endif
            @endif
        }
    </style>
@endsection
@section('content')
<div class="main-wrapper">
    @include('frontsite.elements.banner')
    <!-- END Block Banner and Search Form -->
    <div class="custom-background container bg-white">
    <!-- START BLOCK ABOUT -->
    @include('frontsite.home.elements.block-about')
    <!-- END Block ABOUT -->
    <!-- START Block SERVICE -->
    @include('frontsite.home.elements.block-services')
    <!-- END Block SERVICE -->

    <!-- START BLOCK PROJECT -->
    @include('frontsite.home.elements.block-projects')
    <!-- END BLOCK PROJECT -->

    <!-- START GALLERY VIDEO -->
    @include('frontsite.home.elements.block-videos')
    <!-- END GALLERY VIDEO -->
    </div>
    <!-- START BLOCK PARTNER -->
    @include('frontsite.elements.partner')
    <!-- END BLOCK PARTNER -->
</div>

<!-- START BLOCK INTRO VIDEO -->
@include('frontsite.elements.popup')
<!-- END BLOCK INTRO VIDEO -->

<!-- START BLOCK INTRO VIDEO -->
@include('frontsite.elements.intro-slash')
<!-- END BLOCK INTRO VIDEO -->
@endsection
@section('script')
<script type="text/javascript" src="/fancybox/jquery.fancybox.pack.js?v=2.1.7"></script>
<script>
    $(document).ready(function() {
       




        $(".various").fancybox({
            maxWidth: 1300,
            maxHeight: 900,
            fitToView: false,
            width: '85%',
            height: '85%',
            autoSize: false,
            closeClick: false,
            openEffect: 'none',
            closeEffect: 'none'        
        });
        $(".item-video a").click(function() {
            var iframe = $(this).attr('iframe');
            $('#popup-block-video').html(iframe);
        });
    });
</script>
@endsection