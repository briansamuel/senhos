@extends('frontsite.index')
@section('title', $news->post_title)
@section('title_ab', $news->post_title)
@section('keyword', $news->post_seo_keyword ? $news->post_seo_keyword : $news->post_title)
@section('description', $news->post_seo_description ? $news->post_seo_description : $news->post_title)
@section('style')

@endsection
@section('content')
<!-- START Block Banner  -->

@include('frontsite.elements.banner')
<!-- END Block Banner -->
<!-- START BREADCRUMB -->
@include('frontsite.elements.breadcrumb')
<!-- END BREADCRUMB -->
<div class="main-wrapper mt-4 mb-4">
    <div class="container">
        <div class="row list-post-row">

            <!-- START SIDEBAR -->
            @include('frontsite.sidebar.sidebar_news')
            <!-- END SIDEBAR -->

            <div id="post-{{ $news->id }}" class="col-12 col-xl-8 mt-0 pt-0 pt-sm-3 mt-sm-4 single-post">
                <div class="post-header">
                    <h2 class="post-title">{{ $news->post_title }}</h2>
                </div>
                <div class="post-meta">
                    <span class="post-time"><i class="fas fa-clock"></i> {{ date("d-m-Y H:i:s", strtotime($news->created_at)) }}</span>
                    <span class="ml-4 mr-4">|</span>
                    <span class="post-view">{{$news->total_view}} lượt xem</span>
                </div>
                <!-- Go to www.addthis.com/dashboard to customize your tools --> <div class="addthis_inline_share_toolbox"></div>
                <div class="post-content">

                    {!! $news->post_content !!}
                    


                </div>
                <div class="related-posts mt-4">
                    <div class="row ">
                        <div class="col-12">
                            <div class="header-related-block">
                                <span>{{ __('frontsite.news.other_news')}}</span>
                            </div>

                        </div>

                    </div>
                    <!-- Related News -->
                    @include('frontsite.news.elements.related_news')
                    <!-- End Related News -->
                </div>
            </div>
        </div>
    </div>
</div>

<!-- START TOP FOOTER 1 -->
@include('frontsite.elements.top-footer-1')
<!-- END TOP FOOTER 1 -->
<!-- START TOP FOOTER 2  -->
@include('frontsite.elements.top-footer-2')
<!-- END TOP FOOTER 2 -->
@endsection
@section('script')
<!-- Go to www.addthis.com/dashboard to customize your tools --> 
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-59250aab9e629a6f"></script>
@endsection