@forelse($related_news as $key=>$new)
    @if($key === 0)
    <div class="row post-3-col">
    @endif
    @if($key < 3)
    <div class="col-12 col-xl-4 item-related-post">
        <a href="{{$new->post_slug}}.html" class="thumbnail-related-post effect-zoom-in">
            <img src="{{$new->post_thumbnail}}">
        </a>
        <a href="{{$new->post_slug}}.html" title="{{$new->post_title}}">
            <h4>{{$new->post_title}}</h4>
        </a>
        <span class="related-post-time"><i class="fas fa-clock"></i> {{date("m-Y H:i", strtotime($new->created_at))}}</span>
    </div>
    @elseif($key === 3)
    <div class="row post-2-col">
        <div class="col-12">
            <hr class="bg-dark mb-4">
        </div>
    @else
        <div class="col-12 col-xl-6 item-news item-related-post">
            <a class="thumbnail-news effect-zoom-in" href="{{$new->post_slug}}.html">
                <img src="{{$new->post_thumbnail}}" class="img-fluid">
            </a>
            <div class="info-news">
                <a href="{{$new->post_slug}}.html">
                    <h4>{{$new->post_title}}</h4>
                </a>
                <p>{!! $new->post_seo_description !!}</p>
                <span class="related-post-time"><i class="fas fa-clock"></i> {{date("d-m-Y", strtotime($new->created_at))}}</span>
            </div>
        </div>
    @endif
    @if($key === 2 || $key === 6)
    </div>
    @endif
@empty
@endforelse
</div>