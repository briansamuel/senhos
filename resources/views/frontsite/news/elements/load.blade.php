<div id="load" style="position: relative;">
@foreach($news as $index => $new)
    <article class="row item-post">
        <div class="thumbnail-post">
            <a href="{{ $new->post_slug }}.html" class="effect-zoom-in">
                <img src="{{ $new->post_thumbnail }}" onError="this.onerror=null;this.src='/images/default/no-image.jpg';"  class="img-fluid w-100 h-100">
            </a>
        </div>
        <div class="info-post">
            <a href="{{ $new->post_slug }}.html">
                <h3>{{ $new->post_title }}</h3>
            </a>
            <div class="timeline-post">
                <span>{{ $new->post_author }}</span>
                <span>{{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $new->created_at)->format('d/m/Y') }}</span>
                <span>{{ $new->total_view }} {{ __('frontsite.news.views') }}</span>
            </div>
            <p>{{ html_entity_decode(strip_tags($new->post_description)) }}</p>
            <a href="{{ $new->post_slug }}.html" class="btn-post-more">{{ __('frontsite.news.loadmore') }}</a>
        </div>
    </article>
@endforeach
</div>
{{$news->links()}}