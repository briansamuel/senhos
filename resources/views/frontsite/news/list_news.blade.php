@extends('frontsite.index')
@section('title', __('seo.news.title'))
@section('title_ab', __('seo.news.title_ab'))
@section('keyword', __('seo.news.keyword') )
@section('description', __('seo.news.description'))
@section('style')

@endsection
@section('content')
<!-- START Block Banner  -->

@include('frontsite.elements.banner')
<!-- END Block Banner -->
<!-- START BREADCRUMB -->
@include('frontsite.elements.breadcrumb')
<!-- END BREADCRUMB -->
<div class="main-wrapper mt-4 mb-4">
    <div class="container">
        <div class="row mb-4">
            <div class="col-12">
                <ul id="categories_tabs" class="nav d-flex justify-content-center categories_tabs">
                    @foreach($categories as $index => $category)
                    <li class="{{ $index == 0 ? 'active' : '' }}"><a href="#" data-id="{{ $category->id }}">{{ $category->category_name }}</a></il>
                    @endforeach
                   
                <ul>
            </div>
        </div>
        <div class="row list-post-row">

            <!-- START SIDEBAR -->
            @include('frontsite.sidebar.sidebar_news')
            <!-- END SIDEBAR -->

            <div id="list-news" class="col-12 col-xl-8 mt-4 pt-3 list-view">
                
                @if (count($news) > 0)
                    <section class="news">
                        <div class="loader" style="display: none">Loading...</div>
                        @include('frontsite.news.elements.load')
                    </section>
                @endif
            </div>
        </div>
    </div>
</div>

<!-- START TOP FOOTER 1 -->
@include('frontsite.elements.top-footer-1')
<!-- END TOP FOOTER 1 -->
<!-- START TOP FOOTER 2  -->
@include('frontsite.elements.top-footer-2')
<!-- END TOP FOOTER 2 -->
@endsection
@section('script')
    <script type="text/javascript">

        $(function() {
            var category_id = null;
            $('body').on('click', '.pagination a:not(.is-active)', function(e) {
                e.preventDefault();

                $('#load a').css('color', '#dfecf6');
                $('.loader').show();
                var url = $(this).attr('href');
                getNews(url);
                window.history.pushState("", "", url);
            });
            $('.categories_tabs li a').click(function(e){
                e.preventDefault();
                $('.categories_tabs li').each(function(){
                    $(this).removeClass('active');
                });
                if($(this).parent().hasClass('active') != true) {
                    
                    category_id = $(this).data('id');
                    getNews('tin-tuc?page=1');
                    $(this).parent().addClass('active');
                }
                
            });
            function getNews(url) {
                $.ajax({
                    url : url,
                    data: {
                        category_id: category_id,
                    }
                    
                }).done(function (data) {
                    $('.loader').hide();
                    $('.news').html(data);
                }).fail(function () {
                    alert('Có lỗi xảy ra, vui lòng thử lại sau.');
                });
            }
        });

    </script>
@endsection