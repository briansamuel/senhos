@extends('frontsite.index')
@section('title', __('seo.partner.title'))
@section('title_ab', __('seo.partner.title_ab'))
@section('keyword', __('seo.partner.keyword') )
@section('description', __('seo.partner.description'))
@section('style')

@endsection
@section('content')
<!-- START Block Banner  -->

@include('frontsite.elements.banner')
<!-- END Block Banner -->
<!-- START BREADCRUMB -->
@include('frontsite.elements.breadcrumb')
<!-- END BREADCRUMB -->
<div class="main-wrapper">


    <!-- START BLOCK partner -->
    <section id="list-partner">
        <div class="container">
            <div class="row mt-4 mb-3 justify-content-center">
                <div class="col-12">
                    <h3 class="text-left title-page">{{ __('frontsite.partners.lastest_partner') }}</h3>
                    <p>{{ __('frontsite.partners.description') }}</p>
                    <!-- <h3 class="text-left title-page">DỰ ÁN ĐÃ THỰC HIỆN</h3>
                    <p>SEN HOSPITALITY cung cấp dịch vụ tư vấn và quản lý khách sạn chuyên nghiệp, chất lượng cùng tinh thần
                        “cùng nhau phát triển - cùng nhau thành công” cho các chủ đầu tư trong nước, từ khi thành lập đến
                        nay, SEN Hospitality luôn được các đối tác tin tưởng, được giới chuyên môn đánh giá là một trong
                        những thương hiệu quản lý khách sạn hàng đầu.</p> -->
                </div>

            </div>
            <div class="row">
                @foreach($partners as $partner)
                <div class="col-12 col-lg-4 col-sm-6 col-xl-4 ">
                    <div class="project-item text-center">
                        <a href="doi-tac/{{ $partner->post_slug }}.html" class="effect-zoom-in">
                            <img class="img-fluid" src="{{ $partner->post_thumbnail }}" alt="{{ $partner->post_title }}">
                        </a>
                        <a href="doi-tac/{{ $partner->post_slug }}.html">
                            <h4 class="project-title mt-2 mb-1">{{ $partner->post_title }}</h4>
                        </a>


                        <div class="text-justify mt-4 partner-expert">{!! $partner->post_description !!}</div>
                    </div>
                </div>
                @endforeach

            </div>

        </div>
    </section>
    <!-- END BLOCK partner -->


    <!-- START BLOCK PARTNER -->
    @include('frontsite.elements.partner-brands')
    <!-- END BLOCK PARTNER -->
</div>
@endsection
@section('script')

@endsection