@extends('frontsite.index')
@section('title', $partner->post_title)
@section('title_ab', $partner->post_title)
@section('keyword', $partner->post_seo_keyword ? $partner->post_seo_keyword : $partner->post_title)
@section('description', $partner->post_seo_description ? $partner->post_seo_description : $partner->post_title)
@section('style')

@endsection
@section('content')
<!-- START Block Banner  -->

@include('frontsite.elements.banner')
<!-- END Block Banner -->
<!-- START BREADCRUMB -->
@include('frontsite.elements.breadcrumb')
<!-- END BREADCRUMB -->
<div class="main-wrapper mt-4 mb-4">
    <div class="container">
        <div class="row list-post-row">
            
            <!-- START SIDEBAR -->
                @include('frontsite.sidebar.sidebar_news')
            <!-- END SIDEBAR -->

            <div id="post-{{ $partner->id }}" class="col-12 col-xl-8 mt-0 pt-0 pt-sm-3 mt-sm-4 single-post">
                <div class="post-header">
                    <h2 class="post-title">{{ $partner->post_title }}</h2>
                </div>
                <div class="post-meta">
                    <span class="post-time"><i class="fas fa-clock"></i> {{ $partner->created_at }}</span>
                    <span class="ml-4 mr-4">|</span>
                    <span class="post-view">1 lượt xem</span>
                </div>
                <div class="post-content">

                    {!! $partner->post_content !!}
                   


                </div>
                
            </div>
        </div>
    </div>
</div>

<!-- START TOP FOOTER 1 -->
@include('frontsite.elements.top-footer-1')
<!-- END TOP FOOTER 1 -->
<!-- START TOP FOOTER 2  -->
@include('frontsite.elements.top-footer-2')
<!-- END TOP FOOTER 2 -->
@endsection
@section('script')

@endsection