@extends('frontsite.index')
@section('title', 'Liên hệ  ' )
@section('title_ab', '' )
@section('keyword', '' )
@section('description', '')
@section('style')

@endsection
@section('content')
<!-- START Block Banner  -->

@include('frontsite.elements.banner')
<!-- END Block Banner -->
<!-- START BREADCRUMB -->
@include('frontsite.elements.breadcrumb')
<!-- END BREADCRUMB -->
<div class="main-wrapper wrap-content mt-5 mb-5">
    <div class="container">
        <div class="row">
            <div class="col-12 map-contact">
                <div id="map-phu-quoc">
                    <div id="map1" style="height:500px; width: 600px"></div>
                    <div id="map2" style="height:500px; width: 600px; display: none"></div>
                </div>
            </div>
            <div class="col-12 address-contact">
                <div class="panel-address active" id="address_1">
                    <span class="panel-title">{{$setting->get('theme_option::contact::title_1', 'HỒ CHÍ MINH')}}</span>
                    <span><i class="fas fa-map-marker-alt"></i> {{$setting->get('theme_option::contact::address_1', 'Tòa nhà Deli - 70 Phạm Ngọc Thạch, Phường 6, Quận 3, TP. Hồ Chí Minh')}}</span>
                    <span><i class="fas fa-phone-alt"></i> {{$setting->get('theme_option::contact::phone_number_1', '(+84) 28 888 888')}}</span>
                    <span><i class="fas fa-envelope"></i> {{$setting->get('theme_option::contact::email_1', 'info@senhos.vn')}}</span>
                </div>
                <div class="panel-address" id="address_2">
                    <span class="panel-title">{{$setting->get('theme_option::contact::title_2', 'PHÚ QUỐC')}}</span>
                    <span><i class="fas fa-map-marker-alt"></i> {{$setting->get('theme_option::contact::address_2', 'Bãi Trường, Dương Tơ, Phú Quốc, Kiên Giang')}}</span>
                    <span><i class="fas fa-phone-alt"></i> {{$setting->get('theme_option::contact::phone_number_2', '(+84) 28 888 888')}}</span>
                    <span><i class="fas fa-envelope"></i> {{$setting->get('theme_option::contact::email_2', 'info@senhos.vn')}}</span>
                </div>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-12 form-contact  mb-3 mb-sm-0">
                <span class="form-contact-title">GỬI PHẢN HỒI CHO CHÚNG TÔI</span>
                <form class="mt-3">
                    <div class="form-group">

                        <input type="text" class="form-control" id="name" placeholder="Họ tên">

                    </div>
                    <div class="form-group">

                        <input type="email" class="form-control" id="email" placeholder="Email">

                    </div>
                    <div class="form-group">

                        <input type="text" class="form-control" id="phone_number" placeholder="Số điện thoại">

                    </div>
                    <div class="form-group">

                        <textarea type="text" class="form-control" id="content" rows="6" placeholder="Nội dung phản hồi"></textarea>
                    </div>
                    <div class="form-group" style="display:none" id="show_error">
                        <div>
                            <span class="text text-danger" id="content_error"></span>
                        </div>
                    </div>
                    <div class="form-group" style="display:none" id="show_success">
                        <div>
                            <span class="text text-success" id="content_success"></span>
                        </div>
                    </div>

                    <button type="button" class="btn btn-primary send-contact" onclick="sendContact()" id="btn_contact">GỬI NGAY</button>
                </form>
            </div>
            <div class="col-12 pic-contact effect-shine">
                <img src="{{$setting->get('theme_option::contact::image', 'images/lien-he/lien-he-1.jpg')}}" class="img-fluid">
                <div class="show-off"></div>
            </div>
        </div>
    </div>
</div>
<!-- START TOP FOOTER 1 -->
@include('frontsite.elements.top-footer-1')
<!-- END TOP FOOTER 1 -->
<!-- START TOP FOOTER 2  -->
@include('frontsite.elements.top-footer-2')
<!-- END TOP FOOTER 2 -->
@endsection
@section('script')
    <script type="text/javascript">
        function initMap2() {
            var latitude = parseFloat({{$setting->get('theme_option::contact::latitude_2', 106.6105368)}});
            var longitude = parseFloat({{$setting->get('theme_option::contact::longitude_2', 21.0260162)}});

            var myLatLng = {lat: latitude, lng: longitude};

            var map2 = new google.maps.Map(document.getElementById('map2'), {
                zoom: 14,
                center: myLatLng
            });

            var marker = new google.maps.Marker({
                position: myLatLng,
                map: map2,
            });
        }

        function initMap1() {
            var latitude = parseFloat({{$setting->get('theme_option::contact::latitude_1', 106.6105368)}});
            var longitude = parseFloat({{$setting->get('theme_option::contact::longitude_1', 21.0260162)}});

            var myLatLng = {lat: latitude, lng: longitude};

            map1 = new google.maps.Map(document.getElementById('map1'), {
                center: myLatLng,
                zoom: 14
            });

            var marker = new google.maps.Marker({
                position: myLatLng,
                map: map1,
            });

            initMap2();
        }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD5Bgn-aIQI5I3FM7NZ2vOjx6X-w9Mi-LE&callback=initMap1"
    async defer></script>
    <script type="text/javascript">

        $("#address_1").click(function() {
            $(this).addClass('active');
            $("#address_2").removeClass('active');
            $("#map1").show();
            $("#map2").hide();
        });

        $("#address_2").click(function() {
            $(this).addClass('active');
            $("#address_1").removeClass('active');
            $("#map1").hide();
            $("#map2").show();
        });

        function checkSendContact(){
            var email = $("#email").val();
            var name = $("#name").val();
            var phone_number = $("#phone_number").val();
            var content = $("#content").val();
            if(name === ""){
                $("#show_error").show();
                $("#content_error").text("Bạn chưa nhập họ tên !");
                $("#name").focus();
                return false;
            }
            if(email === ""){
                $("#show_error").show();
                $("#content_error").text("Bạn chưa nhập Email !");
                $("#email").focus();
                return false;
            }
            if(!validateEmail(email)){
                $("#show_error").show();
                $("#content_error").text("Email không đúng định dạng !");
                $("#email").focus();
                return false;
            }
            if(phone_number === ""){
                $("#show_error").show();
                $("#content_error").text("Bạn chưa nhập số điện thoại !");
                $("#phone_number").focus();
                return false;
            }
            if(content === ""){
                $("#show_error").show();
                $("#content_error").text("Bạn chưa nhập nội dung liên hệ !");
                $("#content").focus();
                return false;
            }

            return true;
        }

        function sendContact(){
            if(checkSendContact()){
                //display notice
                $("#btn_contact").text("Đang xử lý....");
                $("#btn_contact").removeAttr("onclick");

                $.post('/lien-he', {
                    email: $("#email").val(),
                    phone_number: $("#phone_number").val(),
                    content: $("#content").val(),
                    name: $("#name").val(),
                    _token: "{{csrf_token()}}"
                }, function(res) {
                    $("#btn_contact").text("GỬI NGAY");
                    $("#btn_contact").attr("onclick", "return sendContact();");
                    if (res.success) {
                        $("#show_error").hide();
                        $("#show_success").show();
                        $("#content_success").text(res.message);
                        $("#content").val('');
                        return false;
                    } else {
                        $("#show_success").hide();
                        $("#show_error").show();
                        $("#content_error").text(res.message);
                        return false;
                    }
                }).fail(function() {
                    $("#show_success").hide();
                    $("#btn_contact").text("GỬI NGAY");
                    $("#btn_contact").attr("onclick", "return sendContact();");
                    $("#show_error").hide();
                    $("#content_error").text("");
                    alert('Hệ thống gặp lỗi, vui lòng thử lại sau.');
                    return false;
                });
            }
        }

    </script>
@endsection