@extends('frontsite.index')
@section('title', $page->page_title )
@section('title_ab', $page->page_seo_title )
@section('keyword', $page->page_seo_keyword )
@section('description', $page->page_seo_description)
@section('style')

@endsection
@section('content')
<!-- START Block Banner  -->
@include('frontsite.elements.banner')
<!-- END Block Banner -->
<!-- START BREADCRUMB -->
@include('frontsite.elements.breadcrumb')
<!-- END BREADCRUMB -->
<div class="main-wrapper">

    <!-- START BLOCK ABOUT -->

    <section class="" id="block-about-senhos">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-12 mt-0 mt-sm-5 mb-1 mb-sm-5 " data-animation="slideInLeft">
                    <div class="about-content" data-scrollbar>
                        <h4 class="text-primary font-weight-bold mb-0">SEN HOSPITALITY</h4>

                        <span class="font-style-italic text-primary">Điểm đến của sự hoàn hảo</span>
                        <p class="font-weight-bold">
                            SEN Hospitality - khẳng định là một đơn vị Tư vấn, Setup, Quản lý & Vận hành chuyên nghiệp, mang
                            thương hiệu Việt Nam.
                            Đội ngũ chuyên gia trong nước và quốc tế giàu kinh nghiệm, chuyên môn cao, năng động và tâm
                            huyết.
                            SENHOS có sẵn nguồn lực mạnh về khách hàng thông qua quan hệ đối tác chiến lược và các nhà đầu
                            tư phát triển bất động sản trong và ngoài nước.
                        </p>
                        <p>SENHOS là đơn vị chuyên nghiệp trong lĩnh vực: <br>
                            - Quản lý vận hành khách sạn & resort, nhà hàng, lounge - bar, cafe, spa - massage,<br>
                            - Tư vấn, set up khách sạn & resort, nhà hàng, lounge - bar, cafe, spa - massage,<br>
                            - Tái cấu trúc doanh nghiệp khách sạn & resort, nhà hàng...<br>
                            - Cung ứng đào tạo nguồn nhân lực lĩnh vực quản lý & vận hành</p>
                        <p>Với sự thấu hiểu về văn hóa, du lịch và thị trường khách sạn tại Việt Nam, SENHOS
                            luôn tự tin về việc mang lại thành công và giá trị lâu dài cho dự án.
                            Đội ngũ lãnh đạo và các chuyên gia trong và ngoài nước của STS Hospitality có
                            kinh nghiệm sâu rộng trong lĩnh vực quản lý, tư vấn, và setup các khách sạn 3-5 sao cùng những dịch vụ đi kèm.
                            <br>
                            Không chỉ là đơn vị cung cấp dịch vụ, Sen Hospitality cam kết luôn là người góp phần vào thành công của dự án trên tinh thần hợp tác cùng phát triển.</p>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!-- END Block ABOUT -->

    <!-- START BLOCK TRADE MARK -->

    <section class="" id="block-trademark">
        <div class="container">
            <div class="row">
                <div class="col-12 mt-0 mt-sm-4" data-animation="slideInLeft">
                    <h4 class="title-about">HỆ GIÁ TRỊ CỐT LÕI THƯƠNG HIỆU</h4>
                </div>
                <div class="col-xl-4 col-12 p-2 mb-1">
                    <div class="item-trademark mb-1">
                        <img src="images/tnsm-1.jpg">
                        <span class="circle-trademark">TẦM NHÌN</span>
                        <p>SENHOS không chỉ là công ty Tư vấn - Setup - Quản lý & Vận hành Hotel & Resort hàng đầu Việt
                            Nam mà với mục tiêu vươn xa toàn cầu sánh ngang những tập đoàn quản lý hàng đầu thế giới.
                            Tạo ra những sản phẩm dịch vụ đa dạng, độc đáo riêng, đẳng cấp, vươn tầm cao mới cho ngành
                            công nghiệp khách sạn, du lịch Việt Nam trên trường quốc tế. “Ở đâu có dự án khách sạn, ở đó
                            có SENHOS”
                        </p>
                    </div>

                </div>
                <div class="col-xl-4 col-12 p-2 mb-1" data-animation="slideInDown">
                    <div class="item-trademark mb-1">
                        <img src="images/tnsm-2.jpg">
                        <span class="circle-trademark">SỨ MỆNH</span>
                        <p>Cung cấp chiến lược kinh doanh rõ ràng, giải pháp tối ưu, khai thác tối đa tiềm năng của dự
                            án, mang lại giá trị lợi ích thiết thực.
                            Luôn học hỏi, đổi mới, chất lượng, sáng tạo, hiệu quả, bền vững, tôn trọng khách hàng và sự
                            hợp tác lâu dài. Đưa những nét đẹp tinh túy, độc đáo khác biệt của văn hóa Việt đến du khách
                            quốc tế. Kiến tạo cuộc sống tốt đẹp hướng tới thiên nhiện và bảo vệ môi trường.
                        </p>
                    </div>

                </div>
                <div class="col-xl-4 col-12 p-2 mb-1" data-animation="slideInRight">
                    <div class="item-trademark mb-1">
                        <img src="images/tnsm-3.jpg">
                        <span class="circle-trademark">GIÁ TRỊ<br>CỐT LÕI</span>
                        <p>Khẳng định thương hiệu Việt thực sự uy tín, đẳng cấp, chuyên nghiệp, phát triển bền vững, gia
                            tăng giá trị dài hạn. Khách hàng quyết định sự tồn tại của doanh nghiệp, đặt lợi ích của nhà
                            đầu tư làm mục tiêu dài hạn.
                            Ý thức cao bảo vệ môi trường và những giá trị văn hóa cao đẹp, có trách nhiệm với cộng đồng,
                            chấp hành pháp luật.

                        </p>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <!-- END Block TRADE MARK -->

    <!-- START BLOCK STRONG POINT -->
    <section id="block-strong-point ">
        <div class="container">
            <div class="row mb-1">
                <div class="col-12 ">
                    <h4 class="title-about mb-4">THẾ MẠNH CỦA SEN HOSPITALITY</h4>
                    <p>Với đội ngũ chuyên gia tư vấn trong nước và quốc tế dày dạn kinh nghiệm trong xây dựng chiến lược
                        và chủ động giải quyết vấn đề, phân tích thành thạo, ra quyết định chắc chắn, quản lý tự chủ,
                        kịp thời, nắm bắt đúng xu hướng quốc tế. Đồng thời với trình độ chuyên môn, khả năng đào tạo,
                        kiến thức rộng đã tạo cho SenHos sự khác biệt.
                        Với phương châm không ngừng nỗ lực, sự nhiệt huyết, trung thực, am hiểu thị trường, hết lòng vì
                        thành công bền vững của khách hàng. Cam kết mang lại dịch vụ hoàn hảo nhất cho khách hàng, thành
                        công cho đối tác, là minh chứng cho thành công của SenHos.
                        Chúng tôi đã đạt được nhiều thành tựu to lớn và liên tục phát triển lớn mạnh, hoàn thành sứ mệnh
                        đối với khách hàng, đóng góp đáng kể cho ngành quản lý và vận hành – khách sạn – du lịch.</p>
                </div>
            </div>
            <div class="row mb-4">
                <div id="slideStrongPoint" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img class="d-block w-100" src="images/the manh-11.jpg" alt="First slide">
                            <p>Sen Hospitality có sự hiểu biết sâu sắc về lịch sử văn hóa bản địa đặc biệt là văn hóa
                                kinh doanh và đặc tính thị trường trong lĩnh vực kinh doanh nhà hàng khách sạn tại Việt
                                Nam và đặc biệt là Phú Quốc.</p>
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="images/the manh-2-11.jpg" alt="Second slide">
                            <p>Sen Hospitality hiểu rõ thị trường lao động, khả năng ứng biến trước những biến động phức tạp trong thị trường nguồn nhân lực cũng như khả năng triển khai đào tạo, quản lý, giám sát hoạt động của nhân viên một cách chuyên nghiệp, hiệu quả.
                            </p>
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="images/the manh-3-11.jpg" alt="Third slide">
                            <p>
                                - Sen Hospitality Chịu trách nhiệm trước chủ đầu tư trong việc kiểm soát chi phí và giảm thiểu rủi ro một cách hiệu quả nhất.

                                <br>- Sen Hospitality luôn nỗ lực hết mình trong việc mang lại nguồn doanh thu và lợi nhuận tối đa cho Chủ đầu tư.
                            </p>
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#slideStrongPoint" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#slideStrongPoint" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>

    </section>
    <!-- END BLOCK STRONG POINT -->

    
    <!-- START BLOCK PARTNER -->
    @include('frontsite.elements.partner')
    <!-- END BLOCK PARTNER -->
</div>
@endsection
@section('script')

@endsection