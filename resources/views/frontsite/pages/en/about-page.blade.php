@extends('frontsite.index')
@section('title', $page->page_title )
@section('title_ab', $page->page_seo_title )
@section('keyword', $page->page_seo_keyword )
@section('description', $page->page_seo_description)
@section('style')

@endsection
@section('content')
<!-- START Block Banner  -->
@include('frontsite.elements.banner')
<!-- END Block Banner -->
<!-- START BREADCRUMB -->
@include('frontsite.elements.breadcrumb')
<!-- END BREADCRUMB -->

<div class="main-wrapper">

    <!-- START BLOCK ABOUT -->

    <section class="" id="block-about-senhos">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-12 mt-0 mt-sm-5 mb-1 mb-sm-5 slideInLeft animated" data-animation="slideInLeft">
                    <div class="about-content" data-scrollbar="">
                        <h4 class="text-primary font-weight-bold mb-0">SEN HOSPITALITY</h4>

                        <span class="font-style-italic text-primary">The destination of perfection</span>
                        <p class="font-weight-bold">
                            SEN Hospitality - affirms to be a professional consulting, setup, management  operation unit with Vietnamese brand. A team of domestic and international experts who are experienced, highly specialized, motivated and dedicated. SENHOS has strong customer resources available through strategic partnerships and domestic and foreign real estate developers.
                        </p>
                        <p>SENHOS is a professional unit in the field of: 
                            - Management of hotel and resort operation, restaurant, lounge - bar, cafe, spa - massage,
                            - Consulting, setting up hotel resort, restaurant, lounge - bar , cafe, spa - massage,
                            - Restructuring hotel resort businesses, restaurants ...
                            - Providing human resource training in management </p>
                        <p>With the understanding of culture, tourism and hotel market in Vietnam, SENHOS is always confident about bringing success and long-term value to the project. STS Hospitality's domestic and foreign management team has extensive experience in the field of management, consulting, and setup of 3-5 star hotels and associated services.
                        Not only is the service provider, Sen Hospitality is committed to always contributing to the success of the project in the spirit of cooperation and development. </p>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!-- END Block ABOUT -->

    <!-- START BLOCK TRADE MARK -->

    <section class="" id="block-trademark">
        <div class="container">
            <div class="row">
                <div class="col-12 mt-0 mt-sm-4 slideInLeft animated" data-animation="slideInLeft">
                    <h4 class="title-about">CORE VALUES BRAND NAME </h4>
                </div>
                <div class="col-xl-4 col-12 p-2 mb-1">
                    <div class="item-trademark mb-1">
                        <img src="images/tnsm-1.jpg">
                        <span class="circle-trademark">VISION </span>
                        <p>SENHOS is not only the leading Consulting - Setup - Management  Operating Hotel  Resort in Vietnam but with the goal of reaching out to the world as the leading management corporations in the world. Create a variety of unique and unique products and services, reaching new heights for Vietnam's hospitality industry in the international arena. "Where there is a hotel project, there is SENHOS"
                         </p>
                    </div>

                </div>
                <div class="col-xl-4 col-12 p-2 mb-1 slideInDown animated" data-animation="slideInDown">
                    <div class="item-trademark mb-1">
                        <img src="images/tnsm-2.jpg">
                        <span class="circle-trademark">MISSION </span>
                        <p>Provide clear business strategies, optimal solutions, maximize the potential of the project, bring practical benefits. Always learning, innovation, quality, creativity, efficiency, sustainability, respect for customers and long-term cooperation. Bringing the quintessential and unique beauty of Vietnamese culture to international visitors. Creating good life towards nature and environmental protection.
                         </p>
                    </div>

                </div>
                <div class="col-xl-4 col-12 p-2 mb-1 slideInRight animated" data-animation="slideInRight">
                    <div class="item-trademark mb-1">
                        <img src="images/tnsm-3.jpg">
                        <span class="circle-trademark">VALUES  <br>CORE </span>
                        <p>Affirming true Vietnamese brand reputation, class, professionalism, sustainable development, long-term added value. Customers determine the existence of the business, setting the interests of investors as a long-term goal. High sense of environmental protection and beautiful cultural values, responsible to the community, obey the law.

                         </p>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <!-- END Block TRADE MARK -->

    <!-- START BLOCK STRONG POINT -->
    <section id="block-strong-point ">
        <div class="container">
            <div class="row mb-1">
                <div class="col-12 ">
                    <h4 class="title-about mb-4">THE POWER OF SEN HOSPITALITY </h4>
                    <p>With a team of experienced international and domestic consultants in strategy formulation and proactive problem solving, expert analysis, solid decision making, autonomy management, timely, capture right international trend. Simultaneously with the professional qualifications, training ability, broad knowledge has made SenHos difference. With the motto of continuous effort, enthusiasm, honesty, market knowledge, wholeheartedly for the sustainable success of customers. Commitment to bring the most perfect service to customers, success for partners, is a testament to the success of SenHos. We have achieved great accomplishments and continuously grown, </p>
                </div>
            </div>
            <div class="row mb-4">
                <div id="slideStrongPoint" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        <div class="carousel-item">
                            <img class="d-block w-100" src="images/the manh-11.jpg" alt="First slide">
                            <p>Sen Hospitality has a deep understanding of indigenous cultural history, especially business culture and market characteristics in the field of hotel and restaurant business in Vietnam and especially Phu Quoc. </p>
                        </div>
                        <div class="carousel-item active">
                            <img class="d-block w-100" src="images/the manh-2-11.jpg" alt="Second slide">
                            <p>Sen Hospitality understands the labor market, the ability to respond to complex fluctuations in the human resource market as well as the ability to deploy training, management and supervision of employees' operations in a professional and effective manner. fruit.
                             </p>
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="images/the manh-3-11.jpg" alt="Third slide">
                            <p>
                                - Sen Hospitality Chịu trách nhiệm trước chủ đầu tư trong việc kiểm soát chi phí và giảm thiểu rủi ro một cách hiệu quả nhất.

                                <br>- Sen Hospitality luôn nỗ lực hết mình trong việc mang lại nguồn doanh thu và lợi nhuận tối đa cho Chủ đầu tư.
                            </p>
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#slideStrongPoint" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous </span>
                    </a>
                    <a class="carousel-control-next" href="#slideStrongPoint" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">next </span>
                    </a>
                </div>
            </div>
        </div>

    </section>
    <!-- END BLOCK STRONG POINT -->

    
    <!-- START BLOCK PARTNER -->
    @include('frontsite.elements.partner')
    <!-- END BLOCK PARTNER -->
</div>
@endsection
@section('script')

@endsection