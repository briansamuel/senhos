@extends('frontsite.index')
@section('title', __('seo.booking.comfirm_page'))
@section('title_ab', __('seo.booking.comfirm_page'))
@section('body_class', 'page-comfirm-booking')
@section('style')

@endsection
@section('content')
@php
$checkin = !empty(Request::get('checkin'))? Request::get('checkin') : date('d/m/Y');
$checkout = !empty(Request::get('checkout'))? Request::get('checkout') : date('d/m/Y');
$date1 = new DateTime(date("Y-m-d", strtotime($checkin)));
$date2 = new DateTime(date("Y-m-d", strtotime($checkout)));
$diff = $date1->diff($date2);
$diff->days
@endphp
<div class="wrap-content mt-5">
    <div class="container">
        <div class="row">
            <div class="jumbotron text-center">
                <h1 class="display-3">Cám ơn bạn đã đặt phòng!</h1>
                <p class="lead"><strong>Thông tin đặt phòng đã gửi đến email của bạn</strong> vui lòng kiểm tra mail nhận thông tin từ chúng tôi.</p>
                <hr>
                <p>
                    Bạn có vấn đề ? <a href="{{ route('frontsite.contact') }}">Liên hệ chúng tôi</a>
                </p>
                <p class="lead">
                    <a class="btn btn-primary btn-sm" href="{{ route('booking.index') }}" role="button">Tìm các phòng khác</a>
                </p>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')

@endsection