@extends('frontsite.index')
@section('title', $video->post_title)
@section('title_ab', $video->post_title)
@section('keyword', $video->post_seo_keyword ? $video->post_seo_keyword : $video->post_title)
@section('description', $video->post_seo_description ? $video->post_seo_description : $video->post_title)
@section('style')

@endsection
@section('content')
<div class="main-wrapper">
    <!-- START Block Banner  -->

    @include('frontsite.elements.banner')
    <!-- END Block Banner -->
   
    <div class="main-wrapper">

        <!-- START BREADCRUMB -->
        @include('frontsite.elements.breadcrumb')
        <!-- END BREADCRUMB -->
        <!-- WRAP CONTENT -->

        <div class="wrap-content mt-2 mb-2">
            <div class="container">
                <div class="row">

                    <div id="post-" class="col-12 col-xl-12 pt-2 single-post">
                        <div class="post-header">
                            <h2 class="post-title">{{ $video->post_title }}</h2>
                        </div>
                        <div class="post-meta">
                            <span class="post-time"><i class="fas fa-clock"></i> {{ $video->created_at }}</span>
                            <span class="ml-4 mr-4">|</span>
                            <span class="post-view">302 lượt xem</span>
                        </div>
                        <div class="post-content">

                            {!! $video->post_content !!}



                        </div>
                        <div class="related-posts mt-4">
                            <div class="widget lastest-videos mt-4">
                                <h6 class="widget-title">CÁC VIDEO KHÁC</h6>
                                <div class="widger-content">
                                    <div class="row">
                                        @foreach($lasted_video as $index => $video)
                                        <div class="col-12 col-xl-3 item-video ">
                                            <a href="thu-vien/{{ $video->post_slug}}.html" title="{{ $video->post_title }}" class="thumbnail-video ">
                                                <img src="{{ $video->post_thumbnail }}" alt="{{ $video->post_title }}" class="img-fluid">
                                                <span>{{ $video->post_title }}</span>
                                            </a>
                                        </div>
                                        @endforeach
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END BLOCK TYPE PLACE -->

    </div>
</div>
@endsection
@section('script')

@endsection