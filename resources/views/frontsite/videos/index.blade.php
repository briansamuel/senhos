@extends('frontsite.index')
@section('title', __('seo.videos.title'))
@section('title_ab', __('seo.videos.title_ab'))
@section('keyword', __('seo.videos.keyword') )
@section('description', __('seo.videos.description'))
@section('style')

@endsection
@section('content')
<div class="main-wrapper">
    <!-- START Block Banner  -->

    @include('frontsite.elements.banner')
    <!-- END Block Banner -->

    <!-- START BREADCRUMB -->
    @include('frontsite.elements.breadcrumb')
    <!-- END BREADCRUMB -->

    <!-- START BLOCK PROJECT -->
    <section class="" id="block-project">
        <div class="container">
            
            <div class="row mb-4">
                <div class="col-12">
                    <ul id="categories_tabs" class="nav d-flex justify-content-center categories_tabs">
                        <li class="active"><a href="#" data-type="video">{{ __('frontsite.videos.gallery-video') }}</a></il>
                        <li><a href="#" data-type="image">{{ __('frontsite.videos.gallery-album') }}</a></il>

                            <ul>
                </div>
            </div>
            <div class="list-video mb-5">
                @include('frontsite.videos.elements.load')
            </div>


        </div>
    </section>
    <!-- END BLOCK PROJECT -->
    <!-- START GALLERY PROJECT -->

    <!-- END GALLERY PROJECT -->

    <!-- START BLOCK PARTNER -->
    @include('frontsite.elements.partner')
    <!-- END BLOCK PARTNER -->
</div>
@endsection
@section('script')
<script type="text/javascript">
    $(function() {

        var type_gallery = 'video';
        $('body').on('click', '.pagination a:not(.is-active)', function(e) {
            e.preventDefault();

            $('#load a').css('color', '#dfecf6');

            var url = $(this).attr('href');
            getNews(url);
            window.history.pushState("", "", url);
        });

        $('.categories_tabs li a').click(function(e){
                e.preventDefault();
                $('.categories_tabs li').each(function(){
                    $(this).removeClass('active');
                });
                if($(this).parent().hasClass('active') != true) {
                    
                    type_gallery = $(this).data('type');
                    getNews('thu-vien?page=1');
                    $(this).parent().addClass('active');
                }
                
            });

        function getNews(url) {
            $.ajax({
                url: url,
                data: {
                    type_gallery: type_gallery
                }
            }).done(function(data) {
                $('.list-video').html(data);
            }).fail(function() {
                alert('Có lỗi xảy ra, vui lòng thử lại sau.');
            });
        }
    });
</script>
@endsection