<div class="lastest-videos row">
    @foreach($videos as $video)

    <div class="col-12 col-sm-4 p-2 slideInLeft animated" data-animation="slideInLeft" data-delay="0">
        <div class="item-video text-center ">
            <a href="thu-vien/{{ $video->post_slug }}.html">
                <img class="img-fluid" src="{{ $video->post_thumbnail }}" alt="{{ $video->post_title }}">

            </a>

            <h4 class="project-title mt-1 mb-0 mt-sm-4 mb-sm-1">{{ $video->post_title }}</h4>

        </div>
    </div>
    @endforeach

</div>
{{$videos->links()}}