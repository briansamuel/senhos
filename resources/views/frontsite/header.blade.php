<header id="header">
    <nav class="top-bar navbar navbar-expand-md py-0" id="top-bar">
        <div class="container">
            <ul class="navbar-nav mr-auto" id="top-bar-left">
                <li class="nav-item mr-4">
                    <a class="nav-link" href="javascript:;"><i class="fas fa-phone text-primary mr-1"></i> Hotline : <span class="text-primary">{{$setting->get('theme_option::header::hotline', __('frontsite.header.holline'))}}</span></a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="javascript:;"><i class="far fa-envelope text-primary mr-1"></i> Email : <span class="text-primary">{{$setting->get('theme_option::header::email', __('frontsite.header.email'))}}</span> </a>
                </li>

            </ul>
            <ul class="navbar-nav ml-auto" id="top-bar-right">
                <li class="nav-item dropdown nav-account">
                    @if(Auth::guard('user')->check())
                    <a class="dropdown-item nav-link text-primary nav-link dropdown-toggle" href="javascript:;" id="userInfo" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="far fa-user-circle text-primary mr-1"></i>{{Auth::guard('user')->user()->full_name}}</a>
                    <div class="dropdown-menu" aria-labelledby="userInfo">
                        <a class="dropdown-item" href="{{ route('account.info') }}"> {{ __('frontsite.header.account.info') }}</a>
                        <a class="dropdown-item" href="/dang-xuat"> {{ __('frontsite.header.logout') }}</a>
                    </div>
                    @else
                    <a class="nav-link text-priary" href="//booking.{{ Config::get('domain.web.domain') }}/dang-nhap"><i class="far fa-user-circle text-primary mr-1"></i>{{ __('frontsite.header.login') }}</a>
                    @endif
                </li>
                <li class="nav-item dropdown nav-flag">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        @if(App::getLocale() == 'vi')
                        <img src="images/icons/vn-flag.jpg">
                        @elseif(App::getLocale() == 'en')
                        <img src="images/icons/en-flag.jpg">
                        @endif
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{ route('lang',['lang' => 'vi']) }}"><img src="images/icons/vn-flag.jpg"> {{ __('frontsite.header.language_vi') }}</a>
                        <a class="dropdown-item" href="{{ route('lang',['lang' => 'en']) }}"><img src="images/icons/en-flag.jpg"> {{ __('frontsite.header.language_en') }}</a>

                    </div>
                </li>
            </ul>
        </div>
    </nav>

    <nav class="navbar navbar-expand-md navbar-dark " id="header-main">
        <div class="container">
            <!-- Brand -->
            <a class="navbar-brand" href="."><img src="{{$setting->get('theme_option::header::logo', 'images/logo-header.png')}}"></a>

            <!-- Toggler/collapsibe Button -->
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapseMenuMain">
                <span class="navbar-toggler-icon"></span>
            </button>
            @include('frontsite.elements.menu')
            <!-- Navbar links -->
            <div id="search-form" class=" pull-right">
                <form class="navbar-form" role="search" action="/tin-tuc">
                    <div class="input-group ui-widget">
                        <input type="text" class="form-control" placeholder="{{ __('frontsite.header.search') }}" name="search" id="search" value="{{ Request::get('search') }}">

                        <div class="input-group-btn">
                            <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                        </div>
                        <div id="autocomple-posts" class="autocomple-posts" style="display: none">
                            <ul>

                            </ul>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </nav>

</header>
<script type="text/javascript">
    $(function() {
        $("#search").autocomplete({
            delay: 1000,
            source: function(request, response) {
                $.ajax({
                    url: "{{ route('frontsite.ajax.search') }}",
                    data: {
                        search: request.term
                    },
                    success: function(data) {
                        $('#autocomple-posts ul').html('');
                        console.log(data);
                        data.forEach(function(item) {
                            console.log(item);
                            $('#autocomple-posts ul').append('<li><a target="_blank" href="' + item.url + '">' + item.title + '</a></li>')
                        });
                        $('#autocomple-posts').show();
                    }
                });
            },
            autoFocus: true
        });
        $("#search").blur(function() {
            $('#autocomple-posts').hide();
        });
    });

    function subcribeEmail() {
        var subcribe_email = $("#subcribe_email").val();
        if (subcribe_email === '') {
            alert('Vui lòng nhập email để nhận ưu đãi');
            return false;
        }

        if (!validateEmail(subcribe_email)) {
            alert('Vui lòng nhập đúng định dạng email để nhận ưu đãi');
            return false;
        }

        //display notice
        $("#btn_subcribe").text("Đang xử lý....");
        $("#btn_subcribe").removeAttr("onclick");

        $.post('/subcribe-email', {
            email: subcribe_email,
            _token: '{{csrf_token()}}'
        }, function(res) {
            $("#btn_subcribe").text("ĐĂNG KÝ");
            $("#btn_subcribe").attr("onclick", "return subcribeEmail();");
            if (res.success) {
                $("#subcribe_email").val('');
            }
            alert(res.message);
            return false;
        }).fail(function() {
            $("#btn_subcribe").text("ĐĂNG KÝ");
            $("#btn_subcribe").attr("onclick", "return subcribeEmail();");
            alert('Hệ thống gặp lỗi, vui lòng thử lại sau.');
            return false;
        });
    }

    function validateEmail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }
</script>