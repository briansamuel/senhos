@inject('setting', 'App\Services\SettingService')
@php

$intro_active = $setting->get('theme_option::general::intro_video', false);
$intro_video = $setting->get('theme_option::general::intro_video_upload', 'videos/intro-video.mp4');
@endphp
@if($intro_active && $intro_active == 'on')
<div id="opVid" class="video-intro" style="display: none;">
    <div class="video">
        <div class="vsc-controller" data-vscid="4tl51qmzo"></div> <video id="video_background" preload="auto" autoplay="" muted="" volume="1" playsinline="" data-vscid="4tl51qmzo">
            <source src="{{ $intro_video }}" type="video/mp4"></video>

    </div>
    <div class="buttons skip"> <a style="color: {{$setting->get('theme_option::general::intro_video_color_button')}}; border-color: {{$setting->get('theme_option::general::intro_video_color_button')}}" href="javascript:;">{{$setting->get('theme_option::general::intro_video_text_button', 'Skip')}}</a> </div>
</div>
@endif

