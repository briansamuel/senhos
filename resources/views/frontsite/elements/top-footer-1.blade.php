<section id="top-footer" class="">
    <div class="container">
        <div class="row justify-content-between pt-4 pb-4">
            <div class="col-12 col-xl-3 align-content-center icon-box">
                <img src="images/booking/icon-phone.png" />
                <span>HỖ TRỢ TỐI ĐA 24/7</span>
                <p class="text-primary">Liên hệ với chúng tôi ngay qua số hotline 0932 92 94 96
                    nếu bạn cần sự hỗ trợ trong đặt phòng hoặc thắc mắc.</p>
            </div>
            <div class="col-12 col-xl-3 align-content-center icon-box">
                <img src="images/booking/icon-money.png" />
                <span>THANH TOÁN NHANH - DỄ DÀNG</span>
                <p class="text-primary">Liên hệ với chúng tôi ngay qua số hotline 0932 92 94 96
                    nếu bạn cần sự hỗ trợ trong đặt phòng hoặc thắc mắc.</p>
            </div>
            <div class="col-12 col-xl-3 align-content-center icon-box">
                <img src="images/booking/icon-phone.png" />
                <span>KHÔNG PHỤ PHÍ - PHÍ ẨN</span>
                <p class="text-primary">Liên hệ với chúng tôi ngay qua số hotline 0932 92 94 96
                    nếu bạn cần sự hỗ trợ trong đặt phòng hoặc thắc mắc.</p>
            </div>
        </div>
    </div>
</section>