@if($setting->get('theme_option::general::popup_banner') !== '')
@if (!Cookie::get('show_popup'))
<div id="popup-banner" class="jPopup" style="display: none;">
    <div class="popup-relative">
        <a href="#">
            <img src="{{$setting->get('theme_option::general::popup_banner', __('frontsite.general.popup_banner'))}}">
        </a>
        <span class="close"><i class="fal fa-times-circle"></i></span>
    </div>
</div>

<script>
    $(document).ready(function() {
        setTimeout(function() {
            $('.jPopup').fadeIn("slow");
        }, 15000);
        $('.jPopup .close').click(function() {
            $('.jPopup').fadeOut("slow");
        });
    });
</script>
@php Cookie::queue('show_popup', 'popup', 60) @endphp
@endif
@endif