<section class="" id="block-partner">
    <div class="container">
        <div class="row justify-content-center">
            <div id="carousel-partner" class="mt-3 mt-sm-5">
                @foreach($brands_partner as $brand)
                <a href="{{$brand->link}}" class="item" title="{{$brand->title}}">
                    <img src="{{$brand->image}}">
                </a>
                @endforeach
            </div>
        </div>
        <div class="row mt-3 mt-sm-5">
            <div class="col-12">
                <div id="subcription" class="newsletter">
                    <div class="row">
                        <div class="col-12 col-xl-4">
                            <h4>{{ __('frontsite.partner.social_header') }}</h4>
                            <!-- <h4>MẠNG LIÊN KẾT</h4> -->
                            <ul class="list-unstyled list-inline social" id="list-social">
                            <li class="list-inline-item"><a target="_blank" href="{{$setting->get('theme_option::footer::youtube', __('frontsite.footer.youtube'))}}"><i class="fab fa-youtube"></i></a></li>
                <li class="list-inline-item"><a target="_blank" href="{{$setting->get('theme_option::footer::facebook', __('frontsite.footer.facebook'))}}"><i class="fab fa-facebook-f"></i></a></li>
                <li class="list-inline-item"><a target="_blank" href="{{$setting->get('theme_option::footer::twitter', __('frontsite.footer.twitter'))}}"><i class="fab fa-twitter"></i></a>
                </li>
                <li class="list-inline-item"><a target="_blank" href="{{$setting->get('theme_option::footer::google', __('frontsite.footer.google'))}}"><i class="fab fa-google-plus-g"></i></a>
                </li>
                <li class="list-inline-item"><a target="_blank" href="{{$setting->get('theme_option::footer::instagram', __('frontsite.footer.instagram'))}}"><i class="fab fa-instagram"></i></a></li>
                            </ul>
                        </div>
                        <div class="col-12 col-xl-8">
                            <!-- <h5>ĐĂNG KÝ</h5> -->
                            <h5>{{ __('frontsite.subcription.header') }}</h5>
                            <p>{{ __('frontsite.subcription.description') }}</p>
                            <!-- <p>Nhận thông tin về email</p> -->
                            <div class="input-group">
                                <input type="email" class="form-control" placeholder="{{ __('frontsite.subcription.placeholder') }}" id="subcribe_email" name="subcribe_email">
                                <span class="input-group-btn">
                                    <button class="btn" type="submit" id="btn_subcribe" onclick="subcribeEmail()">{{ __('frontsite.subcription.button_submit') }}</button>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>