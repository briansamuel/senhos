<section class="" id="block-banner">

    @if(isset($banners))
    <div id="serviceSlide" class="carousel slide" data-ride="carousel">
        @if(count($banners) > 1)
        <ol class="carousel-indicators">
            @foreach($banners as $k=>$banner)
                @if($k === 0)
                <li data-target="#serviceSlide" data-slide-to="{{$k}}" class="active"></li>
                @else
                <li data-target="#serviceSlide" data-slide-to="{{$k}}"></li>
                @endif
            @endforeach
        </ol>
        @endif
        <div class="carousel-inner">
            @foreach($banners as $k=>$banner)
                @if($k === 0)
                <div class="block-banner-slide carousel-item active">
                @else
                <div class="block-banner-slide carousel-item">
                @endif
                    @if($banner->banner_type === 'image')
                        <img src="{{$banner->image}}">
                    @else
                        <video preload="auto" autoplay="" muted="" width="100%" height="auto">
                            <source src="{{ $banner->image }}" type="video/mp4">
                        </video>
                    @endif
                    @if($banner->caption === 1)
                    <div class="text-in-banner {{$banner->caption_position}}" data-animation="fadeInLeft">
                        <h3 class="text-sm-left text-center text-primary "><span class="text-white">{{$banner->title}}</span></h3>
                        <p class="">{{$banner->description}}</p>
                        <a href="{{$banner->link}}" class="btn bg-primary btn-banner-link">Xem thêm</a>
                    </div>
                    @endif
                </div>
            @endforeach
        </div>
        @if(count($banners) > 1)
        <a class="carousel-control-prev" href="#serviceSlide" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#serviceSlide" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
        @endif
    </div>
    @endif
</section>
<script>
    $('#serviceSlide').carousel({
        interval: 5000
    })

</script>