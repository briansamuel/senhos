<div class="collapse navbar-collapse" id="collapseMenuMain">
    <ul class="navbar-nav m-auto">
        @if($menus)
            @foreach($menus as $menu)
                  <li class="nav-item {{ Request::url().'/' == $menu['link'] ? 'active' : '' }}" >
                
                    @if( $menu['child'] )
                        @php
                        $parrent_active = '';
                        foreach( $menu['child'] as $child ) {
                            if(Request::url().'/' == $child['link']) {
                                $parrent_active = "active";
                            }
                        }
                        
                        @endphp
                        <li class="nav-item dropdown {{ $parrent_active }}">
                            <a class="nav-link dropdown-toggle" href="{{ $menu['link'] != '#' ? $menu['link'] :  'javascript:0;'}}" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ $menu['label'] }}</a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                @foreach( $menu['child'] as $child )
                                <a class="dropdown-item" href="{{ $child['link'] }}">{{ $child['label'] }}</a>
                                @endforeach
                            </div>
                        </li>
                    @else
                        <a class="nav-link" href="{{ $menu['link'] }}" title="{{ $menu['label'] }}">{{ $menu['label'] }}</a>
                    @endif
                </li>
            @endforeach
        @endif
    </ul>
</div>