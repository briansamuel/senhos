<section id="block-banner" class="banner-booking">
    <img src="images/booking/banner.jpg">
    <div class="container">
        <form class="m-auto collapse-booking hide" id="booking-form" method="GET" action="{{ route('hosts.search') }}">
            <div class="row booking-form ">
                <div class="col-12 col-xl-4 border-left-1">
                    <div class="input-group">
                        <i class="fal fa-2x fa-map-marker-alt"></i>
                        <input id="type-query" class="form-control" type="hidden" name="">
                        <!-- Thành phố, Khách sạn, điểm đến -->
                        <select id="keyword" class="js-example-basic-multiple form-control border-0" name="q" placeholder="{{ __('frontsite.booking.search_keyword') }}" >
                            
                        </select>
                    </div>
                </div>
                <div class="col-12 col-xl-2">
                    <div class="input-group">
                        <i class="fal fa-2x fa-calendar-alt"></i>
                        <input id="checkin_date" class="form-control" required name="checkin" type="text" value="{{ !empty(Request::get('checkin'))? Request::get('checkin') : date('d/m/Y') }}">

                    </div>
                </div>
                <div class="col-12 col-xl-2">
                    <div class="input-group">
                        <i class="fal fa-2x fa-calendar-alt"></i>
                        @php 
                        $datetime = new DateTime('tomorrow');
                        @endphp
                        <input id="checkout_date" class="form-control" required name="checkout" type="text" value="{{ !empty(Request::get('checkout'))? Request::get('checkout') : $datetime->format('d/m/Y') }}">

                    </div>
                </div>
                <div class="col-12 col-xl-2">
                    <div class="input-group">
                        <i class="far fa-2x fa-bed"></i>
                        <input id="amount_room" class="form-control" name="amount" required type="number" min="1" max="20" placeholder="{{ __('frontsite.booking.amount_guest') }}" value="{{ !empty(Request::get('amount'))? Request::get('amount') : '' }}">

                    </div>
                </div>
                <div class="col-12 col-xl-2 pl-sm-0">
                    <button id="btn-search-hotel" type="submit" class="btn search-hotel">
                        <i class="fas fa-search"></i> {{ __('frontsite.booking.find_hotel') }}
                    </button>
                </div>
            </div>
        </form>
    </div>

</section>