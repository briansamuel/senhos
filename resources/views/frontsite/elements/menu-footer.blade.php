<div class="menu-footer" id="menuFooter">
    <ul class="list-unstyled list-inline social">

        @if($menus)
            @foreach($menus as $menu)
                <li class="list-inline-item"> <a class="nav-link" href="{{ $menu['link'] }}" title="{{ $menu['label'] }}">{{ $menu['label'] }}</a></li>
            @endforeach
        @endif

</div>