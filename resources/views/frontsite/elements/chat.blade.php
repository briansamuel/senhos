@if($setting->get('theme_option::chat::is_chat') === 'on')
    @if($setting->get('theme_option::chat::option_chat') === 'tawk')
        <script>
            var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
            (function(){
                var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
                s1.async=true;
                s1.src='https://embed.tawk.to/{{$setting->get('theme_option::chat::tawk_page_id')}}/default';
                s1.charset='UTF-8';
                s1.setAttribute('crossorigin','*');
                s0.parentNode.insertBefore(s1,s0);
            })();
        </script>
    @else
        <!-- Load Facebook SDK for JavaScript -->
        <div id="fb-root"></div>
        <script>
            window.fbAsyncInit = function() {
                FB.init({
                    xfbml            : true,
                    version          : 'v6.0'
                });
            };

            (function(d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s); js.id = id;
                js.src = 'https://connect.facebook.net/vi_VN/sdk/xfbml.customerchat.js#xfbml=1&version=v2.12&autoLogAppEvents=1';
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));</script>

        <!-- Your customer chat code -->
        <div style="margin-bottom: 50px" class="fb-customerchat"
             attribution=setup_tool
             page_id="{{$setting->get('theme_option::chat::facebook_page_id')}}"
             logged_in_greeting="Chào bạn, đã đến với Senhos">
        </div>

        <!-- Check Update CI/CD -->
    @endif
@endif