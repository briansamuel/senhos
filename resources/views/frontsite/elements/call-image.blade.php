@if($setting->get('theme_option::general::call_image_float') == 'on')
<div class="call-image-float">
    <a href="tel:{{$setting->get('theme_option::general::call_image_number', __('frontsite.header.holline'))}}">
        <img src="{{$setting->get('theme_option::general::call_image_url', '') }}"></a>
    </a>
</div>
<style>
    .call-image-float {
        max-width: 200px;
        position: fixed;
        left: 0px;
        bottom: 0px;
    }
</style>
@endif