<section id="breadcrumb">
    <div class="container">
        <nav aria-label="breadcrumb ">

            <ol class="breadcrumb arr-right ">

                <li class="breadcrumb-item "><a href="{{route('home')}}" class="">{{ __('frontsite.breadcrumb.home_text') }}</a></li>
                @if(isset($breadcrumbs))
                    @foreach($breadcrumbs as $item)
                        <li class="breadcrumb-item"><a href="{{ $item->slug }}/" class="">{{ $item->title }}</a></li>
                    @endforeach
                @endif
                
                <li class="breadcrumb-item active" aria-current="page">@yield('title', '404 Not Found')</li>

            </ol>

        </nav>
    </div>
</section>