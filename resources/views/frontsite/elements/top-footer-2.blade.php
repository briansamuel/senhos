<section id="top-footer-2" class="">
    <div class="container">
        <div class="row justify-content-between pt-4 pb-4">


            <div class="col-12 col-xl-7">
                <h5>ĐĂNG KÝ ĐỂ KHÔNG BỎ LỠ CÁC ƯU ĐÃI MỚI NHẤT!</h5>

                <div class="input-group">
                    <input type="email" class="form-control" placeholder="Nhập email tại đây" id="subcribe_email" name="subcribe_email">
                    <span class="input-group-btn">
                        <button class="btn btn-subcribe" type="submit" id="btn_subcribe" onclick="subcribeEmail()">ĐĂNG KÝ</button>
                    </span>
                </div>
            </div>
            <div class="col-12 col-xl-3">


                <h5>MẠNG XÃ HỘI</h5>
                <ul class="list-unstyled list-inline social" id="list-social">
                    <li class="list-inline-item"><a href="javascript:void();"><i class="fab fa-facebook-f"></i></a>
                    </li>
                    <li class="list-inline-item"><a href="javascript:void();"><i class="fab fa-twitter"></i></a>
                    </li>
                    <li class="list-inline-item"><a href="javascript:void();"><i class="fab fa-google-plus-g"></i></a></li>
                    <li class="list-inline-item"><a href="javascript:void();"><i class="fab fa-instagram"></i></a>
                    </li>
                </ul>
            </div>
        </div>

    </div>
</section>