@if($setting->get('theme_option::general::subscription_popup') !== '')
<div class="cd-popup" role="alert">
	<div id="container-popup" class="subcription-popup cd-popup-container">
		<h2>{{ __('frontsite.subcription.header') }}</h2>
		<p class="text-center">{{ __('frontsite.subcription.description') }}</p>
		<form>
			<input type="email" placeholder="{{ __('frontsite.subcription.placeholder') }}" id="subcribe_popup_email" name="subcribe_popup_email">
			<br>
			<button type="button" onclick="subcribePopupEmail()" id="btn_popup_subcribe_popup">{{ __('frontsite.subcription.button_submit') }}</button>
		</form>
	</div>
	<a href="#0" class="cd-popup-close img-replace">Close</a>
</div>
<div class="shadown-subcription-popup"></div>
<a href="#" id="open-subcribe-popup" class="float">
	<i class="fa fa-envelope my-float"></i>
</a>
<div class="label-container">
	<div class="label-text">Feedback</div>
	<i class="fa fa-play label-arrow"></i>
</div>
<style>
	.cd-popup {
		position: fixed;
		left: 0;
		top: 0;
		height: 100%;
		width: 100%;
		background-color: rgba(94, 110, 141, 0.9);
		opacity: 0;
		visibility: hidden;
		-webkit-transition: opacity 0.3s 0s, visibility 0s 0.3s;
		-moz-transition: opacity 0.3s 0s, visibility 0s 0.3s;
		transition: opacity 0.3s 0s, visibility 0s 0.3s;
		z-index: 10001;

	}

	.cd-popup.is-visible {
		opacity: 1;
		visibility: visible;
		-webkit-transition: opacity 0.3s 0s, visibility 0s 0s;
		-moz-transition: opacity 0.3s 0s, visibility 0s 0s;
		transition: opacity 0.3s 0s, visibility 0s 0s;
	}



	.cd-popup-container {

		box-shadow: 0 15px 30px 1px rgba(128, 128, 128, 0.31);
		text-align: center;
		border-radius: 5px;
		margin: 4em auto;
		height: 300px;
		width: 480px;
		padding: 1em;

		position: relative;
		box-shadow: 0 0 20px rgba(0, 0, 0, 0.2);
		-webkit-transform: translateY(-40px);
		-moz-transform: translateY(-40px);
		-ms-transform: translateY(-40px);
		-o-transform: translateY(-40px);
		transform: translateY(-40px);
		/* Force Hardware Acceleration in WebKit */
		-webkit-backface-visibility: hidden;
		-webkit-transition-property: -webkit-transform;
		-moz-transition-property: -moz-transform;
		transition-property: transform;
		-webkit-transition-duration: 0.3s;
		-moz-transition-duration: 0.3s;
		transition-duration: 0.3s;
		background: white;

	}

	.cd-popup-container .cd-popup-close {
		position: absolute;
		top: 8px;
		right: 8px;
		width: 30px;
		height: 30px;
	}

	.cd-popup-container .cd-popup-close::before,
	.cd-popup-container .cd-popup-close::after {
		content: '';
		position: absolute;
		top: 12px;
		width: 14px;
		height: 3px;
		background-color: #8f9cb5;
	}

	.cd-popup-container .cd-popup-close::before {
		-webkit-transform: rotate(45deg);
		-moz-transform: rotate(45deg);
		-ms-transform: rotate(45deg);
		-o-transform: rotate(45deg);
		transform: rotate(45deg);
		left: 8px;
	}

	.cd-popup-container .cd-popup-close::after {
		-webkit-transform: rotate(-45deg);
		-moz-transform: rotate(-45deg);
		-ms-transform: rotate(-45deg);
		-o-transform: rotate(-45deg);
		transform: rotate(-45deg);
		right: 8px;
	}

	.is-visible .cd-popup-container {
		-webkit-transform: translateY(0);
		-moz-transform: translateY(0);
		-ms-transform: translateY(0);
		-o-transform: translateY(0);
		transform: translateY(0);
	}

	#container-popup h2 {
		background: url(https://static.pexels.com/photos/5782/nature-flowers-vintage-plant.jpg);
		/*   الكود المسؤال عن اضافة شفافية للنص */
		-webkit-text-fill-color: transparent;
		-webkit-background-clip: text;
		/* 	background-image: linear-gradient(to top, #cd9cf2 0%, #f6f3ff 100%); */
		font-family: 'Playfair Display', serif;
		background-repeat: no-repeat;
		background-attachment: fixed;
		background-size: cover;
		letter-spacing: 2px;
		font-size: 2.5em;
		margin: 0.5em 0;



	}

	#container-popup p {
		font-family: 'Farsan', cursive;
		margin: 3px 0 1.5em 0;
		font-size: 1em;
		color: #7d7d7d;
	}

	#container-popup input {
		/* 	background: rgba(255, 13, 254, 0.90); */
		width: 100%;
		display: inline-block;
		text-align: center;
		border-radius: 7px;
		background: #eee;
		padding: 1em 2em;
		outline: none;
		border: none;
		color: #222;
		transition: 0.3s linear;
	}

	::placeholder {
		color: #999;
	}

	#container-popup input:focus {
		background: rgba(0, 0, 333, 0.10);
	}

	#container-popup button {
		background-image: linear-gradient(to left, rgba(255, 146, 202, 0.75) 0%, rgba(145, 149, 251, 0.86) 100%);
		box-shadow: 0 9px 25px -5px #df91fb;
		font-family: 'Abel', sans-serif;
		padding: 0.5em 1.9em;
		margin: 2.3em 0 0 0;
		border-radius: 7px;
		font-size: 1.4em;
		cursor: pointer;
		color: #FFFFFF;
		font-size: 1em;
		outline: none;
		border: none;
		transition: 0.3s linear;

	}

	#container-popup button:hover {
		transform: translatey(2px);
	}

	#container-popup button:active {
		transform: translatey(5px);
	}

	.label-container {
		position: fixed;
		bottom: 95px;
		right: 80px;
		display: table;
		visibility: hidden;
	}

	.label-text {
		color: #FFF;
		background: rgba(51, 51, 51, 0.5);
		display: table-cell;
		vertical-align: middle;
		padding: 10px;
		border-radius: 3px;
	}

	.label-arrow {
		display: table-cell;
		vertical-align: middle;
		color: #333;
		opacity: 0.5;
	}

	#open-subcribe-popup.float {
		position: fixed;
		width: 60px;
		height: 60px;
		bottom: 90px;
		right: 20px;
		background-color: #06C;
		color: #FFF;
		border-radius: 50px;
		text-align: center;
		box-shadow: 2px 2px 3px #999;
	}

	.my-float {
		font-size: 24px;
		margin-top: 18px;
	}

	a.float+div.label-container {
		visibility: hidden;
		opacity: 0;
		transition: visibility 0s, opacity 0.5s ease;
	}

	a.float:hover+div.label-container {
		visibility: visible;
		opacity: 1;
	}

	.shadown-subcription-popup {
		position: fixed;
		width: 100%;
		height: 100%;
		background: #242323d9;
		left: 0;
		top: 0;
		z-index: 10000;
		display: none;
	}

	@media (max-width: 576px) {
		#container-popup {
			width: 90%;
			margin: 16em auto;
		}
	}
</style>
<script>
	$(document).ready(function() {


		$("#open-subcribe-popup").click(function(e) {
			e.preventDefault();

			$('.cd-popup').addClass('is-visible');
		});

		$('.cd-popup').on('click', function(event) {
			if ($(event.target).is('.cd-popup-close') || $(event.target).is('.cd-popup')) {
				event.preventDefault();
				$(this).removeClass('is-visible');
			}
		});
	});

	function subcribePopupEmail(){
		var subcribe_popup_email = $("#subcribe_popup_email").val();
		if(subcribe_popup_email === '') {
			alert('Vui lòng nhập email để nhận ưu đãi');
			return false;
		}

		if(!validateEmail(subcribe_popup_email)) {
			alert('Vui lòng nhập đúng định dạng email để nhận ưu đãi');
			return false;
		}

		//display notice
		$("#btn_popup_subcribe").text("Đang xử lý....");
		$("#btn_popup_subcribe").removeAttr("onclick");

		$.post('/subcribe-email', {
			email: subcribe_popup_email,
			_token: '{{csrf_token()}}'
		}, function(res) {
			$("#btn_popup_subcribe").text("ĐĂNG KÝ");
			$("#btn_popup_subcribe").attr("onclick", "return subcribePopupEmail();");
			if(res.success) {
				$("#subcribe_popup_email").val('');
				$(".cd-popup").removeClass('is-visible');
			}
			alert(res.message);
			return false;
		}).fail(function() {
			$("#btn_popup_subcribe").text("ĐĂNG KÝ");
			$("#btn_popup_subcribe").attr("onclick", "return subcribePopupEmail();");
			alert('Hệ thống gặp lỗi, vui lòng thử lại sau.');
			return false;
		});
	}
</script>
@endif