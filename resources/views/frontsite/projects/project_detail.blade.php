@extends('frontsite.index')
@section('title', $project->project_title)
@section('title_ab', $project->project_title)
@section('keyword', $project->project_seo_keyword ? $project->project_seo_keyword : $project->project_title)
@section('description', $project->project_seo_description ? $project->project_seo_description : $project->project_title)
@section('style')

@endsection
@section('content')
<div class="main-wrapper">
    <!-- START Block Banner  -->

    @include('frontsite.elements.banner')
    <!-- END Block Banner -->

    <div class="main-wrapper">

        <!-- START BREADCRUMB -->
        @include('frontsite.elements.breadcrumb')
        <!-- END BREADCRUMB -->
        <!-- WRAP CONTENT -->

        <div class="wrap-content mt-2 mb-2">
            <div class="container">
                <div class="row">

                    <div id="post-" class="col-12 col-xl-12 pt-2 single-post">
                        <div class="post-header">
                            <h2 class="post-title">{{ $project->project_title }}</h2>
                        </div>
                        <div class="post-meta">
                            <span class="post-time"><i class="fas fa-clock"></i> {{ $project->created_at }}</span>
                            <span class="ml-4 mr-4">|</span>
                            <span class="post-view">302 lượt xem</span>
                        </div>
                        <div class="post-content">

                            {!! $project->project_content !!}



                        </div>
                        <div class="related-posts mt-4">
                            <div class="row ">
                                <div class="col-12">
                                    <div class="header-related-block">
                                        <span>{{ __('frontsite.project.other_project') }}</span>
                                    </div>

                                </div>

                            </div>
                            <div class="row post-3-col">
                                @foreach($projects as $prj)
                                <div class="col-12 col-xl-4 item-related-post">
                                    <a href="du-an/{{ $prj->project_slug }}.html" class="thumbnail-related-post effect-zoom-in">
                                        <img src="{{ $prj->project_thumbnail }}">
                                    </a>
                                    <a href="du-an/{{ $prj->project_slug }}.html" title="{{ $prj->project_slug }}">
                                        <h4>{{ $prj->project_title }}</h4>
                                    </a>
                                    <span class="related-post-time"><i class="fas fa-clock"></i> {{ $prj->created_at }}</span>
                                </div>
                                @endforeach


                            </div>
                            <div class="widget lastest-videos mt-4">
                                <h6 class="widget-title">{{ __('frontsite.project.video_project') }}</h6>
                                <div class="widger-content">
                                    <div class="row">
                                        @foreach($lasted_video as $index => $video)
                                        <div class="col-12 col-xl-3 item-video ">
                                            <a href="thu-vien/{{ $video->post_slug}}.html" title="{{ $video->post_title }}" class="thumbnail-video ">
                                                <img src="{{ $video->post_thumbnail }}" alt="{{ $video->post_title }}" class="img-fluid">
                                                <span>{{ $video->post_title }}</span>
                                            </a>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END BLOCK TYPE PLACE -->

    </div>
</div>
@endsection
@section('script')

@endsection