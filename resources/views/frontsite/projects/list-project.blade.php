@extends('frontsite.index')
@section('title', __('seo.project.title'))
@section('title_ab', __('seo.project.title_ab'))
@section('keyword', __('seo.project.keyword') )
@section('description', __('seo.project.description'))
@section('style')

@endsection
@section('content')
<div class="main-wrapper">
    <!-- START Block Banner  -->

    @include('frontsite.elements.banner')
    <!-- END Block Banner -->
    
    <!-- START BREADCRUMB -->
    @include('frontsite.elements.breadcrumb')
    <!-- END BREADCRUMB -->

    <!-- START BLOCK PROJECT -->
    <section class="" id="block-project">
        <div class="container">
            <div class="row mt-4 mb-3 justify-content-center">
                <div class="col-12">
                    <!-- DỰ ÁN ĐÃ THỰC HIỆN -->
                    <h3 class="text-left title-page">{{ __('frontsite.projects.lastest_project') }}</h3>
                    <p>{{ __('frontsite.projects.description') }}</p>
                    <!-- SEN HOSPITALITY cung cấp dịch vụ tư vấn và quản lý khách sạn chuyên nghiệp, chất lượng cùng tinh thần
                        “cùng nhau phát triển - cùng nhau thành công” cho các chủ đầu tư trong nước, từ khi thành lập đến
                        nay, SEN Hospitality luôn được các đối tác tin tưởng, được giới chuyên môn đánh giá là một trong
                        những thương hiệu quản lý khách sạn hàng đầu. -->
                </div>

            </div>
            <div class="row">
                @foreach($projects as $project)
                <div class="col-12 col-lg-4 col-sm-6 col-xl-4 ">
                    <div class="project-item text-center">
                        <a href="du-an/{{ $project->project_slug }}.html" class="effect-zoom-in">
                            <img class="img-fluid" src="{{ $project->project_thumbnail }}" alt="{{ $project->project_title }}">
                        </a>
                        <a href="du-an/{{ $project->project_slug }}.html">
                            <h4 class="project-title mt-2 mb-1">{{ $project->project_title }}</h4>
                        </a>
                        <span class="project-location ">{{ $project->project_location }}</span>
                        <div class="text-justify mt-2 project-expert">{!! $project->project_description !!}</div>
                    </div>
                </div>
                @endforeach

            </div>
           
        </div>
    </section>
    <!-- END BLOCK PROJECT -->
    <!-- START GALLERY PROJECT -->
   
    <!-- END GALLERY PROJECT -->

    <!-- START BLOCK PARTNER -->
    @include('frontsite.elements.partner')
    <!-- END BLOCK PARTNER -->
</div>
@endsection
@section('script')

@endsection