<div id="gallery-project" class="my-4">
    <div class="row mt-4 mb-3 justify-content-center">
        <div class="col-12">
            <!-- THƯ VIỆN DỰ ÁN -->
            <h3 class="text-left title-page">{{ __('frontsite.projects.gallery_project') }}</h3>
            
        </div>

    </div>
    <div class="row">
        <div class="col-12 col-sm-3 my-2 px-2">
            <img src="http://senhos.local/uploads/files/coconut.jpg" class="w-100 h-100  img-fluid img-box">
        </div>
        <div class="col-12 col-sm-3 my-2 px-2">
            <img src="http://senhos.local/uploads/files/du-an1.jpg" class="w-100 h-100 img-fluid img-box">
        </div>
        <div class="col-12 col-sm-3 my-2 px-2">
            <img src="http://senhos.local/uploads/files/du-an2.jpg" class="w-100 h-100 img-fluid img-box">
        </div>
        <div class="col-12 col-sm-3 my-2 px-2">
            <img src="http://senhos.local/uploads/files/khanh-van.jpg" class="w-100 h-100 img-fluid img-box">
        </div>
        <div class="col-12 col-sm-3 my-2 px-2">
            <img src="https://znews-photo.zadn.vn/w660/Uploaded/wyhktpu/2019_01_16/image003_5.jpg" class="w-100 h-100 img-fluid img-box">
        </div>
        <div class="col-12 col-sm-3 my-2 px-2">
            <img src="https://img.theleader.vn/thumbs/788x0/upload/vananh/2019/1/21/best%20phu%20quoc.jpg" class="w-100 h-100 img-fluid img-box">
        </div>
        <div class="col-12 col-sm-3 my-2 px-2">
            <img src="http://senhos.local/uploads/files/coconut.jpg" class="w-100 h-100 img-fluid img-box">
        </div>
        <div class="col-12 col-sm-3 my-2 px-2">
            <img src="http://senhos.local/uploads/files/du-an1.jpg" class="w-100 h-100 img-fluid img-box">
        </div>
        <div class="col-12 col-sm-3 my-2 px-2">
            <img src="http://senhos.local/uploads/files/du-an2.jpg" class="w-100 h-100 img-fluid img-box">
        </div>
        <div class="col-12 col-sm-3 my-2 px-2">
            <img src="http://senhos.local/uploads/files/khanh-van.jpg" class="w-100 h-100 img-fluid img-box">
        </div>
    </div>
</div>