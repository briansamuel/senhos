<div class="widget lastest-news">
    <h6 class="widget-title">{{ __('frontsite.widget.top_view_news') }}</h6>
    <div class="widget-content">
        @foreach($hot_news as $index => $new)
        <div class="item-news">
            <a class="thumbnail-news effect-zoom-in" href="#">
                <img src="{{ $new->post_thumbnail }}" class="img-fluid">
            </a>
            <div class="info-news">
                <a href="#">
                    <h4>{{ $new->post_title }}</h4>
                </a>
                <span>{{ $new->post_author }}</span>
                <span>{{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $new->created_at)->format('d/m/Y') }}</span>
            </div>
        </div>
        @endforeach
    </div>


</div>