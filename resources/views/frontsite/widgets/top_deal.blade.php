@inject('topDealService', 'App\Services\TopDealService')
@php
$top_deals = $topDealService::getForFrontEnd();
@endphp
@if(isset($top_deals))
<div class="widget top-deal mt-5">
    <div class="widger-content">
        @foreach($top_deals as $deal)
        <div class="item-top-deal">
            <a href="{{$deal->link}}" class="thumbnail-top-deal effect-zoom-in">
                <img src="{{$deal->image}}" class="img-fluid">
                <span class="discount-deal">{{$deal->label}}</span>
            </a>
            <div class="info-top-deal">
                <h5>{{$deal->title}}</h5>
                <span class="location">{{$deal->location}}</span>
                <span class="type-room font-weight-bold">{{$deal->description}}</span>
                <span class="font-weight-bold">{{ __('frontsite.booking.length_of_stay') }}</span>
                <span class="checkin-time">{{date("d/m-Y", $deal->start_time)}} - {{date("d/m-Y", $deal->end_time)}}</span>
                <div class="price-top-deal">
                    <span class="regular-price">
                        {{number_format($deal->regular_price, 0, ",", ".")}} VND
                    </span>
                    <span class="sale-price">
                        {{number_format($deal->sale_price, 0, ",", ".")}} VND
                    </span>
                    <div class="btn-around">
                        <a href="{{$deal->link}}" class="top-sale-booking">{{ __('frontsite.booking.booking_now') }}</a>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
@endif