<div class="widget lastest-videos mt-5">
    <h6 class="widget-title text-uppercase">{{ __('frontsite.home.gallery_video') }}</h6>
    <div class="widger-content">
        <div class="row">
            @foreach($lasted_video as $index => $video)

            <div class="col-12 col-xl-6 item-video ">
                <a href="thu-vien/{{ $video->post_slug }}.html" class="thumbnail-video ">
                    <img src="{{ $video->post_thumbnail }}" class="img-fluid">
                    <span>{{ $video->post_title }}</span>
                </a>
            </div>
            @endforeach

        </div>
    </div>
</div>