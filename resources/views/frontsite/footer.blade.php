<footer id="footer" class="pt-4 container-fluid">
    <div class="row justify-content-center pb-4">
        <div class="col-12 text-center">
            <a href="#" class="footer-logo">
                <img src="{{$setting->get('theme_option::footer::banner', __('frontsite.footer.banner'))}}">
            </a>
        </div>
        <div class="col-12 text-center mt-2">
            @include('frontsite.elements.menu-footer');
        </div>
        <div class="col-12 text-center footer-social">
            <h4>{{ __('frontsite.footer.social_header') }}</h4>
            <ul class="list-unstyled list-inline social" id="footer-social">
                <li class="list-inline-item"><a target="_blank" href="{{$setting->get('theme_option::footer::youtube', __('frontsite.footer.youtube'))}}"><i class="fab fa-youtube"></i></a></li>
                <li class="list-inline-item"><a target="_blank" href="{{$setting->get('theme_option::footer::facebook', __('frontsite.footer.facebook'))}}"><i class="fab fa-facebook-f"></i></a></li>
                <li class="list-inline-item"><a target="_blank" href="{{$setting->get('theme_option::footer::twitter', __('frontsite.footer.twitter'))}}"><i class="fab fa-twitter"></i></a>
                </li>
                <li class="list-inline-item"><a target="_blank" href="{{$setting->get('theme_option::footer::google', __('frontsite.footer.google'))}}"><i class="fab fa-google-plus-g"></i></a>
                </li>
                <li class="list-inline-item"><a target="_blank" href="{{$setting->get('theme_option::footer::instagram', __('frontsite.footer.instagram'))}}"><i class="fab fa-instagram"></i></a></li>
            </ul>
        </div>
        <div class="col-12 text-center">
            <h2 class="company-name">{{$setting->get('theme_option::footer::company_name', __('frontsite.footer.company_name'))}}</h2>
        </div>
        <div class="col-12 text-center ">
            <span class="text-white">{{$setting->get('theme_option::footer::address_1', __('frontsite.footer.address_1'))}}</span>
        </div>
        <div class="col-12 text-center ">
            <span class="text-white">{{$setting->get('theme_option::footer::address_2', __('frontsite.footer.address_2'))}}</span>
        </div>
        
        <div class="col-12 col-xl-6 text-center text-xl-right border-right-1 mt-2">
            <span class="text-white">{{$setting->get('theme_option::footer::phone_number', __('frontsite.footer.phone_number'))}}</span>
        </div>
        <div class="col-12 col-xl-6 text-center text-xl-left mt-2">
            <span class="text-white">{{$setting->get('theme_option::footer::email', __('frontsite.footer.email'))}}</span>
        </div>
    </div>
    <div class="row copy-right">
        <span class="d-flex justify-content-center w-100">{{$setting->get('theme_option::footer::copyright', __('frontsite.footer.copyright'))}}</span>
    </div>
</footer>

<a id="back-to-top" href="#" class="btn btn-light btn-lg back-to-top" role="button"><i class="fas fa-chevron-up"></i></a>


<!-- START BLOCK POPUP -->
@include('frontsite.elements.popup')
<!-- END BLOCK POPUP -->

<!-- Main Script -->
<script type="text/javascript" src="js/script.js"></script>
<script type="text/javascript" src="js/popup.js"></script>
<!-- End Main Script -->
<!-- Script For page -->
<!-- End Script For page -->