@extends('frontsite.index')
@section('title', 'Đăng Nhập')
@section('style')

@endsection
@section('content')
<!-- START Block Banner  -->

@include('frontsite.elements.banner')
<!-- END Block Banner -->

<div class="wrap-content">
    <div class="container">
        <div class="row">

            @include('frontsite.accounts.elements.left-sidebar')
            <div class="col-8 pt-2 pt-sm-5 right-account-content">
                <div class="alert alert-success" role="alert">
                    <i class="fal fa-file-search"></i> Xem tất cả phiếu thanh toán trong Đặt chỗ của tôi
                </div>
                <div class="row m-0 bg-white rounded mb-4">
                    <div class="col-12 p-0 ">
                        <div class="d-flex justify-content-between p-3">
                            <span class="_1KrnW _1EnnQ Edww1 _2ObOC">Mã đặt chỗ
                                <strong>585484035</strong>
                            </span>
                            <div class="pcGxz"><span class="_1KrnW jjGhl _2HSse _1dKIX">2.998.635 VND</span>
                            </div>
                        </div>
                        <div class="_32Rks QsYcC p-3 bg-light">
                            <i class="fal fa-building text-third font-weight-bold"></i>
                            <span class="_1KrnW _1EnnQ _2HSse">The View Sapa Hotel</span>
                        </div>
                        <div class="d-flex justify-content-between p-3">
                            <div class="_2ObOC">
                                <span class="badge badge-pill badge-success">Giao dịch thành công</span>
                            </div>
                            <span class="_1KrnW _1EnnQ _2HSse pcGxz">
                                <a href="#">Xem chi
                                    tiết</a> <i class="fa fa-ellipsis-h text-third font-weight-bold"></i>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('script')

@endsection