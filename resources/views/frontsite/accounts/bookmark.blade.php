@extends('frontsite.index')
@section('title', 'Đăng Nhập')
@section('style')

@endsection
@section('content')
<!-- START Block Banner  -->

@include('frontsite.elements.banner')
<!-- END Block Banner -->

<div class="wrap-content">
    <div class="container">
        <div class="row">
            
            @include('frontsite.accounts.elements.left-sidebar')
            <div class="col-8 pt-2 pt-sm-5 right-account-content">
                <div class="account-content">


                    <form role="form" class="form-horizontal">
                        <div class="form-group">
                            <label for="name" class="col-12 control-label">
                                Họ và tên</label>
                            <div class="col-12">
                                <input type="text" class="form-control rounded-0" id="name" placeholder="Email" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-12 control-label">
                                Số điện thoại</label>
                            <div class="col-12">
                                <input type="text" class="form-control rounded-0" id="name" placeholder="Email" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="name" class="col-12 control-label">
                                Địa chỉ Email</label>
                            <div class="col-12">
                                <input type="text" class="form-control rounded-0" id="name" placeholder="Email" />
                            </div>
                        </div>
                        <div class="form-group ">

                            <div class="col-12">
                                <button type="button" class="btn btn-primary bg-primary rounded-0">
                                    Cập nhật</button>
                                <button type="button" class="btn btn-default rounded-0">
                                    Hủy</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('script')

@endsection