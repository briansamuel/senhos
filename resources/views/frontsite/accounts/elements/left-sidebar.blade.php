<div class="col-4 p-0">
    <div class="" id="sidebar-wrapper">
        <div class="sidebar-heading">
            <strong>Tài khoản</strong>
        </div>
        <div class="list-group list-group-flush">
            <a href="{{ route('account.info') }}" class="list-group-item list-group-item-action {{ Request::is('account') ? 'active' : '' }} "><i class="fal fa-user-alt"></i> Chỉnh sửa hồ sơ</a>
            @if(Auth::guard('user')->check() && Auth::guard('user')->user()->password === '')
            <a href="{{ route('account.updatePassword') }}" class="list-group-item list-group-item-action {{ Request::is('account/update-pass') ? 'active' : '' }}"><i class="fal fa-key"></i>
                Cập nhập mật khẩu</a>
            @else
            <a href="{{ route('account.resetPassword') }}" class="list-group-item list-group-item-action {{ Request::is('account/change-pass') ? 'active' : '' }}"><i class="fal fa-key"></i>
                Đổi mật khẩu</a>
            @endif
            <a href="{{ route('account.bookmark') }}" class="list-group-item list-group-item-action {{ Request::is('account/bookmark') ? 'active' : '' }}"><i class="fal fa-bookmark"></i>
                Đã lưu</a>
            <a href="{{ route('account.historyBooking') }}" class="list-group-item list-group-item-action {{ Request::is('account/history-booking') ? 'active' : '' }}"><i class="fal fa-ballot"></i> Đặt
                chỗ của tôi</a>
            <a href="{{ route('frontsite.logout') }}" class="list-group-item list-group-item-action"><i class="fal fa-sign-out-alt"></i> Đăng xuất</a>

        </div>
    </div>
</div>