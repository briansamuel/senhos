@extends('frontsite.index')
@section('title', 'Cập nhập mật khẩu')
@section('style')

@endsection
@section('content')
<!-- START Block Banner  -->

@include('frontsite.elements.banner')
<!-- END Block Banner -->

<div class="wrap-content">
    <div class="container">
        <div class="row">
            
            @include('frontsite.accounts.elements.left-sidebar')
            <div class="col-8 pt-2 pt-sm-5 right-account-content">
                <div class="account-content">


                    <form role="form" class="form-horizontal">
                        <div class="form-group">
                            <label for="password" class="col-12 control-label">
                                Mật khẩu</label>
                            <div class="col-12">
                                <input type="password" class="form-control rounded-0" id="password" name="password" placeholder="Mật khẩu mới" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="comfirm-password" class="col-12 control-label">
                                Xác nhận mật khẩu</label>
                            <div class="col-12">
                                <input type="password" class="form-control rounded-0" name="confirm-password" id="confirm-password" placeholder="Nhập lại mật khẩu mới"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-12">
                                <div style="display:none;" id="show_error">
                                    <span class="text text-danger" id="content_error"></span>
                                </div>
                                <div style="display:none;" id="show_success">
                                    <span class="text text-success" id="content_success"></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group ">
                            <div class="col-12">
                                <button type="button" class="btn btn-primary bg-primary rounded-0" onclick="updateInfo()">
                                    Đổi mật khẩu</button>
                                <button type="button" class="btn btn-default rounded-0">
                                    Hủy</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('script')
    <script>
        function CheckInfo(){
            var password = $("#password").val();
            var confirm_password = $("#confirm-password").val();
            if(password === ""){
                $("#show_success").hide();
                $("#show_error").show();
                $("#content_error").text("Bạn chưa nhập mật khẩu mới !");
                $("#password").focus();
                return false;
            }
            if(confirm_password === ""){
                $("#show_success").hide();
                $("#show_error").show();
                $("#content_error").text("Bạn chưa xác nhận mật khẩu !");
                $("#confirm_password").focus();
                return false;
            }

            if(confirm_password !== password){
                $("#show_success").hide();
                $("#show_error").show();
                $("#content_error").text("Mật khẩu xác nhận không trùng khớp !");
                $("#confirm_password").focus();
                return false;
            }

            return true;
        }

        function updateInfo(){
            if(CheckInfo()){
                //display notice
                $("#btn_edit").text("Đang xử lý....");
                $("#btn_edit").removeAttr("onclick");

                $.post('/account/update-pass', {
                    password: $("#password").val(),
                    confirm_password: $("#confirm-password").val(),
                    _token: "{{csrf_token()}}"
                }, function(res) {
                    if (res.success) {
                        $("#show_error").hide();
                        $("#show_success").show();
                        $("#content_success").text(res.message);
                        return true;
                    } else {
                        $("#btn_edit").text("Đổi mật khẩu");
                        $("#btn_edit").attr("onclick", "return updateInfo();");
                        $("#show_success").hide();
                        $("#show_error").show();
                        $("#content_error").text(res.error.message);
                        return false;
                    }
                }).fail(function() {
                    $("#btn_edit").text("Đăng ký");
                    $("#btn_edit").attr("onclick", "return updateInfo();");
                    $("#show_success").hide();
                    $("#show_error").hide();
                    $("#content_error").text("");
                    alert('Hệ thống gặp lỗi, vui lòng thử lại sau.');
                    return false;
                });
            }
        }

    </script>
@endsection