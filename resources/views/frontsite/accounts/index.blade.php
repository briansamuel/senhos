@extends('frontsite.index')
@section('title', 'Đăng Nhập')
@section('style')

@endsection
@section('content')
<!-- START Block Banner  -->

@include('frontsite.elements.banner')
<!-- END Block Banner -->

<div class="wrap-content">
    <div class="container">
        <div class="row">
            
            @include('frontsite.accounts.elements.left-sidebar')
            <div class="col-8 pt-2 pt-sm-5 right-account-content">
                <div class="account-content">


                    <form role="form" class="form-horizontal" id="kt_edit_form">
                        <div class="form-group">
                            <label for="name" class="col-12 control-label">
                                Họ và tên</label>
                            <div class="col-12">
                                <input type="text" class="form-control rounded-0" id="full_name" value="{{$account['full_name']}}"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-12 control-label">
                                Số điện thoại</label>
                            <div class="col-12">
                                <input type="text" class="form-control rounded-0" id="phone_number" value="{{$account['guest_phone']}}"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="name" class="col-12 control-label">
                                Địa chỉ Email</label>
                            <div class="col-12">
                                <input type="text" class="form-control rounded-0" id="email" value="{{$account['email']}}"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-12">
                                <div style="display:none;" id="show_error">
                                    <span class="text text-danger" id="content_error"></span>
                                </div>
                                <div style="display:none;" id="show_success">
                                    <span class="text text-success" id="content_success"></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group ">
                            <div class="col-12">
                                <button type="button" class="btn btn-primary bg-primary rounded-0" id="btn_edit" onclick="updateInfo()">
                                    Cập nhật</button>
                                <button type="button" class="btn btn-default rounded-0">
                                    Hủy</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('script')
    <script>
        function CheckInfo(){
            var full_name = $("#full_name").val();
            var email = $("#email").val();
            var guest_phone = $("#phone_number").val();
            if(full_name === ""){
                $("#show_success").hide();
                $("#show_error").show();
                $("#content_error").text("Bạn chưa nhập họ tên !");
                $("#full_name").focus();
                return false;
            }
            if(guest_phone === ""){
                $("#show_success").hide();
                $("#show_error").show();
                $("#content_error").text("Bạn chưa nhập số điện thoại !");
                $("#phone_number").focus();
                return false;
            }
            if(email === ""){
                $("#show_success").hide();
                $("#show_error").show();
                $("#content_error").text("Bạn chưa nhập Email !");
                $("#email").focus();
                return false;
            }
            if(!validateEmail(email)){
                $("#show_success").hide();
                $("#show_error").show();
                $("#content_error").text("Email không đúng định dạng !");
                $("#email").focus();
                return false;
            }

            return true;
        }

        function updateInfo(){
            if(CheckInfo()){
                //display notice
                $("#btn_edit").text("Đang xử lý....");
                $("#btn_edit").removeAttr("onclick");

                $.post('/account', {
                    full_name: $("#full_name").val(),
                    email: $("#email").val(),
                    guest_phone: $("#phone_number").val(),
                    _token: "{{csrf_token()}}"
                }, function(res) {
                    $("#btn_edit").text("Cập nhập");
                    $("#btn_edit").attr("onclick", "return updateInfo();");
                    if (res.success) {
                        $("#show_error").hide();
                        $("#show_success").show();
                        $("#content_success").text(res.message);
                        return true;
                    } else {
                        $("#show_success").hide();
                        $("#show_error").show();
                        $("#content_error").text(res.error.message);
                        return false;
                    }
                }).fail(function() {
                    $("#btn_edit").text("Cập nhập");
                    $("#btn_edit").attr("onclick", "return updateInfo();");
                    $("#show_success").hide();
                    $("#show_error").hide();
                    $("#content_error").text("");
                    alert('Hệ thống gặp lỗi, vui lòng thử lại sau.');
                    return false;
                });
            }
        }

    </script>
@endsection