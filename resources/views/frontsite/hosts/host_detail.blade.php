@extends('frontsite.index')
@section('title', $host->host_name)
@section('title_ab', $host->host_name )
@section('description', $host->host_description)
@section('style')

<link rel="stylesheet" href="/fancybox/jquery.fancybox.css?v=2.1.7" type="text/css" media="screen" />
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="select/css/select2-bootstrap4.min.css">
@endsection
@section('content')
@include('frontsite.elements.banner-search')
<!-- START BLOCK ABOUT -->

<div class="wrap-content mb-4">
    <div class="container">
        <div class="row">
            <!-- Widget Sidebar -->
            @include('frontsite.hosts.elements.sidebar')
            <!-- Widget Sidebar -->
            <div id="host-{{ $host->id }}" class="col-12 col-sm-8 mt-4 pt-3 single-host">
                <input id="host_id" name="host_id" value="{{ $host->id }}" type="hidden">
                <div class="host-menu">
                    <nav class="navbar navbar-expand-lg navbar-light">

                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navHotel" aria-controls="navHotel" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navHotel">
                            <ul id="host-menu-ul" class="navbar-nav justify-content-around">
                                <!-- Thông tin & giá -->
                                <li class="nav-item active">
                                    <a class="nav-link scroll-to-ele" href="javascript:void(0)" data-scroll="booking-now">{{ __('frontsite.booking.info_and_price') }}</a>
                                </li>
                                <!-- Tiện nghi -->
                                <li class="nav-item">
                                    <a class="nav-link scroll-to-ele" href="javascript:void(0)" data-scroll="convenient">{{ __('frontsite.booking.convenient') }}</a>
                                </li>
                                <!-- Chính sách -->
                                <li class="nav-item">
                                    <a class="nav-link scroll-to-ele" href="javascript:void(0)" data-scroll="policy">{{ __('frontsite.booking.policy') }}</a>
                                </li>
                                <!-- Ghi chú -->
                                <li class="nav-item">
                                    <a class="nav-link scroll-to-ele" href="javascript:void(0)" data-scroll="note">{{ __('frontsite.booking.note') }}</a>
                                </li>
                                <!-- Đáng giá của khách -->
                                <li class="nav-item">
                                    <a class="nav-link scroll-to-ele" href="javascript:void(0)" data-scroll="review">{{ __('frontsite.booking.review_by_guest') }} ({{ $reviews->count() }})</a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
                <div class="header-line-host">
                    <span class="badge-host">{{ $host->host_type }}</span>
                    <h3 class="host-name">{{ $host->host_name }}</h3>
                    <div class="host-star">
                        @for($i = 1; $i <= $host->host_star; $i++)
                        <i class="fas fa-star "></i>
                        @endfor
                        
                    </div>
                </div>
                <div class="address-line-host">
                    <span><i class="fas fa-map-marker-alt"></i> {{ $host->ward_name }}, {{ $host->district_name }}, {{ $host->province_name }} </span> - <a href="#current-position-host" class="show-map">Vị trí xuất sắc - hiển thị bản đồ</a>
                    <input class="form-control" type="hidden" value="{{ $host->host_lat }}" id="host_lat" name="host_lat" placeholder="">
                    <input class="form-control" type="hidden" value="{{ $host->host_lng }}" id="host_lng" name="host_lng" placeholder="">
                </div>
                @include('frontsite.hosts.elements.host-gallery')
                <div class="host-content pt-4">
                    <!-- Description Hotel -->
                    <div class="row mb-5">
                        <div class="col-12 col-xl-8">
                            <span class="d-block text-secondary text-uppercase">MÔ TẢ</span>
                            {!! $host->host_description !!}
                        </div>
                        <div class="col-12 col-xl-4 good-review p-3">
                            <span class="d-flex text-secondary font-weight-bold mb-3">Khách yêu thích chỗ này
                                vì</span>
                            <div class="img-box">

                                <span>Vị trí rất tốt</span>
                                <span>{{ $reviews->count() }} đánh giá liên quan</span>
                                <i class="fas fa-star"></i>
                            </div>
                            <div class="img-box">

                                <span>Nhân viên thân thiện</span>
                                <span>{{ $reviews->count() }} đánh giá liên quan</span>
                                <i class="fas fa-star"></i>
                            </div>
                            <div class="img-box">

                                <span>Sạch sẽ gọn gàng</span>
                                <span>52 đánh giá liên quan</span>
                                <i class="fas fa-star"></i>
                            </div>
                            <div class="img-box">

                                <span>Thông tin bữa sáng</span>
                                <span>Món đa dạng, Cafe ngon</span>
                                <i class="fas fa-star"></i>
                            </div>
                            <div class="img-box-2">
                                <p class="wifi-box-text">Wifi miễn phí
                                    Khách thường xuyên đánh giá kết nối wifi tuyệt vời!</p>
                                <i class="fas fa-wifi"></i>
                            </div>
                            <div class="img-box-2">
                                <p>Bãi đỗ xe riêng tại Khách Sạn</p>
                                <i class="fas fa-parking"></i>
                            </div>
                            <a href="xac-nhan.html" class="btn btn-booking-now">{{ __('frontsite.booking.booking_now') }}</a>
                        </div>
                    </div>
                    <!-- List Room -->
                    @include('frontsite.hosts.elements.rooms')
                    <!-- Review Hotel -->
                    <div id="policy" class="host-policy mt-5">
                        <span class="policy-header text-uppercase">CHÍNH SÁCH</span>
                        <div class="policy-content mt-2">
                            {!! $host->host_policy !!}
                        </div>
                    </div>
                    <!-- Convenient -->
                    @include('frontsite.hosts.elements.convenients')
                    <!-- Convenient -->
                    <!-- Rating From Guest -->
                    @include('frontsite.hosts.elements.rating')
                    <!-- Rating From Guest -->
                    <!-- Review Form -->
                    @if(Auth::guard('user')->check())
                    @include('frontsite.hosts.elements.review-form')
                    @endif
                    <!-- Review Form -->
                    <!-- Review From Guest -->
                    <div class="loader">Loading...</div>
                    <div id="reviews">

                    </div>
                    <!-- Review From Guest -->
                    <div id="note" class="host-panel-note">
                        <p>
                            Giữ giá tốt cho kỳ nghỉ sắp tới của bạn
                            <br>Nhận xác nhận tức thì với hủy đặt phòng <span>MIỄN PHÍ</span> tại hầu hết các chỗ
                            nghỉ trên trang web chúng tôi
                        </p>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<!-- END BLOCK TYPE PLACE -->
<!-- ROOM INFO POPUP -->
<div id="room-popup" class="room-popup gallery-modal" style="display: none">

</div>
<div id="current-position-host" class="h-100" style="display: none">
    <div id="map" class="h-100 w-100"></div>
</div>
<!-- END ROOM INFO POPUP -->


@endsection
@section('script')
<script src="//maps.google.com/maps/api/js?key=AIzaSyAMMeGEeYSevUQYg9ZtSDC7PWuATS3fAVw" type="text/javascript"></script>
<script type="text/javascript" src="/fancybox/jquery.fancybox.pack.js?v=2.1.7"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
<script type="text/javascript" src="/js/gmaps.min.js"></script>
<!-- Script For page -->
<script type="text/javascript" src="js/booking.js"></script>
<script type="text/javascript" src="js/rating-star.js"></script>
<script>
    var map = null;
    $(document).ready(function() {
        
        $('.slider').slick({

            pauseOnHover: true,
            arrows: false,
            dots: false,
            infinite: true,
            fade: true,
            asNavFor: '.slider-nav'
        });
        $('.slider-nav').slick({
            slidesToShow: 8,
            slidesToScroll: 1,

            asNavFor: '.slider',
            centerMode: true,
            focusOnSelect: true,

        });



        $(".show-map").fancybox({
            maxWidth: 1300,
            maxHeight: 900,
            fitToView: false,
            width: '90%',
            height: '90%',
            autoSize: false,
            closeClick: false,
            openEffect: 'none',
            closeEffect: 'none',
            afterLoad: function(current) {
                let lat = $('#host_lat').val();
                let lng = $('#host_lng').val();
                try {
                    if(!map) {
                    map = new GMaps({
                    el: '#map',
                    lat: lat,
                    lng: lng,
                    zoomControl : true,
                    zoomControlOpt: {
                        style : 'SMALL',
                        position: 'TOP_LEFT'
                    },

                    });
                    let icon = {
                        url: '/images/default/current-property-pin.svg', //url
                        scaledSize: new google.maps.Size(50, 50), // scaled size
                        origin: new google.maps.Point(0,0), // origin
                        anchor: new google.maps.Point(0, 0) // anchor
                    };

                    var infoWindow = new google.maps.InfoWindow({
                    
                        maxHeight: 66,
                        content: '<div class="HotelMap-leaflet"><table class="HotelMap-popup"><tbody><tr><td class="HotelMap-popup-blockImage"><div class="HotelMap-popup-imgCover"><img class="HotelMap-popup-img" src="{{ $host->host_thumbnail }}"></div></td><td class="HotelMap-popup-blockContent"><div class="HotelMap-popup-content"><div class="HotelMap-popup-title">{{ $host->host_name }}</div><div class="HotelMap-review"><span class="host-star"><i class="fas fa-star "></i> <i class="fas fa-star "></i><i class="fas fa-star "></i></span><span>|</span><span>Trên cả tuyệt vời&nbsp;<span>9,3</span></span></div></div></td></tr></tbody></table></div>'
                    });

                    // Create marker
    
                    var marker = map.addMarker({
                        lat: lat,
                        lng: lng,
                        icon: icon,
                        title: "Vị trí",
                        infoWindow: infoWindow
                    });
                    // // This opens the infoWindow
                    infoWindow.open(map, marker);
                }     
                } catch (error) {
                    console.log(error);
                }
                
                
            },
            afterClose: function() {
               
            }
        });

        $(".various").fancybox({
            maxWidth: 1300,
            maxHeight: 900,
            fitToView: false,
            width: '90%',
            height: '90%',
            autoSize: false,
            closeClick: false,
            openEffect: 'none',
            closeEffect: 'none'
        });

        $(".various-gallery-modal").fancybox({
            maxWidth: 1300,
            maxHeight: 680,
            fitToView: false,
            width: '80%',
            height: '680px',
            autoSize: false,
            closeClick: false,
            openEffect: 'none',
            closeEffect: 'none',
            afterLoad: function(current) {
                $('#room-popup').html('');
                let url = $(this).attr('title');
                $.fancybox.showLoading();
                $.ajax({

                    url: url,

                }).done(function(data) {
                    $('#room-popup').html(data);
                    $.fancybox.hideLoading();
                    var rgl = $('.room-gallery-slider').slick({
                        autoplay: false,
                        infinite: true,
                        autoplaySpeed: 3000,
                        pauseOnHover: true,
                        arrows: true,
                        dots: false,

                        fade: false,
                        asNavFor: '.room-gallery-slider-nav',
                        responsive: [{
                                breakpoint: 1024,
                                settings: {
                                    slidesToShow: 3,
                                    slidesToScroll: 3,
                                    infinite: false,
                                    dots: false
                                }
                            },
                            {
                                breakpoint: 600,
                                settings: {
                                    slidesToShow: 2,
                                    slidesToScroll: 2,
                                    infinite: false,
                                    dots: false
                                }
                            },
                            {
                                breakpoint: 480,
                                settings: {
                                    slidesToShow: 1,
                                    slidesToScroll: 1
                                }
                            },
                            {
                                breakpoint: 320,
                                settings: {
                                    slidesToShow: 1,
                                    slidesToScroll: 1
                                }
                            },
                            // You can unslick at a given breakpoint now by adding:
                            // settings: "unslick"
                            // instead of a settings object
                        ]
                    });
                    var rglnav = $('.room-gallery-slider-nav').slick({
                        slidesToShow: 6,
                        slidesToScroll: 1,
                        infinite: true,
                        arrows: false,
                        dots: false,
                        centerMode: true,
                        focusOnSelect: true,
                        margin: 10,
                        asNavFor: '.room-gallery-slider',
                        responsive: [{
                                breakpoint: 1024,
                                settings: {
                                    slidesToShow: 3,
                                    slidesToScroll: 3,
                                    infinite: false,
                                    dots: false
                                }
                            },
                            {
                                breakpoint: 600,
                                settings: {
                                    slidesToShow: 2,
                                    slidesToScroll: 2,
                                    infinite: false,
                                    dots: false
                                }
                            },
                            {
                                breakpoint: 480,
                                settings: {
                                    slidesToShow: 3,
                                    slidesToScroll: 1
                                }
                            },
                            {
                                breakpoint: 320,
                                settings: {
                                    slidesToShow: 3,
                                    slidesToScroll: 1
                                }
                            },
                            // You can unslick at a given breakpoint now by adding:
                            // settings: "unslick"
                            // instead of a settings object
                        ]
                    });
                }).fail(function() {
                    alert('Có lỗi xảy ra, vui lòng thử lại sau.');
                });
            },
            
            afterClose: function() {
                $('.room-gallery-slider').slick('unslick');
                $('.room-gallery-slider-nav').slick('unslick');
            }
        });
        $('#btn-review').click(function(e) {
            console.log('Click');
            let host_id = $('#host_id').val();
            let review_title = $('#review_title').val();
            let review_content = $('#review_content').val();
            let rating_review = $('#rating_review').val();
            $.ajax({
                headers: {

                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                },
                url: 'add-review',
                method: 'POST',
                data : {
                    host_id: host_id,
                    review_title: review_title,
                    review_content: review_content,
                    rating_review: rating_review,
                },
                dataType: "json",
                success: function(data) {
                    console.log(data);
                    if(data.status == 'success') {
                        getNews();
                    } else {
                        alert(data.message)
                    }
                }
            });
        });
        $('.scroll-to-ele').click(function(e) {
            e.preventDefault();
            var ele = $(this).attr('data-scroll');
            $([document.documentElement, document.body]).stop().animate({
                scrollTop: $('#' + ele).offset().top - 200
            }, 300);
        });

        getNews();
        $('body').on('click', '.pagination a:not(.is-active)', function(e) {
            e.preventDefault();

            $('#load a').css('color', '#dfecf6');
            $('.loader').show();
            var url = $(this).attr('href');
            getNews(url);
            window.history.pushState("", "", url);
        });

        function getNews(url) {
            $('.loader').show();
            $('#reviews').html('');
            $.ajax({

                url: url,


            }).done(function(data) {
                $('.loader').hide();
                $('#reviews').html(data);
            }).fail(function() {
                alert('Có lỗi xảy ra, vui lòng thử lại sau.');
            });
        }

        $('.btn-booking-now').click(function(e){
            e.preventDefault();
            let url = $(this).attr('href');
            let room_id = $(this).attr('room');
            let amount_room = $('#amout-room-'+room_id+' option:selected').val();
            let checkin = $("input[name='checkin']").val();
            let checkout = $("input[name='checkout']").val();
            let amount = $("input[name='amount']").val();
            
            if(checkin == null || checkin == '') {
                alert('Vui lòng chọn ngày checkin');
            } else if(checkout == null || checkout == '') {
                alert('Vui lòng chọn ngày checkout');
            }
            else if(amount == null || amount == '') {
                console.log('TEst'+amount);
                alert('Vui lòng chọn số người ở');
            }
            else if(amount_room == null || amount_room == '' || amount_room < 1 ) {
                alert('Vui lòng chọn số phòng tối thiểu = 1');
            } else {
                window.location.href = url+'amount_room='+amount_room+'&room_id='+room_id;
            }
           
        });
        // $('#new-review').autosize({
        //     append: "\n"
        // });
            
        var reviewBox = $('#post-review-box');
        var newReview = $('#new-review');
        var openReviewBtn = $('#open-review-box');
        var closeReviewBtn = $('#close-review-box');
        var ratingsField = $('#rating_review');

        $('body').on('click', '#open-review-box', function(e) {
            e.preventDefault();
            reviewBox.slideDown(400, function() {

            });
            openReviewBtn.fadeOut(100);
            closeReviewBtn.show();
        });

        closeReviewBtn.click(function(e) {
            e.preventDefault();
            reviewBox.slideUp(300, function() {
                newReview.focus();
                openReviewBtn.fadeIn(200);
            });
            closeReviewBtn.hide();

        });

        $('.starrr').on('starrr:change', function(e, value) {
            ratingsField.val(value);
        });
    });
</script>
<!-- End Script For page -->
@endsection