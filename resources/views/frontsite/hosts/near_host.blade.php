<p>Click để export danh sách khách sạn gần đây.</p>

<button onclick="getLocation()">Export</button>


<script>
    var x = document.getElementById("demo");

    function getLocation() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition);
        } else {
            x.innerHTML = "Geolocation is not supported by this browser.";
        }
    }

    function showPosition(position) {
        window.open(
            `search-near-hotel-action?lat=${position.coords.latitude}&long=${position.coords.longitude}`,
            '_blank'
        );
    }
</script>
