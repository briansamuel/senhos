@foreach($hosts as $host)
<div class="row border mt-3 p-2 item-list-place">
    <div class="thumbnail-place">
        <a href="thong-tin-phong.html">
            <img src="images/list-place/thumbnail-1.jpg" class="img-fluid">
        </a>
    </div>
    <div class="info-place py-2 py-sm-0 px-0 px-sm-4">
        <h4>{{ $host->host_name }}</h4>
        <span class="place-address text-primary">{{ $host->host_address }}</span>
        <p>{{ $host->host_description }}</p>
        <ul class="list-unstyled list-inline list-convenient" id="list-convenient">
            <li class="convenient-item"><i class="fas fa-wifi"></i>
            </li>
            <li class="convenient-item"><i class="fas fa-car"></i>
            </li>
            <li class="convenient-item"><i class="fas fa-tv"></i>
            </li>
            <li class="convenient-item"><i class="fas fa-parking"></i>
            </li>
        </ul>
    </div>
    <div class="price-place">
        <span class="score-rating text-primary d-block mt-0 mt-sm-3 font-weight-bold">Tuyệt
            vời</span>
        <span class="d-block mb-1 mb-sm-4">9 đánh giá</span>
        <span class="regular-price">2.300.000 VND</span>
        <span class="deal-price">1.500.000 VND</span>
        <span class="status-place">Còn phòng</span>
        <div class="bui-review-score__badge" aria-label="Đạt điểm 9,2"> 9,2 </div>
    </div>
</div>
@endforeach