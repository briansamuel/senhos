<aside class="col-12 col-lg-4 d-none d-sm-block">

    <div class="widget bookking-now mt-3">
        <div class="booking-now-around">
            <a href="javascript:void(0)" data-scroll="booking-now" class="btn btn-booking-now scroll-to-ele"><span>Đặt ngay</span></a>
        </div>
        <span class="checkbox-text"><i class="far fa-check-square mr-2"></i>Chúng tôi luôn khớp
            giá</span>
    </div>
    <!-- <div class="widget place-map mt-3">

        <div class="widger-content">
            <div class="row">
                <div class="col-12 ">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1963.4497934465844!2d103.9670128081537!3d10.18880949818047!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31a78c513c16c02f%3A0x11960b2003125c0a!2sLong%20Beach%20Resort!5e0!3m2!1svi!2s!4v1580972737442!5m2!1svi!2s" width="100%" height="200" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                </div>

            </div>
        </div>
    </div> -->
    <!-- Widget Deal -->
    <div class="widget top-deal mt-3">

        <div class="row widger-content">
            <div class="col-12 ">
                <div class="item-top-deal mb-4">
                    <a href="#" class="thumbnail-top-deal effect-zoom-in">
                        <img src="images/booking/sale-1.jpg" class="img-fluid">
                        <span class="discount-deal">Giảm 10%</span>
                    </a>
                    <div class="info-top-deal">
                        <h5>Sonata Resort & Spa</h5>
                        <span class="location">Phú Quốc</span>
                        <span class="type-room font-weight-bold">Deluxe Double Or Twin Room With Garden
                            View</span>
                        <span class="font-weight-bold">Thời gian lưu trú</span>
                        <span class="checkin-time">15/01/2020 - 31/01/2020</span>
                        <div class="price-top-deal">
                            <span class="regular-price">
                                1.320.281 VND
                            </span>
                            <span class="sale-price">
                                1.188.254 VND
                            </span>
                            <div class="btn-around">
                                <a href="#" class="top-sale-booking">Đặt ngay</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 ">
                <div class="item-top-deal mb-4">
                    <a href="#" class="thumbnail-top-deal effect-zoom-in">
                        <img src="images/booking/sale-3.jpg" class="img-fluid">
                        <span class="discount-deal">Giảm 10%</span>
                    </a>
                    <div class="info-top-deal">
                        <h5>Sonata Resort & Spa</h5>
                        <span class="location">Phú Quốc</span>
                        <span class="type-room font-weight-bold">Deluxe Double Or Twin Room With Garden
                            View</span>
                        <span class="font-weight-bold">Thời gian lưu trú</span>
                        <span class="checkin-time">15/01/2020 - 31/01/2020</span>
                        <div class="price-top-deal">
                            <span class="regular-price">
                                1.320.281 VND
                            </span>
                            <span class="sale-price">
                                1.188.254 VND
                            </span>
                            <div class="btn-around">
                                <a href="#" class="top-sale-booking">Đặt ngay</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 ">
                <div class="item-top-deal mb-4">
                    <a href="#" class="thumbnail-top-deal effect-zoom-in">
                        <img src="images/booking/sale-1.jpg" class="img-fluid">
                        <span class="discount-deal">Giảm 10%</span>
                    </a>
                    <div class="info-top-deal">
                        <h5>Sonata Resort & Spa</h5>
                        <span class="location">Phú Quốc</span>
                        <span class="type-room font-weight-bold">Deluxe Double Or Twin Room With Garden
                            View</span>
                        <span class="font-weight-bold">Thời gian lưu trú</span>
                        <span class="checkin-time">15/01/2020 - 31/01/2020</span>
                        <div class="price-top-deal">
                            <span class="regular-price">
                                1.320.281 VND
                            </span>
                            <span class="sale-price">
                                1.188.254 VND
                            </span>
                            <div class="btn-around">
                                <a href="#" class="top-sale-booking">Đặt ngay</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

</aside>