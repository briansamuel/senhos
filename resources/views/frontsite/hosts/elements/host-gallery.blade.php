<div class="wrapper host-slide mt-4">
    <div class="slider">
        @foreach($host->host_gallery as $image)
        <div><img src="{{ $image }}" alt=""></div>
        @endforeach

    </div>
    <div class="slider-nav">
        @foreach($host->host_gallery as $image)
        <div><img src="{{ $image }}" alt=""></div>
        @endforeach
    </div>
</div>