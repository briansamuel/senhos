<div class="row" style="margin-top:40px;">
    <div class="col-md-12">
        <div class="well well-sm">
            
            
            <div class="row" id="post-review-box" style="display:none;">
                <div class="col-md-12">
                    <form>
                        
                        <input id="rating_review" name="rating" type="hidden">
                        <div class="form-group">
                            <input class="form-control animated" id="review_title" name="review_title" type="text" placeholder="{{ __('frontsite.booking.title_review')}}">
                        </div>
                        <div class="form-group">    
                            <textarea class="form-control animated" cols="50" id="review_content" name="comment" placeholder="{{ __('frontsite.booking.enter_review_here') }}" rows="5"></textarea>
                        </div>
                        <div class="text-right">
                            <div class="stars starrr text-warning" data-rating="0"></div>
                            <a class="btn btn-danger btn-sm" href="#" id="close-review-box" style="display:none; margin-right: 10px;">
                                <span class="glyphicon glyphicon-remove"></span>{{ __('frontsite.booking.cancel') }}l</a>
                            <button id="btn-review" class="btn bg-primary text-white" type="button">{{ __('frontsite.booking.save') }}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
</div>
<style>
    .animated {
        -webkit-transition: height 0.2s;
        -moz-transition: height 0.2s;
        transition: height 0.2s;
    }

    .stars {
        margin: 20px 0;
        font-size: 24px;
        color: #d17581;
    }
</style>
