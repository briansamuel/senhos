<div id="rating" class="host-review mt-3">
    <span class="policy-header text-text-uppercase">{{ __('frontsite.booking.review_by_guest') }}</span>
    <div class="row mt-3">
        <div class="col-12 col-lg-2">
            <div class="rating-block mt-2">

                <span class="circle-score bold">{{ number_format($host->rating, 1, '.', '')  }}</span>
                <span class="text-third text-text-uppercase">{{ __('frontsite.booking.rate') }}</span>
            </div>
        </div>
        <div class="col-12 col-lg-6">

            <div class="pull-left">
                <div class="pull-left" style="width:80px; line-height:1;">
                    <div style="height:9px; margin:5px 0;">{{ __('frontsite.booking.great') }} <span class="glyphicon glyphicon-star"></span></div>
                </div>
                <div class="pull-left" style="width:230px;">
                    <div class="progress" style="height:9px; margin:8px 0;">
                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="5" aria-valuemin="0" aria-valuemax="5" style="width: {{ round($host->great->count('rating_review') * 100 / $host->user_rating, 0) }}%">
                            <span class="sr-only"></span>
                        </div>
                    </div>
                </div>
                <div class="pull-right" style="margin-left:10px;">{{ round($host->great->count('rating_review') * 100 / $host->user_rating, 0) }}%</div>
            </div>
            <div class="pull-left">
                <div class="pull-left" style="width:80px; line-height:1;">
                    <div style="height:9px; margin:5px 0;">{{ __('frontsite.booking.very_good') }} <span class="glyphicon glyphicon-star"></span></div>
                </div>
                <div class="pull-left" style="width:230px;">
                    <div class="progress" style="height:9px; margin:8px 0;">
                        <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="4" aria-valuemin="0" aria-valuemax="5" style="width: {{ round($host->very_good->count('rating_review') * 100 / $host->user_rating, 0) }}%">
                            <span class="sr-only">80% Complete (danger)</span>
                        </div>
                    </div>
                </div>
                <div class="pull-right" style="margin-left:10px;">{{ round($host->very_good->count('rating_review') * 100 / $host->user_rating, 0) }}%</div>
            </div>
            <div class="pull-left">
                <div class="pull-left" style="width:80px; line-height:1;">
                    <div style="height:9px; margin:5px 0;">{{ __('frontsite.booking.good') }} <span class="glyphicon glyphicon-star"></span></div>
                </div>
                <div class="pull-left" style="width:230px;">
                    <div class="progress" style="height:9px; margin:8px 0;">
                        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="3" aria-valuemin="0" aria-valuemax="5" style="width: {{ round($host->good->count('rating_review') * 100 / $host->user_rating, 0) }}%">
                            <span class="sr-only">80% Complete (danger)</span>
                        </div>
                    </div>
                </div>
                <div class="pull-right" style="margin-left:10px;">{{ round($host->good->count('rating_review') * 100 / $host->user_rating, 0) }}%</div>
            </div>
            <div class="pull-left">
                <div class="pull-left" style="width:80px; line-height:1;">
                    <div style="height:9px; margin:5px 0;">{{ __('frontsite.booking.bad') }} <span class="glyphicon glyphicon-star"></span></div>
                </div>
                <div class="pull-left" style="width:230px;">
                    <div class="progress" style="height:9px; margin:8px 0;">
                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="2" aria-valuemin="0" aria-valuemax="5" style="width: {{ round($host->bad->count('rating_review') * 100 / $host->user_rating, 0) }}%">
                            <span class="sr-only">80% Complete (danger)</span>
                        </div>
                    </div>
                </div>
                <div class="pull-right" style="margin-left:10px;">{{ round($host->bad->count('rating_review') * 100 / $host->user_rating, 0) }}%</div>
            </div>
            <div class="pull-left">
                <div class="pull-left" style="width:80px; line-height:1;">
                    <div style="height:9px; margin:5px 0;">{{ __('frontsite.booking.very_bad') }} <span class="glyphicon glyphicon-star"></span></div>
                </div>
                <div class="pull-left" style="width:230px;">
                    <div class="progress" style="height:9px; margin:8px 0;">
                        <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="1" aria-valuemin="0" aria-valuemax="5" style="width: {{ round($host->very_bad->count('rating_review') * 100 / $host->user_rating, 0) }}%">
                            <span class="sr-only">80% Complete (danger)</span>
                        </div>
                    </div>
                </div>
                <div class="pull-right" style="margin-left:10px;">{{ round($host->very_bad->count('rating_review') * 100 / $host->user_rating, 0) }}%</div>
            </div>
        </div>
        <div class="col-12 col-lg-4">
            <span class="d-block p-3 text-center text-secondary">{{ __('frontsite.booking.share_review_about_service') }}</span>
            @if(Auth::guard('user')->check())    
            <a href="#" id="open-review-box" class="btn-review-now">{{ __('frontsite.booking.your_review') }}</a>
            @else 
            <a href="dang-nhap" class="btn-review-now">{{ __('frontsite.booking.login_review') }}</a>
            @endif
        </div>
    </div>
</div>