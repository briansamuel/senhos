<div id="review" class="row">
    <div class="col-sm-12">
        <hr />
        <div class="review-block">
            @forelse($reviews as $review)
            <div class="row">
                <div class="col-3">
                    <img src="http://dummyimage.com/60x60/666/ffffff&text=No+Image" class="img-rounded w-75">
                    <div class="review-block-name"><span class="text-primary" href="#">{{ $review->name_guest }}</span></div>
                    <div class="review-block-date">{{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $review->created_at)->format('d/m/Y') }}<br />{{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $review->created_at)->diffForHumans() }}</div>
                </div>
                <div class="col-9">
                    <div class="review-block-rate">
                        @for($i = 0; $i < $review->rating_review; $i++ )
                        <i class="fas fa-star text-warning"></i>
                        @endfor
                    </div>
                    <div class="review-block-title">{{ $review->review_title }}</div>
                    <div class="review-block-description">{{ $review->review_content }}</div>
                </div>
            </div>
            <hr />
            @empty
            <span class="text-primary text-center d-block">{{ __('frontsite.booking.no_review') }}</span>
            @endforelse
                    
        </div>
    </div>
</div>
{{ $reviews->links() }}
