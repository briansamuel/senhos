<div class="container-fluid">
    <div class="row room-info">
        <div class="details col-12 col-sm-4 p-0">
            <div class="details__header details__header_shadowed">
                <p class="details__header-text" data-selenium="details__header-text">{{ $room->room_name }}</p>
                <div>
                    <div class="details__header__price-header">{{ __('frontsite.booking.from_text')}}</div>
                    <div><span class="details__header__price-display">{{ number_format($room->price_one_night, 0, '.', ',') }}</span><span class="details__header__price-currency-after">{{ __('frontsite.booking.currency') }}</span></div>
                </div>
            </div>
            <div class="details__info-area">
                <!-- Đặc trưng phòng -->
                <div class="details__info-area-block details__features details__info-area-block_padding-top" data-selenium="Features">
                    <!-- Điểm đặc trưng -->
                    <p class="details__info-area-block-header">{{ __('frontsite.booking.room_feature') }}</p>
                    <ul class="details__info-area-block-list">
                        
                        <li class="details__features-item"><i class="fal fa-home-lg  details__features-item-icon"></i><span class="details__features-item-description">{{ __('frontsite.booking.room_area')}}: {{ $room->room_area }} m²</span></li>
                        @if(isset($room->room_option->single_bed) && $room->room_option->single_bed > 0)
                        <li class="details__features-item">
                            <i class="fal fa-bed details__bed-layout-icon"></i>
                            <span class="details__bed-layout-text-block">{{ $room->room_option->single_bed }} {{ __('frontsite.booking.single_bed') }}</span>
                        </li>
                        @endif
                        @if(isset($room->room_option->twin_bed) && $room->room_option->twin_bed > 0)
                        <li class="details__features-item">
                            <i class="fal fa-bed-alt details__bed-layout-icon"></i>
                            <span class="details__bed-layout-text-block">{{ $room->room_option->twin_bed }} {{ __('frontsite.booking.twin_bed') }}</span>
                        
                        <div class="details__bed-layout-icons"><i class="fal fa-bed-alt details__bed-layout-icon"></i></div>
   
                        @endif
                        <li class="details__features-item"><i class="fal fa-door-open details__features-item-icon"></i><span class="details__features-item-description">Hướng phòng: Thành phố</span></li>
                        <li class="details__features-item"><i class="fal fa-shower details__features-item-icon"></i><span class="details__features-item-description">Vòi sen</span></li>
                        <li class="details__features-item"><i class="fal fa-bath details__features-item-icon"></i><span class="details__features-item-description">Phòng tắm chung</span></li>
                        
                        @if(isset($convenients->entertaiments->wifi))
                        <li class="details__features-item"><i class="fal fa-wifi details__features-item-icon"></i><span class="details__features-item-description">{{ __('convenient.room.wifi') }}</span></li>
                        @endif
                    </ul>
                </div>
                <!-- Loại giường -->
                <div class="details__info-area-block details__bed-layout" data-selenium="BedLayout">
                    <p class="details__info-area-block-header">{{ __('frontsite.booking.type_bed') }}</p>
                    <ul class="details__info-area-block-list">
                        <li>
                            <ul>
                                @if(isset($room->room_option->single_bed) && $room->room_option->single_bed > 0)
                                <li class="details__bed-layout-item">
                                    <div class="details__bed-layout-icons"><i class="fal fa-bed details__bed-layout-icon"></i></div>
                                    <div class="details__bed-layout-text"><span class="details__bed-layout-text-block">{{ $room->room_option->single_bed }} {{ __('frontsite.booking.single_bed') }}</span></div>
                                </li>
                                @endif
                                @if(isset($room->room_option->twin_bed) && $room->room_option->twin_bed > 0)
                                <li class="details__bed-layout-item">
                                    <div class="details__bed-layout-icons"><i class="fal fa-bed-alt details__bed-layout-icon"></i></div>
                                    <div class="details__bed-layout-text"><span class="details__bed-layout-text-block">{{ $room->room_option->twin_bed }} {{ __('frontsite.booking.twin_bed') }}</span></div>
                                </li>
                                @endif
                            </ul>
                        </li>
                    </ul>
                </div>
                <!-- Phòng tắm và vật dụng phòng tắm -->
                @if(!empty($convenients))

                @foreach($convenients as $key => $convenient_type)

                <div class="details__info-area-block details__amenities details__info-area-block_padding-top details__info-area-block_prelined">
                    <p class="details__info-area-block-header">{{ __('convenient.room.'.$key) }}</p>
                    <ul class="details__info-area-block-list">
                        @foreach($convenient_type as $key => $item)


                        <li class="details__amenities-item">
                            <i class="fal {{ $item }} details__amenities-item-icon"></i>
                            <span class="details__amenities-item-description">{{ __('convenient.room.'.$key) }}</span>
                        </li>
                        @endforeach
                    </ul>
                </div>
                @endforeach

                @endif
            </div>

        </div>
        <div class="gallery col-12 col-sm-8 p-0">
            <div class="room-gallery-slider">
                @foreach($room->room_gallery as $index => $image)
                <div><img src="{{ $image }}" alt="Image {{ $index+1 }}"></div>
                @endforeach

            </div>
            <div class="room-gallery-slider-nav">
                @foreach($room->room_gallery as $index => $image)
                <div><img src="{{ $image }}" alt="Image {{ $index+1 }}"></div>
                @endforeach
            </div>
        </div>
    </div>
</div>