<div id="booking-now" class="container list-room mt-5">
    @foreach($rooms as $room)
    <div class="row item-room">
        <div class="col-12 col-lg-3 col-xl-3 p-0 thumbnail-room">
            <a href="#room-popup" title="room/{{ $room->id }}" class="various-gallery-modal">
                @php
                $room_gallery = json_decode($room->room_gallery);
                @endphp
                @if(count($room_gallery) > 0)
                <img src="{{ $room_gallery[0] }}">
                @else
                <img src="images/booking/room-1.jpg">
                @endif
            </a>
        </div>
        <div class="col-12 col-lg-5 col-xl-5 ">
            <h4>{{ $room->room_name }}</h4>
            {!! $room->room_description !!}
            <div class="row">
                <div class="col-12 d-flex">
                    @php
                    $query = substr(\Request::getRequestUri(), strrpos(Request::getRequestUri(), '?') + 1);
                    @endphp
                    @if(Auth::guard('user')->check())
                    <a href="comfirm?{{ $query }}" room="{{ $room->id }}" class="btn btn-booking-now">{{ __('frontsite.booking.booking_now') }}</a>
                    @else
                    <a href="{{ route('frontsite.login') }}" class="btn btn-primary">{{ __('frontsite.header.login') }}</a>
                    @endif
                    <select id="amout-room-{{ $room->id }}" name="amout_room" class="ml-2 text-center w-100 w-sm-25" style="text-align-last:center">
                    
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                    </select>
                </div>
            </div>

        </div>
        <div class="col-12 col-lg-4 col-xl-4 justify-content-end text-right booking-room-div pr-sm-0">

            <div class="info-room">
                <span>{{ $room->guest_amount }} {{ __('frontsite.booking.guest_text') }}</span>
                <span class="room-price">{{ number_format($room->price_one_night, 0, '.', ',') }} {{ __('frontsite.booking.currency') }}</span>
                <span class="info-more">Bao bữa sáng</span>
                <span class="info-more">Miễn phí hủy phòng</span>
                <span class="info-more">Không cần thanh toán trước</span>
            </div>
        </div>
    </div>
    @endforeach

</div>