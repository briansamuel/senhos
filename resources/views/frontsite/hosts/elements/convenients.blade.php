<div id="convenient" class="host-convenient mt-3">
    <span class="policy-header text-uppercase">{{ __('frontsite.booking.convenient')}}</span>
    <div class="policy-content mt-2">
        <div class="row m-0">
            @foreach($convenient as $types)
                @foreach($types as $key => $item)
                
                <div class="col-4 col-xs-6 custom-control custom-checkbox2">
                    <input type="checkbox" class="custom-control-input" checked disabled id="customCheck" name="example1">
                    <label class="custom-control-label" for="customCheck">{{ __('convenient.'.$key )}}</label>
                </div>
                @endforeach
            @endforeach
            
        </div>
    </div>
</div>