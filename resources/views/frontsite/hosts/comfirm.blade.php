@extends('frontsite.index')
@section('title', __('seo.booking.comfirm_page'))
@section('title_ab', __('seo.booking.comfirm_page'))
@section('body_class', 'page-comfirm-booking')
@section('style')

@endsection
@section('content')
@php
$checkin = !empty(Request::get('checkin'))? Request::get('checkin') : date('d/m/Y');
$checkout = !empty(Request::get('checkout'))? Request::get('checkout') : date('d/m/Y');
$date1 = new DateTime(date("Y-m-d", strtotime($checkin)));
$date2 = new DateTime(date("Y-m-d", strtotime($checkout)));
$diff = $date1->diff($date2);
@endphp
<div class="wrap-content mt-5">
    <div class="container">
        @if(Session::has('message'))
        <div class="alert alert-solid-{{ Session::get('alert-class', 'success') }} alert-bold" role="alert">
            <div class="alert-text">{{ Session::get('message') }}</div>
            <div class="alert-close">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true"><i class="la la-close"></i></span>
                </button>
            </div>
        </div>
        @endif
        <div class="row">

            <div class="col-12 col-sm-8 guest-info">
                <form method="POST" action="{{ route('host.comfirmRoomAction') }}">
                    {{ csrf_field() }}
                    <input type="hidden" name="host_id" value="{{ $host->id }}">
                    <input type="hidden" name="room_id" value="{{ $room->id }}">
                    <input type="hidden" name="checkin_date" value="{{ $checkin }}">
                    <input type="hidden" name="checkout_date" value="{{ $checkout}}">
                    <input type="hidden" name="night_booking" value="{{ $diff->days }}">
                    <input type="hidden" name="room_amount" value="{{ !empty(Request::get('amount_room'))? Request::get('amount_room') : '1' }}">
                    <input type="hidden" name="room_type" value="{{ $room->room_name }}">
                    <input type="hidden" name="bed_type['single_bed']" value="{{ $room->room_option->single_bed }}">
                    <input type="hidden" name="bed_type['twin_beds']" value="{{ $room->room_option->twin_beds }}">
                    <input type="hidden" name="guest_amount" value="{{ !empty(Request::get('amount'))? Request::get('amount') : '1' }}">
                    <input type="hidden" name="booking_price" value="{{ $room->price_one_night*Request::get('amount_room') }}">
                    <!-- Đặt phòng khách sạn -->
                    <h4>{{ __('frontsite.booking.booking_hotel')}}</h4>
                    <!-- Thông tin của bạn -->
                    <h5>{{ __('frontsite.booking.your_info')}}</h5>
                    <div class="info-contact">

                        <div class="form-group">
                            <!-- Tên người liên hệ-->
                            <label for="fullname_contact">{{ __('frontsite.booking.contact_fullname')}}:</label>
                            <input type="text" class="form-control rounded-0" id="fullname_contact" readonly required aria-describedby="fullname_contact" placeholder="{{ $user->full_name }}" placeholder="{{ __('frontsite.booking.contact_fullname_placeholder')}}">
                            <!-- Nhập tên như trên CMND/hộ chiếu (không dấu) -->
                            <small id="fullname_contact" class="form-text text-muted">*{{ __('frontsite.booking.contact_fullname_description')}}</small>
                        </div>
                        <div class="form-group row">
                            <div class="col-12 col-sm-6">
                                <label for="phone_contact">{{ __('frontsite.booking.contact_phone')}}:</label>
                                <input type="text" class="form-control rounded-0" id="phone_contact" required aria-describedby="phone_contact" value="{{ $user->phone }}" placeholder="{{ __('frontsite.booking.contact_phone_placeholder')}}">
                                <!-- VD: +84 901234567 trong đó (+84) là mã quốc gia còn 901234567 là số di động -->
                                <small id="phone_contact" class="form-text text-muted">{{ __('frontsite.booking.contact_phone_description')}}</small>
                            </div>
                            <div class="col-12 col-sm-6">
                                <label for="email_contact">{{ __('frontsite.booking.contact_email')}}:</label>
                                <input type="email" class="form-control rounded-0" id="email_contact" required readonly aria-describedby="email_contact" value="{{ $user->email }}" placeholder="{{ __('frontsite.booking.contact_email_placeholder')}}">
                                <small id="email_contact" class="form-text text-muted">{{ __('frontsite.booking.contact_email_description')}}</small>
                            </div>
                        </div>


                    </div>
                    <h5>{{ __('frontsite.booking.detail_price') }}</h5>
                    <div class="info-contact mb-0">
                        <span class="host-name">{{ $host->host_name }}</span>
                        <div class="table-price">
                            <div class="row">
                                <div class="col-12 d-flex justify-content-between mb-2">
                                    <span>{{ $room->room_name }} ({{ $diff->days }} đêm)</span>
                                    <span class="table-price-right">{{ number_format($room->price_one_night*Request::get('amount_room'), 0, ',', '.') }} VND</span>
                                </div>
                                <!-- <div class="col-12 d-flex justify-content-between mb-2">
                                    <span>{{ __('frontsite.booking.fee_hotel') }}</span>
                                    <span class="table-price-right">383.222 VND</span>
                                </div> -->

                            </div>

                        </div>
                    </div>
                    <div class="info-contact border-0 py-2 total-price">
                        <div class="row">
                            <div class="col-12 d-flex justify-content-between">
                                <span>{{ __('frontsite.booking.total_price')}}</span>
                                <span class="table-price-right">{{ number_format($room->price_one_night*Request::get('amount_room'), 0, ',', '.') }} VND</span>
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-end al mb-5">
                        <div class="col-12 d-flex justify-content-end">
                            <button type="submit" class="btn bg-third text-white align-content-end">{{ __('frontsite.booking.comfirm_booking')}}</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-12 col-sm-4 info-room">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h5>{{ __('frontsite.booking.booking_info')}}</h5>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-12 col-sm-4">
                                <img class="img-fluid" src="images/booking/hotel.jpg">
                            </div>
                            <div class="col-12 col-sm-8">
                                <span>{{ $host->host_name }}</span>
                            </div>
                            <hr>
                            <div class="col-12 mt-2 ">
                                <table>
                                    <tbody>
                                        <tr>
                                            <td>{{ __('frontsite.booking.amount_night') }}</td>
                                            <td>{{ $diff->days }}</td>
                                        </tr>
                                        <tr>
                                            <td>{{ __('frontsite.booking.checkin_date') }}</td>
                                            <td>T2, 20 Th04 2020 </td>
                                        </tr>
                                        <tr>
                                            <td>{{ __('frontsite.booking.checkout_date') }}</td>
                                            <td>T5, 23 Th04 2020</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-12 mt-2 ">
                                <table>
                                    <tbody>
                                        <tr>
                                            <td>{{ __('frontsite.booking.room_type') }}</td>
                                            <td>{{ $room->room_name }}</td>
                                        </tr>
                                        <tr>
                                            <td>{{ __('frontsite.booking.bed_type') }}</td>
                                            <td>{{ $room->room_option->single_bed }} {{ __('frontsite.booking.single_bed') }}</td>

                                        </tr>
                                        <tr>
                                            <td>{{ __('frontsite.booking.bed_type') }}</td>
                                            <td>{{ $room->room_option->twin_beds }} {{ __('frontsite.booking.twin_bed') }}</td>
                                        </tr>
                                        <tr>
                                            <td>{{ __('frontsite.booking.amount_room') }}</td>
                                            <td>{{ !empty(Request::get('amount_room'))? Request::get('amount_room') : '1' }} {{ __('frontsite.booking.room_text') }}</td>
                                        </tr>
                                        <tr>
                                            <td>{{ __('frontsite.booking.guest_per_room') }}</td>
                                            <td>{{ !empty(Request::get('amount'))? Request::get('amount') : '1' }} {{ __('frontsite.booking.guest_text') }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')

@endsection