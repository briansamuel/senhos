@extends('frontsite.index')
@section('title', 'Cập nhập mật khẩu')
@section('style')

@endsection
@section('content')
<!-- START Block Banner  -->

@include('frontsite.elements.banner')
<!-- END Block Banner -->

<div class="wrap-content mt-5">
    <div class="container">
        <div class="row mb-5">
            <div class="col-md-8 login-form">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs">

                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane active" id="login">
                        <form role="form" class="form-horizontal">
                            <div class="form-group">
                                <label for="password" class="col-12 control-label">
                                    Mật khẩu</label>
                                <div class="col-12">
                                    <input type="hidden" name="token" id="token" value="{{ $token  }}"/>
                                    <input type="password" class="form-control" id="password" placeholder="Nhập mật khẩu của bạn">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="password" class="col-12 control-label">
                                    Nhập lại mật khẩu</label>
                                <div class="col-12">
                                    <input type="password" class="form-control" id="confirm_password" placeholder="Nhập lại mật khẩu của bạn">
                                </div>
                            </div>
                            <div class="form-group" style="display:none" id="show_error">
                                <div class="col-12">
                                    <span class="text text-danger" id="content_error"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-3 d-none d-sm-block">
                                </div>
                                <div class="col-12 col-sm-9">
                                    <button onclick="return resetPassword();" type="button" class="btn bg-third text-white mr-2" id="btn_reset_password">
                                        Cập nhập mật khẩu
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('script')
    <script type="text/javascript">
        $(document).ready(function(){
            $(document).keypress(function (e) {
                if (e.which == 13) {
                    resetPassword();
                }
            });
        });

        function resetPassword(){
            var password = $("#password").val();
            var confirm_password = $("#confirm_password").val();
            if(password === ""){
                $("#show_error").show();
                $("#content_error").text("Bạn chưa nhập mật khẩu !");
                $("#password").focus();
                return false;
            }
            if(confirm_password === ""){
                $("#show_error").show();
                $("#content_error").text("Bạn chưa nhập lại mật khẩu !");
                $("#confirm_password").focus();
                return false;
            }
            if(confirm_password !== password){
                $("#show_error").show();
                $("#content_error").text("Mật khẩu bạn nhập không trùng khớp!");
                $("#confirm_password").focus();
                return false;
            }
            //display notice
            $("#btn_reset_password").text("Đang xử lý....");
            $("#btn_reset_password").removeAttr("onclick");

            $.post('/reset_password', {
                password: password,
                token: $("#token").val(),
                _token: "{{csrf_token()}}"
            }, function(res) {
                if (res.success) {
                    alert('Bạn đã cập nhập mật khẩu thành công, vui lòng đăng nhập vào hệ thống để tiếp tục!');
                    window.location.href = '/dang-nhap';
                } else {
                    $("#btn_reset_password").text("Cập nhập mật khẩu");
                    $("#btn_reset_password").attr("onclick", "return resetPassword();");
                    $("#show_error").show();
                    $("#content_error").text(res.message);
                    return false;
                }
            }).fail(function() {
                $("#btn_reset_password").text("Cập nhập mật khẩu");
                $("#btn_reset_password").attr("onclick", "return resetPassword();");
                $("#show_error").hide();
                $("#content_error").text("");
                alert('Hệ thống gặp lỗi, vui lòng thử lại sau.');
                return false;
            });
        }
    </script>
@endsection