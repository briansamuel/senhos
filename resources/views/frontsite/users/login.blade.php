@extends('frontsite.index')
@section('title', 'Đăng Nhập')
@section('body_class', 'page-login')
@section('style')

@endsection
@section('content')
<!-- START Block Banner  -->

@include('frontsite.elements.banner')
<!-- END Block Banner -->

<div class="wrap-content mt-5">
    <div class="container">
        <div class="row mb-5">
            <div class="col-md-8 login-form">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs">
                    <li><a href="#login" class="active" data-toggle="tab">Đăng nhập</a></li>
                    <li><a href="#Registration" data-toggle="tab">Tạo tài khoản</a></li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane active" id="login">
                        <form role="form" class="form-horizontal">
                            <div class="form-group">
                                <label for="email" class="col-12 control-label">
                                    Email</label>
                                <div class="col-12">
                                    <input type="email" class="form-control" id="email" placeholder="Email">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="password" class="col-12 control-label">
                                    Mật khẩu</label>
                                <div class="col-12">
                                    <input type="password" class="form-control" id="password" placeholder="Nhập mật khẩu của bạn">
                                </div>
                            </div>
                            <div class="form-group" style="display:none" id="show_error">
                                <div class="col-12">
                                    <span class="text text-danger" id="content_error"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-3 d-none d-sm-block">
                                </div>
                                <div class="col-12 col-sm-9">
                                    <button onclick="return login();" type="button" class="btn bg-third text-white mr-2" id="btn_login">
                                        Đăng nhập
                                    </button>
                                    <a href="javascript:;" data-toggle="modal" data-target="#forgot_password">Quên mật khẩu?</a>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="tab-pane" id="Registration">
                        <form role="form" class="form-horizontal">
                            <div class="form-group">
                                <label for="email" class="col-sm-3 control-label">
                                    Họ tên
                                </label>
                                <div class="col-12">
                                    <div class="row">
                                        <div class="col-12 mb-2 mb-sm-0 col-md-3">
                                            <select class="form-control" id="register_gender">
                                                <option value="male">Ông </option>
                                                <option value="female">Bà </option>
                                            </select>
                                        </div>
                                        <div class="col-md-9">
                                            <input type="text" class="form-control" id="register_name">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="email" class="col-sm-3 control-label">
                                    Email</label>
                                <div class="col-12">
                                    <input type="email" class="form-control" id="register_email">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="mobile" class="col-sm-3 control-label">
                                    Số điện thoại</label>
                                <div class="col-12">
                                    <input type="email" class="form-control" id="register_phone_number">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="password" class="col-sm-3 control-label">
                                    Mật khẩu</label>
                                <div class="col-12">
                                    <input type="password" class="form-control" id="register_password">
                                </div>
                            </div>
                            <div class="form-group" style="display:none" id="show_success_register">
                                <div class="col-12">
                                    <span class="text text-success" id="content_success_register"></span>
                                </div>
                            </div>
                            <div class="form-group" style="display:none" id="show_error_register">
                                <div class="col-12">
                                    <span class="text text-danger" id="content_error_register"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-3 d-none d-sm-block-12">
                                </div>
                                <div class="col-12 col-sm-9">
                                    <button type="button" class="btn btn-primary " id="btn-submit-register" onclick="register()">
                                        Tạo tài khoản</button>
                                    <button type="button" class="btn btn-default ">
                                        Hủy</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div id="OR" class="hidden-xs">
                    OR</div>
            </div>
            <div class="col-md-4">
                <div class="row text-center sign-with">
                    <div class="col-12">
                        <h3>
                            Đăng nhập với</h3>
                    </div>
                    <div class="col-12">
                        <a href="{{ url('/auth/redirect/facebook') }}" class="btn bg-third text-white login-social"><i class="fab fa-facebook-f"></i> Facebook</a>
                        <a href="{{ url('/auth/redirect/google') }}" class="btn btn-danger login-social"><i class="fab fa-google"></i> Google</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="forgot_password" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Quên mật khẩu?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <input type="email" class="form-control" placeholder="Email" id="forgot_email" value=""/>
                <div style="display:none; margin-top: 15px" id="show_error_forgot">
                        <span class="text text-danger" id="content_error_forgot"></span>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                <button type="button" id="btn-forgot-password" class="btn btn-primary" onclick="forgotPassword()">Lấy mật khẩu</button>
            </div>
        </div>
    </div>
</div>


@endsection
@section('script')
    <script type="text/javascript">

        function CheckLogin(){
            var email = $("#email").val();
            var password = $("#password").val();
            if(email === ""){
                $("#show_error").show();
                $("#content_error").text("Bạn chưa nhập Email !");
                $("#email").focus();
                return false;
            }
            if(!validateEmail(email)){
                $("#show_error").show();
                $("#content_error").text("Email không đúng định dạng !");
                $("#email").focus();
                return false;
            }
            if(password === ""){
                $("#show_error").show();
                $("#content_error").text("Bạn chưa nhập mật khẩu !");
                $("#password").focus();
                return false;
            }

            return true;
        }

        function CheckRegister(){
            var full_name = $("#register_name").val();
            var email = $("#register_email").val();
            var gender = $("#register_gender").val();
            var guest_phone = $("#register_phone_number").val();
            var password = $("#register_password").val();
            if(full_name === ""){
                $("#show_error_register").show();
                $("#content_error_register").text("Bạn chưa nhập họ tên !");
                $("#register_name").focus();
                return false;
            }
            if(gender === ""){
                $("#show_error_register").show();
                $("#content_error_register").text("Bạn chưa chọn giới tính !");
                $("#register_gender").focus();
                return false;
            }
            if(guest_phone === ""){
                $("#show_error_register").show();
                $("#content_error_register").text("Bạn chưa nhập số điện thoại !");
                $("#register_phone_number").focus();
                return false;
            }
            if(email === ""){
                $("#show_error_register").show();
                $("#content_error_register").text("Bạn chưa nhập Email !");
                $("#register_email").focus();
                return false;
            }
            if(!validateEmail(email)){
                $("#show_error_register").show();
                $("#content_error_register").text("Email không đúng định dạng !");
                $("#register_email").focus();
                return false;
            }
            if(password === ""){
                $("#show_error_register").show();
                $("#content_error_register").text("Bạn chưa nhập mật khẩu !");
                $("#register_password").focus();
                return false;
            }

            return true;
        }

        function login(){
            if(CheckLogin()){
                var email = $("#email").val();
                var password = $("#password").val();
                //display notice
                $("#btn-login").text("Đang xử lý....");
                $("#btn-login").removeAttr("onclick");

                $.post('/dang-nhap', {
                    email: email,
                    password: password,
                    _token: "{{csrf_token()}}"
                }, function(res) {
                    if (res.status) {
                        window.location.href = res.url;
                    } else {
                        $("#btn-submit-login").text("Đăng nhập");
                        $("#btn-submit-login").attr("onclick", "return login();");
                        $("#show_error").show();
                        $("#content_error").text(res.msg);
                        return false;
                    }
                }).fail(function() {
                    $("#btn-submit-login").text("Đăng nhập");
                    $("#btn-submit-login").attr("onclick", "return login();");
                    $("#show_error").hide();
                    $("#content_error").text("");
                    alert('Hệ thống gặp lỗi, vui lòng thử lại sau.');
                    return false;
                });
            }
        }

        function register(){
            if(CheckRegister()){
                //display notice
                $("#btn-login").text("Đang xử lý....");
                $("#btn-login").removeAttr("onclick");

                $.ajax({
                    url: "/dang-ky",
                    type: "POST",
                    dataType: "json",
                    data: {
                        full_name: $("#register_name").val(),
                        email: $("#register_email").val(),
                        password: $("#register_password").val(),
                        guest_phone: $("#register_phone_number").val(),
                        gender: $("#register_gender").val(),
                        _token: "{{csrf_token()}}"
                    },
                    beforeSend: function () {
                        $("#show_success_register").hide();
                        $("#show_error_register").hide();
                        $("#btn-submit-register").text("Đang Xử Lý.....");
                        $("#btn-submit-register").removeAttr("onclick");
                    },
                    success: function (res) {
                        $("#btn-submit-register").text("Đăng ký");
                        $("#btn-submit-register").attr("onclick", "return register();");
                        if (res.status) {
                            $("#show_success_register").show();
                            $("#content_success_register").text(res.msg);
                        } else {
                            $("#show_error_register").show();
                            $("#content_error_register").text(res.msg);
                        }
                        return false;
                    }
                }).fail(function() {
                    $("#btn-submit-register").text("Đăng ký");
                    $("#btn-submit-register").attr("onclick", "return register();");
                    $("#show_error").hide();
                    $("#content_error").text("");
                    alert('Hệ thống gặp lỗi, vui lòng thử lại sau.');
                    return false;
                });
            }
        }

        function forgotPassword() {
            let forgot_email = $("#forgot_email").val();
            if(forgot_email === ""){
                $("#show_error_forgot").show();
                $("#content_error_forgot").text("Bạn chưa nhập email !");
                $("#forgot_email").focus();
                return false;
            }
            //display notice
            $("#btn-forgot-password").text("Đang xử lý....");
            $("#btn-forgot-password").removeAttr("onclick");

            $.post('/quen-mat-khau', {
                forgot_email: forgot_email,
                _token: "{{csrf_token()}}"
            }, function(res) {
                $("#btn-forgot-password").attr("onclick", "return forgotPassword();");
                $("#btn-forgot-password").text("Quên mật khẩu");
                if (res.success) {
                    alert('Đường dẫn thay đổi mật khẩu đã được gửi đến email của bạn!');
                } else {
                    $("#show_error").show();
                    $("#content_error").text(res.msg);
                    return false;
                }
            }).fail(function() {
                $("#btn-forgot-password").text("Quên mật khẩu");
                $("#btn-forgot-password").attr("onclick", "return forgotPassword();");
                $("#show_error").hide();
                $("#content_error").text("");
                alert('Hệ thống gặp lỗi, vui lòng thử lại sau.');
                return false;
            });
        }

    </script>
@endsection