@extends('frontsite.index')
@section('title', $page->page_title )
@section('title_ab', $page->page_seo_title )
@section('keyword', $page->page_seo_keyword )
@section('description', $page->page_seo_description)
@section('style')

@endsection
@section('content')
<!-- START Block Banner  -->

@include('frontsite.elements.banner')
<!-- END Block Banner -->
<!-- START BREADCRUMB -->
@include('frontsite.elements.breadcrumb')
<!-- END BREADCRUMB -->
<div class="main-wrapper">

    <!-- START BLOCK PROJECT -->
    <section class="" id="block-intro">
        <div class="container">
            <div class="row mt-4 mb-3 justify-content-center">
                <div class="col-12">
                    <h3 class="text-center title-page" data-animation="slideInDown">TƯ VẤN, SETUP, QUẢN LÝ VÀ VẬN
                        HÀNH HOTEL & RESORT</h3>
                    <p class="text-center" data-animation="slideInUp">SEN HOSPITALITY xem xét mức độ đầu tư của dự
                        án mới hay tình hình kinh doanh của khách sạn, resort đã vào hoạt động chưa hiệu quả, sẽ đưa
                        ra phương án phù hợp cho các hình thức tư vấn quản lý, định hướng phát triển ngắn hạn, dài
                        hạn, các yếu tốt cạnh tranh, giúp chủ đầu tư xây dựng thương hiệu và kinh doanh lợi nhuận
                        bền vững lâu dài.</p>
                </div>
            </div>

        </div>
    </section>
    <!-- END BLOCK PROJECT -->

    <!-- START BLOCK FUNCTION -->
    <section class="mb-3 block-service" id="block-function">
        <div class="container">
            <div class="row pt-5 pb-5">
                <div class="col-6 d-sm-none d-none d-lg-block">

                </div>
                <div class="col-12 col-lg-6" data-animation="slideInLeft">
                    <p class="text-light m-0 text-justify">
                        <span class="sub-title d-inline-block w-100">TƯ VẤN VỀ THIẾT KẾ CÔNG NĂNG CÔNG TRÌNH</span>
                        Công ty SENHOS sẽ tham gia tư vấn ngay từ giai đoạn khởi đầu của dự án, giúp gia tăng hiệu
                        quả khi vận hành cơ sở kinh doanh, tiết kiệm được nhiều thời gian và chi phí cho chủ đầu tư.
                        Chúng tôi:
                        <br>- Tập hợp những kiến trúc sư, những chuyên gia hàng đầu trong lĩnh vực thiết kế cảnh
                        quan, thiết kế bếp, tư vấn thực phẩm và đồ uống, tư vấn công năng sử dụng của khách sạn.
                        <br>- Phối hợp với các chuyên gia thiết kế, các chuyên gia tư vấn và nhà cung cấp để hỗ trợ
                        quá trình thiết kế và lập kế hoạch đầu tư trang thiết bị cho dự án.
                        <br>- Hướng dẫn nhóm phát triển, các nhà tư vấn cũng như đối tác bên thứ ba cho đến khi bàn
                        giao sản phẩm hoàn thiện.
                    </p>
                </div>
            </div>

        </div>
    </section>
    <!-- END BLOCK FUNCTION -->

    <!-- START BLOCK STRACTEGY-->
    <section class="mb-3 block-service" id="block-strategy">
        <div class="container">
            <div class="row pt-2 pt-sm-5 pb-2 pb-sm-5">

                <div class="col-12 col-sm-6" data-animation="slideInLeft">
                    <p class="text-dark m-0 text-justify block-service-content">
                        <span class="sub-title d-inline-block w-100">TƯ VẤN CHIẾN LƯỢC KINH DOANH</span>


                        <span class="text-primary">Tư vấn chiến lược: Khảo sát, đánh giá thực trạng</span>
                        <br>- Phân tích nhu cầu thị trường và thị trường tiềm năng.
                        <br>- Phân tích SWOT.
                        <br>- Tư vấn cơ cấu dịch vụ phù hợp với dự án đang triển khai và phát triển trong tương lai.
                        <br>- Phân tích, đánh giá hoạt động bán hàng và hoạt động marketing.
                        <br>- Tư vấn định hướng phát triển khách hàng.
                        <br>- Phân tích năng lực cạnh tranh.
                        <br>- Phân tích tài chính.
                        <br>- Tư vấn chiến lược phát triển kinh doanh.
                        <br>- Phân tích tầm nhìn và mục tiêu đã có của doanh nghiệp.
                        <br>- Phân tích tình hình tác động của các yếu tố.
                        <br><span class="text-primary">Đưa ra chiến lược:</span>
                        <br>- Dự báo và định hướng về thị trường mục tiêu để phát triển trong ngắn hạn, dài hạn.
                        <br>- Đưa ra chỉ tiêu kinh doanh từ 1 đến 5 năm.
                        <br>- Đưa ra các chiến lược ưu tiên dựa vào các ngành kinh doanh chủ yếu.
                        <br>- Đưa ra chiến lược về tài chính, đầu tư.
                        <br>- Đưa ra chiến lược về phát triển thương hiệu.
                    </p>
                </div>
                <div class="col-12 col-sm-6 pt-2 pb-2 pt-sm-4 pb-sm-3">
                    <img class="img-fluid mt-0 mt-sm-4" src="images/resort/chien-luoc.jpg" alt="Card image cap">
                </div>
            </div>

        </div>
    </section>
    <!-- END BLOCK STRACTEGY -->

    <!-- START BLOCK MANAGEMENT -->
    <section class="mb-3 block-service" id="block-management">
        <div class="container">
            <div class="row py-2 py-sm-5">
                <div class="col-6 d-sm-none d-none d-lg-block">

                </div>
                <div class="col-12 col-sm-6 py-0 py-sm-5">
                    <p class="text-light m-0 text-justify">
                        <span class="sub-title d-inline-block w-100">TƯ VẤN SET UP VÀ TƯ VẤN QUẢN LÝ KHÁCH
                            SẠN</span>

                        <br><span class="text-primary">- Thời gian thực hiện:</span> Thông thường từ sáu tháng đến
                        một năm (tối thiểu là ba tháng), tùy theo quy mô từng loại hình kinh doanh của dự án.
                        <br><span class="text-primary">- Nội dung Set up:</span> Toàn bộ công việc khởi tạo để nhà
                        hàng có thể đi vào hoạt động.
                        <br><span class="text-primary">- Phương pháp thực hiện:</span> Đề xuất các phương án, giải
                        pháp để chủ đầu tư chọn lựa và quyết định.
                        <br><span class="text-primary">- Chi phí:</span> Tùy thuộc vào thỏa thuận của từng dự án.
                    </p>
                </div>
            </div>

        </div>
    </section>
    <!-- END BLOCK MANAGEMENT -->

    <!-- START BLOCK Operating-->
    <section class="block-service" id="block-operating">
        <div class="container">
            <div class="row pt-3">

                <div class="col-12 col-sm-6 p-3 p-sm-0">
                    <p class="text-dark m-0 text-justify block-service-content">
                        <span class="sub-title d-inline-block w-100">QUẢN LÝ ĐIỀU HÀNH SAU KHAI TRƯƠNG</span>

                        <span class="text-primary">Phương pháp quản lý, điều hành:</span> Lấy lợi ích của nhà đầu tư
                        làm mục tiêu dài hạn; được áp dụng đối với việc hình thành thiết kế, dịch vụ kĩ thuật trước
                        khi khai trương và sửa đổi cho đến khi hoàn thiện trên thực tế.
                        <br><span class="text-primary">Thời hạn hợp đồng quản lý:</span> Từ 1 năm trở lên và tự động
                        gia hạn tùy vào thỏa thuận của hai bên.
                        <br><span class="text-primary">>Dịch vụ quản lý:</span> Cty SENHOS cung cấp dịch vụ quản lý
                        trọn gói hoặc từng phần. Nhà đầu tư có thể linh hoạt lựa chọn các gói tư vấn, quản lý để phù
                        hợp với nhu cầu.
                        <br><span class="text-primary">Vận hành khách sạn, resort:</span> Thay mặt chủ đầu tư quản
                        lý, giám sát, chỉ đạo, kiểm soát các hoạt động hàng ngày của khu nghỉ dưỡng cũng như các
                        hoạt động liên quan theo ngân sách, tiêu chuẩn đã được phê duyệt, bao gồm thu hút và duy trì
                        lượng khách của khu nghỉ.
                        <br><span class="text-primary">Kiểm soát chất lượng:</span> Theo tiêu chuẩn nghiêm ngặt,
                        giúp duy trì hiệu quả của dự án.
                        <br><span class="text-primary">Kiểm soát và giám sát quá trình lập ngân sách:</span> Các
                        ngân sách được lập cùng với kế hoạch hoạt động chính là một phần trong kế hoạch kinh doanh
                        hàng năm được đệ trình thông qua.
                    </p>
                </div>
                <div class="col-12 col-sm-6 p-0">
                    <img class="img-fluid mt-5" src="images/resort/khai-truong.jpg" alt="Card image cap">
                </div>
            </div>

        </div>
    </section>
    <!-- END BLOCK Operating -->

    <!-- START BLOCK MARKETING -->
    <section class="block-service" id="block-marketing">
        <div class="container">
            <div class="row py-4 py-sm-5">
                <div class="col-12 col-sm-6 d-sm-none d-none d-lg-block">

                </div>
                <div class="col-12 col-sm-6">
                    <p class="text-light m-0 text-justify block-service-content">
                        <span class="sub-title d-inline-block w-100">KINH DOANH VÀ TIẾP THỊ</span>

                        Cty SENHOS đã hình thành hệ thống kinh doanh và đại diện ở một số tỉnh thành lớn trên cả
                        nước, thành công trong việc kinh doanh và tiếp thị các khu nghỉ dưỡng và khách sạn. Chúng
                        tôi:

                        <br><span class="text-primary">01.</span> Trang bị hệ thống đặt phòng giữa doanh nghiệp với
                        doanh nghiệp B2B, doanh nghiệp với khách hàng B2C và hệ thống phân phối đặt phòng.
                        <br><span class="text-primary">02.</span> Cung cấp dịch vụ bán hàng, tham gia các hội chợ
                        xúc tiến thương mại nhằm nâng cao hiệu quả kinh doanh của dự án.
                        <br><span class="text-primary">03.</span> Tổ chức các hoạt động bán hàng và quan hệ công
                        chúng, các chương trình famtrip nhằm quảng bá khu nghỉ dưỡng; chủ động đăng cai các sự kiện
                        quan trọng của địa phương nhằm nâng cao tầm ảnh hưởng của thương hiệu.
                    </p>
                </div>

            </div>

        </div>
    </section>
    <!-- END BLOCK MARKETING -->
</div>
@endsection
@section('script')

@endsection