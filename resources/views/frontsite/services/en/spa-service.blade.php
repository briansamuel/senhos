@extends('frontsite.index')
@section('title', $page->page_title )
@section('title_ab', $page->page_seo_title )
@section('keyword', $page->page_seo_keyword )
@section('description', $page->page_seo_description)
@section('style')

@endsection
@section('content')
<!-- START Block Banner  -->

@include('frontsite.elements.banner')
<!-- END Block Banner -->
<!-- START BREADCRUMB -->
@include('frontsite.elements.breadcrumb')
<!-- END BREADCRUMB -->
<div class="main-wrapper">


    <!-- START BLOCK PROJECT -->
    <section class="" id="block-intro">
        <div class="container">
            <div class="row mt-4 mb-3 justify-content-center">
                <div class="col-12">


                    <h3 class="text-center title-page">TƯ VẤN, SETUP, QUẢN LÝ VÀ VẬN HÀNH SPA - MASSAGE</h3>
                    <p class="text-center">SENHOS chuyên tư vấn setup vận hành Spa, Massage; cung cấp kiến thức về các
                        loại
                        hình spa phổ biến trên thế giới và tại Việt Nam hiện nay đến với chủ đầu tư. Chúng tôi giúp chủ
                        đầu
                        tư lựa chọn loại hình spa, massage phù hợp với mô hình hoạt động và xây dựng các bước setup
                        thích
                        hợp.</p>
                </div>
            </div>

        </div>
    </section>
    <!-- END BLOCK PROJECT -->

    <!-- START BLOCK INVEST -->
    <section class="mb-3 block-service overlay-bg-container" id="block-invest-spa">
        <div class="container">
            <div class="row pt-5 pb-5">
                <div class="col-6 d-sm-none d-none d-lg-block">

                </div>
                <div class="col-12 col-lg-6" data-animation="slideInRight">
                    <p class="text-light m-0 text-justify">
                        <span class="sub-title d-inline-block w-100">LẬP DỰ TOÁN ĐẦU TƯ SPA - MASSAGE</span>



                        <br>Sau khi có bản phác thảo thiết kế, chúng tôi sẽ cung cấp đến chủ đầu tư bản Dự toán đầu tư
                        và kinh doanh cho việc xây dựng spa:
                        <br>- Bảng phân tích thị trường spa, massage tại địa phương
                        <br>- Tổng mức đầu tư
                        <br>- Chi phí
                        <br>- Dự kiến doanh thu


                    </p>
                </div>
            </div>

        </div>
    </section>
    <!-- END BLOCK INVEST -->

    <!-- START BLOCK SETUP SPA -->
    <section class="mb-3 block-service " id="block-setup-spa">
        <div class="container">
            <div class="row py-2 py-sm-5">

                <div class="col-12 col-sm-6 pt-3" data-animation="slideInLeft">
                    <p class="text-dark m-0 text-justify block-service-content">
                        <span class="sub-title d-inline-block w-100">TƯ VẤN THIẾT KẾ - SETUP SPA - MASSAGE</span>



                        <br>- Tư vấn thiết kế spa hợp phong thủy, sáng tạo, công năng hợp lý.
                        <br>- Tư vấn công năng; lựa chọn trang thiết bị, dịch vụ phù hợp với quy định của ngành nghề và
                        hướng dẫn của ngành Y tế liên quan đến spa, massage trị liệu. Công năng tốt giúp tiết kiệm chi
                        phí đầu tư, đồng thời nâng cao hiệu quả sử dụng.
                        <br>
                        <br><span class="text-primary ">Chúng tôi sẽ kết hợp với kiến trúc sư để thực hiện, nhằm bảo đảm
                            rằng chủ đầu tư sở hữu 1 spa đúng nghĩa về sự sang trọng, tiết kiệm chi phí và đạt được 10
                            tiêu chuẩn do Hiệp hội spa Thế giới đưa ra về: Không gian, màu sắc, mùi hương, vệ sinh, âm
                            nhạc, tiếng động, sản phẩm, máy móc, phương tiện vật tư, kỹ thuật và dịch vụ.</span>

                    </p>
                </div>
                <div class="col-12 col-sm-6 mt-3 mt-sm-0" data-animation="slideInRight">
                    <img class="img-fluid" src="images/spa/lbg-1.jpg" alt="Card image cap">
                </div>
            </div>

        </div>
    </section>
    <!-- END BLOCK SETUP SPA -->

    <!-- START BLOCK PARADIGM -->
    <section class="mb-3 block-service overlay-bg-container" id="block-paradigm">
        <div class="container">
            <div class="row py-3 py-sm-5">
                <div class="col-7 d-none d-sm-block">

                </div>
                <div class="col-12 col-sm-5 pt-5 pb-5" data-animation="slideInRight">
                    <p class="text-light m-0 text-justify">
                        <span class="sub-title d-inline-block w-100"> TƯ VẤN MÔ HÌNH QUẢN TRỊ SPA - MASSAGE</span>


                        <br>- Tư vấn mô hình quản trị spa hiệu quả, như: phần mềm quản lý spa -massage; quản trị nhân
                        sự; quản lý doanh số, sản phẩm, khách hàng…
                        <br>
                        <br>- Tư vấn các tiêu chuẩn và chính sách liên quan đến nhân viên spa - massage.

                    </p>
                </div>
            </div>

        </div>
    </section>
    <!-- END BLOCK MANAGEMENT -->

    <!-- START BLOCK Operating-->
    <section class="block-service" id="block-cosmetics">
        <div class="container">
            <div class="row my-2 my-sm-5">

                <div class="col-12 col-sm-6 " data-animation="slideInLeft">
                    <p class="text-dark m-0 text-justify block-service-content">
                        <span class="sub-title d-inline-block w-100">TƯ VẤN MỸ PHẨM - HƯƠNG LIỆU</span>


                        <br>- Tùy vào mô hình spa - massage mà chủ đầu tư chọn lựa, chúng tôi sẽ tư vấn cho chủ đầu tư
                        những dòng sản phẩm tốt nhất từ điều trị đến chăm sóc thư giãn.
                        <br>- Tư vấn hoặc trực tiếp soạn thảo cho chủ đầu tư bộ tài liệu quản trị nhân sự spa cao cấp:
                        <br>+ Quy trình phục vụ khách, thu ngân, các loại bảng biểu trong spa.
                        <br>+ Bản mô tả công việc, nội quy, quy trình phân tour xếp lịch cho kĩ thuật viên và khách
                        hàng.

                    </p>
                    <img class="img-fluid mt-4" src="images/spa/spa-01.jpg" alt="Card image cap">
                </div>
                <div class="col-12 col-sm-6 py-2 py-0" data-animation="slideInRight">
                    <p class="text-dark m-0 text-justify block-service-content">
                        <span class="sub-title d-inline-block w-100">TƯ VẤN DANH MỤC MENU</span>

                        <br>- Tư vấn về danh mục menu cho Spa - massage
                        <br>- Tư vấn về những vấn đề liên quan đến lập menu và những loại dịch vụ trong spa - massage
                        <br>- Tư vấn thiết lập và xây dựng hệ thống nhận dạng thương hiệu cho Spa - massage
                        <br>- Tư vấn hoặc thiết kế cho chủ đầu tư bộ nhận dạng thương hiệu: Logo, menu, brochure,
                        leaflet, coupon, gift voucher, …

                    </p>
                    <img class="img-fluid mt-5" src="images/spa/spa-02.jpg" alt="Card image cap">
                </div>
            </div>

        </div>
    </section>
    <!-- END BLOCK Operating -->

    <!-- START BLOCK MARKETING -->
    <section class="block-service overlay-bg-container" id="block-train-educate">
        <div class="container">
            <div class="row">
                <div class="col-7 d-sm-none d-none d-lg-block">

                </div>
                <div class="col-12 col-sm-5 py-sm-5" data-animation="fadeInLeft">
                    <p class="text-light m-0 text-justify block-service-content pt-5 pb-5">
                        <span class="sub-title d-inline-block w-100">HUẤN LUYỆN VÀ ĐÀO TẠO </span>

                        <br>- Huấn luyện, đào tạo chuyên môn nghiệp vụ cho từng vị trí trong spa. Các kỹ thuật viên được
                        huấn luyện bài bản, tay nghề được nâng cao và tiêu chuẩn phục vụ khách hàng được thống nhất,
                        đồng đều.
                        <br>- Huấn luyện, đào tạo tác phong phục vụ chuyên nghiệp cho nhân viên.
                    </p>
                </div>

            </div>

        </div>
    </section>
    <!-- END BLOCK MARKETING -->

    <!-- START BLOCK PLAN MARKETING -->
    <section class="mb-3 block-service" id="block-setup-spa">
        <div class="container">
            <div class="row py-2 py-sm-5">

                <div class="col-12 col-sm-6 py-2 py-sm-5" data-animation="fadeInRight">
                    <p class="text-dark mt-4 text-justify block-service-content">
                        <span class="sub-title d-inline-block w-100">TƯ VẤN KẾ HOẠCH KINH DOANH VÀ TIẾP THỊ</span>



                        <br>Tư vấn, lập kế hoạch các chương trình sale & marketing hiệu quả với chi phí thấp.
                        <br><span class="text-primary font-weight-bold">SENHOS chuyên tư vấn, setup, vận hành Spa –
                            Massage đạt chuẩn với chiến lược marketing chiếm lĩnh thị trường hiệu quả nhất, chi phí thấp
                            nhất, cùng chuyên gia chuyên lĩnh vực về các loại hình spa phổ biến trên thế giới và tại
                            Việt Nam hiện nay.</span>


                    </p>
                </div>
                <div class="col-12 col-sm-6" data-animation="fadeInLeft">
                    <img class="img-fluid" src="images/spa/spa-04.jpg" alt="Card image cap">
                </div>
            </div>

        </div>
    </section>
    <!-- END BLOCK PLAN MARKETING -->

    <!-- START BLOCK DEVELOPE -->
    <section class="block-service" id="block-develope">
        <div class="container">
            <div class="row py-2">
                <div class="col-12 col-sm-6" data-animation="fadeInDown">
                    <img class="img-responsive mt-3 mb-3" src="images/spa/spa-05.jpg" alt="Card image cap">
                </div>
                <div class="col-12 col-sm-6 py-2 py-sm-5" data-animation="fadeIn">
                    <p class="text-light mt-3 mb-3 text-justify block-service-content">
                        <span class="sub-title d-inline-block w-100">TƯ VẤN XÂY DỰNG - ĐIỀU HÀNH VÀ PHÁT TRIỂN</span>



                        <br>- Tư vấn cho chủ đầu tư hoặc trực tiếp thực hiện cho chủ đầu tư lắp đặt máy móc, trang thiết
                        bị, vật dụng trang trí tạo không gian spa đúng tiêu chuẩn.
                        <br>- Hướng dẫn nhân viên vận hành máy móc, thiết bị chuyên ngành spa
                        <br>- Điều hành hoạt động spa và chuyển giao cho chủ đầu tư.
                        <br>
                        <br><span class="text-primary font-weight-bold">Một khách sạn hay khu nghỉ dưỡng cao cấp phải có
                            dịch vụ chăm sóc sức khỏe, sắc đẹp và Spa, nơi chuyên biệt để thư giản và thoải mái cho tinh
                            thần, kích thích giác quan, tái tạo nguồn năng lượng cho cơ thể.</span>




                    </p>
                </div>

            </div>

        </div>
    </section>
    <!-- END BLOCK DEVELOPE  -->
</div>
@endsection
@section('script')

@endsection