@extends('frontsite.index')
@section('title', $page->page_title )
@section('title_ab', $page->page_seo_title )
@section('keyword', $page->page_seo_keyword )
@section('description', $page->page_seo_description)
@section('style')

@endsection
@section('content')
<!-- START Block Banner  -->

@include('frontsite.elements.banner')
<!-- END Block Banner -->
<!-- START BREADCRUMB -->
@include('frontsite.elements.breadcrumb')
<!-- END BREADCRUMB -->
<div class="main-wrapper">

    <!-- START BLOCK PROJECT -->
    <section class="" id="block-intro">
        <div class="container">
            <div class="row mt-4 mb-3 justify-content-center">
                <div class="col-12">
                    <h3 class="text-center title-page">CUNG ỨNG ĐÀO TẠO NGUỒN NHÂN LỰC</h3>
                    <p class="text-center">SENHOS chuyên cung cấp - đào tạo, huấn luyện chuyên môn nghiệp vụ về quản lý, vận hành nhà hàng, khách sạn, resort, spa, lounge, bar chất lượng cao; xây dựng đội ngũ nhân sự có phong cách phục vụ nhiệt tình và chuyên nghiệp.</p>

                </div>
            </div>

        </div>
    </section>
    <!-- END BLOCK PROJECT -->

    <!-- START BLOCK POLICY -->
    <section class="mb-3 block-service overlay-bg-container" id="block-human-policy">
        <div class="container">
            <div class="row py-2 py-sm-5">
                <div class="col-6 d-sm-none d-none d-lg-block">

                </div>
                <div class="col-12 col-lg-6">
                    <p class="text-light m-0 text-center pt-5 pb-5">
                        <span class="sub-title d-inline-block w-100">CHÍNH SÁCH TUYỂN DỤNG</span>

                        <br>“Tâm – Tài – Đức”
                        <br>là ba giá trị cốt lõi của chính sách tuyển dụng giúp
                        <br>SENHOS tuyển mộ, nuôi dưỡng nhân tài.

                    </p>
                </div>
            </div>

        </div>
    </section>
    <!-- END BLOCK POLICY -->

    <!-- START BLOCK POLICY TRAINING -->
    <section class="mb-3 block-service" id="block-human-policy-training">
        <div class="container">
            <div class="row py-2 py-sm-5">

                <div class="col-12 col-sm-6 ">
                    <p class="text-dark m-0 text-justify block-service-content">
                        <span class="sub-title d-inline-block w-100">CHÍNH SÁCH ĐÀO TẠO</span>


                        <span class="text-primary ">SENHOS luôn chú trọng công tác đào tạo nhằm nâng chất – nâng tầm cho CBNV, từ đó xây dựng niềm tin và sự gắn bó lâu dài giữa nhân viên và công ty. </span>
                        <br>
                        <br>Đội ngũ quản lý, chuyên gia trong nước và đến từ nước ngoài do SENHOS sẽ bố trí đến để huấn luyện đào tạo, tư vấn các giải pháp.
                        <br>Thời gian đào tạo theo quy chuẩn từ 1 - 3 tháng đối với nhân viên đã có nền tảng trước khi vào vận hành:
                        <br>- Đào tạo các công thức, định lượng, cách pha chế: café, đồ uống, làm bánh theo tiêu chuẩn của SENHOS.
                        <br>- Tiêu chuẩn phục vụ khách hàng hài lòng.
                        <br>- Thực hiện các qui trình phục vụ khách hàng.
                        <br>- Qui trình quản lý, kiểm sát về chi phí, doanh thu.
                        <br>- Qui trình quản lý vệ sinh an toàn thực phẩm.
                        <br>- Qui trình quản lý hàng hóa xuất, nhập, tồn.
                        <br>- Qui trình an ninh từ khâu giữ xe, đến quản lý an ninh nội bộ, quản lý tài sản, trang thiết bị.
                        <br>- Hệ thống biểu mẫu quản lý.
                        <br>- Tiêu chí đánh giá năng lực làm việc của nhân viên.

                    </p>
                </div>
                <div class="col-12 col-sm-6" data-animation="fadeInRight">
                    <img class="img-fluid mt-2 mt-sm-5 pt-0 pt-sm-4" src="images/human/human-01.jpg" alt="Card image cap">
                </div>
            </div>

        </div>
    </section>
    <!-- END BLOCK POLICY TRAINING -->

    <!-- START BLOCK HUMAN INTEGRATION TRAINING -->
    <section class="block-service" id="block-human-integration-training">
        <div class="container">
            <div class="row justify-content-between">
                <div class="col-12 col-sm-6" data-animation="fadeInLeft">
                    <img class="img-responsive mt-4 mb-4" src="images/human/human-02.jpg" alt="Card image cap">
                </div>

                <div class="col-12 col-sm-6 pl-3 pl-sm-5">
                    <p class="text-light text-justify block-service-content mt-0 mt-sm-5">
                        <span class="sub-title d-inline-block w-100 pb-2">CHƯƠNG TRÌNH ĐÀO TẠO HỘI NHẬP</span>
                        <br>Nhằm giúp cho nhân viên mới nhanh chóng hòa nhập với SENHOS thông qua những kiến thức về:
                        <br><span class="text-primary font-weight-bold">1. Văn hóa công ty</span>
                        <br>Giới thiệu tổng quan về lịch sử hình thành, văn hóa doanh nghiệp, các quy trình tác nghiệp.
                        <br><span class="text-primary font-weight-bold">2. Chia sẻ của Ban lãnh đạo</span>
                        <br>Ban lãnh đạo chia sẻ về quan điểm quản trị điều hành, định hướng và chiến lược phát triển của Công ty trong những năm tiếp theo.
                        <br><span class="text-primary font-weight-bold">3. Chính sách & quyền lợi</span>
                        <br>Các chính sách và quyền lợi mà Công ty dành cho cán bộ nhân viên (CBNV)…


                    </p>
                </div>

            </div>

        </div>
    </section>
    <!-- END BLOCK HUMAN INTEGRATION TRAINING  -->




    <!-- START BLOCK HUMAN PROGRAM DEVELOPE SKILL -->
    <section class="mb-3 block-service" id="block-human-program-devlope">
        <div class="container">
            <div class="row py-2 py-sm-5">

                <div class="col-12 col-sm-6 ">
                    <p class="text-dark mb-0 text-justify block-service-content pr-3 pt-4 mt-1">
                        <span class="sub-title d-inline-block w-100">CHƯƠNG TRÌNH PHÁT TRIỂN KỸ NĂNG</span>

                        <br>Đào tạo, hỗ trợ nhân viên trong việc phát triển kỹ năng và nâng cao trình độ.
                        <br>Kỹ năng làm việc
                        <br>Kỹ năng lập kế hoạch; Kỹ năng sử dụng hệ thống công nghệ thông tin; Nhận thức và thực hành công tác 5S, Chương trình nâng cao trình độ CBNV
                        <br>Hỗ trợ nhân viên được tiếp tục theo học để nâng cao trình độ ở các cấp bậc đại học và sau đại học.
                        <br>Chương trình liên kết
                        <br>Liên kết với các trung tâm đào tạo uy tín nhằm đào tạo cho cấp quản lý và lãnh đạo của công ty.

                    </p>
                </div>
                <div class="col-12 col-sm-5" data-animation="fadeInLeft">
                    <img class="img-fluid mt-3 mt-sm-0" src="images/human/human-03.jpg" alt="Card image cap">
                </div>
            </div>

        </div>
    </section>
    <!-- END BLOCK HUMAN PROGRAM DEVELOPE SKILL -->

    <!-- START BLOCK HUMAN TREATMENT -->
    <section class="block-service" id="block-human-treatment">
        <div class="container">
            <div class="row py-2">

                <div class="col-12 py-2 py-sm-5">
                    <p class="text-light mt-3 mb-3 text-center block-service-content" data-animation="fadeInDown">
                        <span class="sub-title d-inline-block w-100">CHÍNH SÁCH ĐÃI NGỘ</span>


                        <br>1. CHẾ ĐỘ LƯƠNG
                        <br>Quan điểm trong chiến lược nhân sự của công ty là: tuyển dụng và bổ nhiệm đúng năng lực, đúng vị trí; trả công xứng đáng với năng lực làm việc và thành tích đóng góp. CBNV sẽ được hưởng mức thu nhập tương xứng với năng lực làm việc, sự nỗ lực và thành tích đóng góp.
                        <br>2. CHẾ ĐỘ PHÚC LỢI
                        <br>Luôn được chú trọng nhằm mang đến cho CBNV môi trường làm việc thoải mái, đoàn kết: Du lịch, Đời sống nhân viên, bảo hiểm – Sức khỏe
                        <br>
                        <br data-animation="fadeInDown" data-delay="2000"><span class="text-primary font-weight-bold">Chúng tôi dựa trên nhu cầu của khách hàng tuyển chọn chặt chẽ để đáp ứng kịp thời yêu cầu của mọi quý khách hàng về chất lượng và số lượng nguồn nhân lực đồng thời cũng sẽ kiểm soát được tiến độ tuyển dụng cũng như đào tạo hợp lý nhất.</span>

                    </p>
                </div>

            </div>

        </div>
    </section>
    <!-- END BLOCK HUMAN TREATMENT  -->
</div>
@endsection
@section('script')

@endsection