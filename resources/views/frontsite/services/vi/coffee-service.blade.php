@extends('frontsite.index')
@section('title', $page->page_title )
@section('title_ab', $page->page_seo_title )
@section('keyword', $page->page_seo_keyword )
@section('description', $page->page_seo_description)
@section('style')

@endsection
@section('content')
<!-- START Block Banner  -->

@include('frontsite.elements.banner')
<!-- END Block Banner -->
<!-- START BREADCRUMB -->
@include('frontsite.elements.breadcrumb')
<!-- END BREADCRUMB -->
<div class="main-wrapper">

    <!-- START BLOCK PROJECT -->
    <section class="" id="block-intro">
        <div class="container">
            <div class="row mt-4 mb-3 justify-content-center">
                <div class="col-12">
                    <h3 class="text-center title-page text-uppercase" data-animation="slideInLeft">{{ $page->page_title }}</h3>
                    <p class="text-center" data-animation="slideInRight">{{ html_entity_decode(strip_tags($page->page_description)) }}</p>
                </div>
            </div>

        </div>
    </section>
    <!-- END BLOCK PROJECT -->

    <!-- START BLOCK SURVEY PRODUCT -->
    <section class="mb-3 block-service overlay-bg-container" id="block-survey-product">
        <div class="container">
            <div class="row py-2 py-sm-5">
                <div class="col-6 d-none d-lg-block">

                </div>
                <div class="col-12 col-lg-6 py-2 py-sm-5" data-animation="fadeInDown">
                    <p class="text-light m-0 text-justify block-service-content pt-5 pb-5">
                        <span class="sub-title d-inline-block w-100">KHẢO SÁT THỊ TRƯỜNG, ĐỊNH HƯỚNG SẢN PHẨM NHÀ HÀNG - CAFE</span>



                        <br>Nghiên cứu, khảo sát vị trí, đánh giá các nhóm đối tượng khách hàng tiềm năng tại khu vực kinh doanh, hoạch định đưa ra các phương án sản phẩm phù hợp.





                    </p>
                </div>
            </div>

        </div>
    </section>
    <!-- END BLOCK SURVEY PRODUCT -->

    <!-- START BLOCK ORIENTATION -->
    <section class="mb-3 block-service" id="block-orientation">
        <div class="container">
            <div class="row py-2 py-sm-5 py-lg-3">

                <div class="col-12 col-sm-6 py-2 py-sm-5 py-lg-3">
                    <p class="text-dark m-0 text-justify block-service-content py-2 py-sm-5 py-lg-3">
                        <span class="sub-title d-inline-block w-100"> ĐỊNH HƯỚNG CHIẾN LƯỢC KINH DOANH NHÀ HÀNG - CAFE </span>



                        <br>Định hướng quy mô phát triển kinh doanh và khả năng tổ chức hoạt động kinh doanh của nhà hàng dựa trên số vốn ban đầu, xác định được nguồn khách hàng mục tiêu để tập trung thực hiện.

                    </p>
                </div>
                <div class="col-12 col-sm-6">
                    <img class="img-fluid" src="images/coffee/coffe-01.jpg" alt="Card image cap">
                </div>
            </div>

        </div>
    </section>
    <!-- END BLOCK ORIENTATION -->

    <!-- START BLOCK determine the plan -->
    <section class="mb-3 block-service overlay-bg-container" id="block-determine-plan">
        <div class="container">
            <div class="row py-sm-2 py-sm-5">
                <div class="col-6 d-sm-none d-none d-lg-block">

                </div>
                <div class="col-12 col-lg-6 py-2 py-sm-4">
                    <p class="text-light m-0 text-justify py-2 py-sm-5">
                        <span class="sub-title d-inline-block w-100"> XÁC ĐỊNH PHƯƠNG ÁN KINH DOANH</span>



                        <br>Tại bản kế hoạch hướng dẫn setup nhà hàng, chủ đầu tư sẽ nắm bắt được nhiệm vụ, mục tiêu ngắn và dài hạn; nhận thấy điểm mạnh và điểm yếu của nhà hàng cùng những cơ hội và thách thức trong quá trình khởi tạo kinh doanh. Từ đó, xác định được phương án kinh doanh, cạnh tranh.


                    </p>
                </div>
            </div>

        </div>
    </section>
    <!-- END BLOCK determine the plan -->

    <!-- START BLOCK advisory -->
    <section class="block-service" id="block-advisory">
        <div class="container">
            <div class="row my-2 my-sm-5">

                <div class="col-12 col-sm-6 ">
                    <p class="text-dark m-0 text-justify block-service-content">
                        <span class="sub-title d-inline-block w-100">TƯ VẤN THIẾT KẾ CÔNG TRÌNH, SETUP ĐỊNH HƯỚNG PHONG CÁCH NHÀ HÀNG, CAFE</span>



                        <br>Đội ngũ tư vấn nhà hàng tiến hành thiết kế, xây dựng công trình, Setup định hướng phong cách của thương hiệu, thi công hoàn thiện và đưa nhà hàng bước đầu đi vào hoạt động.
                        <br>Các bản báo cáo chi phí và doanh thu sẽ liên tục được cập nhật.

                    </p>
                    <img class="img-fluid mt-2 mt-sm-4" src="images/coffee/coffe-02.jpg" alt="Card image cap">
                </div>
                <div class="col-12 col-sm-6 pt-2 pt-sm-0">
                    <p class="text-dark m-0 text-justify block-service-content">
                        <span class="sub-title d-inline-block w-100">XÂY DỰNG NGÂN SÁCH HOẠT ĐỘNG</span>

                        <br>Ngân sách hoạt động sẽ bao gồm bảng ngân sách năm, bao gồm tỷ lệ chi phí tháng theo từng giai đoạn cụ thể, xác định giá xuất trung bình/người/bữa, doanh thu từ các nhà tài trợ; bảng ngân sách hoạt động theo kế hoạch từ 6 tháng, 1 năm; bảng dự trù nguồn khách hàng trong tương lai. Ngoài ra, chủ nhà hàng còn nắm rõ tổng chi phí trong quá trình setup nhà hàng như: Chi phí hoạt động, chi phí gián tiếp, quản lý, chi phí khấu hao…


                    </p>
                    <img class="img-fluid mt-2 mt-sm-4" src="images/coffee/coffe-03.jpg" alt="Card image cap">
                </div>
            </div>

        </div>
    </section>
    <!-- END BLOCK advisory -->

    <!-- START BLOCK media organization -->
    <section class="block-service overlay-bg-container" id="block-media-organization">
        <div class="container">
            <div class="row">
                <div class="col-7 d-sm-none d-none d-lg-block">

                </div>
                <div class="col-12 col-sm-5 py-2 py-sm-5">
                    <p class="text-light m-0 text-justify block-service-content py-4 py-sm-5">
                        <span class="sub-title d-inline-block w-100">TỔ CHỨC CHIẾN DỊCH TRUYỀN THÔNG,<br> MARKETING, BÁN HÀNG ONLINE HIỆU QUẢ</span>


                        <br>Xây dựng chiến lược marketing nhằm quảng bá thương hiệu ngay trước khi nhà hàng bắt đầu đi vào hoạt động. Chuyên gia setup sẽ giúp bạn lập kế hoạch truyền thông trong từng thời kỳ cụ thể, theo kế hoạch ngắn hạn và dài hạn. Chiến lược marketing phải đảm bảo việc định vị thương hiệu, cấu trúc thương hiệu, thu hút khách hàng, duy trì hình ảnh ấn tượng thông qua chất lượng phục vụ, chất lượng món ăn và giá cả.

                    </p>
                </div>

            </div>

        </div>
    </section>
    <!-- END BLOCK media organization -->

    <!-- START BLOCK Recruitment -->
    <section class="mb-3 block-service" id="block-recruitment-personnel">
        <div class="container">
            <div class="row py-3 py-sm-5">

                <div class="col-12 col-sm-6">
                    <p class="text-dark  mb-0 pr-0 pr-sm-5 text-justify block-service-content">
                        <span class="sub-title d-inline-block w-100" style="font-size: 1.2rem;">TUYỂN DỤNG NHÂN SỰ VÀ HUẤN LUYỆN, ĐÀO TẠO CHUYÊN MÔN, NGHIỆP VỤ, THÁI ĐỘ PHỤC VỤ KHÁCH HÀNG</span>
                        Đội ngũ phục vụ có tầm ảnh hưởng vô cùng lớn đến chất lượng cũng như hình ảnh của nhà hàng. Khi sử dụng dịch vụ tư vấn setup nhà hàng trọn gói, chủ đầu tư sẽ được cty SENHOS hỗ trợ cả việc tuyển dụng và đào tạo nhân viên.
                        <br>Việc đào tạo là yêu cầu bắt buộc trong thời điểm ban đầu và tiến hành đào tạo nâng cao trong suốt quá trình vận hành nhà hàng nhằm nâng cao trình độ và đảm bảo nhân viên có đủ khả năng đáp ứng chất lượng dịch vụ.
                        <br>Chúng tôi tuyển dụng nhân sự dựa trên 3 yếu tố: Kiến thức, kỹ năng và thái độ theo tiêu chuẩn của Cty SENHOS; cùng với đó là những chính sách thu hút nhân sự giúp cơ sở được duy trì ổn định.




                    </p>
                </div>
                <div class="col-12 col-sm-6">
                    <img class="img-fluid my-2 my-sm-0" src="images/coffee/coffe-04.jpg" alt="Card image cap">
                </div>
            </div>

        </div>
    </section>
    <!-- END BLOCK Recruitment -->

    <!-- START BLOCK PROJECT -->
    <section class="block-service" id="block-manager-restaurant">
        <div class="container">
            <div class="row p-3 p-sm-5 justify-content-center">
                <span class="sub-title d-inline-block w-100 text-light text-center">QUẢN LÝ VẬN HÀNH HOẠT ĐỘNG NHÀ HÀNG - CAFE</span>
                <p class="text-justify text-sm-center text-light px-0 px-sm-5">Trong thời gian đầu hoạt động, đội ngũ setup sẽ điều hành chặt chẽ mọi hoạt động và báo cáo lại cho chủ nhà hàng nắm được cụ thể tình hình kinh doanh; đồng thời giải quyết phát sinh không mong muốn trong suốt quá trình này.</p>
            </div>

        </div>
    </section>
    <!-- END BLOCK PROJECT -->
</div>
@endsection
@section('script')

@endsection