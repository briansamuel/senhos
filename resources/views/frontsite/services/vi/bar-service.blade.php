@extends('frontsite.index')
@section('title', $page->page_title )
@section('title_ab', $page->page_seo_title )
@section('keyword', $page->page_seo_keyword )
@section('description', $page->page_seo_description)
@section('style')

@endsection
@section('content')
<!-- START Block Banner  -->

@include('frontsite.elements.banner')
<!-- END Block Banner -->
<!-- START BREADCRUMB -->
@include('frontsite.elements.breadcrumb')
<!-- END BREADCRUMB -->
<div class="main-wrapper">



    <!-- START BLOCK PROJECT -->
    <section class="" id="block-intro">
        <div class="container">
            <div class="row mt-4 mb-3 justify-content-center">
                <div class="col-12">
                    <h3 class="text-center title-page text-uppercase" data-animation="slideInLeft">{{ $page->page_title }}</h3>
                    <p class="text-center" data-animation="slideInRight">{{ html_entity_decode(strip_tags($page->page_description)) }}</p>
                </div>
            </div>

        </div>
    </section>
    <!-- END BLOCK PROJECT -->

    <!-- START BLOCK INVEST -->
    <section class="mb-3 block-service overlay-bg-container" id="block-bar-target">
        <div class="container">
            <div class="row pt-5 pb-5">
                <div class="col-6 d-sm-none d-none d-lg-block">

                </div>
                <div class="col-12 col-sm-6" data-animation="fadeInLeft">
                    <p class="text-light m-0 text-justify">
                        <span class="sub-title d-inline-block w-100">MỤC TIÊU</span>


                        <br>Tạo ra sản phẩm chất lượng ngay từ đầu
                        <br>- Tuyển dụng và sàng lọc đội ngũ nhân sự phù hợp, nỗ lực vì mục tiêu của doanh nghiệp
                        <br>- Đào tạo, trang bị cho người lao động những kiến thức, kỹ năng, kinh nghiệm và ý thức làm việc tại cơ sở
                        kinh doanh.
                        <br>- Cung cấp dịch vụ với chất lượng và tiêu chuẩn cao nhất
                        <br>- Tăng doanh thu của quán Bar, Lounge, tăng lượng khách quay trở lại nhà hàng
                        <br>- Kiểm soát tốt doanh thu hằng ngày
                        <br>- Kiểm soát và tiết kiệm chi phí (đầu tư, vận hành)
                        <br>- Áp dụng hệ thống tiêu chuẩn để quản lý và vận hành
                        <br>- Phát huy được lợi thế cạnh tranh
                        <br>- Xây dựng thương hiệu, chuỗi thương hiệu; gia tăng giá trị thương hiệu



                    </p>
                </div>
            </div>

        </div>
    </section>
    <!-- END BLOCK INVEST -->

    <!-- START BLOCK SETUP SPA -->
    <section class="mb-3 block-service" id="block-bar-design">
        <div class="container">
            <div class="row py-2 py-sm-5">

                <div class="col-12 col-sm-6 " data-animation="slideInLeft">
                    <p class="text-dark m-0 text-justify block-service-content">
                        <span class="sub-title d-inline-block w-100">TƯ VẤN THIẾT KẾ - SETUP</span>


                        <span class="text-primary ">Chuyên gia của tập đoàn SENHOS tư vấn về công năng, tính năng của từng phần trong thiết kế để chủ đầu tư lựa chọn và thực hiện các phương án thiết kế phù hợp (chưa bao gồm chi phí thiết kế). Gồm:</span>
                        <br>
                        <br>- Thiết kế tổng quát.
                        <br>- Thiết kế nội thất bài trí Lounge, Bar.
                        <br>- Thiết kế bộ nhận dạng thương hiệu Lounge, Bar: Logo, đồng phục, công cụ nhận dạng, menu …
                        <br>- Thiết kế Setup các không gian riêng biệt để khách hàng lựa chọn phù hợp với những mục đích khác nhau; thể hiện được các gout sử dụng rượu, bia, cocktails… Mỗi không gian thiết kế phải thể hiện được đặc tính sản phẩm, tôn vinh thương hiệu, để khách hàng dễ ghi nhớ và nhận diện.
                        <br>- Giám sát thi công hoàn thiện.

                    </p>
                </div>
                <div class="col-12 col-sm-6">
                    <img class="img-fluid" src="images/bar/bar-01.jpg" alt="Card image cap">
                </div>
            </div>

        </div>
    </section>
    <!-- END BLOCK SETUP SPA -->

    <!-- START BLOCK BAR PRODUCT SERIVE -->
    <section class="block-service overlay-bg-container" id="block-bar-product">
        <div class="container">
            <div class="row py-2 justify-content-between">
                <div class="col-12 col-sm-6 align-self-start">
                    <img class="img-responsive my-1 my-sm-3" src="images/bar/bar-02.jpg" alt="Card image cap">
                </div>

                <div class="col-12 col-sm-5 py-2 py-sm-5 pl-sm-5 align-self-end" data-animation="slideInRight">
                    <p class="text-light mt-3 mb-3 text-justify block-service-content">
                        <span class="sub-title d-inline-block w-100">TƯ VẤN SẢN PHẨM DỊCH VỤ</span>

                        <br>Các sản phẩm bao gồm:
                        <br>Các loại rượu trên thế giới.
                        <br>Các loại thức uống pha chế không cồn
                        <br>Các loại thức uống pha chế có cồn
                        <br>Các loại thức uống là nước trái cây
                        <br>Các loại thức uống khác
                        <br>
                        <br>Chuyên gia của SENHOS tư vấn và xây dựng Menu và chính sách giá sản phẩm phù hợp với thị trường.




                    </p>
                </div>

            </div>

        </div>
    </section>
    <!-- END BLOCK BAR PRODUCT SERIVE  -->

    <!-- START BLOCK RECRUITMENT AND TRAINING -->
    <section class="block-service" id="block-recruitment-and-training">
        <div class="container">
            <div class="row my-2 my-sm-5">

                <div class="col-12 col-sm-6 " id="block-bar-recruitment">
                    <p class="text-dark m-0 text-justify block-service-content">
                        <span class="sub-title d-inline-block w-100">TUYỂN DỤNG</span>
                        <br>Đội ngũ nhân viên sẽ do các chuyên gia của tập đoàn SENHOS trực tiếp tuyển dụng dựa trên:
                        <br>- Ngoại hình
                        <br>- Thái độ
                        <br>- Kỹ năng phục vụ khách hàng.
                        <br>
                        <br>Ban hành:
                        <br>- Các quy chế, quy định và hướng dẫn thi hành.
                        <br>- Nội quy lao động.
                        <br>- Sổ tay nhân viên.
                        <br>- Quy định về tuyển dụng, bổ nhiệm, chuyển vị trí và sa thải.
                        <br>- Quy định về khen thưởng, kỷ luật và quyền lợi nhân viên.
                        Chúng tôi sẽ đưa ra cơ cấu định biên nhân sự; lập ngân sách lương hướng, phương án trả lương, thưởng đến tính chuyên nghiệp và hiệu quả kinh doanh bền vững.


                    </p>
                    <img class="img-fluid mt-4" data-animation="fadeInLeft" src="images/bar/bar-03.jpg" alt="Card image cap">
                </div>
                <div class="col-12 col-sm-6 mt-3 mt-sm-0" id="block-bar-training">
                    <p class="text-dark m-0 text-justify block-service-content">
                        <span class="sub-title d-inline-block w-100">ĐÀO TẠO</span>

                        <br>Đội ngũ quản lý do tập đoàn SENHOS bố trí đến để huấn luyện đào tạo,Tư vấn các giải pháp. Thời gian đào tạo 1 tháng trước khi vào vận hành:
                        <br>• Đào tạo các công thức, định lượng, cách pha chế rượu, cách phục vụ rượu, rượu vang, café, đồ uống theo tiêu chuẩn quốc tế.
                        <br>• Tiêu chuẩn phục vụ khách hàng hài lòng.
                        <br>• Thực hiện các qui trình phục vụ khách hàng.
                        <br>• Qui trình quản lý, kiểm sát về chi phí, doanh thu.
                        <br>• Qui trình quản lý vệ sinh an toàn thực phẩm.
                        <br>• Qui trình quản lý hàng hóa xuất, nhập, tồn.
                        <br>• Qui trình an ninh từ khâu giữ xe, đến quản lý an ninh nội bộ, quản lý tài sản, trang thiết bị.
                        <br>• Hệ thống biểu mẫu quản lý, tiêu chí đánh giá năng lực làm việc của nhân viên, tất cả các qui trình có tính hệ thống, kế thừa kể cả khi nhân sự thay đổi.


                    </p>
                    <img class="img-fluid mt-5" data-animation="fadeInRight" src="images/bar/bar-04.jpg" alt="Card image cap">
                </div>
            </div>

        </div>
    </section>
    <!-- END BLOCK BLOCK RECRUITMENT AND TRAINING -->

    <!-- START BLOCK CONSULTING EQUIPMENT, MATERIALS, CCDC -->
    <section class="block-service overlay-bg-container" id="block-bar-consulting">
        <div class="container">
            <div class="row">
                <div class="col-7 d-sm-none d-none d-lg-block">

                </div>
                <div class="col-12 col-sm-5 py-2 py-sm-5" data-animation="fadeInUp">
                    <p class="text-light m-0 text-justify block-service-content pt-5 pb-5">
                        <span class="sub-title d-inline-block w-100">TƯ VẤN TRANG THIẾT BỊ, VẬT TƯ, CCDC</span>
                        <br>SENHOS lập danh mục các trang thiết bị theo ngân sách nằm trong hạn mức đầu tư (tỷ suất đầu tư so với hoạt động kinh doanh):
                        <br>- Bàn ghế, ly tách, vật trang trí, …
                        <br>- Máy pha café, tủ lạnh, tủ mát bảo quản.
                        <br>- Phần mềm, máy POS quản lý doanh thu.
                        <br>- Tất cả các danh mục CCDC theo tỷ lệ phục vụ khách hàng, lưu kho, quay vòng.
                        <br>Chi phí do nhà đầu tư duyệt mua.

                    </p>
                </div>

            </div>

        </div>
    </section>
    <!-- END BLOCK CONSULTING EQUIPMENT, MATERIALS, CCDC -->

    <!-- START BLOCK PLAN MARKETING -->
    <section class="mb-3 block-service" id="block-bar-marketing">
        <div class="container">
            <div class="row py-2 py-sm-5">

                <div class="col-12 col-sm-6 ">
                    <p class="text-dark mb-0 text-justify block-service-content pr-3">
                        <span class="sub-title d-inline-block w-100" data-animation="fadeInLeft">TƯ VẤN, QUẢNG BÁ MARKETING - XÂY DỰNG CHIẾN LƯỢC KẾ HOẠCH KINH DOANH</span>


                        <br><span class="text-primary font-weight-bold">Marketting:</span>
                        <br>- Tư cấn hoạt động cụ thể của các chương trình khuyến mãi, kích thích tiêu dùng
                        <br>- Đánh giá thị trường mục tiêu
                        <br>- Tư vấn, xây dựng kế hoạch quảng bá dịch vụ tiền khai trương, để đảm bảo hiệu quả kinh doanh ngay từ ban đầu.
                        <br>- Tư vấn thực hiện việc đăng ký thương hiệu cửa hàng
                        <br>
                        <br><span class="text-primary font-weight-bold">Chiến lược, kế hoạch kinh doanh:</span>
                        <br>Nhà đầu tư sẽ nhìn thấy được bức tranh tổng quát của việc đầu tư một quán Bar hay Lounge bao gồm:
                        <br>- Kế hoạch kinh doanh ngắn hạn, dài hạn
                        <br>- Ngân sách hoạt động
                        <br>- Chiến lược kinh doanh được thể hiện qua sự phân tích tổng thể, đưa ra định hướng kinh doanh trong vòng 5 năm.
                        <br>- Định hướng quy mô kinh doanh dựa trên khả năng đầu tư ban đầu và khả năng tái đầu tư trang thiết bị.
                        <br>- Định hướng nguồn khách hàng mục tiêu, phù hợp với khả năng tổ chức hoạt động kinh doanh.




                    </p>
                </div>
                <div class="col-12 col-sm-6 py-2 py-sm-5 " data-animation="fadeInRight">
                    <img class="img-fluid mt-5 pl-3" src="images/bar/bar-04.jpg" alt="Card image cap">
                </div>
            </div>

        </div>
    </section>
    <!-- END BLOCK PLAN MARKETING -->

    <!-- START BLOCK EXECUTIVE AND MONITORING -->
    <section class="block-service" id="block-bar-executive-monitoring">
        <div class="container">
            <div class="row pb-2 pt-2">

                <div class="col-12 pt-5 pb-5" data-animation="fadeInDown">
                    <p class="text-light mt-3 mb-3 text-justify block-service-content">
                        <span class="sub-title d-inline-block w-100">ĐIỀU HÀNH VÀ GIÁM SÁT TRONG QUÁ TRÌNH THIẾT KẾ - SETUP</span>

                        <br>- Vận hành bộ máy, quy định, kiểm soát về chi phí tài chính.
                        <br>- Đánh giá nhân sự hàng tháng dựa trên tình hình hoạt động kinh doanh.
                        <br>- Báo cáo tài chính hàng tháng và đưa ra những quyết định điều chỉnh chính xác và hiệu quả.
                        <br><span class="text-primary font-weight-bold">Vận hành thử nghiệm: Soft Opening</span>
                        <br>Là giai đoạn vận hành thử để kiểm nghiệm, đánh giá để điều chỉnh sản phẩm cũng như chất lượng dịch vụ. Đây là bước chuẩn bị vô cùng quan trọng để cửa hàng đi vào vận hành chính thức một cách chuyên nghiệp nhất.
                        <br>Soft opening được thực hiện trước khi tổng khai trương 1- 2 tháng.
                        <br><span class="text-primary font-weight-bold">Chuyển giao</span>
                        <br>- Đội ngũ quản lý, nhân sự được đào tạo kỹ càng.
                        <br>- Hồ sơ các qui trình, định lượng, cách pha chế, công thức….
                        <br>- Cửa hàng được set up chuyên nghiệp để phục vụ khách hàng
                        <br>- Chiến lược kinh doanh cửa hàng…
                        <br><span class="text-primary font-weight-bold">Cam kết của Tập đoàn SENHOS là đồng hành, hỗ trợ mọi hoạt động để đi vào vận hành hoàn thiện. Chúng tôi luôn tâm huyết với từng dự án để mang lại hiệu quả cho nhà đầu tư. </span>




                    </p>
                </div>

            </div>

        </div>
    </section>
    <!-- END BLOCK EXECUTIVE AND MONITORING  -->
</div>
@endsection
@section('script')

@endsection