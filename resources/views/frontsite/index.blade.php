<!DOCTYPE html>
<html lang="vi-vn">

<head>
    <base href="{{ Request::root() }}">
    <title>@yield('title', __('seo.home.title')) | {{$setting->get('general::admin_appearance::site-names', __('seo.home.site-names'))}}</title>

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,height=device-height, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta name="robots" content="noodp, index, follow">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-title" content="{{ Request::root() }}">
    <meta name="keyword" content="@yield('keyword', __('seo.home.keyword'))">
    <meta name="description" content="@yield('description', __('seo.home.description'))">

    @include('frontsite.elements.seo')
    <link rel="stylesheet" type="text/css" href="{{asset('')}}boostrap/css/bootstrap.min.css">

    <link rel="stylesheet" type="text/css" href="{{asset('')}}fontawesome/css/regular.min.css">
    <link rel="stylesheet" type="text/css" href="{{asset('')}}fontawesome/css/light.min.css">
    <link rel="stylesheet" type="text/css" href="{{asset('')}}fontawesome/css/all.min.css">
    <link rel="stylesheet" type="text/css" href="{{asset('')}}slick/slick.css" />
    <link rel="stylesheet" type="text/css" href="{{asset('')}}slick/slick-theme.css" />
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{asset('')}}css/main.css?v1.1">
    <link rel="stylesheet" type="text/css" href="{{asset('')}}css/effect-load.css">
    <link rel="stylesheet" type="text/css" href="{{asset('')}}css/effect-hover.css">
    <link rel="stylesheet" type="text/css" href="{{asset('')}}css/responsive.css">
    <link href = "{{asset('')}}css/jquery-ui.css"
          rel = "stylesheet">
    @yield('style')
    <link rel="stylesheet" href="{{asset('')}}assets/css/customs/desktop.style.integration.css">
    <link rel="stylesheet" href="{{asset('')}}assets/css/customs/table.style.integration.css">
    <link rel="stylesheet" href="{{asset('')}}assets/css/customs/mobile.style.integration.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script type="text/javascript" src="{{asset('')}}boostrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="{{asset('')}}slick/slick.min.js"></script>
    <script type="text/javascript" src="{{asset('')}}js/waypoint.js"></script>
    <script type="text/javascript" src="{{asset('')}}js/effect-load.js"></script>
    
    <script src = "{{asset('')}}js/jquery-ui.js"></script>
    <style>
        
        .fb_dialog_advanced {
            bottom: 122pt !important;
            right: 15pt !important;
        }
        .fb-customerchat iframe {
            right: 60pt !important;
        }
    </style>
</head>
<body class="@yield('body_class')">

    <!-- Header -->
    @include('frontsite.header')
    <!-- End Header -->
    @yield('content')
    <!-- END BLOCK PARTNER -->
    <!-- END TOP FOOTER -->
    @include('frontsite.footer')
    @include('frontsite.elements.ring-phone')
    @include('frontsite.elements.subcribe-popup')    
    <!--end::Global Theme Bundle -->
    <!--begin::Page Vendors(used by this page) -->
    @yield('vendor-script', '')
    <!--end::Page Vendors -->
    <!--begin::Page Scripts(used by this page) -->
    @yield('script')
    {{-- chat website--}}
    @include('frontsite.elements.chat')
    {{-- Call Image Float --}}
    @include('frontsite.elements.call-image')
</body>

</html>