@extends('frontsite.index')
@section('title', __('seo.booking.title'))
@section('title_ab', __('seo.booking.title_ab'))
@section('keyword', __('seo.booking.keyword'))
@section('description', __('seo.booking.keyword'))
@section('style')
<link rel="stylesheet" type="text/css" href="css/slide-range.css">
<link rel="stylesheet" href="/fancybox/jquery.fancybox.css?v=2.1.7" type="text/css" media="screen" />
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="select/css/select2-bootstrap4.min.css">
@endsection
@section('content')
@include('frontsite.elements.banner-search')
<div class="wrap-content">
    <section class="mt-2 mt-sm-5 " id="block-city">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-12 p-2 item-block-city">

                    <a href="tim-phong/?province_id=1" class="effect-zoom-in">
                        <img class="img-fluid" src="images/booking/hcm-pic.jpg">
                        <span class="city-name">Hồ Chí Minh</span>
                        <span class="total-place">5,022 {{ __('frontsite.booking.place') }}</span>

                    </a>

                </div>
                <div class="col-xl-6 col-12 p-2 item-block-city">
                    <a href="tim-phong/?district_id=212" class="effect-zoom-in">
                        <img src="images/booking/hcm-pic.jpg">
                        <span class="city-name">Phú Quốc</span>
                        <span class="total-place">3,022 {{ __('frontsite.booking.place') }}</span>
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-xl-4 p-2 item-block-city">
                    <a href="tim-phong/?province_id=10" class="effect-zoom-in">
                        <img src="images/booking/vung-tau.jpg">
                        <span class="city-name">Vũng Tàu</span>
                        <span class="total-place">5,022 {{ __('frontsite.booking.place') }}</span>
                    </a>
                </div>
                <div class="col-12 col-xl-4 p-2 item-block-city">
                    <a href="tim-phong/?district_id=178" class="effect-zoom-in">
                        <img src="images/booking/phan-thiet.jpg">
                        <span class="city-name">Phan Thiết</span>
                        <span class="total-place">5,022 {{ __('frontsite.booking.place') }}</span>
                    </a>
                </div>
                <div class="col-12 col-xl-4 p-2 item-block-city">
                    <a href="tim-phong/?district_id=87" class="effect-zoom-in">
                        <img src="images/booking/nha-trang.jpg">
                        <span class="city-name">Nha Trang</span>
                        <span class="total-place">5,022 {{ __('frontsite.booking.place') }}</span>
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-xl-4 p-2 item-block-city">
                    <a href="tim-phong/?province_id=3" class="effect-zoom-in">
                        <img src="images/booking/vung-tau.jpg">
                        <span class="city-name">Đà Nẵng</span>
                        <span class="total-place">5,022 {{ __('frontsite.booking.place') }}</span>
                    </a>
                </div>
                <div class="col-12 col-xl-4 p-2 item-block-city">
                    <a href="tim-phong/?province_id=2" class="effect-zoom-in">
                        <img src="images/booking/phan-thiet.jpg">
                        <span class="city-name">Hà Nội</span>
                        <span class="total-place">5,022 {{ __('frontsite.booking.place') }}</span>
                    </a>
                </div>
                <div class="col-12 col-xl-4 p-2 item-block-city">
                    <a href="tim-phong/?district_id=231" class="effect-zoom-in">
                        <img src="images/booking/nha-trang.jpg">
                        <span class="city-name">Hạ Long</span>
                        <span class="total-place">5,022 {{ __('frontsite.booking.place') }}</span>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <!-- END Block ABOUT -->

    <!-- START BLOCK BOOKING NOW -->
    <section class="" id="block-booking-now">
        <div class="container">
            <div class="row mt-2 mt-sm-4 justify-content-start">
                <div class="col-12 p-2">
                    <h3 class="text-primary">BOOKING SỚM NHẬN ƯU ĐÃI LỚN</h3>
                </div>
            </div>


        </div>
        <div class="container-fluid">
            <div class="row mt-0 py-2 py-sm-5 mt-sm-4 justify-content-center bg-booking-now">
                <div class="col-12 p-2 text-center">
                    <a href="#" class="button-see-now">XEM NGAY</a>
                </div>
            </div>
        </div>

    </section>
    <!-- END BLOCK BOOKING NOW -->

    @include('frontsite.booking.elements.list-host')

    <!-- START BLOCK TYPE PLACE -->
    <section class="mt-1 mt-sm-5" id="block-type-place">
        <div class="container">
            <div class="row mt-4 mb-4 justify-content-start">
                <div class="col-12">
                    <h3 class="text-primary">TÌM THEO LOẠI CHỖ NGHỈ</h3>
                </div>
            </div>


        </div>
        <div class="bg-type-place">
            <div class="container">
                <div class="row pt-4 pb-4">
                    <div class="col-12 mx-auto s_container">
                        <div id="carousel-place-type">

                            <div class="item-place p-3">
                                <a href="tim-phong/?host_type=hotel" class="place-thumbnail">
                                    <img src="images/booking/hotel-type.jpg">
                                </a>
                                <h4 class="place-type mt-4"><a href="tim-phong/?host_type=hotel">Hotel</a></h4>

                                <div class="place-total d-flex">
                                    <span class="text-primary">9,123 Hotel</span>
                                </div>
                            </div>
                            <div class="item-place p-3">
                                <a href="tim-phong/?host_type=resort" class="place-thumbnail">
                                    <img src="images/booking/resort-type.jpg">
                                </a>
                                <h4 class="place-type mt-4"><a href="tim-phong/?host_type=resort">Resort</a></h4>

                                <div class="place-total d-flex ">
                                    <span class="text-primary">271 Resort</span>
                                </div>
                            </div>
                            <div class="item-place p-3">
                                <a href="tim-phong/?host_type=homestay" class="place-thumbnail">
                                    <img src="images/booking/homestay-type.jpg">
                                </a>
                                <h4 class="place-type mt-4"><a href="tim-phong/?host_type=homestay">HomeStay</a></h4>

                                <div class="place-total d-flex">
                                    <span class="text-primary">1,239 HomeStay</span>
                                </div>
                            </div>
                            <div class="item-place p-3">
                                <a href="tim-phong/?host_type=codontel" class="place-thumbnail">
                                    <img src="images/booking/codontel-type.jpg">
                                </a>
                                <h4 class="place-type mt-4"><a href="tim-phong/?host_type=codontel">Codontel</a></h4>

                                <div class="place-total d-flex">
                                    <span class="text-primary">245 Codontel</span>
                                </div>
                            </div>
                            <div class="item-place p-3">
                                <a href="tim-phong/?host_type=villa" class="place-thumbnail">
                                    <img src="images/booking/hotel-type.jpg">
                                </a>
                                <h4 class="place-type mt-4"><a href="tim-phong/?host_type=villa">Villa</a></h4>

                                <div class="place-total d-flex">
                                    <span class="text-primary">235 Villa</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END BLOCK TYPE PLACE -->
</div>

<!-- START TOP FOOTER 1 -->
@include('frontsite.elements.top-footer-1')
<!-- END TOP FOOTER 1 -->
<!-- START TOP FOOTER 2  -->
@include('frontsite.elements.top-footer-2')
<!-- END TOP FOOTER 2 -->
@endsection
@section('script')

<script type="text/javascript" src="/fancybox/jquery.fancybox.pack.js?v=2.1.7"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
<!-- Script For page -->
<script type="text/javascript" src="js/booking.js"></script>
<script>
    $(document).ready(function() {
        $(".various").fancybox({
            maxWidth: 1300,
            maxHeight: 900,
            fitToView: false,
            width: "90%",
            height: "90%",
            autoSize: false,
            closeClick: false,
            openEffect: "none",
            closeEffect: "none"
        });
    });
</script>
<!-- End Script For page -->
@endsection