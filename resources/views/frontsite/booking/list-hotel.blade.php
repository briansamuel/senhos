@extends('frontsite.index')
@section('title', __('seo.booking.title'))
@section('title_ab', __('seo.booking.title_ab'))
@section('keyword', __('seo.booking.keyword'))
@section('description', __('seo.booking.keyword'))
@section('style')
<link rel="stylesheet" type="text/css" href="css/slide-range.css">
<link rel="stylesheet" href="/fancybox/jquery.fancybox.css?v=2.1.7" type="text/css" media="screen" />
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<link rel="stylesheet" href="boostrap/slider/css/bootstrap-slider.min.css">
<link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="select/css/select2-bootstrap4.min.css">
@endsection
@section('content')
@include('frontsite.elements.banner-search')
<div class="wrap-content">
    <div class="container">
        <div class="row mt-1 mt-sm-5">

            <!-- Filter Widget -->
            @include('frontsite.booking.elements.filter-widget')
            <!-- Filter Widget -->
            <div class="col-12 col-xl-8">

                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                <input type="hidden" name="province_id[]" value="{{ !empty(Request::get('province_id')) ? Request::get('province_id') : '' }}" />
                <input type="hidden" name="district_id[]" value="{{ !empty(Request::get('district_id')) ? Request::get('district_id') : '' }}" />
                <input type="hidden" name="host_id" value="{{ !empty(Request::get('host_id'))? Request::get('host_id') : '' }}" />
                <input type="hidden" name="host_type" value="{{ !empty(Request::get('host_type')) ? Request::get('host_type') : '' }}" />
                <div class="row m-0">
                    <div class="col-12 col-xl-12 p-0">
                        <h4 class="title-search-place mt-5 mt-sm-0">{{ $query }} : {{ __('frontsite.booking.find_total_place', ['total' => $total_host]) }}.</h4>
                    </div>
                    <!-- tìm thấy {{ $total_host }} chỗ nghỉ -->
                </div>
                <input type="hidden" name="page" value="2" />
                <div class="loader">Loading...</div>
                <div id="list-hotel" class="list-hotel mb-4">

                </div>
                <div id="loadMore" class="d-flex justify-content-center m-2" style="display: none">
                    <button class="btn bg-primary">{{ __('frontsite.booking.loadmore') }}</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="list-position-host" class="h-100" style="display: none">
    <div id="map" class="w-100 h-100"></div>
</div>


@endsection
@section('script')
<script src="//maps.google.com/maps/api/js?key=AIzaSyAMMeGEeYSevUQYg9ZtSDC7PWuATS3fAVw" type="text/javascript"></script>
<script type="text/javascript" src="/fancybox/jquery.fancybox.pack.js?v=2.1.7"></script>
<script src="/boostrap/slider/js/bootstrap-slider.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
<script type="text/javascript" src="/js/gmaps.min.js"></script>
<!-- Script For page -->
<script type="text/javascript" src="js/booking.js"></script>
<script type="text/javascript" src="js/list-host.js"></script>
<!-- Script For page -->
<script>
    var map = null;
    var lat = '21.029243';
    var lng = '105.851232';
    var radius = 10;
    let province =  $("input[name='province_id[]']").val() ? [$("input[name='province_id[]']").val()] : null;
    let host =  $("input[name='host_id']").val() ? $("input[name='host_id']").val() : null;

    function getLocationCurrent() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition);
        }
    }

    function showPosition(position) {
        lat = position.coords.latitude;
        lng = position.coords.longitude;
    }

    function getLocationByProvince(province_id)
    {
        $.ajax({
            url: '/ajax-get-location-by-province',
            type: 'GET',
            data: {
                province_id: province_id,
            },
            dataType: "json",
            success: function (res) {
                if(res) {
                    if(res._lat) lat = res._lat;
                    if(res._lng) lng = res._lng;
                }
            },
            error: function(res) {
                return true;
            }
        });
    }

    function getLocationByHost(host_id)
    {
        $.ajax({
            url: '/ajax-get-location-by-host',
            type: 'GET',
            data: {
                host_id: host_id,
            },
            dataType: "json",
            success: function (res) {
                console.log(res);
                lat = res.host_lat;
                lng = res.host_lng;
            }
        });
    }

    function getLocation() {
        if (province === 0 && !host) {
            getLocationCurrent();
        } else if(province) {
            getLocationByProvince(province);
        } else if(host) {
            getLocationByHost(host);
        }
    }

    function ajaxLoadMap() {
        getLocation();

        map = new GMaps({
            el: '#map',
            lat: lat,
            lng: lng,
            zoom: 15,
            zoomControl : true,
            zoomControlOpt: {
                style : 'SMALL',
                position: 'TOP_LEFT'
            },

        });
        let icon = {
            url: '/images/default/current-property-pin.svg', //url
            scaledSize: new google.maps.Size(50, 50), // scaled size
            origin: new google.maps.Point(0,0), // origin
            anchor: new google.maps.Point(0, 0) // anchor
        };

        if (province) {
            radius = 100;
        }
        $.ajax({
            url: '/search-near-hotel-action',
            type: 'GET',
            data: {
                lat: lat,
                lng: lng,
                radius: radius
            },
            dataType: "json",
            success: function (res) {
                res.map(hotel => {
                    var infoWindow = new google.maps.InfoWindow({
                        maxHeight: 66,
                        // content: `<div class="HotelMap-leaflet"><table class="HotelMap-popup"><tbody><tr><td class="HotelMap-popup-blockImage"><div class="HotelMap-popup-imgCover"><img class="HotelMap-popup-img" src="${hotel.host_thumbnail}"></div></td><td class="HotelMap-popup-blockContent"><div class="HotelMap-popup-content"><div class="HotelMap-popup-title">${hotel.host_name}</div><div class="HotelMap-review"><span class="host-star"><i class="fas fa-star "></i> <i class="fas fa-star "></i><i class="fas fa-star "></i></span><span>|</span><span>Trên cả tuyệt vời&nbsp;<span>9,3</span></span></div></div></td></tr></tbody></table></div>`
                    });

                    // Create marker

                    var marker = map.addMarker({
                        lat: hotel.host_lat,
                        lng: hotel.host_lng,
                        icon: icon,
                        title: "Vị trí",
                        infoWindow: infoWindow
                    });
                    // // This opens the infoWindow
                    infoWindow.open(map, marker);
                });
            }
        });
    }

    $(document).ready(function() {
        $(".various").fancybox({
            maxWidth: 1300,
            maxHeight: 900,
            fitToView: false,
            width: '90%',
            height: '90%',
            autoSize: false,
            closeClick: false,
            openEffect: 'none',
            closeEffect: 'none',
            afterLoad: function(current) {
                try {
                    ajaxLoadMap();
                } catch (error) {
                    console.log(error);
                }
            },
            afterClose: function() {

            }
        });
    });
</script>
<!-- End Script For page -->
@endsection