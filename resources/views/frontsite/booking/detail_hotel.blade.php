@extends('frontsite.index')
@section('title', __('seo.booking.title'))
@section('title_ab', __('seo.booking.title_ab'))
@section('keyword', __('seo.booking.keyword'))
@section('description', __('seo.booking.keyword'))
@section('style')

<link rel="stylesheet" href="/fancybox/jquery.fancybox.css?v=2.1.7" type="text/css" media="screen" />
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

@endsection
@section('content')
@include('frontsite.elements.banner-search')
<!-- START BLOCK ABOUT -->

<div class="wrap-content mb-4">
    <div class="container">
        <div class="row">
            <aside class="col-12 col-lg-4 d-none d-sm-block">

                <div class="widget bookking-now mt-3">
                    <div class="booking-now-around">
                        <a href="javascript:void(0)" data-scroll="booking-now" class="btn btn-booking-now scroll-to-ele"><span>Đặt ngay</span></a>
                    </div>
                    <span class="checkbox-text"><i class="far fa-check-square mr-2"></i>Chúng tôi luôn khớp
                        giá</span>
                </div>
                <div class="widget place-map mt-3">

                    <div class="widger-content">
                        <div class="row">
                            <div class="col-12 ">
                                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1963.4497934465844!2d103.9670128081537!3d10.18880949818047!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31a78c513c16c02f%3A0x11960b2003125c0a!2sLong%20Beach%20Resort!5e0!3m2!1svi!2s!4v1580972737442!5m2!1svi!2s" width="100%" height="200" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- Widget Deal -->
                <div class="widget top-deal mt-3">

                    <div class="row widger-content">
                        <div class="col-12 ">
                            <div class="item-top-deal mb-4">
                                <a href="#" class="thumbnail-top-deal effect-zoom-in">
                                    <img src="images/booking/sale-1.jpg" class="img-fluid">
                                    <span class="discount-deal">Giảm 10%</span>
                                </a>
                                <div class="info-top-deal">
                                    <h5>Sonata Resort & Spa</h5>
                                    <span class="location">Phú Quốc</span>
                                    <span class="type-room font-weight-bold">Deluxe Double Or Twin Room With Garden
                                        View</span>
                                    <span class="font-weight-bold">Thời gian lưu trú</span>
                                    <span class="checkin-time">15/01/2020 - 31/01/2020</span>
                                    <div class="price-top-deal">
                                        <span class="regular-price">
                                            1.320.281 VND
                                        </span>
                                        <span class="sale-price">
                                            1.188.254 VND
                                        </span>
                                        <div class="btn-around">
                                            <a href="#" class="top-sale-booking">Đặt ngay</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 ">
                            <div class="item-top-deal mb-4">
                                <a href="#" class="thumbnail-top-deal effect-zoom-in">
                                    <img src="images/booking/sale-3.jpg" class="img-fluid">
                                    <span class="discount-deal">Giảm 10%</span>
                                </a>
                                <div class="info-top-deal">
                                    <h5>Sonata Resort & Spa</h5>
                                    <span class="location">Phú Quốc</span>
                                    <span class="type-room font-weight-bold">Deluxe Double Or Twin Room With Garden
                                        View</span>
                                    <span class="font-weight-bold">Thời gian lưu trú</span>
                                    <span class="checkin-time">15/01/2020 - 31/01/2020</span>
                                    <div class="price-top-deal">
                                        <span class="regular-price">
                                            1.320.281 VND
                                        </span>
                                        <span class="sale-price">
                                            1.188.254 VND
                                        </span>
                                        <div class="btn-around">
                                            <a href="#" class="top-sale-booking">Đặt ngay</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 ">
                            <div class="item-top-deal mb-4">
                                <a href="#" class="thumbnail-top-deal effect-zoom-in">
                                    <img src="images/booking/sale-1.jpg" class="img-fluid">
                                    <span class="discount-deal">Giảm 10%</span>
                                </a>
                                <div class="info-top-deal">
                                    <h5>Sonata Resort & Spa</h5>
                                    <span class="location">Phú Quốc</span>
                                    <span class="type-room font-weight-bold">Deluxe Double Or Twin Room With Garden
                                        View</span>
                                    <span class="font-weight-bold">Thời gian lưu trú</span>
                                    <span class="checkin-time">15/01/2020 - 31/01/2020</span>
                                    <div class="price-top-deal">
                                        <span class="regular-price">
                                            1.320.281 VND
                                        </span>
                                        <span class="sale-price">
                                            1.188.254 VND
                                        </span>
                                        <div class="btn-around">
                                            <a href="#" class="top-sale-booking">Đặt ngay</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </aside>
            <div id="host-" class="col-12 col-sm-8 mt-4 pt-3 single-host">
                <div class="host-menu">
                    <nav class="navbar navbar-expand-lg navbar-light">

                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navHotel" aria-controls="navHotel" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navHotel">
                            <ul id="host-menu-ul" class="navbar-nav justify-content-around">
                                <li class="nav-item active">
                                    <a class="nav-link scroll-to-ele" href="javascript:void(0)" data-scroll="booking-now">Thông tin & giá</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link scroll-to-ele" href="javascript:void(0)" data-scroll="convenient">Tiện nghi</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link scroll-to-ele" href="javascript:void(0)" data-scroll="policy">Chính sách</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link scroll-to-ele" href="javascript:void(0)" data-scroll="note">Ghi chú</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link scroll-to-ele" href="javascript:void(0)" data-scroll="review">Đáng giá của khách (1453.1)</a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
                <div class="header-line-host">
                    <span class="badge-host">Khách Sạn</span>
                    <h3 class="host-name">Hotel Paris Villa</h3>
                    <div class="host-star">
                        <i class="fas fa-star "></i>
                        <i class="fas fa-star "></i>
                        <i class="fas fa-star "></i>
                    </div>
                </div>
                <div class="address-line-host">
                    <span><i class="fas fa-map-marker-alt"></i> Bãi trường, Dương tơ, Phú Quốc</span> - <a href="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1963.4497934465844!2d103.9670128081537!3d10.18880949818047!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31a78c513c16c02f%3A0x11960b2003125c0a!2sLong%20Beach%20Resort!5e0!3m2!1svi!2s!4v1580972737442!5m2!1svi!2s" class="show-map various fancybox.iframe">Vị trí xuất sắc - hiển thị bản đồ</a>
                </div>
                <div class="wrapper host-slide mt-4">
                    <div class="slider">
                        <div><img src="images/booking/slide-1.jpg" alt=""></div>
                        <div><img src="images/booking/slide-2.jpg" alt=""></div>
                        <div><img src="images/booking/slide-3.jpg" alt=""></div>
                        <div><img src="images/booking/slide-4.jpg" alt=""></div>
                        <div><img src="images/booking/slide-1.jpg" alt=""></div>
                        <div><img src="images/booking/slide-2.jpg" alt=""></div>
                        <div><img src="images/booking/slide-3.jpg" alt=""></div>
                        <div><img src="images/booking/slide-4.jpg" alt=""></div>
                        <div><img src="images/booking/slide-1.jpg" alt=""></div>
                        <div><img src="images/booking/slide-2.jpg" alt=""></div>
                        <div><img src="images/booking/slide-3.jpg" alt=""></div>
                        <div><img src="images/booking/slide-4.jpg" alt=""></div>
                    </div>
                    <div class="slider-nav">
                        <div><img src="images/booking/slide-1.jpg" alt=""></div>
                        <div><img src="images/booking/slide-2.jpg" alt=""></div>
                        <div><img src="images/booking/slide-3.jpg" alt=""></div>
                        <div><img src="images/booking/slide-4.jpg" alt=""></div>
                        <div><img src="images/booking/slide-1.jpg" alt=""></div>
                        <div><img src="images/booking/slide-2.jpg" alt=""></div>
                        <div><img src="images/booking/slide-3.jpg" alt=""></div>
                        <div><img src="images/booking/slide-4.jpg" alt=""></div>
                        <div><img src="images/booking/slide-1.jpg" alt=""></div>
                        <div><img src="images/booking/slide-2.jpg" alt=""></div>
                        <div><img src="images/booking/slide-3.jpg" alt=""></div>
                        <div><img src="images/booking/slide-4.jpg" alt=""></div>
                    </div>
                </div>
                <div class="host-content pt-4">
                    <!-- Description Hotel -->
                    <div class="row mb-5">
                        <div class="col-12 col-xl-8">
                            <span class="d-block text-secondary">MÔ TẢ</span>
                            <p class="text-secondary">
                                Khách sạn Paris Villas - toạ lạc ngay Bãi Trường - Phú Quốc, mới được hoàn thiện và
                                đi vào sử dụng từ tháng 5 năm 2018. Với vị trí thuận lợi sát biển, Quý khách có thể
                                dễ dàng hoà mình vào thiên nhiên, ngắm nhìn bãi biển đẹp nhất Phú Quốc hùng vĩ trong
                                ánh hoàng hôn, điều được các du khách trong và ngoài nước yêu thích nhất tại khách
                                sạn Paris Villas hay đắm mình làn nước biển trong mát ngay trước khách sạn. Đặc
                                biệt, do nằm cách không xa trung tâm thành phố, Quý khách không mất nhiều thời gian
                                để ghé thăm những địa danh nổi tiếng của Phú Quốc như: Chơ Đêm PQ, Cáp Treo, Suối
                                Bàn Thạch, Hàm Ninh,... các quán ăn địa phương nổi tiềng,... Từ khách sạn, Quý khách
                                chỉ mất khoảng 10 phút để tới sân bay Quốc tế PQ, đây được cho là một trong những lý
                                do hàng đầu các du khách trong và ngoài nước lựa chọn khách sạn Paris Villas do tiết
                                kiệm được nhiều hơn thời gian và tiền bạc, cũng như thuận tiện hơn cho những chuyến
                                bay sớm hoặc đêm khuya mệt mỏi.

                            </p>
                        </div>
                        <div class="col-12 col-xl-4 good-review p-3">
                            <span class="d-flex text-secondary font-weight-bold mb-3">Khách yêu thích chỗ này
                                vì</span>
                            <div class="img-box">

                                <span>Vị trí rất tốt</span>
                                <span>150 đánh giá liên quan</span>
                                <i class="fas fa-star"></i>
                            </div>
                            <div class="img-box">

                                <span>Nhân viên thân thiện</span>
                                <span>150 đánh giá liên quan</span>
                                <i class="fas fa-star"></i>
                            </div>
                            <div class="img-box">

                                <span>Sạch sẽ gọn gàng</span>
                                <span>52 đánh giá liên quan</span>
                                <i class="fas fa-star"></i>
                            </div>
                            <div class="img-box">

                                <span>Thông tin bữa sáng</span>
                                <span>Món đa dạng, Cafe ngon</span>
                                <i class="fas fa-star"></i>
                            </div>
                            <div class="img-box-2">
                                <p class="wifi-box-text">Wifi miễn phí
                                    Khách thường xuyên đánh giá kết nối wifi tuyệt vời!</p>
                                <i class="fas fa-wifi"></i>
                            </div>
                            <div class="img-box-2">
                                <p>Bãi đỗ xe riêng tại Khách Sạn</p>
                                <i class="fas fa-parking"></i>
                            </div>
                            <a href="xac-nhan.html" class="btn btn-booking-now">Đặt ngay</a>
                        </div>
                    </div>
                    <!-- List Room -->
                    <div id="booking-now" class="container list-room mt-5">
                        <div class="row item-room">
                            <div class="col-12 col-lg-3 col-xl-3 p-0 thumbnail-room">
                                <a href="#room-popup" class="various-gallery-modal">
                                    <img src="images/booking/room-1.jpg">
                                </a>
                            </div>
                            <div class="col-12 col-lg-5 col-xl-5 ">
                                <h4>Economy Standard Double</h4>
                                <p>Phòng Giường đôi tiêu chuẩn - 2 người
                                    > 1m4 l > Diện tích: 20m2
                                    Không có rủi ro: Có thể hủy sau, nên hãy đặt ngay hôm nay để có giá tốt.</p>
                                <a href="xac-nhan.html" class="btn btn-booking-now">Đặt ngay</a>
                            </div>
                            <div class="col-12 col-lg-4 col-xl-4 justify-content-end text-right booking-room-div pr-sm-0">

                                <div class="info-room">
                                    <span>1 đêm, 2 người lớn</span>
                                    <span class="room-price">699.00 vnđ</span>
                                    <span class="info-more">Bao bữa sáng</span>
                                    <span class="info-more">Miễn phí hủy phòng</span>
                                    <span class="info-more">Không cần thanh toán trước</span>
                                </div>
                            </div>
                        </div>
                        <div class="row item-room">
                            <div class="col-12 col-lg-3 col-xl-3 p-0 thumbnail-room">
                                <a href="#room-popup" class="various-gallery-modal">
                                    <img src="images/booking/room-1.jpg">
                                </a>
                            </div>
                            <div class="col-12 col-lg-5 col-xl-5 ">
                                <h4>Economy Standard Double</h4>
                                <p>Phòng Giường đôi tiêu chuẩn - 2 người
                                    > 1m4 l > Diện tích: 20m2
                                    Không có rủi ro: Có thể hủy sau, nên hãy đặt ngay hôm nay để có giá tốt.</p>
                                <a href="xac-nhan.html" class="btn btn-booking-now">Đặt ngay</a>
                            </div>
                            <div class="col-12 col-lg-4 col-xl-4 justify-content-end text-right booking-room-div  pr-sm-0">

                                <div class="info-room">
                                    <span>1 đêm, 2 người lớn</span>
                                    <span class="room-price">699.00 vnđ</span>
                                    <span class="info-more">Bao bữa sáng</span>
                                    <span class="info-more">Miễn phí hủy phòng</span>
                                    <span class="info-more">Không cần thanh toán trước</span>
                                </div>
                            </div>
                        </div>
                        <div class="row item-room">
                            <div class="col-12 col-lg-3 col-xl-3 p-0 thumbnail-room">
                                <a href="#room-popup" class="various-gallery-modal">
                                    <img src="images/booking/room-1.jpg">
                                </a>
                            </div>
                            <div class="col-12 col-lg-5 col-xl-5 ">
                                <h4>Economy Standard Double</h4>
                                <p>Phòng Giường đôi tiêu chuẩn - 2 người
                                    > 1m4 l > Diện tích: 20m2
                                    Không có rủi ro: Có thể hủy sau, nên hãy đặt ngay hôm nay để có giá tốt.</p>
                                <a href="xac-nhan.html" class="btn btn-booking-now">Đặt ngay</a>
                            </div>
                            <div class="col-12 col-lg-4 col-xl-4 justify-content-end text-right booking-room-div  pr-sm-0">

                                <div class="info-room">
                                    <span>1 đêm, 2 người lớn</span>
                                    <span class="room-price">699.00 vnđ</span>
                                    <span class="info-more">Bao bữa sáng</span>
                                    <span class="info-more">Miễn phí hủy phòng</span>
                                    <span class="info-more">Không cần thanh toán trước</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Review Hotel -->
                    <div id="policy" class="host-policy mt-5">
                        <span class="policy-header ">CHÍNH SÁCH</span>
                        <div class="policy-content mt-2">
                            <p>
                                <span class="text-third ">Chính sách trẻ em:</span>
                                <br>- Đối với trẻ em dưới 6 tuổi: miễn phí
                                <br>- Đối với trẻ e từ 6 đến dưới 12 tuổi: miễn phí
                                <br>- Đối với trẻ em từ 12 đến dưới 16 tuổi: số tiền thêm vào là 100,000VNĐ
                                <br><span class="text-third">Chính sách hủy phòng:</span>
                                <br>- Hủy phòng trước 03 ngày trước ngày nhận phòng: miễn phí hủy
                                <br>- Hủy trong vòng 03 ngày trước ngày nhận phòng: 50% tiền phòng đêm đầu tiên.</p>
                        </div>
                    </div>
                    <div id="convenient" class="host-convenient mt-3">
                        <span class="policy-header ">TIỆN ÍCH</span>
                        <div class="policy-content mt-2">
                            <div class="row m-0">
                                <div class="col-4 col-xs-6 custom-control custom-checkbox2">
                                    <input type="checkbox" class="custom-control-input" checked disabled id="customCheck" name="example1">
                                    <label class="custom-control-label" for="customCheck">Massage & Spa</label>
                                </div>
                                <div class="col-4 col-xs-6 custom-control custom-checkbox2">
                                    <input type="checkbox" class="custom-control-input" checked disabled id="customCheck1" name="example1">
                                    <label class="custom-control-label" for="customCheck1">Bar - Cafe</label>
                                </div>
                                <div class="col-4 col-xs-6 custom-control custom-checkbox2">
                                    <input type="checkbox" class="custom-control-input" checked disabled id="customCheck2" name="example1">
                                    <label class="custom-control-label" for="customCheck2">Free wifi</label>
                                </div>
                                <div class="col-4 col-xs-6 custom-control custom-checkbox2">
                                    <input type="checkbox" class="custom-control-input" checked disabled id="customCheck" name="example1">
                                    <label class="custom-control-label" for="customCheck">Airport pickup</label>
                                </div>
                                <div class="col-4 col-xs-6 custom-control custom-checkbox2">
                                    <input type="checkbox" class="custom-control-input" checked disabled id="customCheck1" name="example1">
                                    <label class="custom-control-label" for="customCheck1">Shopping</label>
                                </div>
                                <div class="col-4 col-xs-6 custom-control custom-checkbox2">
                                    <input type="checkbox" class="custom-control-input" checked disabled id="customCheck2" name="example1">
                                    <label class="custom-control-label" for="customCheck2">Onsite Parking</label>
                                </div>
                                <div class="col-4 col-xs-6 custom-control custom-checkbox2">
                                    <input type="checkbox" class="custom-control-input" checked disabled id="customCheck" name="example1">
                                    <label class="custom-control-label" for="customCheck">Restaurant</label>
                                </div>
                                <div class="col-4 col-xs-6 custom-control custom-checkbox2">
                                    <input type="checkbox" class="custom-control-input" checked disabled id="customCheck1" name="example1">
                                    <label class="custom-control-label" for="customCheck1">Swimming Pool</label>
                                </div>
                                <div class="col-4 col-xs-6 custom-control custom-checkbox2">
                                    <input type="checkbox" class="custom-control-input" checked disabled id="customCheck2" name="example1">
                                    <label class="custom-control-label" for="customCheck2">Beverage</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="review" class="host-review mt-3">
                        <span class="policy-header ">ĐÁNH GIÁ TỪ NGƯỜI DÙNG</span>
                        <div class="row mt-3">
                            <div class="col-12 col-lg-2">
                                <div class="rating-block mt-2">

                                    <span class="circle-score bold">4.3</span>
                                    <span class="text-third">ĐÁNH GIÁ</span>
                                </div>
                            </div>
                            <div class="col-12 col-lg-6">

                                <div class="pull-left">
                                    <div class="pull-left" style="width:80px; line-height:1;">
                                        <div style="height:9px; margin:5px 0;">Tuyệt vời <span class="glyphicon glyphicon-star"></span></div>
                                    </div>
                                    <div class="pull-left" style="width:230px;">
                                        <div class="progress" style="height:9px; margin:8px 0;">
                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="5" aria-valuemin="0" aria-valuemax="5" style="width: 1000%">
                                                <span class="sr-only">80% Complete (danger)</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="pull-right" style="margin-left:10px;">55%</div>
                                </div>
                                <div class="pull-left">
                                    <div class="pull-left" style="width:80px; line-height:1;">
                                        <div style="height:9px; margin:5px 0;">Rất tốt <span class="glyphicon glyphicon-star"></span></div>
                                    </div>
                                    <div class="pull-left" style="width:230px;">
                                        <div class="progress" style="height:9px; margin:8px 0;">
                                            <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="4" aria-valuemin="0" aria-valuemax="5" style="width: 80%">
                                                <span class="sr-only">80% Complete (danger)</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="pull-right" style="margin-left:10px;">25%</div>
                                </div>
                                <div class="pull-left">
                                    <div class="pull-left" style="width:80px; line-height:1;">
                                        <div style="height:9px; margin:5px 0;">Tốt <span class="glyphicon glyphicon-star"></span></div>
                                    </div>
                                    <div class="pull-left" style="width:230px;">
                                        <div class="progress" style="height:9px; margin:8px 0;">
                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="3" aria-valuemin="0" aria-valuemax="5" style="width: 60%">
                                                <span class="sr-only">80% Complete (danger)</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="pull-right" style="margin-left:10px;">10%</div>
                                </div>
                                <div class="pull-left">
                                    <div class="pull-left" style="width:80px; line-height:1;">
                                        <div style="height:9px; margin:5px 0;">Dở <span class="glyphicon glyphicon-star"></span></div>
                                    </div>
                                    <div class="pull-left" style="width:230px;">
                                        <div class="progress" style="height:9px; margin:8px 0;">
                                            <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="2" aria-valuemin="0" aria-valuemax="5" style="width: 40%">
                                                <span class="sr-only">80% Complete (danger)</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="pull-right" style="margin-left:10px;">10%</div>
                                </div>
                                <div class="pull-left">
                                    <div class="pull-left" style="width:80px; line-height:1;">
                                        <div style="height:9px; margin:5px 0;">Tệ <span class="glyphicon glyphicon-star"></span></div>
                                    </div>
                                    <div class="pull-left" style="width:230px;">
                                        <div class="progress" style="height:9px; margin:8px 0;">
                                            <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="1" aria-valuemin="0" aria-valuemax="5" style="width: 20%">
                                                <span class="sr-only">80% Complete (danger)</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="pull-right" style="margin-left:10px;">5%</div>
                                </div>
                            </div>
                            <div class="col-12 col-lg-4">
                                <span class="d-block p-3 text-center text-secondary">Chia sẻ nhận xét về dịch vụ của
                                    chúng tôi</span>
                                <a href="#" class="btn-review-now">Viết nhận xét của bạn</a>
                            </div>
                        </div>
                    </div>
                    <div id="note" class="host-panel-note">
                        <p>
                            Giữ giá tốt cho kỳ nghỉ sắp tới của bạn
                            <br>Nhận xác nhận tức thì với hủy đặt phòng <span>MIỄN PHÍ</span> tại hầu hết các chỗ
                            nghỉ trên trang web chúng tôi
                        </p>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<!-- END BLOCK TYPE PLACE -->
<!-- ROOM INFO POPUP -->
<div id="room-popup" class="room-popup gallery-modal">
    <div class="container-fluid">
        <div class="row room-info">
            <div class="details col-12 col-sm-4 p-0">
                <div class="details__header details__header_shadowed">
                    <p class="details__header-text" data-selenium="details__header-text">Phòng giường đôi (Double
                        Room)</p>
                    <div>
                        <div class="details__header__price-header">Từ</div>
                        <div><span class="details__header__price-display">254.545</span><span class="details__header__price-currency-after">₫</span></div>
                    </div>
                </div>
                <div class="details__info-area">
                    <!-- Đặc trưng phòng -->
                    <div class="details__info-area-block details__features details__info-area-block_padding-top" data-selenium="Features">
                        <p class="details__info-area-block-header">Điểm đặc trưng</p>
                        <ul class="details__info-area-block-list">
                            <li class="details__features-item"><i class="fal fa-bed details__features-item-icon"></i><span class="details__features-item-description">1 giường đôi lớn</span></li>
                            <li class="details__features-item"><i class="fal fa-home-lg  details__features-item-icon"></i><span class="details__features-item-description">Diện tích phòng: 15 m²</span></li>
                            <li class="details__features-item"><i class="fal fa-door-open details__features-item-icon"></i><span class="details__features-item-description">Hướng phòng: Thành phố</span></li>
                            <li class="details__features-item"><i class="fal fa-shower details__features-item-icon"></i><span class="details__features-item-description">Vòi sen</span></li>
                            <li class="details__features-item"><i class="fal fa-bath details__features-item-icon"></i><span class="details__features-item-description">Phòng tắm chung</span></li>
                            <li class="details__features-item"><i class="fal fa-wifi details__features-item-icon"></i><span class="details__features-item-description">Wifi miễn phí</span></li>
                        </ul>
                    </div>
                    <!-- Loại giường -->
                    <div class="details__info-area-block details__bed-layout" data-selenium="BedLayout">
                        <p class="details__info-area-block-header">Loại giường</p>
                        <ul class="details__info-area-block-list">
                            <li>
                                <ul>
                                    <li class="details__bed-layout-item">
                                        <div class="details__bed-layout-icons"><i class="fal fa-bed-alt details__bed-layout-icon"></i></div>
                                        <div class="details__bed-layout-text"><span class="details__bed-layout-text-block">1 giường đôi lớn</span></div>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <!-- Phòng tắm và vật dụng phòng tắm -->
                    <div class="details__info-area-block details__amenities details__info-area-block_padding-top details__info-area-block_prelined">
                        <p class="details__info-area-block-header">Phòng tắm và vật dụng phòng tắm</p>
                        <ul class="details__info-area-block-list">
                            <li class="details__amenities-item"><i class="fal fa-heat details__amenities-item-icon"></i><span class="details__amenities-item-description">Các loại khăn</span></li>
                            <li class="details__amenities-item"><i class="fal fa-hairdryer details__amenities-item-icon"></i><span class="details__amenities-item-description">Máy sấy tóc</span></li>
                            <li class="details__amenities-item"><i class="ficon ficon-18 ficon-toiletries details__amenities-item-icon"></i><span class="details__amenities-item-description">Vật dụng tắm rửa</span></li>
                        </ul>
                    </div>
                    <!-- Giải trí -->
                    <div class="details__info-area-block details__amenities">
                        <p class="details__info-area-block-header">Giải trí</p>
                        <ul class="details__info-area-block-list">
                            <li class="details__amenities-item"><i class="fal fa-tv details__amenities-item-icon"></i><span class="details__amenities-item-description">Truyền hình cáp/vệ tinh</span></li>
                            <li class="details__amenities-item"><i class="fal fa-wifi details__amenities-item-icon"></i><span class="details__amenities-item-description">Wi-Fi miễn phí trong tất cả các
                                    phòng!</span></li>
                        </ul>
                    </div>
                    <!-- Tiện nghi -->
                    <div class="details__info-area-block details__amenities">
                        <p class="details__info-area-block-header">Tiện nghi</p>
                        <ul class="details__info-area-block-list">
                            <li class="details__amenities-item"><i class="fal fa-volume-slash details__amenities-item-icon"></i><span class="details__amenities-item-description">Cách âm</span></li>
                            <li class="details__amenities-item"><i class="fal fa-shoe-prints details__amenities-item-icon"></i><span class="details__amenities-item-description">Dép đi trong nhà</span></li>
                            <li class="details__amenities-item"><i class="fal fa-alarm-clock details__amenities-item-icon"></i><span class="details__amenities-item-description">Dịch vụ báo thức</span></li>

                            <li class="details__amenities-item"><i class="fal fa-fireplace details__amenities-item-icon"></i><span class="details__amenities-item-description">Sưởi</span></li>
                            <li class="details__amenities-item details__amenities-item_disabled"><i class="fal fa-air-conditioner details__amenities-item-icon"></i><span class="details__amenities-item-description">Điều hòa</span></li>
                        </ul>
                    </div>
                </div>

            </div>
            <div class="gallery col-12 col-sm-8 p-0">
                <div class="room-gallery-slider">
                    <div><img src="images/room/room-1.jpg" alt=""></div>
                    <div><img src="images/room/room-2.jpg" alt=""></div>
                    <div><img src="images/room/room-3.jpg" alt=""></div>
                    <div><img src="images/room/room-4.jpg" alt=""></div>
                    <div><img src="images/room/room-5.jpg" alt=""></div>
                </div>
                <div class="room-gallery-slider-nav">
                    <div><img src="images/room/room-1.jpg" alt=""></div>
                    <div><img src="images/room/room-2.jpg" alt=""></div>
                    <div><img src="images/room/room-3.jpg" alt=""></div>
                    <div><img src="images/room/room-4.jpg" alt=""></div>
                    <div><img src="images/room/room-5.jpg" alt=""></div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END ROOM INFO POPUP -->


@endsection
@section('script')

<script type="text/javascript" src="/fancybox/jquery.fancybox.pack.js?v=2.1.7"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
<!-- Script For page -->
<script type="text/javascript" src="js/booking.js"></script>
<script type="text/javascript" src="js/list-host.js"></script>
<script>
    $(document).ready(function() {
        $('.slider').slick({
            autoplay: true,
            autoplaySpeed: 3000,
            pauseOnHover: true,
            arrows: false,
            dots: false,
            infinite: true,
            fade: true,
            asNavFor: '.slider-nav'
        });
        $('.slider-nav').slick({
            slidesToShow: 8,
            slidesToScroll: 1,
            arrows: false,
            dots: false,
            infinite: false,
            focusOnSelect: true,
            asNavFor: '.slider',
        });




        $(".various").fancybox({
            maxWidth: 1300,
            maxHeight: 900,
            fitToView: false,
            width: '90%',
            height: '90%',
            autoSize: false,
            closeClick: false,
            openEffect: 'none',
            closeEffect: 'none'
        });

        $(".various-gallery-modal").fancybox({
            maxWidth: 1300,
            maxHeight: 680,
            fitToView: false,
            width: '80%',
            height: '680px',
            autoSize: false,
            closeClick: false,
            openEffect: 'none',
            closeEffect: 'none',
            afterLoad: function(current, previous) {
                var rgl = $('.room-gallery-slider').slick({
                    autoplay: false,
                    infinite: true,
                    autoplaySpeed: 3000,
                    pauseOnHover: true,
                    arrows: true,
                    dots: false,

                    fade: false,
                    asNavFor: '.room-gallery-slider-nav',
                    responsive: [{
                            breakpoint: 1024,
                            settings: {
                                slidesToShow: 3,
                                slidesToScroll: 3,
                                infinite: false,
                                dots: false
                            }
                        },
                        {
                            breakpoint: 600,
                            settings: {
                                slidesToShow: 2,
                                slidesToScroll: 2,
                                infinite: false,
                                dots: false
                            }
                        },
                        {
                            breakpoint: 480,
                            settings: {
                                slidesToShow: 1,
                                slidesToScroll: 1
                            }
                        },
                        {
                            breakpoint: 320,
                            settings: {
                                slidesToShow: 1,
                                slidesToScroll: 1
                            }
                        },
                        // You can unslick at a given breakpoint now by adding:
                        // settings: "unslick"
                        // instead of a settings object
                    ]
                });
                var rglnav = $('.room-gallery-slider-nav').slick({
                    slidesToShow: 6,
                    slidesToScroll: 1,
                    infinite: true,
                    arrows: false,
                    dots: false,
                    focusOnSelect: true,
                    margin: 10,
                    asNavFor: '.room-gallery-slider',
                    responsive: [{
                            breakpoint: 1024,
                            settings: {
                                slidesToShow: 3,
                                slidesToScroll: 3,
                                infinite: false,
                                dots: false
                            }
                        },
                        {
                            breakpoint: 600,
                            settings: {
                                slidesToShow: 2,
                                slidesToScroll: 2,
                                infinite: false,
                                dots: false
                            }
                        },
                        {
                            breakpoint: 480,
                            settings: {
                                slidesToShow: 3,
                                slidesToScroll: 1
                            }
                        },
                        {
                            breakpoint: 320,
                            settings: {
                                slidesToShow: 3,
                                slidesToScroll: 1
                            }
                        },
                        // You can unslick at a given breakpoint now by adding:
                        // settings: "unslick"
                        // instead of a settings object
                    ]
                });
            },
            afterClose: function() {
                $('.room-gallery-slider').slick('unslick');
                $('.room-gallery-slider-nav').slick('unslick');
            }
        });

        $('.scroll-to-ele').click(function(e) {

            var ele = $(this).attr('data-scroll');
            $([document.documentElement, document.body]).animate({
                scrollTop: $('#' + ele).offset().top - 200
            }, 2000);
        });
    });
</script>
<!-- End Script For page -->
@endsection