@foreach($hosts as $index => $host)
<div class="row border mt-3 p-2 item-list-place">
    <div class="thumbnail-place">
        <a href="{{ $host->host_type }}/{{ $host->host_slug }}/{{ $host->id }}{{ $host->host_query }}">
            <img src="{{ str_replace('/uploads/files/', '/thumbs/', $host->host_thumbnail) }}" class="img-fluid w-100">
            <!-- <img src="{{ str_replace('/uploads/files/', '/thumbnail/', $host->host_thumbnail) }}" class="img-fluid w-100"> -->
        </a>
    </div>
    <div class="info-place py-2 py-sm-0 px-0 px-sm-4">
        <h4>{{ $host->host_name }}</h4>
        <span class="place-address text-primary" data-toggle="tooltip" title="{{ $host->host_address }}, {{ $host->district_name }}, {{ $host->province_name }}">{{ $host->host_address }}, {{ $host->ward_name }}, {{ $host->district_name }}, {{ $host->province_name }}</span>
        {!! $host->host_description !!}
        <ul class="list-unstyled list-inline list-convenient" id="list-convenient">
            <li class="convenient-item"><i class="fas fa-wifi"></i>
            </li>
            <li class="convenient-item"><i class="fas fa-car"></i>
            </li>
            <li class="convenient-item"><i class="fas fa-tv"></i>
            </li>
            <li class="convenient-item"><i class="fas fa-parking"></i>
            </li>
        </ul>
    </div>
    <div class="price-place">
        <span class="score-rating text-primary d-block font-weight-bold">{{ $host->rating_text }}</span>
        <span class="d-block mb-1 mb-sm-4">{{ $host->user_rating }} {{ __('frontsite.booking.rate') }}</span>
        @if(!empty($host->room))
        @if(!isset($host->room->sale_for_room) || $host->room->sale_for_room < 0 || $host->room->sale_for_room !=  $host->lowest_room_rates)
        <span class="regular-price"></span>
        <span class="deal-price">{{ number_format($host->lowest_room_rates, 0, '.', ',') }} {{ __('frontsite.booking.currency') }}</span>
        
        @else
        <span class="regular-price">{{ number_format($host->room->price_one_night, 0, '.', ',') }} {{ __('frontsite.booking.currency') }}</span>
        <span class="deal-price">{{ number_format($host->lowest_room_rates, 0, '.', ',') }} {{ __('frontsite.booking.currency') }}</span>
        @endif
        <span class="status-place">{{ __('frontsite.booking.available_room')}}</span>
        @else
        <span class="regular-price"></span>
        <span class="deal-price"></span>
        <span class="status-place text-third">{{ __('frontsite.booking.out_of_room')}}</span>
        @endif
        
        
        <div class="bui-review-score__badge" aria-label="Đạt điểm {{ $host->rating }}"> {{ number_format($host->rating, 1, ',', '')  }} </div>
    </div>
</div>
@endforeach