<!-- START BLOCK LOCATION -->
<section class="" id="block-location">
    <div class="container">
        <div class="row mt-4 mb-4 justify-content-start">
            <div class="col-12">
                <h3 class="text-primary">{{ __('frontsite.booking.header-list-hotel') }}</h3>
                <!-- VỊ TRÍ TUYỆT VỜI - ƯU ĐÃI TỐT -->
            </div>
        </div>
        <div class="row mt-4 mb-4 justify-content-start">

            @foreach($hosts as $index => $host)
            <div class="col-12 col-xl-3 item-place mb-3">
                <a href="{{ $host->host_type }}/{{ $host->host_slug }}/{{ $host->id }}" class="place-thumbnail">
                    <img src="{{ str_replace('/uploads/files/', '/thumbs/', $host->host_thumbnail) }}" class="w-100">
                </a>
                <div class="col-12">
                    <h4 class="place-name"><a href="{{ $host->host_type }}/{{ $host->host_slug }}/{{ $host->id }}">{{ $host->host_name }}</a></h4>
                    <div class="place-rate ">
                        <span class="float-left"><i class="text-warning fa fa-star"></i></span>
                        <span class="float-left"><i class="text-warning fa fa-star"></i></span>
                        <span class="float-left"><i class="text-warning fa fa-star"></i></span>
                        <span class="float-left"><i class="text-warning fa fa-star"></i></span>
                        <span class="float-left"><i class="text-warning fa fa-star"></i></span>
                    </div>
                    @if(!empty($host->room))
                    <div class="place-price d-flex justify-content-between">
                        <span class="deal-price">{{ $host->room->price_one_night }} {{ __('frontsite.booking.currency') }}</span>
                        @if(isset($host->room->price_one_night))
                        <span class="regular-price">{{ $host->room->price_one_night }} {{ __('frontsite.booking.currency') }}</span>
                        @endif
                    </div>
                    @endif
                    <div class="place-price d-flex justify-content-between">
                        <span class="text-primary">{{ $host->user_rating }} {{ __('frontsite.booking.rate') }}</span>
                        <a href="{{ $host->host_type }}/{{ $host->host_slug }}/{{ $host->id }}" class="btn-place-review">{{ __('frontsite.booking.rate') }}</a>
                    </div>
                </div>

            </div>
            @endforeach
            
        </div>

    </div>

</section>
<!-- END BLOCK LOCATION -->