<aside id="list-place-sidebar" class="col-12 col-xl-4">
    <h3 class="text-primary">{{ __('frontsite.booking.result_search') }}</h3>
    <span class="time-booking">{{ !empty(Request::get('checkin'))? Request::get('checkin') : '' }} - {{ !empty(Request::get('checkout'))? Request::get('checkout') : '' }}</span>
    <div class="panel-map mt-3">
        <img src="images/list-place/map-sidebar.jpg">
        <a href="#list-position-host" class="big-map various">Xem trên bản đồ</a>
    </div>
    <div class="panel card mt-3 no-border accordion">
        <article class="card-group-item rating-select">
            <header class="card-header " data-toggle="collapse" data-target="#filterStar" aria-expanded="true" aria-controls="filterStar">
                <h6 class="title">{{ __('frontsite.booking.rate_star') }}</h6>

            </header>
            <div id="filterStar" class="filter-content no-border collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                <div class="card-body">
                    <div class="custom-control custom-checkbox">

                        <input type="checkbox" class="custom-control-input" id="Check1" name="host_star" value="1">

                        <label class="custom-control-label" for="Check1" >
                            <span class="float-left"><i class="text-warning fa fa-star"></i></span>
                            <span class="float-left"><i class="fa fa-star"></i></span>
                            <span class="float-left"><i class="fa fa-star"></i></span>
                            <span class="float-left"><i class="fa fa-star"></i></span>
                            <span class="float-left"><i class="fa fa-star"></i></span>
                        </label>
                    </div> <!-- form-check.// -->

                    <div class="custom-control custom-checkbox">

                        <input type="checkbox" class="custom-control-input" id="Check2" name="host_star" value="2">
                        <label class="custom-control-label" for="Check2">
                            <span class="float-left"><i class="text-warning fa fa-star"></i></span>
                            <span class="float-left"><i class="text-warning fa fa-star"></i></span>
                            <span class="float-left"><i class="fa fa-star"></i></span>
                            <span class="float-left"><i class="fa fa-star"></i></span>
                            <span class="float-left"><i class="fa fa-star"></i></span>
                        </label>
                    </div> <!-- form-check.// -->

                    <div class="custom-control custom-checkbox">

                        <input type="checkbox" class="custom-control-input" id="Check3" name="host_star" value="3">
                        <label class="custom-control-label" for="Check3">
                            <span class="float-left"><i class="text-warning fa fa-star"></i></span>
                            <span class="float-left"><i class="text-warning fa fa-star"></i></span>
                            <span class="float-left"><i class="text-warning fa fa-star"></i></span>
                            <span class="float-left"><i class="fa fa-star"></i></span>
                            <span class="float-left"><i class="fa fa-star"></i></span>
                        </label>
                    </div> <!-- form-check.// -->

                    <div class="custom-control custom-checkbox">

                        <input type="checkbox" class="custom-control-input" id="Check4" name="host_star" value="4">
                        <label class="custom-control-label" for="Check4">
                            <span class="float-left"><i class="text-warning fa fa-star"></i></span>
                            <span class="float-left"><i class="text-warning fa fa-star"></i></span>
                            <span class="float-left"><i class="text-warning fa fa-star"></i></span>
                            <span class="float-left"><i class="text-warning fa fa-star"></i></span>
                            <span class="float-left"><i class="fa fa-star"></i></span>
                        </label>
                    </div> <!-- form-check.// -->
                    <div class="custom-control custom-checkbox">

                        <input type="checkbox" class="custom-control-input" id="Check5" name="host_star" value="5">
                        <label class="custom-control-label" for="Check5">
                            <span class="float-left"><i class="text-warning fa fa-star"></i></span>
                            <span class="float-left"><i class="text-warning fa fa-star"></i></span>
                            <span class="float-left"><i class="text-warning fa fa-star"></i></span>
                            <span class="float-left"><i class="text-warning fa fa-star"></i></span>
                            <span class="float-left"><i class="text-warning fa fa-star"></i></span>
                        </label>
                    </div> <!-- form-check.// -->
                </div> <!-- card-body.// -->
            </div>
        </article> <!-- card-group-item.// -->
        <article class="card-group-item price-filter">
            <header class="card-header" data-toggle="collapse" data-target="#filterPrice" aria-expanded="true" aria-controls="filterPrice">
                <h6 class="title">{{ __('frontsite.booking.price') }}</h6>
            </header>

            <div id="filterPrice" class="filter-content no-border collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                <div class="card-body">
                    <div class="slider-wrapper mt-3 mb-3">
                        <input class="input-range" data-slider-id='ex12cSlider' type="text" data-slider-step="1" data-slider-value="20000, 50000" data-slider-min="100" data-slider-max="150000" data-slider-range="true" data-slider-tooltip_split="true" />
                    </div>
                </div>
            </div>
        </article>
        <!-- Widget Địa điểm -->
        <article class="card-group-item">
            <header class="card-header" data-toggle="collapse" data-target="#filterPlace" aria-expanded="true" aria-controls="filterPlace">
                <h6 class="title">{{ __('frontsite.booking.place') }}</h6>
            </header>
            <div id="filterPlace" class="filter-content collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                <div class="card-body">
                    <div class="custom-control custom-checkbox">
                        <span class="float-right badge badge-light round">52</span>
                        <input type="checkbox" class="custom-control-input" id="Check11" name="district_id" value="185">
                        <label class="custom-control-label" for="Check11">Đà Lạt</label>
                    </div> <!-- form-check.// -->

                    <div class="custom-control custom-checkbox">
                        <span class="float-right badge badge-light round">132</span>
                        <input type="checkbox" class="custom-control-input" id="Check12" name="province_id" value="2">
                        <label class="custom-control-label" for="Check12">Hà Nội</label>
                    </div> <!-- form-check.// -->

                    <div class="custom-control custom-checkbox">
                        <span class="float-right badge badge-light round">17</span>
                        <input type="checkbox" class="custom-control-input" id="Check13" name="province_id" value="10">
                        <label class="custom-control-label" for="Check13">Vũng Tàu</label>
                    </div> <!-- form-check.// -->

                    <div class="custom-control custom-checkbox">
                        <span class="float-right badge badge-light round">7</span>
                        <input type="checkbox" class="custom-control-input" id="Check14" name="province_id" value="1">
                        <label class="custom-control-label" for="Check14">TP. Hồ Chí Minh</label>
                    </div> <!-- form-check.// -->
                    <div class="custom-control custom-checkbox">
                        <span class="float-right badge badge-light round">17</span>
                        <input type="checkbox" class="custom-control-input" id="Check15" name="district_id" value="87">
                        <label class="custom-control-label" for="Check15">Nha Trang</label>
                    </div> <!-- form-check.// -->

                    <div class="custom-control custom-checkbox">
                        <span class="float-right badge badge-light round">7</span>
                        <input type="checkbox" class="custom-control-input" id="Check16" name="province_id" value="18">
                        <label class="custom-control-label" for="Check16">Quảng Ninh</label>
                    </div> <!-- form-check.// -->
                </div> <!-- card-body.// -->
            </div>
        </article> <!-- card-group-item.// -->

        <!-- Widget Hình thức lưu trú -->
        <article class="card-group-item">
            <header class="card-header" data-toggle="collapse" data-target="#filterType" aria-expanded="true" aria-controls="filterType">
                <h6 class="title">{{ __('frontsite.booking.type') }}</h6>
            </header>
            <div id="filterType" class="filter-content collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                <div class="card-body">
                    <div class="custom-control custom-checkbox">
                        <span class="float-right badge badge-light round">{{ $type_count['hotel'] }}</span>
                        <input type="checkbox" class="custom-control-input" name="type" id="Check21" value="hotel">
                        <label class="custom-control-label" for="Check21">{{ __('frontsite.booking.hotel') }}</label>
                    </div> <!-- form-check.// -->

                    <div class="custom-control custom-checkbox">
                        <span class="float-right badge badge-light round">{{ $type_count['apartment'] }}</span>
                        <input type="checkbox" class="custom-control-input" name="type" id="Check22" value="apartment">
                        <label class="custom-control-label" for="Check22">{{ __('frontsite.booking.apartment') }}</label>
                    </div> <!-- form-check.// -->

                    <div class="custom-control custom-checkbox">
                        <span class="float-right badge badge-light round">{{ $type_count['resort'] }}</span>
                        <input type="checkbox" class="custom-control-input" name="type" id="Check23" value="resort">
                        <label class="custom-control-label" for="Check23">{{ __('frontsite.booking.resort') }}</label>
                    </div> <!-- form-check.// -->

                    <div class="custom-control custom-checkbox">
                        <span class="float-right badge badge-light round">{{ $type_count['villa'] }}</span>
                        <input type="checkbox" class="custom-control-input" name="type" id="Check24" value="villa">
                        <label class="custom-control-label" for="Check24">{{ __('frontsite.booking.villa') }}</label>
                    </div> <!-- form-check.// -->
                    <div class="custom-control custom-checkbox">
                        <span class="float-right badge badge-light round">{{ $type_count['homestay'] }}</span>
                        <input type="checkbox" class="custom-control-input" name="type" id="Check25" value="homestay">
                        <label class="custom-control-label" for="Check25">{{ __('frontsite.booking.homestay') }}</label>
                    </div> <!-- form-check.// -->

                    <div class="custom-control custom-checkbox">
                        <span class="float-right badge badge-light round">{{ $type_count['campsite'] }}</span>
                        <input type="checkbox" class="custom-control-input" name="type" id="Check26" value="campsite">
                        <label class="custom-control-label" for="Check26">{{ __('frontsite.booking.campsite') }}</label>
                    </div> <!-- form-check.// -->
                </div> <!-- card-body.// -->
            </div>
        </article> <!-- card-group-item.// -->
    </div>
</aside>