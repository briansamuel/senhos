@extends('frontsite.index')
@section('title', __('seo.recruitment.title'))
@section('title_ab', __('seo.recruitment.title_ab'))
@section('keyword', __('seo.recruitment.keyword') )
@section('description', __('seo.recruitment.description'))
@section('style')

@endsection
@section('content')
<!-- START Block Banner  -->

@include('frontsite.elements.banner')
<!-- END Block Banner -->
<!-- START BREADCRUMB -->
@include('frontsite.elements.breadcrumb')
<!-- END BREADCRUMB -->
<div class="main-wrapper mt-4 mb-4">
    <div class="container">
        <div class="row list-post-row">

            <!-- START SIDEBAR -->
            @include('frontsite.sidebar.sidebar_news')
            <!-- END SIDEBAR -->
            
            <div id="list-recruitment" class="col-12 col-xl-8 mt-4 pt-3 list-view">
                @if (count($recruitments) > 0)
                    <section class="news">
                        @include('frontsite.recruitments.elements.load')
                    </section>
                @endif
            </div>
        </div>
    </div>
</div>

<!-- START TOP FOOTER 1 -->
@include('frontsite.elements.top-footer-1')
<!-- END TOP FOOTER 1 -->
<!-- START TOP FOOTER 2  -->
@include('frontsite.elements.top-footer-2')
<!-- END TOP FOOTER 2 -->
@endsection
@section('script')
    <script type="text/javascript">

        $(function() {
            $('body').on('click', '.pagination a', function(e) {
                e.preventDefault();

                $('#load a').css('color', '#dfecf6');

                var url = $(this).attr('href');
                getNews(url);
                window.history.pushState("", "", url);
            });

            function getNews(url) {
                $.ajax({
                    url : url
                }).done(function (data) {
                    $('.news').html(data);
                }).fail(function () {
                    alert('Có lỗi xảy ra, vui lòng thử lại sau.');
                });
            }
        });

    </script>
@endsection