<div id="load" style="position: relative;">
@foreach($recruitments as $index => $recruitment)
    <article class="row item-post">
        <div class="thumbnail-post">
            <a href="{{ $recruitment->post_slug }}.html" class="effect-zoom-in">
                <img src="{{ $recruitment->post_thumbnail }}" onError="this.onerror=null;this.src='/images/default/no-image.jpg';"  class="img-fluid w-100 h-100">
            </a>
        </div>
        <div class="info-post">
            <a href="{{ $recruitment->post_slug }}.html">
                <h3>{{ $recruitment->post_title }}</h3>
            </a>
            <div class="timeline-post">
                <span>{{ $recruitment->post_author }}</span>
                <span>{{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $recruitment->created_at)->format('d/m/Y') }}</span>
                <span>1 lượt xem</span>
            </div>
            <p>{{ html_entity_decode(strip_tags($recruitment->post_description)) }}</p>
            <a href="{{ $recruitment->post_slug }}.html" class="btn-post-more">{{ __('frontsite.news.loadmore') }}</a>
        </div>
    </article>
@endforeach

{{$recruitments->links()}}