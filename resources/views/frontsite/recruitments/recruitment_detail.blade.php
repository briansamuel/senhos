@extends('frontsite.index')
@section('title', $recruitment->post_title)
@section('title_ab', $recruitment->post_title)
@section('keyword', $recruitment->post_seo_keyword ? $recruitment->post_seo_keyword : $recruitment->post_title)
@section('description', $recruitment->post_seo_description ? $recruitment->post_seo_description : $recruitment->post_title)
@section('style')

@endsection
@section('content')
<!-- START Block Banner  -->

@include('frontsite.elements.banner')
<!-- END Block Banner -->
<!-- START BREADCRUMB -->
@include('frontsite.elements.breadcrumb')
<!-- END BREADCRUMB -->
<div class="main-wrapper mt-4 mb-4">
    <div class="container">
        <div class="row list-post-row">
            <aside class="col-12 col-xl-4">
                <div class="widget lastest-news">
                    <h6 class="widget-title">TIN XEM NHIỀU NHẤT</h6>
                    <div class="widger-content">
                        <div class="item-news">
                            <a class="thumbnail-news effect-zoom-in" href="#">
                                <img src="images/tin-tuc/bai-viet-1.jpg" class="img-fluid">
                            </a>
                            <div class="info-news">
                                <a href="#">
                                    <h4>Top 05 SPA không thể bỏ qua tại Phú Quốc</h4>
                                </a>
                                <span>Admin SenHos</span>
                                <span>20/01/2020</span>
                            </div>
                        </div>
                        <div class="item-news">
                            <a class="thumbnail-news effect-zoom-in" href="#">
                                <img src="images/tin-tuc/bai-viet-2.jpg" class="img-fluid">
                            </a>
                            <div class="info-news">
                                <a href="#">
                                    <h4>Top 05 SPA không thể bỏ qua tại Phú Quốc</h4>
                                </a>
                                <span>Admin SenHos</span>
                                <span>20/01/2020</span>
                            </div>
                        </div>
                        <div class="item-news">
                            <a class="thumbnail-news effect-zoom-in" href="#">
                                <img src="images/tin-tuc/bai-viet-3.jpg" class="img-fluid">
                            </a>
                            <div class="info-news">
                                <a href="#">
                                    <h4>Top 05 SPA không thể bỏ qua tại Phú Quốc</h4>
                                </a>
                                <span>Admin SenHos</span>
                                <span>20/01/2020</span>
                            </div>
                        </div>
                        <div class="item-news ">
                            <a class="thumbnail-news effect-zoom-in" href="#">
                                <img src="images/tin-tuc/bai-viet-4.jpg" class="img-fluid">
                            </a>
                            <div class="info-news">
                                <a href="#">
                                    <h4>Top 05 SPA không thể bỏ qua tại Phú Quốc</h4>
                                </a>
                                <span>Admin SenHos</span>
                                <span>20/01/2020</span>
                            </div>
                        </div>
                    </div>
                    <!-- Widget Video -->
                    <div class="widget lastest-videos mt-5">
                        <h6 class="widget-title">THƯ VIỆN VIDEO</h6>
                        <div class="widger-content">
                            <div class="row">
                                <div class="col-12 col-xl-6 item-video ">
                                    <a href="#" class="thumbnail-video ">
                                        <img src="images/tin-tuc/bai-viet-7.jpg" class="img-fluid">
                                        <span>GIỚI THIỆU CÔNG TY SENHOS</span>
                                    </a>
                                </div>
                                <div class="col-12 col-xl-6 item-video">
                                    <a href="#" class="thumbnail-video ">
                                        <img src="images/tin-tuc/bai-viet-8.jpg" class="img-fluid">
                                        <span>CLIP TRAVEL PLACE</span>
                                    </a>
                                </div>
                                <div class="col-12 col-xl-6 item-video">
                                    <a href="#" class="thumbnail-video ">
                                        <img src="images/tin-tuc/bai-viet-9.jpg" class="img-fluid">
                                        <span>COCONUT ISLAND</span>
                                    </a>
                                </div>
                                <div class="col-12 col-xl-6 item-video">
                                    <a href="#" class="thumbnail-video ">
                                        <img src="images/tin-tuc/bai-viet-10.jpg" class="img-fluid">
                                        <span>PARIS VILLAS - OUTLETS</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Widget Deal -->
                    <div class="widget top-deal mt-5">

                        <div class="widger-content">
                            <div class="item-top-deal">
                                <a href="#" class="thumbnail-top-deal effect-zoom-in">
                                    <img src="images/tin-tuc/sale.jpg" class="img-fluid">
                                    <span class="discount-deal">Giảm 10%</span>
                                </a>
                                <div class="info-top-deal">
                                    <h5>Sonata Resort & Spa</h5>
                                    <span class="location">Phú Quốc</span>
                                    <span class="type-room font-weight-bold">Deluxe Double Or Twin Room With Garden
                                        View</span>
                                    <span class="font-weight-bold">Thời gian lưu trú</span>
                                    <span class="checkin-time">15/01/2020 - 31/01/2020</span>
                                    <div class="price-top-deal">
                                        <span class="regular-price">
                                            1.320.281 VND
                                        </span>
                                        <span class="sale-price">
                                            1.188.254 VND
                                        </span>
                                        <div class="btn-around">
                                            <a href="#" class="top-sale-booking">Đặt ngay</a>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            </aside>
            <div id="post-{{ $recruitment->id }}" class="col-12 col-xl-8 mt-0 pt-0 pt-sm-3 mt-sm-4 single-post">
                <div class="post-header">
                    <h2 class="post-title">{{ $recruitment->post_title }}</h2>
                </div>
                <div class="post-meta">
                    <span class="post-time"><i class="fas fa-clock"></i> {{ $recruitment->created_at }}</span>
                    <span class="ml-4 mr-4">|</span>
                    <span class="post-view">1 lượt xem</span>
                </div>
                <div class="post-content">

                    {!! $recruitment->post_content !!}
                    <div class="categories-post">
                        <span>Chủ đề:</span> <a href="#">Trong nước</a>, <a href="#">Du lịch</a>, <a href="#">Tour Tết</a>
                    </div>
                    <div class="tags-post">
                        <span>Tags:</span> <a href="#">Du lịch</a>, <a href="#">SenHos</a>, <a href="#">Phú Quốc</a>, <a href="#">Trốn tết</a>, <a href="#">Nghỉ Dưỡng 5 sao</a>
                    </div>


                </div>
                <div class="related-posts mt-4">
                    <div class="row ">
                        <div class="col-12">
                            <div class="header-related-block">
                                <span>Các tin tức khác</span>
                            </div>

                        </div>

                    </div>
                    <div class="row post-3-col">
                        <div class="col-12 col-xl-4 item-related-post">
                            <a href="#" class="thumbnail-related-post effect-zoom-in">
                                <img src="images/tin-tuc/bai-viet-11.jpg">
                            </a>
                            <a href="#" title="Những nơi nhất định phải đến vào mùa đông">
                                <h4>Những nơi nhất định phải phải đến khi đi du lịch Phú Quốc</h4>
                            </a>
                            <span class="related-post-time"><i class="fas fa-clock"></i> 05/2020 09:53</span>
                        </div>
                        <div class="col-12 col-xl-4 item-related-post">
                            <a href="#" class="thumbnail-related-post effect-zoom-in">
                                <img src="images/tin-tuc/bai-viet-12.jpg">
                            </a>
                            <a href="#" title="Những nơi nhất định phải đến vào mùa đông">
                                <h4>Những nơi nhất định phải phải đến khi đi du lịch Phú Quốc</h4>
                            </a>
                            <span class="related-post-time"><i class="fas fa-clock"></i> 05/2020 09:53</span>
                        </div>
                        <div class="col-12 col-xl-4 item-related-post">
                            <a href="#" class="thumbnail-related-post effect-zoom-in">
                                <img src="images/tin-tuc/bai-viet-13.jpg">
                            </a>
                            <a href="#" title="Những nơi nhất định phải đến vào mùa đông">
                                <h4>Những nơi nhất định phải phải đến khi đi du lịch Phú Quốc</h4>
                            </a>
                            <span class="related-post-time"><i class="fas fa-clock"></i> 05/2020 09:53</span>
                        </div>

                    </div>

                    <div class="row post-2-col">
                        <div class="col-12">
                            <hr class="bg-dark mb-4">
                        </div>
                        <div class="col-12 col-xl-6 item-news item-related-post">
                            <a class="thumbnail-news effect-zoom-in" href="#">
                                <img src="images/tin-tuc/bai-viet-1.jpg" class="img-fluid">
                            </a>
                            <div class="info-news">
                                <a href="#">
                                    <h4>Top 05 SPA không thể bỏ qua tại Phú Quốc</h4>
                                </a>
                                <p>Top 05 SPA không thể bỏ qua tại Phú Quốc Top 05 SPA không thể bỏ qua tại Phú Quốc</p>
                                <span class="related-post-time"><i class="fas fa-clock"></i> 20/01/2020</span>
                            </div>
                        </div>
                        <div class="col-12 col-xl-6 item-news item-related-post">
                            <a class="thumbnail-news effect-zoom-in" href="#">
                                <img src="images/tin-tuc/bai-viet-4.jpg" class="img-fluid">
                            </a>
                            <div class="info-news">
                                <a href="#">
                                    <h4>Top 05 SPA không thể bỏ qua tại Phú Quốc</h4>
                                </a>
                                <p>Top 05 SPA không thể bỏ qua tại Phú Quốc Top 05 SPA không thể bỏ qua tại Phú Quốc</p>
                                <span class="related-post-time"><i class="fas fa-clock"></i> 20/01/2020</span>
                            </div>
                        </div>
                        <div class="col-12 col-xl-6 item-news item-related-post">
                            <a class="thumbnail-news effect-zoom-in" href="#">
                                <img src="images/tin-tuc/bai-viet-2.jpg" class="img-fluid">
                            </a>
                            <div class="info-news">
                                <a href="#">
                                    <h4>Top 05 SPA không thể bỏ qua tại Phú Quốc</h4>
                                </a>
                                <p>Top 05 SPA không thể bỏ qua tại Phú Quốc Top 05 SPA không thể bỏ qua tại Phú Quốc</p>
                                <span class="related-post-time"><i class="fas fa-clock"></i> 20/01/2020</span>
                            </div>
                        </div>
                        <div class="col-12 col-xl-6 item-news item-related-post">
                            <a class="thumbnail-news effect-zoom-in" href="#">
                                <img src="images/tin-tuc/bai-viet-3.jpg" class="img-fluid">
                            </a>
                            <div class="info-news">
                                <a href="#">
                                    <h4>Top 05 SPA không thể bỏ qua tại Phú Quốc</h4>
                                </a>
                                <p>Top 05 SPA không thể bỏ qua tại Phú Quốc Top 05 SPA không thể bỏ qua tại Phú Quốc</p>
                                <span class="related-post-time"><i class="fas fa-clock"></i> 20/01/2020</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- START TOP FOOTER 1 -->
@include('frontsite.elements.top-footer-1')
<!-- END TOP FOOTER 1 -->
<!-- START TOP FOOTER 2  -->
@include('frontsite.elements.top-footer-2')
<!-- END TOP FOOTER 2 -->
@endsection
@section('script')

@endsection