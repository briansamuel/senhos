@extends('admin.index')
@section('page-header', 'Agent')
@section('page-sub_header', 'Danh sách agent')
@section('style')

@endsection
@section('content')
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
                                    <span class="kt-portlet__head-icon">
                                        <i class="kt-font-brand flaticon2-line-chart"></i>
                                    </span>
            <h3 class="kt-portlet__head-title">
                Danh sách Agent
            </h3>
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-wrapper">
                <a href="" class="btn btn-clean btn-icon-sm">
                    <i class="la la-long-arrow-left"></i>
                    Back
                </a>
                &nbsp;
                <div class="dropdown dropdown-inline">
                    <a href="/agent/add" class="btn btn-brand btn-icon-sm"><i class="flaticon2-plus"></i> Add Agent</a>
                </div>
            </div>
        </div>
    </div>
    <div class="kt-portlet__body">

        <!--begin: Search Form -->
        <div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
            <div class="row align-items-center">
                <div class="col-xl-12 order-2 order-xl-1">
                    <div class="row align-items-center">
                        <div class="col-md-3 kt-margin-b-20-tablet-and-mobile">
                            <label>Email:</label>
                            <div class="kt-input-icon kt-input-icon--left">
                                <input type="text" class="form-control" placeholder="Vui lòng nhập từ khóa..." id="kt_form_email">
                                <span class="kt-input-icon__icon kt-input-icon__icon--left">
                                            <span><i class="la la-search"></i></span>
                                        </span>
                            </div>
                        </div>
                        <div class="col-md-3 kt-margin-b-20-tablet-and-mobile">
                            <label>Username:</label>
                            <div class="kt-input-icon kt-input-icon--left">
                                <input type="text" class="form-control" placeholder="Vui lòng nhập từ khóa..." id="kt_form_username">
                                <span class="kt-input-icon__icon kt-input-icon__icon--left">
                                            <span><i class="la la-search"></i></span>
                                        </span>
                            </div>
                        </div>
                        <div class="col-md-3 kt-margin-b-20-tablet-and-mobile">
                            <div class="form__group kt-form__group--inline">
                                <label>Tình trạng:</label>
                                <select class="form-control bootstrap-select" id="kt_form_status">
                                    <option value="">All</option>
                                    <option value="inactive">Inactive</option>
                                    <option value="deactive">Deactive</option>
                                    <option value="active">Active</option>
                                    <option value="blocked">Blocked</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3 kt-margin-b-20-tablet-and-mobile" id="kt_form_daterange1">
                            <div class="form__group kt-form__group--inline">
                                <div class="kt-form__label">
                                    <label>Theo ngày tạo:</label>
                                </div>
                                <input type='text' class="form-control" id="kt_daterangepicker_1" readonly placeholder="Chọn thời gian" value=""/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 order-1 order-xl-2 kt-align-right">
                    <a href="#" class="btn btn-default kt-hidden">
                        <i class="la la-cart-plus"></i> New Order
                    </a>
                    <div class="kt-separator kt-separator--border-dashed kt-separator--space-lg d-xl-none"></div>
                </div>
            </div>
        </div>

        <!--begin: Selected Rows Group Action Form -->
        <div class="kt-form kt-form--label-align-right kt-margin-t-20 collapse" id="kt_datatable_group_action_form1">
            <div class="row align-items-center">
                <div class="col-xl-12">
                    <div class="kt-form__group kt-form__group--inline">
                        <div class="kt-form__label kt-form__label-no-wrap">
                            <label class="kt-font-bold kt-font-danger-">Chọn
                                <span id="kt_datatable_selected_number1">0</span> dòng:</label>
                                {{csrf_field()}}
                        </div>

                        <div class="kt-form__control">
                            <div class="btn-toolbar">
                                <div class="dropdown kt-margin-r-5">
                                    <button type="button" class="btn btn-brand btn-sm dropdown-toggle" data-toggle="dropdown">
                                        Cập Nhập Trạng Thái
                                    </button>
                                    <div class="dropdown-menu">
                                        <a class="dropdown-item kt_datatable_update_status_all" href="javascript:void(0)" attr="inactive">Inactive</a>
                                        <a class="dropdown-item kt_datatable_update_status_all" href="javascript:void(0)" attr="deactive">Deactive</a>
                                        <a class="dropdown-item kt_datatable_update_status_all" href="javascript:void(0)" attr="active">Active</a>
                                        <a class="dropdown-item kt_datatable_update_status_all" href="javascript:void(0)" attr="blocked">Blocked</a>
                                    </div>
                                </div>
                                <button class="btn btn-sm btn-danger" id="kt_datatable_delete_all">Xóa Tất Cả</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--end: Selected Rows Group Action Form -->
    </div>
    <div class="kt-portlet__body kt-portlet__body--fit">
        @include('admin.elements.alert_flash')
        <!--begin: Datatable -->
        <div class="kt-datatable" id="ajax_data"></div>

        <!--end: Datatable -->
    </div>
</div>
@endsection
@section('script')
    <!--begin::Page Vendors(used by this page) -->


    <!--end::Page Vendors -->
    <script src="assets/js/pages/components/portlets/tools.js" type="text/javascript"></script>
    <script src="assets/js/pages/crud/forms/widgets/select2.js" type="text/javascript"></script>
    <script src="assets/js/pages/crud/forms/widgets/bootstrap-daterangepicker.js" type="text/javascript"></script>
    <script src="admin/js/pages/agent/agents.js" type="text/javascript"></script>
@endsection
