@extends('admin.index')
@section('page-header', 'Giao Diện')
@section('page-sub_header', 'Tùy biến')
@section('style')
<link rel="stylesheet" href="admin/plugins/fancybox/jquery.fancybox.min.css" />
@endsection
@section('content')
<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

    <!-- begin:: Subheader -->
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title"> Tùy biến </h3>
            </div>
            <div class="kt-subheader__main">
                <select class="form-control" id="language" name="language" onchange="updateOptionByLanguage()">
                    @foreach($arrayLang as $key=>$lang)
                        <option value="{{$key}}" {{Request::get('lang') === $key ? 'selected' : ''}}>{{$lang}}</option>
                     @endforeach
                </select>
            </div>
        </div>
    </div>

    <!-- end:: Subheader -->

    <!-- begin:: Content -->
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="col-md-12">

                <!--begin::Portlet-->
                <div class="kt-portlet">

                    <form class="kt-form" id="kt_edit_form">
                        <div class="kt-portlet__body">
                            <ul class="nav nav-tabs nav-tabs-line nav-tabs-line-3x nav-tabs-bold nav-tabs-line-primary" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#general" role="tab" aria-selected="false">General</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#header" role="tab" aria-selected="false">Header</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#footer" role="tab" aria-selected="false">Footer</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#post" role="tab" aria-selected="false">Post</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#contact" role="tab" aria-selected="false">Contact</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#chat" role="tab" aria-selected="false">Chat</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#home" role="tab" aria-selected="false">Home</a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                {{csrf_field()}}
                                <input type="hidden" name="language" value="{{$language}}" />

                                <!--  General Tab Content-->
                                @include('admin.themes.elements.general')

                                <!--  Header Tab Content-->
                                @include('admin.themes.elements.header')

                                <!--  Footer Tab Content-->
                                @include('admin.themes.elements.footer')

                                <!--  Post Tab Content-->
                                @include('admin.themes.elements.post')

                                <!--  Contact Tab Content-->
                                @include('admin.themes.elements.contact')

                                <!--  Contact Tab Content-->
                                @include('admin.themes.elements.chat')

                                <!--  Home Tab Content-->
                                @include('admin.themes.elements.home')
                            </div>
                            <div class="kt-portlet__foot">
                                <div class="kt-form__actions">
                                    <button type="button" id="btn_edit" class="btn btn-primary">Cập Nhập</button>
                                    <button type="reset" class="btn btn-secondary">Hủy bỏ</button>
                                </div>
                            </div>
                    </form>
                    <!--end::Form-->
                </div>

                <!--end::Portlet-->
            </div>
        </div>
    </div>

    <!-- end:: Content -->
</div>
@endsection
@section('script')
<!--end::Page Vendors -->
<script src="assets/js/pages/crud/forms/widgets/bootstrap-datepicker.js" type="text/javascript"></script>
<script src="admin/js/pages/theme/theme-option.js?v1" type="text/javascript"></script>
<script src="admin/plugins/fancybox/jquery.fancybox.min.js"></script>
<script src="admin/plugins/fancybox/jquery.observe_field.js"></script>
<script src="assets/plugins/custom/tinymce/tinymce.bundle.js" type="text/javascript"></script>

<script>
    // init
    init();

    function showBackground() {
        var isChecked = $('#is_background:checkbox:checked').length > 0;
        if (isChecked) {
            $("#is_background_hidden").val('on');
            $(".div_wrap").show(100);
        } else {
            $("#is_background_hidden").val('off');
            $(".div_wrap").hide(100);
        }
    }

    function showOptionBackground() {
        var option = $('#option_background').val();
        if (option === 'image') {
            $("#div_image").show(100);
            $("#div_color").hide();
        } else {
            $("#div_color").show(100);
            $("#div_image").hide();
        }
    }

    function showIntroVideo() {
        var isChecked = $('#is_intro_video:checkbox:checked').length > 0;
        if (isChecked) {
            $("#is_intro_video_hidden").val('on');
            $(".div_wrap_intro_video").show(100);
        } else {
            $("#is_intro_video_hidden").val('off');
            $(".div_wrap_intro_video").hide(100);
        }
    }

    function showOptionIntroVideo() {
        var option = $('#option_intro_video').val();
        if (option === 'upload') {
            $("#div_intro_video_upload").show(100);
            $("#div_intro_video_link").hide();
        } else {
            $("#div_intro_video_link").show(100);
            $("#div_intro_video_upload").hide();
        }
    }

    function showOptionAboutMedia() {
        var option = $('#option_about_media').val();

        if (option === 'image') {
            $("#div_about_image").show(100);
            $("#div_about_video").hide();
        } else {

            $("#div_about_video").show(100);
            $("#div_about_image").hide();
        }
    }

    function showOptionRingPhone() {
        var isChecked = $('#ring_phone:checkbox:checked').length > 0;
        if (isChecked) {
            $("#ring_phone_hidden").val('on');
            $(".div_wrap_ring_phone").show(100);
        } else {
            $("#ring_phone_hidden").val('off');
            $(".div_wrap_ring_phone").hide(100);
        }
    }

    function showOptionCallImage() {
        var isChecked = $('#is_call_image:checkbox:checked').length > 0;
        if (isChecked) {
            $("#is_call_image_hidden").val('on');
            $(".div_wrap_call_image").show(100);
        } else {
            $("#is_call_image_hidden").val('off');
            $(".div_wrap_call_image").hide(100);
        }
    }
    function showSubscription() {
        var isChecked = $('#is_subscription:checkbox:checked').length > 0;
        if (isChecked) {
            $("#is_subscription_hidden").val('on');
        } else {
            $("#is_subscription_hidden").val('off');
        }
    }

    $('.iframe-btn').fancybox({
        'iframe': {
            'css': {
                'width': '90%',
                'height': '90%',
            }
        },
    });

    $(document).ready(function() {

        $("#theme_option_background_image").observe_field(1, function() {
            $("#preview_thumbnail").attr('src', this.value).show();
        });

        $("#theme_option_general_popup_banner").observe_field(1, function() {
            $("#preview_thumbnail_general_popup_banner").attr('src', this.value).show();
        });

        $("#theme_option_intro_video_upload").observe_field(1, function() {
            $("#preview_thumbnail_intro_video").attr('src', this.value).show();
            $("video")[0].load();
        });

        $("#theme_option_contact_image").observe_field(1, function() {
            $("#preview_thumbnail_contact_image").attr('src', this.value).show();
        });

        tinymce.init({
            selector: "#about_content",
            toolbar: !1,
            statusbar: !1,
            height: 200,
        })
    });

    function updateOptionByLanguage() {
        var lang = $("#language").val();
        // var tab_active = $(".nav .nav-item .active").attr('href');
        window.location.href = "/theme-option?lang=" + encodeURIComponent(lang);
    }

    // Chat
    function showChat() {
        var isChecked = $('#is_chat:checkbox:checked').length > 0;
        if (isChecked) {
            $("#is_chat_hidden").val('on');
            $(".div_wrap_chat").show(100);
        } else {
            $("#is_chat_hidden").val('off');
            $(".div_wrap_chat").hide(100);
        }
    }

    function showOptionChat() {
        var option_chat = $('#option_chat').val();
        if (option_chat === 'facebook') {
            $("#div_chat_facebook").show(100);
            $("#div_chat_tawk").hide();
        } else {
            $("#div_chat_tawk").show(100);
            $("#div_chat_facebook").hide();
        }
    }

    function init() {
        // in it
        showBackground();
        showOptionBackground();
        showIntroVideo();
        showOptionIntroVideo();
        showOptionAboutMedia();
        showOptionRingPhone();
        showChat();
        showOptionChat();
        showOptionCallImage();
        showSubscription();
        $("video")[0].load();
    }
</script>
@endsection