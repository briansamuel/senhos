<div class="tab-pane" id="footer" role="tabpanel">
    <div class="form-group row">
        <label class="col-lg-3 col-form-label">Copyright:</label>
        <div class="col-lg-6">
            <input type="text" class="form-control" name="theme_option::footer::copyright" value="{{isset($option['theme_option::footer::copyright']) ? $option['theme_option::footer::copyright'] : ''}}">
        </div>
    </div>
    <div class="form-group row">
        <label class="col-lg-3 col-form-label">Banner</label>
        <div class="kt-avatar kt-avatar--outline kt-avatar--circle" id="theme_option::footer::banner">
            <label></label>
            <div class="kt-avatar__holder" style="background-image: url('{{isset($option['theme_option::footer::banner']) ? $option['theme_option::footer::banner'] : ''}}')"></div>
            <label class="kt-avatar__upload" data-toggle="kt-tooltip" title="" data-original-title="Change banner">
                <i class="fa fa-pen"></i>
                <input type="file" name="theme_option::footer::banner" accept="image/*">
            </label>
            <span class="kt-avatar__cancel" data-toggle="kt-tooltip" title="" data-original-title="Cancel banner">
                <i class="fa fa-times"></i>
            </span>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-lg-3 col-form-label">Facebook Url:</label>
        <div class="col-lg-6">
            <input type="text" class="form-control" name="theme_option::footer::facebook" value="{{isset($option['theme_option::footer::facebook']) ? $option['theme_option::footer::facebook'] : ''}}">
        </div>
    </div>
    <div class="form-group row">
        <label class="col-lg-3 col-form-label">Twitter Url:</label>
        <div class="col-lg-6">
            <input type="text" class="form-control" name="theme_option::footer::twitter" value="{{isset($option['theme_option::footer::twitter']) ? $option['theme_option::footer::twitter'] : ''}}">
        </div>
    </div>
    <div class="form-group row">
        <label class="col-lg-3 col-form-label">Google+ Url:</label>
        <div class="col-lg-6">
            <input type="text" class="form-control" name="theme_option::footer::google" value="{{isset($option['theme_option::footer::google']) ? $option['theme_option::footer::google'] : ''}}">
        </div>
    </div>
    <div class="form-group row">
        <label class="col-lg-3 col-form-label">Instagram Url:</label>
        <div class="col-lg-6">
            <input type="text" class="form-control" name="theme_option::footer::instagram" value="{{isset($option['theme_option::footer::instagram']) ? $option['theme_option::footer::instagram'] : ''}}">
        </div>
    </div>
    <div class="form-group row">
        <label class="col-lg-3 col-form-label">Youtube Url:</label>
        <div class="col-lg-6">
            <input type="text" class="form-control" name="theme_option::footer::youtube" value="{{isset($option['theme_option::footer::youtube']) ? $option['theme_option::footer::youtube'] : ''}}">
        </div>
    </div>
    <div class="form-group row">
        <label class="col-lg-3 col-form-label">Số điện thoại:</label>
        <div class="col-lg-6">
            <input type="text" class="form-control" name="theme_option::footer::phone_number" value="{{isset($option['theme_option::footer::phone_number']) ? $option['theme_option::footer::phone_number'] : ''}}">
        </div>
    </div>
    <div class="form-group row">
        <label class="col-lg-3 col-form-label">Email liên hệ:</label>
        <div class="col-lg-6">
            <input type="text" class="form-control" name="theme_option::footer::email" value="{{isset($option['theme_option::footer::email']) ? $option['theme_option::footer::email'] : ''}}">
        </div>
    </div>
    <div class="form-group row">
        <label class="col-lg-3 col-form-label">Địa chỉ 1:</label>
        <div class="col-lg-6">
            <input type="text" class="form-control" name="theme_option::footer::address_1" value="{{isset($option['theme_option::footer::address_1']) ? $option['theme_option::footer::address_1'] : ''}}">
        </div>
    </div>
    <div class="form-group row">
        <label class="col-lg-3 col-form-label">Địa chỉ 2:</label>
        <div class="col-lg-6">
            <input type="text" class="form-control" name="theme_option::footer::address_2" value="{{isset($option['theme_option::footer::address_2']) ? $option['theme_option::footer::address_2'] : ''}}">
        </div>
    </div>
</div>