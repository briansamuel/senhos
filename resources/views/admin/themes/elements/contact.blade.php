<div class="tab-pane" id="contact" role="tabpanel">
    <div class="form-group row">
        <label class="col-lg-3 col-form-label">Image</label>
        <div class="col-lg-6">
            <a data-src="@filemanager_get_resource(dialog.php)?type=1&field_id=theme_option_contact_image&lang=vi&akey=@filemanager_get_key()" class="iframe-btn" data-fancybox data-fancybox data-type="iframe" href="javascript:;">
                @if(isset($option['theme_option::contact::image']) && $option['theme_option::contact::image'] !== '')
                <img style="width:50%" id="preview_thumbnail_contact_image" class="img-fluid" src="{{ $option['theme_option::contact::image'] }}">
                @else
                <img style="width:50%" id="preview_thumbnail_contact_image" class="img-fluid" src="admin/images/upload-thumbnail.png">
                @endif
            </a>
            <input type="hidden" name="theme_option::contact::image" id="theme_option_contact_image" value="">
        </div>
    </div>
    <div class="alert alert-secondary" role="alert">
        <div class="alert-icon"><i class="flaticon-star kt-font-brand"></i></div>
        <div class="alert-text">
            Địa chỉ 1
        </div>
    </div>
    <div class="form-group row">
        <label class="col-lg-3 col-form-label">Tiêu đề:</label>
        <div class="col-lg-6">
            <input type="text" class="form-control" name="theme_option::contact::title_1" value="{{isset($option['theme_option::contact::title_1']) ? $option['theme_option::contact::title_1'] : ''}}">
        </div>
    </div>
    <div class="form-group row">
        <label class="col-lg-3 col-form-label">Địa chỉ:</label>
        <div class="col-lg-6">
            <input type="text" class="form-control" name="theme_option::contact::address_1" value="{{isset($option['theme_option::contact::address_1']) ? $option['theme_option::contact::address_1'] : ''}}">
        </div>
    </div>
    <div class="form-group row">
        <label class="col-lg-3 col-form-label">Số điện thoại:</label>
        <div class="col-lg-6">
            <input type="text" class="form-control" name="theme_option::contact::phone_number_1" value="{{isset($option['theme_option::contact::phone_number_1']) ? $option['theme_option::contact::phone_number_1'] : ''}}">
        </div>
    </div>
    <div class="form-group row">
        <label class="col-lg-3 col-form-label">Email:</label>
        <div class="col-lg-6">
            <input type="text" class="form-control" name="theme_option::contact::email_1" value="{{isset($option['theme_option::contact::email_1']) ? $option['theme_option::contact::email_1'] : ''}}">
        </div>
    </div>
    <div class="form-group row">
        <label class="col-lg-3 col-form-label">Kinh độ:</label>
        <div class="col-lg-6">
            <input type="text" class="form-control" name="theme_option::contact::longitude_1" value="{{isset($option['theme_option::contact::longitude_1']) ? $option['theme_option::contact::longitude_1'] : ''}}">
        </div>
    </div>
    <div class="form-group row">
        <label class="col-lg-3 col-form-label">Vĩ độ:</label>
        <div class="col-lg-6">
            <input type="text" class="form-control" name="theme_option::contact::latitude_1" value="{{isset($option['theme_option::contact::latitude_1']) ? $option['theme_option::contact::latitude_1'] : ''}}">
        </div>
    </div>
    <div class="alert alert-secondary" role="alert">
        <div class="alert-icon"><i class="flaticon-star kt-font-brand"></i></div>
        <div class="alert-text">
            Địa chỉ 2
        </div>
    </div>
    <div class="form-group row">
        <label class="col-lg-3 col-form-label">Tiêu đề:</label>
        <div class="col-lg-6">
            <input type="text" class="form-control" name="theme_option::contact::title_2" value="{{isset($option['theme_option::contact::title_2']) ? $option['theme_option::contact::title_2'] : ''}}">
        </div>
    </div>
    <div class="form-group row">
        <label class="col-lg-3 col-form-label">Địa chỉ:</label>
        <div class="col-lg-6">
            <input type="text" class="form-control" name="theme_option::contact::address_2" value="{{isset($option['theme_option::contact::address_2']) ? $option['theme_option::contact::address_2'] : ''}}">
        </div>
    </div>
    <div class="form-group row">
        <label class="col-lg-3 col-form-label">Số điện thoại:</label>
        <div class="col-lg-6">
            <input type="text" class="form-control" name="theme_option::contact::phone_number_2" value="{{isset($option['theme_option::contact::phone_number_2']) ? $option['theme_option::contact::phone_number_2'] : ''}}">
        </div>
    </div>
    <div class="form-group row">
        <label class="col-lg-3 col-form-label">Email:</label>
        <div class="col-lg-6">
            <input type="text" class="form-control" name="theme_option::contact::email_2" value="{{isset($option['theme_option::contact::email_2']) ? $option['theme_option::contact::email_2'] : ''}}">
        </div>
    </div>
    <div class="form-group row">
        <label class="col-lg-3 col-form-label">Kinh độ:</label>
        <div class="col-lg-6">
            <input type="text" class="form-control" name="theme_option::contact::longitude_2" value="{{isset($option['theme_option::contact::longitude_2']) ? $option['theme_option::contact::longitude_2'] : ''}}">
        </div>
    </div>
    <div class="form-group row">
        <label class="col-lg-3 col-form-label">Vĩ độ:</label>
        <div class="col-lg-6">
            <input type="text" class="form-control" name="theme_option::contact::latitude_2" value="{{isset($option['theme_option::contact::latitude_2']) ? $option['theme_option::contact::latitude_2'] : ''}}">
        </div>
    </div>
</div>