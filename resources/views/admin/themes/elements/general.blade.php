<div class="tab-pane active" id="general" role="tabpanel">
    <div class="form-group row">
        <label class="col-lg-3 col-form-label">Popup Banner</label>
        <div class="col-lg-6">
            <a data-src="@filemanager_get_resource(dialog.php)?type=1&field_id=theme_option_general_popup_banner&lang=vi&akey=@filemanager_get_key()" class="iframe-btn" data-fancybox data-fancybox data-type="iframe" href="javascript:;">
                @if(isset($option['theme_option::general::popup_banner']) && $option['theme_option::general::popup_banner'] !== '')
                <img style="width:50%" id="preview_thumbnail_general_popup_banner" class="img-fluid" src="{{ $option['theme_option::general::popup_banner'] }}">
                @else
                <img style="width:50%" id="preview_thumbnail_general_popup_banner" class="img-fluid" src="admin/images/upload-thumbnail.png">
                @endif
            </a>
            <input type="hidden" name="theme_option::general::popup_banner" id="theme_option_general_popup_banner" value="">
        </div>
    </div>
    <div class="form-group row">
        <label class="col-lg-3 col-form-label">Background?:</label>
        <div class="col-lg-6">
            <span class="kt-switch">
                <label>
                    <input type="checkbox" class="form-control" name="theme_option::general::is_background" onclick="showBackground()" id="is_background" <?= isset($option['theme_option::general::is_background']) && $option['theme_option::general::is_background'] === 'on' ? 'checked' : '' ?>>
                    <input type="hidden" name="theme_option::general::is_background" id="is_background_hidden" >
                    <span></span>
                </label>
            </span>
        </div>
    </div>
    <div class="div_wrap">
        <div class="form-group row">
            <label class="col-lg-3 col-form-label">Tùy chọn:</label>
            <div class="col-lg-6">
                <select class="form-control" name="theme_option::general::option_background" id="option_background" onchange="showOptionBackground()">
                    <option value="image" {{ isset($option['theme_option::general::option_background']) && $option['theme_option::general::option_background'] === 'image' ? 'selected' : ''}}>Ảnh</option>
                    <option value="color" {{ isset($option['theme_option::general::option_background']) && $option['theme_option::general::option_background'] === 'color' ? 'selected' : ''}}>Màu</option>
                </select>
            </div>
        </div>
        <div class="form-group row" id="div_color">
            <label class="col-lg-3 col-form-label">Màu:</label>
            <div class="col-lg-6">
                <input type="color" class="form-control" name="theme_option::general::color" value="{{isset($option['theme_option::general::color']) ? $option['theme_option::general::color'] : ''}}">
            </div>
        </div>
        <div class="form-group row" id="div_image">
            <label class="col-lg-3 col-form-label">Ảnh</label>
            <div class="col-lg-6">
                <a data-src="@filemanager_get_resource(dialog.php)?type=1&field_id=theme_option_background_image&lang=vi&akey=@filemanager_get_key()" class="iframe-btn" data-fancybox data-fancybox data-type="iframe" href="javascript:;">
                    @if(isset($option['theme_option::general::image']) && $option['theme_option::general::image'] !== '')
                    <img style="width:50%" id="preview_thumbnail" class="img-fluid" src="{{ $option['theme_option::general::image'] }}">
                    @else
                    <img style="width:50%" id="preview_thumbnail" class="img-fluid" src="admin/images/upload-thumbnail.png">
                    @endif
                </a>
                <input type="hidden" name="theme_option::general::image" id="theme_option_background_image" value="">
            </div>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-lg-3 col-form-label">Intro Video?:</label>
        <div class="col-lg-6">
            <span class="kt-switch">
                <label>
                    <input type="checkbox" name="theme_option::general::intro_video" class="form-control" id="is_intro_video" onclick="showIntroVideo()" <?= isset($option['theme_option::general::intro_video']) && $option['theme_option::general::intro_video'] === 'on' ? 'checked' : '' ?>>
                    <input type="hidden" name="theme_option::general::intro_video" id="is_intro_video_hidden">
                    <span></span>
                </label>
            </span>
        </div>
    </div>
    <div class="div_wrap_intro_video">
        <div class="form-group row">
            <label class="col-lg-3 col-form-label">Tùy chọn:</label>
            <div class="col-lg-6">
                <select class="form-control" name="theme_option::general::intro_video_option" id="option_intro_video" onchange="showOptionIntroVideo()">
                    <option value="upload" {{ isset($option['theme_option::general::intro_video_option']) && $option['theme_option::general::intro_video_option'] === 'upload' ? 'selected' : ''}}>Upload</option>
                    <option value="color" {{ isset($option['theme_option::general::intro_video_option']) && $option['theme_option::general::intro_video_option'] === 'link' ? 'selected' : ''}}>Đường dẫn</option>
                </select>
            </div>
        </div>
        <div class="form-group row" id="div_intro_video_link">
            <label class="col-lg-3 col-form-label">Đường dẫn:</label>
            <div class="col-lg-6">
                <input type="text" class="form-control" name="theme_option::general::intro_video_link" value="{{isset($option['theme_option::general::intro_video_link']) ? $option['theme_option::general::intro_video_link'] : ''}}">
            </div>
        </div>
        <div class="form-group row" id="div_intro_video_upload">
            <label class="col-lg-3 col-form-label">Video</label>
            <div class="col-lg-6">
                @if(isset($option['theme_option::general::intro_video_upload']))
                <a data-src="@filemanager_get_resource(dialog.php)?type=3&field_id=theme_option_intro_video_upload&lang=vi&akey=@filemanager_get_key()" class="iframe-btn" data-fancybox data-fancybox data-type="iframe" href="javascript:;">
                    <video width="320" height="240" controls>
                        <source id="preview_thumbnail_intro_video" src="{{ $option['theme_option::general::intro_video_upload'] }}" type="video/mp4">
                    </video>
                </a>
                @else
                <a data-src="@filemanager_get_resource(dialog.php)?type=3&field_id=theme_option_intro_video_upload&lang=vi&akey=@filemanager_get_key()" class="iframe-btn" data-fancybox data-fancybox data-type="iframe" href="javascript:;">
                    <video width="320" height="240" controls>
                        <source id="preview_thumbnail_intro_video" src="" type="video/mp4">
                    </video>
                </a>
                @endif
                <input type="hidden" name="theme_option::general::intro_video_upload" id="theme_option_intro_video_upload" value="">
            </div>
        </div>
        <div class="form-group row">
            <label class="col-lg-3 col-form-label">Text Skip Video:</label>
            <div class="col-lg-6">
                <input type="text" class="form-control" name="theme_option::general::intro_video_text_button" value="{{isset($option['theme_option::general::intro_video_text_button']) ? $option['theme_option::general::intro_video_text_button'] : ''}}">
            </div>
        </div>
        <div class="form-group row">
            <label class="col-lg-3 col-form-label">Màu Button:</label>
            <div class="col-lg-6">
                <input type="color" class="form-control" name="theme_option::general::intro_video_color_button" value="{{isset($option['theme_option::general::intro_video_color_button']) ? $option['theme_option::general::intro_video_color_button'] : ''}}">
            </div>
        </div>
    </div>
    <!-- Ring Phone Setting -->
    <div class="form-group row">
        <label class="col-lg-3 col-form-label">Ring Phone?:</label>
        <div class="col-lg-6">
            <span class="kt-switch">
                <label>
                    <input type="checkbox" class="form-control" name="theme_option::general::ring_phone" id="ring_phone" onclick="showOptionRingPhone()" <?= isset($option['theme_option::general::ring_phone']) && $option['theme_option::general::ring_phone'] === 'on' ? 'checked' : '' ?>>
                    <input type="hidden" name="theme_option::general::ring_phone" id="ring_phone_hidden">
                    <span></span>
                </label>
            </span>
        </div>
    </div>
    <div class="div_wrap_ring_phone">
        <div class="form-group row">
            <label class="col-lg-3 col-form-label">Ring Phone Text:</label>
            <div class="col-lg-6">
                <input type="text" class="form-control" name="theme_option::general::ring_phone_text" value="{{isset($option['theme_option::general::ring_phone_text']) ? $option['theme_option::general::ring_phone_text'] : ''}}">
            </div>
        </div>
        <div class="form-group row" >
            <label class="col-lg-3 col-form-label">Số điện thoại:</label>
            <div class="col-lg-6">
                <input type="text" class="form-control" name="theme_option::general::ring_phone_number" value="{{isset($option['theme_option::general::ring_phone_number']) ? $option['theme_option::general::ring_phone_number'] : ''}}">
            </div>
        </div>
    </div>
    <!-- Call Image Float -->
    <div class="form-group row">
        <label class="col-lg-3 col-form-label"> Call Image Float ?:</label>
        <div class="col-lg-6">
            <span class="kt-switch">
                <label>
                    <input type="checkbox" class="form-control" name="theme_option::general::call_image_float" id="is_call_image" onclick="showOptionCallImage()" <?= isset($option['theme_option::general::call_image_float']) && $option['theme_option::general::call_image_float'] === 'on' ? 'checked' : '' ?>>
                    <input type="hidden" name="theme_option::general::call_image_float" id="is_call_image_hidden" >
                    <span></span>
                </label>
            </span>
        </div>
    </div>
    <div class="div_wrap_call_image">
        <div class="form-group row">
            <label class="col-lg-3 col-form-label">Ảnh:</label>
            <div class="col-lg-6">
                <a data-src="@filemanager_get_resource(dialog.php)?type=1&field_id=theme_option_call_image_url&lang=vi&akey=@filemanager_get_key()" class="iframe-btn" data-fancybox data-fancybox data-type="iframe" href="javascript:;">
                    @if(isset($option['theme_option::general::call_image_url']) && $option['theme_option::general::call_image_url'] !== '')
                    <img style="width:50%" id="preview_thumbnail" class="img-fluid" src="{{ $option['theme_option::general::call_image_url'] }}">
                    @else
                    <img style="width:50%" id="preview_thumbnail" class="img-fluid" src="admin/images/upload-thumbnail.png">
                    @endif
                </a>
                <input type="hidden" name="theme_option::general::call_image_url" id="theme_option_call_image_url" value="">
            </div>
        </div>
        <div class="form-group row" >
            <label class="col-lg-3 col-form-label">Số điện thoại:</label>
            <div class="col-lg-6">
                <input type="text" class="form-control" name="theme_option::general::call_image_number" value="{{isset($option['theme_option::general::call_image_number']) ? $option['theme_option::general::call_image_number'] : ''}}">
            </div>
        </div>
    </div>
     <!-- Subcription Popup Setting -->
     <div class="form-group row">
        <label class="col-lg-3 col-form-label">Subscription Popup?:</label>
        <div class="col-lg-6">
            <span class="kt-switch">
                <label>
                    <input type="checkbox" class="form-control" name="theme_option::general::subscription_popup" id="is_subscription" onclick="showSubscription()" <?= isset($option['theme_option::general::subscription_popup']) && $option['theme_option::general::subscription_popup'] === 'on' ? 'checked' : '' ?>>
                    <input type="hidden" name="theme_option::general::subscription_popup" id="is_subscription_hidden" >
                    <span></span>
                </label>
            </span>
        </div>
    </div>
</div>