<div class="tab-pane" id="post" role="tabpanel">
    <div class="form-group row">
        <label class="col-lg-3 col-form-label">Số tin tức mỗi trang:</label>
        <div class="col-lg-6">
            <input type="text" class="form-control" name="theme_option::post::pagination_post" value="{{isset($option['theme_option::post::pagination_post']) ? $option['theme_option::post::pagination_post'] : ''}}">
        </div>
    </div>
    <div class="form-group row">
        <label class="col-lg-3 col-form-label">Số liên hệ mỗi trang:</label>
        <div class="col-lg-6">
            <input type="text" class="form-control" name="theme_option::post::pagination_recruitment" value="{{isset($option['theme_option::post::pagination_recruitment']) ? $option['theme_option::post::pagination_recruitment'] : ''}}">
        </div>
    </div>
</div>