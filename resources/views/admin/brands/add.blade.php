@extends('admin.index')
@section('page-header', 'Brand')
@section('page-sub_header', 'Thêm mới brand')
@section('style')
    <link rel="stylesheet" href="admin/plugins/fancybox/jquery.fancybox.min.css" />
@endsection
@section('content')
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

        <!-- begin:: Subheader -->
        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container  kt-container--fluid ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        Tài Khoản Brand </h3>
                </div>
            </div>
        </div>

        <!-- end:: Subheader -->

        <!-- begin:: Content -->
        <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
            <form id="kt_add_form">
                <div class="row">
                    <div class="col-md-8 col-lg-9">
                        <div class="kt-portlet">
                            <div class="kt-portlet__head kt-portlet__head--right">
                                <div class="kt-portlet__head-label ">
                                    <span class="kt-font-danger"><i class="fa fa-star"></i> Bắt buộc phải nhập / chọn nội dung</span>
                                </div>
                            </div>
                            <!--begin::Form-->
                            <div class="kt-form">
                                <div class="kt-portlet__body">
                                    <div class="form-group row">
                                        <label for="title" class="col-12 col-lg-12 col-xl-3 col-form-label">Tiêu đề:</label>
                                        <div class="col-12 col-lg-12 col-xl-9">
                                            {{csrf_field()}}
                                            <input class="form-control" type="text" value="" id="title" name="title" placeholder="Tiêu đề bắt buộc phải nhập nội dung">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="link" class="col-12 col-lg-12 col-xl-3 col-form-label">Đường dẫn:</label>
                                        <div class="col-12 col-lg-12 col-xl-9">
                                            <input class="form-control" type="text" value="" id="link" name="link" placeholder="Đường dẫn">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="active" class="col-12 col-lg-12 col-xl-3 col-form-label">Active:</label>
                                        <div class="col-12 col-lg-12 col-xl-9">
                                            <select class="form-control kt-select2" id="active" name="active">
                                                <option value="yes">Yes</option>
                                                <option value="no">No</option>
                                            </select>
                                        </div>
                                    </div>
                                     <div class="form-group row">
                                        <label for="active" class="col-12 col-lg-12 col-xl-3 col-form-label">Vị trí:</label>
                                        <div class="col-12 col-lg-12 col-xl-9">
                                            <select class="form-control kt-select2" id="position" name="position">
                                                <option value="home">Trang chủ</option>
                                                <option value="partner">Trang đối tác</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-3">
                        <div class="kt-portlet">
                            <div class="kt-portlet__body p-3">
                                <div class="form-group row mb-3">
                                    <label for="language" class="col-4 col-form-label">Độ ưu tiên:</label>
                                    <div class="col-8">
                                        <input class="form-control" type="text" value="100" id="order" name="order" placeholder="Độ ưu tiên">
                                    </div>
                                </div>
                            </div>
                            <div class="kt-portlet__foot kt-align-right p-2">
                                <div>
                                    <button type="button" class="btn btn-primary" id="btn_add"><i class="la la-save"></i> Lưu dữ liệu</button>
                                </div>
                            </div>
                        </div>
                        <div class="kt-portlet">
                            <div class="kt-portlet__body p-0">


                                <div class="form-group row mb-0">
                                    <div class="col-12">
                                        <a style="display: none" id="div_video"
                                           data-src="@filemanager_get_resource(dialog.php)?type=3&field_id=image&lang=vi&akey=@filemanager_get_key()"
                                           class="iframe-btn" data-fancybox data-fancybox data-type="iframe"
                                           href="javascript:;">
                                                <video width="100%" height="100%" controls>
                                                    <source id="preview_thumbnail_video" src="" type="video/mp4">
                                                </video>
                                        </a>
                                        <a id="div_image"
                                           data-src="@filemanager_get_resource(dialog.php)?type=1&field_id=image&lang=vi&akey=@filemanager_get_key()"
                                           class="iframe-btn" data-fancybox data-fancybox data-type="iframe"
                                           href="javascript:;">
                                                <img id="preview_thumbnail" class="img-fluid" src="admin/images/upload-thumbnail.png">
                                        </a>
                                        <input type="hidden" name="image" id="image" value="">
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </form>
        </div>

        <!-- end:: Content -->
</div>
@endsection
@section('script')
    <!--end::Page Vendors -->
    <script src="assets/js/pages/crud/forms/widgets/bootstrap-datetimepicker.js" type="text/javascript"></script>
    <script src="admin/js/pages/brand/add-brand.js?v1.2" type="text/javascript"></script>
    <script src="assets/js/pages/crud/file-upload/dropzonejs.js?v2" type="text/javascript"></script>
    <script src="admin/plugins/fancybox/jquery.fancybox.min.js"></script>
    <script src="admin/plugins/fancybox/jquery.observe_field.js"></script>

    <script>
        $("#upload").dropzone({
            url: "/upload-image",
            paramName: "file",
            maxFiles: 1,
            maxFilesize: 5,
            addRemoveLinks: !0,
            sending: function(file, xhr, formData) {
                formData.append("_token", "{{ csrf_token() }}");
            },
            removedfile: function(file)
            {
                var name = $("#image").val();
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': "{{csrf_token()}}"
                    },
                    type: 'POST',
                    url: "destroy-image",
                    data: {filename: name},
                    success: function (data){
                        $("#image").val('');
                    },
                    error: function(e) {
                        console.log(e);
                    }});
                var fileRef;
                return (fileRef = file.previewElement) != null ?
                    fileRef.parentNode.removeChild(file.previewElement) : void 0;
            },
            success: function(file, response) {
                if(response.success) {
                    $("#image").val(response.url);
                } else {
                    alert('Có lỗi xảy ra, vui lòng thử lại sau');
                }
            },
            error: function(file, response) {
                alert('Có lỗi xảy ra, vui lòng thử lại sau');
            }
        });

        $('.iframe-btn').fancybox({
            'iframe': {
                'css': {
                    'width': '90%',
                    'height': '90%',
                }
            },
        });

        $(document).ready(function () {

            $("#image").observe_field(1, function () {
                $('#preview_thumbnail').attr('src', this.value).show();
            });

        });
    </script>


@endsection