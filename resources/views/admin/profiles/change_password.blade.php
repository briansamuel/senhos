@extends('admin.profiles.index')
@section('page-header', 'Tài khoản')
@section('page-sub_header', 'Thay đổi mật khẩu')
@section('style')

@endsection
@section('content_profile')

<div class="kt-grid__item kt-grid__item--fluid kt-app__content">
        <div class="row">
            <div class="col-xl-12">
                <div class="kt-portlet kt-portlet--height-fluid kt-change_password">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">Thay đổi mật khẩu<small>thay đổi hoặc đặt lại mật khẩu tài khoản của bạn</small></h3>
                        </div>
                    </div>
                    <form class="kt-form kt-form--label-right">
                        <div class="kt-portlet__body">
                            <div class="kt-section kt-section--first">
                                <div class="kt-section__body">
                                    <div class="row">
                                        <label class="col-xl-3"></label>
                                        <div class="col-lg-9 col-xl-6">
                                            <h3 class="kt-section__title kt-section__title-sm">Thay đổi hoặc khôi phục mật khẩu của bạn:</h3>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label">Mật Khẩu Hiện Tại</label>
                                        <div class="col-lg-9 col-xl-6">
                                            {{ csrf_field() }}
                                            <input type="password" name="password" class="form-control" placeholder="Mật khẩu hiện tại">
                                            <a href="javascript:;" class="kt-link kt-font-sm kt-font-bold kt-margin-t-5" data-toggle="modal" data-target="#kt_modal_5">Quên mật khẩu ?</a>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label">Mật Khẩu Mới</label>
                                        <div class="col-lg-9 col-xl-6">
                                            <input type="password" name="new_password" id="new_password" class="form-control" value="" placeholder="Mật khẩu mới">
                                        </div>
                                    </div>
                                    <div class="form-group form-group-last row">
                                        <label class="col-xl-3 col-lg-3 col-form-label">Xác nhận mật khẩu mới</label>
                                        <div class="col-lg-9 col-xl-6">
                                            <input type="password" name="confirm_new_password" class="form-control" value="" placeholder="Xác nhận mật khẩu mới">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="kt-portlet__foot">
                            <div class="kt-form__actions">
                                <div class="row">
                                    <div class="col-lg-3 col-xl-3">
                                    </div>
                                    <div class="col-lg-9 col-xl-9">
                                        <button type="button" id="btn_change_password" class="btn btn-brand btn-bold">Thay Đổi Mật Khẩu</button>&nbsp;
                                        <button type="reset" class="btn btn-secondary">Hủy Bỏ</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
</div>

<!--begin::Modal-->
<div class="modal fade forgot_password" id="kt_modal_5" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-xs" role="document" id="kt_login">
        <div class="modal-content">
            <form>
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Quên mật khẩu?</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        {{ csrf_field() }}
                        <input type="text" class="form-control" id="recipient-name" name="email" placeholder="Email">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="kt_login_forgot_submit">Reset mật khẩu</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Hủy bỏ</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
@section('script')
    <script src="assets/js/pages/custom/user/profile.js" type="text/javascript"></script>
@endsection