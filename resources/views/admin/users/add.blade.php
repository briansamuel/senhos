@extends('admin.index')
@section('page-header', 'User')
@section('page-sub_header', 'Thêm mới User')
@section('style')

@endsection
@section('content')
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

        <!-- begin:: Subheader -->
        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container  kt-container--fluid ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        Tài Khoản Hệ Thống </h3>
                </div>
            </div>
        </div>

        <!-- end:: Subheader -->

        <!-- begin:: Content -->
        <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
            <div class="row">
                <div class="col-md-12">

                    <!--begin::Portlet-->
                    <div class="kt-portlet">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Thêm mới User
                                </h3>
                            </div>
                        </div>

                        <!--begin::Form-->
                        <form class="kt-form" id="kt_user_add_form">
                            <div class="kt-portlet__body">
                                <div class="form-group">
                                    <label>Email:</label>
                                    <input type="text" class="form-control" placeholder="Nhập email" name="email">
                                </div>
                                <div class="form-group">
                                    <label>Username:</label>
                                    <input type="text" class="form-control" placeholder="Nhập username" name="username">
                                </div>
                                <div class="form-group">
                                    <label>Họ và tên:</label>
                                    {{csrf_field()}}
                                    <input type="text" class="form-control" placeholder="Nhập họ và tên" name="full_name">
                                </div>
                                <div class="form-group">
                                    <div class="kt-avatar kt-avatar--outline kt-avatar--circle" id="kt_user_add_avatar">
                                        <label>Avatar:</label>
                                        <div class="kt-avatar__holder"></div>
                                        <label class="kt-avatar__upload" data-toggle="kt-tooltip" title="" data-original-title="Change avatar">
                                            <i class="fa fa-pen"></i>
                                            <input type="file" name="profile_avatar" accept="image/*">
                                        </label>
                                        <span class="kt-avatar__cancel" data-toggle="kt-tooltip" title="" data-original-title="Cancel avatar">
                                            <i class="fa fa-times"></i>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Mật khẩu:</label>
                                    <input type="password" class="form-control" placeholder="Nhập mật khẩu" name="password" id="password">
                                </div>
                                <div class="form-group">
                                    <label>Xác nhận mật khẩu:</label>
                                    <input type="password" class="form-control" placeholder="Xác nhận mật khẩu" name="confirm_password">
                                </div>
                                <div class="form-group">
                                    <label>Phân quyền:</label>
                                    <div class="kt-checkbox-list">
                                        @forelse($listGroup as $group)
                                        <label class="kt-checkbox">
                                            <input type="checkbox" name="group_id[]" value="{{$group->id}}"> {{$group->group_name}}
                                            <span></span>
                                        </label>
                                        @empty
                                        Không có quyền nào trên hệ thống
                                        @endforelse
                                    </div>
                                </div>
                            </div>
                            <div class="kt-portlet__foot">
                                <div class="kt-form__actions">
                                    <button type="button" id="btn_add_user" class="btn btn-primary">Thêm mới</button>
                                    <button type="reset" class="btn btn-secondary">Hủy bỏ</button>
                                </div>
                            </div>
                        </form>

                        <!--end::Form-->
                    </div>

                    <!--end::Portlet-->
                </div>
            </div>
        </div>

        <!-- end:: Content -->
</div>
@endsection
@section('script')
    <!--begin::Page Vendors(used by this page) -->


    <!--end::Page Vendors -->
    <script src="assets/js/pages/custom/user/add-user.js?v1.1" type="text/javascript"></script>
    <script src="assets/js/pages/components/portlets/tools.js" type="text/javascript"></script>
    <script src="assets/js/pages/crud/forms/widgets/select2.js" type="text/javascript"></script>
    <script src="assets/js/pages/crud/forms/widgets/bootstrap-daterangepicker.js" type="text/javascript"></script>

@endsection