@extends('admin.index')
@section('page-header', 'Subcribe Emails')
@section('page-sub_header', 'Cập nhập subcribe email')
@section('style')

@endsection
@section('content')
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

        <!-- begin:: Subheader -->
        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container  kt-container--fluid ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        Quản lý subcribe emails </h3>
                </div>
            </div>
        </div>

        <!-- end:: Subheader -->

        <!-- begin:: Content -->
        <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
            <div class="row">
                <div class="col-md-12">

                    <!--begin::Portlet-->
                    <div class="kt-portlet">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Cập Nhập subribe emails
                                </h3>
                            </div>
                        </div>

                        <!--begin::Form-->
                        <form class="kt-form" id="kt_edit_form">
                            <div class="kt-portlet__body">
                                <div class="form-group">
                                    {{csrf_field()}}
                                    <input type="hidden" id="_id" value="{{$subcribeInfo->id}}" />
                                    <label>Email:</label>
                                    <input type="text" class="form-control" placeholder="Nhập email" id="email" name="email" value="{{$subcribeInfo->email}}">
                                </div>
                                <div class="form-group">
                                    <label>Active:</label>
                                    <select class="form-control kt-select2" id="active" name="active">
                                        <option value="yes" <?= $subcribeInfo->active === 'yes' ? 'selected' : ''; ?>>Yes</option>
                                        <option value="no" <?= $subcribeInfo->active === 'no' ? 'selected' : ''; ?>>No</option>
                                    </select>
                                </div>
                            </div>
                            <div class="kt-portlet__foot">
                                <div class="kt-form__actions">
                                    <button type="button" id="btn_edit" class="btn btn-primary">Cập nhập</button>
                                    <button type="reset" class="btn btn-secondary">Hủy bỏ</button>
                                </div>
                            </div>
                        </form>

                        <!--end::Form-->
                    </div>

                    <!--end::Portlet-->
                </div>
            </div>
        </div>

        <!-- end:: Content -->
</div>
@endsection
@section('script')
    <!--end::Page Vendors -->
    <script src="admin/js/pages/subcribe_emails/edit-subcribe-emails.js" type="text/javascript"></script>
    <script src="assets/js/pages/components/portlets/tools.js" type="text/javascript"></script>
    <script src="assets/js/pages/crud/forms/widgets/select2.js" type="text/javascript"></script>
    <script src="assets/js/pages/crud/forms/widgets/bootstrap-datepicker.js" type="text/javascript"></script>

@endsection