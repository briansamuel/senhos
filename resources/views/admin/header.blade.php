<div id="kt_header" class="kt-header kt-grid__item  kt-header--fixed ">

    <!-- begin:: Header Menu -->

    <div class="kt-header-menu-wrapper" id="kt_header_menu_wrapper">

    </div>

    <!-- end:: Header Menu -->

    <!-- begin:: Header Topbar -->
    <div class="kt-header__topbar">

       

        

        <!--begin: User Bar -->
        <div class="kt-header__topbar-item kt-header__topbar-item--user">
            <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="0px,0px">
                <div class="kt-header__topbar-user">
                    <span class="kt-header__topbar-welcome kt-hidden-mobile">Hi,</span>
                    <span class="kt-header__topbar-username kt-hidden-mobile">{{isset(Auth::guard('admin')->user()->full_name) ? Auth::user()->full_name : ''}}</span>
                    <img class="kt-hidden" alt="Pic" src="assets/media/users/300_25.jpg" />
                </div>
            </div>
            <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround dropdown-menu-xl">

                <!--begin: Head -->
                <div class="kt-user-card kt-user-card--skin-dark kt-notification-item-padding-x" style="background-image: url(assets/media/misc/bg-1.jpg)">
                    <div class="kt-user-card__avatar">
                        <img class="" alt="Pic" src="{{Auth::guard('admin')->user()->avatar}}" />
                    </div>
                    <div class="kt-user-card__name">
                        {{ isset(Auth::guard('admin')->user()->full_name) ? Auth::guard('admin')->user()->full_name : ''  }}
                    </div>
                </div>

                <!--end: Head -->

                <!--begin: Navigation -->
                <div class="kt-notification">
                    <a href="/my-profile" class="kt-notification__item">
                        <div class="kt-notification__item-icon">
                            <i class="flaticon2-calendar-3 kt-font-success"></i>
                        </div>
                        <div class="kt-notification__item-details">
                            <div class="kt-notification__item-title kt-font-bold">
                                My Profile
                            </div>
                            <div class="kt-notification__item-time">
                                Thiết lập tài khoản
                            </div>
                        </div>
                    </a>
                    
                    <a href="{{ route('logs_user.list') }}" class="kt-notification__item">
                        <div class="kt-notification__item-icon">
                            <i class="flaticon2-rocket-1 kt-font-danger"></i>
                        </div>
                        <div class="kt-notification__item-details">
                            <div class="kt-notification__item-title kt-font-bold">
                                Hoạt động của tài khoản
                            </div>
                            <div class="kt-notification__item-time">
                                Logs Hoạt động
                            </div>
                        </div>
                    </a>
                    
                    
                    <div class="kt-notification__custom kt-space-between">
                        <a href="{{route('logout')}}" class="btn btn-label btn-label-brand btn-sm btn-bold">Đăng xuất</a>
                        
                    </div>
                </div>

                <!--end: Navigation -->
            </div>
        </div>

        <!--end: User Bar -->
    </div>

    <!-- end:: Header Topbar -->
</div>

<!-- end:: Header -->