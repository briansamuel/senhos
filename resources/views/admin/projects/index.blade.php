@extends('admin.index')
@section('page-header', 'DỰ ÁN')
@section('page-sub_header', 'Danh sách dự án')
@section('style')

@endsection
@section('content')

<div class="row">

    <div class="kt-portlet" data-ktportlet="true">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                    <i class="kt-font-brand flaticon2-line-chart"></i>
                </span>
                <h3 class="kt-portlet__head-title">
                    TÌM KIẾM DỰ ÁN
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <a href="{{ route('project.add') }}" class="btn btn-brand kt-btn">
                        <i class="flaticon2-plus"></i>
                        Thêm dự án
                    </a>

                </div>
            </div>
        </div>
        <form class="kt-form" method="POST">
            <div class="kt-portlet__body">

                <!--begin: Search Form -->
                <div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
                    <div class="row align-items-center">
                        <div class="col-xl-12 order-2 order-xl-1">
                            <div class="row align-items-center">
                                <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
                                    <label>Từ khóa:</label>
                                    <div class="kt-input-icon kt-input-icon--left">
                                        <input type="text" class="form-control" placeholder="Vui lòng nhập từ khóa..." id="project_title">
                                        <span class="kt-input-icon__icon kt-input-icon__icon--left">
                                            <span><i class="la la-search"></i></span>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
                                    <div class="form__group kt-form__group--inline">
                                        <label>Tình trạng:</label>
                                        <select class="form-control bootstrap-select" id="kt_form_status1">
                                            <option value="">Tất cả</option>
                                            <option value="draft">Nháp</option>
                                            <option value="trash">Thùng rác</option>
                                            <option value="pending">Chờ duyệt</option>
                                            <option value="publish">Đã xuất bản</option>
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="col-md-4 kt-margin-b-20-tablet-and-mobile" id="kt_form_daterange1">
                                    <div class="form__group kt-form__group--inline">
                                        <div class="kt-form__label">
                                            <label>Theo ngày tạo:</label>
                                        </div>
                                        <input type='text' class="form-control" id="kt_daterangepicker_1" readonly placeholder="Chọn thời gian" type="text" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 order-1 order-xl-2 kt-align-right">
                            <a href="#" class="btn btn-default kt-hidden">
                                <i class="la la-cart-plus"></i> New Order
                            </a>
                            <div class="kt-separator kt-separator--border-dashed kt-separator--space-lg d-xl-none"></div>
                        </div>
                    </div>
                </div>

                <!--end: Search Form -->

                <!--begin: Selected Rows Group Action Form -->
                <div class="kt-form kt-form--label-align-right kt-margin-t-20 collapse" id="kt_datatable_group_action_form1">
                    <div class="row align-items-center">
                        <div class="col-xl-12">
                            <div class="kt-form__group kt-form__group--inline">
                                <div class="kt-form__label kt-form__label-no-wrap">
                                    <label class="kt-font-bold kt-font-danger-">Chọn
                                        <span id="kt_datatable_selected_number1">0</span> dòng:</label>
                                    {{csrf_field()}}
                                </div>
                                <div class="kt-form__control">
                                    <div class="btn-toolbar">
                                        <div class="dropdown">
                                            <button type="button" class="btn btn-brand btn-sm dropdown-toggle" data-toggle="dropdown">
                                                Cập nhật trạng thái
                                            </button>
                                            <div class="dropdown-menu">
                                                <a class="dropdown-item kt_datatable_update_status_all" href="javascript:void(0)" attr="draft">Nháp</a>
                                                <a class="dropdown-item kt_datatable_update_status_all" href="javascript:void(0)" attr="trash">Thùng rác</a>
                                                <a class="dropdown-item kt_datatable_update_status_all" href="javascript:void(0)" attr="pending">Chờ duyệt</a>
                                                <a class="dropdown-item kt_datatable_update_status_all" href="javascript:void(0)" attr="publish">Đã xuất bản</a>
                                            </div>
                                        </div>
                                        &nbsp;&nbsp;&nbsp;
                                        <button class="btn btn-sm btn-danger" type="button" id="kt_datatable_delete_all">Xóa Tất Cả</button>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!--end: Selected Rows Group Action Form -->
            </div>
            <div class="kt-portlet__body kt-portlet__body--fit">
                @include('admin.elements.alert_flash')
                <!--begin: Datatable -->
                <div class="kt-datatable" id="server_projects_selection"></div>

                <!--end: Datatable -->
            </div>

        </form>
    </div>

</div>
@endsection
@section('script')
<!--begin::Page Vendors(used by this page) -->


<!--end::Page Vendors -->
<!-- <script src="admin/js/news.js" type="text/javascript"></script> -->
<script src="assets/js/pages/components/portlets/tools.js" type="text/javascript"></script>
<script src="assets/js/pages/crud/forms/widgets/select2.js" type="text/javascript"></script>
<script src="assets/js/pages/crud/forms/widgets/bootstrap-daterangepicker.js" type="text/javascript"></script>
<script src="admin/js/pages/projects/list-project.js" type="text/javascript"></script>
<!-- <script src="assets/js/pages/crud/metronic-datatable/advanced/record-selection.js" type="text/javascript"></script> -->

@endsection