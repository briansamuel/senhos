@extends('admin.index')
@section('page-header', 'DỰ ÁN')
@section('page-sub_header', 'CHỈNH SỬA DỰ ÁN')
@section('style')
<link rel="stylesheet" href="admin/plugins/fancybox/jquery.fancybox.min.css" />
@endsection
@section('content')
<form method="POST" action="{{ route('project.edit', ['id' => $project->id ])}}">
    {{ csrf_field()}}

    <div class="row">
        @if ($errors->any())
        @foreach ($errors->all() as $error)
        <div class="col-12">
            <div class="alert alert-solid-danger alert-bold" role="alert">
                <div class="alert-text">{{ $error }}</div>
                <div class="alert-close">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true"><i class="la la-close"></i></span>
                    </button>
                </div>
            </div>
        </div>
        @endforeach
        @endif

        <div class="col-12">
            @include('admin.elements.alert_flash')
        </div>
    </div>
    <div class="row">

        <div class="col-md-8 col-lg-9">
            <div class="kt-portlet">
                <div class="kt-portlet__head kt-portlet__head--right">
                    <div class="kt-portlet__head-label ">
                        <span class="kt-font-danger"><i class="fa fa-star"></i> Bắt buộc phải nhập / chọn nội dung</span>
                    </div>
                </div>
                <!--begin::Form-->
                <div class="kt-form ">
                    <div class="kt-portlet__body">
                        <div class="form-group row">
                            <label for="project_title" class="col-12 col-lg-12 col-xl-3">Tiêu đề:</label>
                            <div class="col-12 col-lg-12 col-xl-9">
                                <input class="form-control" type="text" value="{{ $project->project_title }}" id="project_title" name="project_title" placeholder="Tên dự án bắt buộc phải nhập nội dung">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="project_slug" class="col-12 col-lg-12 col-xl-3">Slug:</label>
                            <div class="col-12 col-lg-12 col-xl-9">
                                <input class="form-control" type="text" value="{{ $project->project_slug }}" id="project_slug" name="project_slug" placeholder="Nhập nội dung đường dẫn URL">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="project_slug" class="col-12 col-lg-12 col-xl-3">Vị trí:</label>
                            <div class="col-12 col-lg-12 col-xl-9">
                                <input class="form-control" type="text" value="{{ $project->project_location }}" id="project_location" name="project_location" placeholder="Nhập nội dung vị trí dự án">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="project_description" class="col-12 col-lg-12 col-xl-3">Tóm tắt dự án:</label>
                            <div class="col-12 col-lg-12 col-xl-9">
                                <textarea id="project_description" rows="4" name="project_description" class="tox-target">
                                {{ $project->project_description }}
                                </textarea>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="project_content" class="col-12 col-lg-12 col-xl-3">Nội dung:</label>
                            <div class="col-12 col-lg-12 col-xl-9">
                                <textarea id="project_content" name="project_content" class="tox-target" placeholder="Mô tả dự án">
                                {{ $project->project_content }}
                                </textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            SEO
                        </h3>
                    </div>
                </div>
                <!--begin::Form-->
                <div class="kt-form ">
                    <div class="kt-portlet__body">
                        <div class="form-group row">
                            <label for="project_seo_title" class="col-12 col-lg-12 col-xl-3">Tiêu đề:</label>
                            <div class="col-12 col-lg-12 col-xl-9">
                                <input class="form-control" type="text" value="{{ $project->project_seo_title }}" id="project_seo_title" name="project_seo_title" placeholder="Tiêu đề SEO">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="project_seo_keyword" class="col-12 col-lg-12 col-xl-3">Từ khóa:</label>
                            <div class="col-12 col-lg-12 col-xl-9">
                                <input class="form-control" type="text" value="{{ $project->project_seo_keyword }}" id="project_seo_keyword" name="project_seo_keyword" placeholder="Nhập từ cách nhau bằng dấu phẩy">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="project_seo_description" class="col-12 col-lg-12 col-xl-3">Nội dung:</label>
                            <div class="col-12 col-lg-12 col-xl-9">
                                <textarea class="form-control" type="text" value="{{ $project->project_seo_description }}" id="project_seo_description" name="project_seo_description" rows="6" placeholder="Nội dung SEO không quá 160 từ."></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-lg-3">
            <div class="kt-portlet">
                <div class="kt-portlet__body p-3">
                    
                @if($project->language == 'vi')
               <div class="form-group row mb-0">
                  <div class="col-md-12">
                     <table class="table table-bordered table-striped">
                        <tbody>
                           <tr>
                              <td width="55%"></td>
                              <td width="15%" class="text-center">Dịch</td>
                              <td width="30%" class="text-center">Chi tiết</td>
                           </tr>
                           <tr>
                              <td class="va-middle">English</td>
                              <td class="text-center va-middle">
                                 <a href="{{ route('project.add') }}?parent_id={{ $project->id }}&lang=en">
                                    <i class="fa fa-plus"></i>
                                 </a>
                              </td>
                              <td class="text-center va-middle"><a href="javascript:;">
                                    <i class="fa fa fa-link"></i>
                                 </a>
                              </td>
                           </tr>
                        </tbody>
                     </table>
                  </div>
               </div>
               @endif
                <div class="form-group row mb-3">
                    <label for="language" class="col-4 col-form-label">Ngôn ngữ:</label>
                    <div class="col-8">
                        <select class="form-control kt-select2" id="language" name="language">
                            @foreach($arrayLang as $key=>$lang)
                                <option value="{{$key}}" <?= $project->language === $key ? 'selected' : ''; ?>>{{$lang}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
               </div>
                    <div class="form-group row mb-3">
                        <label for="created_at" class="col-4 col-form-label">Ngày công bố:</label>
                        <div class="col-8">
                            <input type="text" class="form-control" name="created_at" readonly="" value="{{ $project->created_at }}" placeholder="Chọn ngày" id="kt_datetimepicker_2">
                        </div>
                    </div>
                    <div class="form-group row mb-3">
                        <label for="project_status" class="col-4 col-form-label">Ngôn ngữ:</label>
                        <div class="col-8">
                            <select class="form-control kt-select2" id="project_status" name="project_status">
                                <option value="draft" <?= $project->project_status === 'draft' ? 'selected' : ''; ?>>Bản nháp</option>
                                <option value="pending" <?= $project->project_status === 'pending' ? 'selected' : ''; ?>>Chờ duyệt</option>
                                <option value="publish" <?= $project->project_status === 'publish' ? 'selected' : ''; ?>>Xuất bản</option>
                            </select>

                        </div>
                    </div>

                </div>
                <div class="kt-portlet__foot kt-align-right p-2">
                    <div>
                        <button type="submit" class="btn btn-primary"><i class="la la-save"></i> Lưu dữ liệu</button>
                    </div>
                </div>
            </div>

            <div class="kt-portlet">
                <div class="kt-portlet__body p-0">


                    <div class="form-group row mb-0">
                        <div class="col-12">
                            <a data-src="@filemanager_get_resource(dialog.php)?type=1&field_id=project_thumbnail&lang=vi&akey=@filemanager_get_key()" class="iframe-btn" data-fancybox data-fancybox data-type="iframe" href="javascript:;">
                                @if(isset($project->project_thumbnail) && $project->project_thumbnail != '')
                                <img id="preview_thumbnail" class="img-fluid" src="{{ $project->project_thumbnail }}">
                                @else
                                <img id="preview_thumbnail" class="img-fluid" src="admin/images/upload-thumbnail.png">
                                @endif
                            </a>

                            <input type="hidden" name="project_thumbnail" id="project_thumbnail" value="{{ $project->project_thumbnail }}">
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>
</form>
@endsection
@section('vendor-script')
<script src="assets/plugins/custom/tinymce/tinymce.bundle.js" type="text/javascript"></script>
@endsection
@section('script')
<script src="assets/js/pages/crud/forms/widgets/bootstrap-datetimepicker.js" type="text/javascript"></script>
<script src="assets/js/pages/crud/forms/widgets/select2.js" type="text/javascript"></script>
<script src="admin/plugins/fancybox/jquery.fancybox.min.js"></script>
<script type="text/javascript" src="admin/plugins/fancybox/jquery.observe_field.js"></script>
<script src="admin/js/pages/news/add-news.js" type="text/javascript"></script>
<script>
    ! function(t) {
        var e = {};

        function n(i) {
            if (e[i]) return e[i].exports;
            var r = e[i] = {
                i: i,
                l: !1,
                exports: {}
            };
            return t[i].call(r.exports, r, r.exports, n), r.l = !0, r.exports
        }
        n.m = t, n.c = e, n.d = function(t, e, i) {
            n.o(t, e) || Object.defineProperty(t, e, {
                enumerable: !0,
                get: i
            })
        }, n.r = function(t) {
            "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(t, Symbol.toStringTag, {
                value: "Module"
            }), Object.defineProperty(t, "__esModule", {
                value: !0
            })
        }, n.t = function(t, e) {
            if (1 & e && (t = n(t)), 8 & e) return t;
            if (4 & e && "object" == typeof t && t && t.__esModule) return t;
            var i = Object.create(null);
            if (n.r(i), Object.defineProperty(i, "default", {
                    enumerable: !0,
                    value: t
                }), 2 & e && "string" != typeof t)
                for (var r in t) n.d(i, r, function(e) {
                    return t[e]
                }.bind(null, r));
            return i
        }, n.n = function(t) {
            var e = t && t.__esModule ? function() {
                return t.default
            } : function() {
                return t
            };
            return n.d(e, "a", e), e
        }, n.o = function(t, e) {
            return Object.prototype.hasOwnProperty.call(t, e)
        }, n.p = "", n(n.s = 677)
    }({
        677: function(t, e, n) {
            "use strict";
            var i = {
                init: function() {
                    tinymce.init({
                        selector: "#project_description",
                        toolbar: !1,
                        statusbar: !1,
                        height: 200,
                    }), 
                    tinymce.init({
                        selector: "#project_content",
                        menubar: !1,
                        paste_data_images: true,
                        relative_urls: false,
                        remove_script_host: false,
                        toolbar: ["styleselect fontselect fontsizeselect", "undo redo | cut copy paste | bold italic | link image | alignleft aligncenter alignright alignjustify", "bullist numlist | outdent indent | blockquote subscript superscript | advlist | autolink | lists charmap | print preview |  code"],
                        plugins: [
                            "advlist autolink link image lists charmap print preview hr anchor pagebreak",
                            "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
                            "table contextmenu directionality emoticons paste textcolor code"
                        ],
                        image_advtab: true,
                        image_advtab: true,
                        image_advtab: true,
                        filemanager_access_key: '@filemanager_get_key()',
                        filemanager_sort_by: '',
                        filemanager_descending: '',
                        filemanager_subfolder: '',
                        filemanager_crossdomain: '',
                        external_filemanager_path: '@filemanager_get_resource(dialog.php)',
                        filemanager_title: "Responsive Filemanager",
                        external_plugins: {
                            "filemanager": "http://admin.senhos.local/vendor/responsivefilemanager/plugin.min.js"
                        }
                    })
                }
            };
            jQuery(document).ready((function() {
                i.init()
            }))
        }
    });

    $('.iframe-btn').fancybox({
        // 'width': 900,
        // 'height': 600,
        // 'type': 'iframe',
        'iframe': {
            'css': {
                'width': '90%',
                'height': '90%',
            }
        },
        // 'autoScale': true
    });

    $(document).ready(function() {

        $("#project_thumbnail").observe_field(1, function() {
            // alert('Change observed! new value: ' + this.value );
            $('#preview_thumbnail').attr('src', this.value).show();

        });

    });
</script>
@endsection