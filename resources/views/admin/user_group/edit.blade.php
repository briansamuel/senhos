@extends('admin.index')
@section('page-header', 'User Group')
@section('page-sub_header', 'Cập nhập user group')
@section('style')

@endsection
@section('content')
<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

    <!-- begin:: Subheader -->
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    Cập Nhập User Group </h3>
            </div>
        </div>
    </div>

    <!-- end:: Subheader -->

    <!-- begin:: Content -->
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="col-md-12">

                <!--begin::Portlet-->
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                Thêm mới user group
                            </h3>
                        </div>
                    </div>

                    <!--begin::Form-->
                    <form class="kt-form" id="kt_edit_form">
                        <div class="kt-portlet__body">
                            <div class="form-group">
                                <label>Tên User Group:</label>
                                {{csrf_field()}}
                                <input type="hidden" name="_id" id="_id" value="{{$userGroup->id}}" />
                                <input type="text" class="form-control" name="group_name" value="{{$userGroup->group_name}}">
                            </div>
                            <div class="form-group">
                                <label>Kích Hoạt:</label>
                                <select class="form-control" name="enabled">
                                    <option value="">Vui lòng chọn</option>
                                    <option value="1" <?= $userGroup->enabled === 1 ? 'selected' : ''; ?>>Yes</option>
                                    <option value="0" <?= $userGroup->enabled === 0 ? 'selected' : ''; ?>>No</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Mô Tả:</label>
                                <textarea class="form-control" name="short_description">{{$userGroup->short_description}}</textarea>
                            </div>
                            <!--begin::Section-->
                            <div class="kt-section">
                                <div class="kt-section__content">
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>Controller Name</th>
                                                <th>Chức Năng</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $i = 0 ?>
                                            @foreach($controllers as $key => $value)
                                            <tr class="tr<?= $i % 2 ?>">
                                                <td width="30%">
                                                    {{$key}}
                                                    <a href="javascript:toggle('{{$key}}');">(nhấn vào để ẩn/hiện)</a>
                                                </td>
                                                <td>
                                                    <span id="{{$key}}">
                                                        <?php $j = 0; ?>
                                                        @foreach ($value as $subkey => $title)
                                                        @if ($j == 0)
                                                        <span style="color:red;width:100%;float:left;">
                                                            <label class="kt-checkbox kt-checkbox--tick kt-checkbox--brand">
                                                                <input style="width:10px;" <?= in_array($key, $permissions) ? 'checked' : '' ?> type="checkbox" class="<?= $key ?>" name="<?= $key; ?>[]" value="{{$subkey}}" onclick="check_input('{{$key}}', this.checked);" />{{$title}}<br />
                                                                <span></span>
                                                            </label>
                                                        </span>
                                                        @else
                                                        <span style="width:33%;float:left;">
                                                            <label class="kt-checkbox kt-checkbox--tick kt-checkbox--brand">
                                                                <input style="width:10px;" <?= in_array($key, $permissions) || in_array($subkey, $permissions) ? 'checked' : '' ?> type="checkbox" class="_{{$key}}" name="{{$key}}[]" value="{{$subkey}}" onclick="check_input('{{$subkey}}', this.checked);" /> {{$title}}
                                                                <span></span>
                                                            </label>
                                                        </span>
                                                        @endif
                                                        <?php $j++ ?>
                                                        @endforeach
                                                    </span>
                                                </td>
                                            </tr>
                                            <?php $i++ ?>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!--end::Form-->
                        </div>
                        <div class="kt-portlet__foot">
                            <div class="kt-form__actions">
                                <button type="button" id="btn_edit" class="btn btn-primary">Cập nhập</button>
                                <button type="reset" class="btn btn-secondary">Hủy bỏ</button>
                            </div>
                        </div>
                    </form>

                    <!--end::Form-->
                </div>

                <!--end::Portlet-->
            </div>
        </div>
    </div>

    <!-- end:: Content -->
</div>
@endsection
@section('script')
<script>
    function toggle(id) {
        $('#' + id).slideToggle('fast');
    }

    function check_input(id, value) {
        if (value) {
            $("._" + id).prop('checked', true);
        } else {
            $("._" + id).prop('checked', false);
        }
    }
</script>
<!--end::Page Vendors -->
<script src="assets/js/pages/components/portlets/tools.js" type="text/javascript"></script>
<script src="assets/js/pages/crud/forms/widgets/select2.js" type="text/javascript"></script>
<script src="admin/js/pages/user_group/edit_user_group.js" type="text/javascript"></script>

@endsection