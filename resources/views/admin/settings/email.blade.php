@extends('admin.index')
@section('page-header', 'Cài Đặt')
@section('page-sub_header', 'Cài đặt Email')
@section('style')

@endsection
@section('content')
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

        <!-- begin:: Subheader -->
        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container  kt-container--fluid ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        Cài Đặt Email </h3>
                </div>
            </div>
        </div>

        <!-- end:: Subheader -->

        <!-- begin:: Content -->
        <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
            <div class="row">
                <div class="col-md-12">

                    <!--begin::Portlet-->
                    <div class="kt-portlet">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Cài Đặt Email
                                </h3>
                            </div>
                        </div>

                        <!--begin::Form-->
                        <form class="kt-form" id="kt_edit_form">
                            <div class="kt-portlet__body">
                                {{csrf_field()}}
                                @foreach($settings as $group_key => $group)
                                    <div class="kt-section kt-section--first">
                                        <h3 class="kt-section__title">{{$group['name']}}</h3>
                                        <div class="kt-section__body">
                                            @foreach($group['data'] as $key => $setting)
                                                <div class="form-group row">
                                                    <label class="col-lg-3 col-form-label">{{$setting['name']}}:</label>
                                                    <div class="col-lg-6">
                                                        @if(isset($dbSetting['email'][$group_key]['data'][$key]))
                                                            <?php $setting_data =  $dbSetting['email'][$group_key]['data'][$key] ?>
                                                            @if($setting['view'] === 'textarea')
                                                                <textarea rows="5" class="form-control" name="{{$setting['description']}}">{{$setting_data->setting_value}}</textarea>
                                                            @elseif($setting['view'] === 'file')
                                                                <div class="kt-avatar kt-avatar--outline kt-avatar--circle" id="{{$key}}">
                                                                    <label></label>
                                                                    <div class="kt-avatar__holder" style="background-image: url('{{$setting_data->setting_value}}')"></div>
                                                                    <label class="kt-avatar__upload" data-toggle="kt-tooltip" title="" data-original-title="Change logo">
                                                                        <i class="fa fa-pen"></i>
                                                                        <input type="file" name="{{$setting['description']}}" accept="image/*">
                                                                    </label>
                                                                    <span class="kt-avatar__cancel" data-toggle="kt-tooltip" title="" data-original-title="Cancel logo">
                                                            <i class="fa fa-times"></i>
                                                        </span>
                                                                </div>
                                                            @else
                                                                <input type="{{$setting['view']}}" class="form-control" name="{{$setting['description']}}" value="{{$setting_data->setting_value}}">
                                                            @endif
                                                        @else
                                                            @if($setting['view'] === 'textarea')
                                                                <textarea rows="5" class="form-control" name="{{$setting['description']}}"></textarea>
                                                            @elseif($setting['view'] === 'file')
                                                                <div class="kt-avatar kt-avatar--outline kt-avatar--circle" id="{{$key}}">
                                                                    <label></label>
                                                                    <div class="kt-avatar__holder"></div>
                                                                    <label class="kt-avatar__upload" data-toggle="kt-tooltip" title="" data-original-title="Change logo">
                                                                        <i class="fa fa-pen"></i>
                                                                        <input type="file" name="{{$setting['description']}}" accept="image/*">
                                                                    </label>
                                                                    <span class="kt-avatar__cancel" data-toggle="kt-tooltip" title="" data-original-title="Cancel logo">
                                                            <i class="fa fa-times"></i>
                                                        </span>
                                                                </div>
                                                            @else
                                                                <input type="{{$setting['view']}}" class="form-control" name="{{$setting['description']}}">
                                                            @endif
                                                        @endif
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <div class="kt-portlet__foot">
                                <div class="kt-form__actions">
                                    <button type="button" id="btn_edit" class="btn btn-primary">Cập Nhập</button>
                                    <button type="reset" class="btn btn-secondary">Hủy bỏ</button>
                                </div>
                            </div>
                        </form>

                        <!--end::Form-->
                    </div>

                    <!--end::Portlet-->
                </div>
            </div>
        </div>

        <!-- end:: Content -->
    </div>
@endsection
@section('script')
    <!--end::Page Vendors -->
    <script src="assets/js/pages/crud/forms/widgets/bootstrap-datepicker.js" type="text/javascript"></script>
    <script src="assets/js/pages/components/portlets/tools.js" type="text/javascript"></script>
    <script src="assets/js/pages/crud/forms/widgets/select2.js" type="text/javascript"></script>
    <script src="admin/js/pages/settings/setting-email.js" type="text/javascript"></script>

@endsection