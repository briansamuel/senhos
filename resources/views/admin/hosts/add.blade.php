@extends('admin.index')
@section('page-header', 'KHÁCH SẠN')
@section('page-sub_header', 'THÊM MỚI KHÁCH SẠN')
@section('style')
<link rel="stylesheet" href="admin/plugins/fancybox/jquery.fancybox.min.css" />
<style type="text/css">
.remove_image {
    position: absolute;
    right: 10%;
    top: 5%;
}
.remove_image i {
    font-size: 20px;
}
</style>
@endsection
@section('content')
<form method="POST" action="{{ route('host.add.action')}}">
    {{ csrf_field()}}


    <div class="row">
        @if ($errors->any())
        @foreach ($errors->all() as $error)
        <div class="col-12">
            <div class="alert alert-solid-danger alert-bold" role="alert">
                <div class="alert-text">{{ $error }}</div>
                <div class="alert-close">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true"><i class="la la-close"></i></span>
                    </button>
                </div>
            </div>
        </div>
        @endforeach
        @endif

        <div class="col-12">
            @include('admin.elements.alert_flash')
        </div>
    </div>
    <div class="row" data-sticky-container>

        <div class="col-md-8 col-lg-9">
            <div class="kt-portlet">
                <div class="kt-portlet__head kt-portlet__head--right">
                    <div class="kt-portlet__head-label ">
                        <span class="kt-font-danger"><i class="fa fa-star"></i> Bắt buộc phải nhập / chọn nội dung</span>
                    </div>
                </div>
                <!--begin::Form-->
                <div class="kt-form ">
                    <div class="kt-portlet__body">
                        <div class="form-group row">
                            <label for="host_name" class="col-12 col-lg-12 col-xl-3 col-form-label">Tên Khách Sạn:</label>
                            <div class="col-12 col-lg-12 col-xl-9">
                                <input class="form-control" type="text" value="" id="host_name" name="host_name" placeholder="Tên khách sạn bắt buộc nhập">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="host_slug" class="col-12 col-lg-12 col-xl-3 col-form-label">Slug:</label>
                            <div class="col-12 col-lg-12 col-xl-9">
                                <input class="form-control" type="text" value="" id="host_slug" name="host_slug" placeholder="Nhập nội dung đường dẫn URL">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="host_description" class="col-12 col-lg-12 col-xl-3 col-form-label">Mô tả:</label>
                            <div class="col-12 col-lg-12 col-xl-9">
                                <textarea id="host_description" name="host_description" class="tox-target">
                     </textarea>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
            <div class="kt-portlet kt-portlet--collapsed" data-ktportlet="true" id="kt_portlet_tools_policy">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Chính sách khách sạn
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <div class="kt-portlet__head-group">
                            <a href="#" data-ktportlet-tool="toggle" class="btn btn-sm btn-icon btn-clean btn-icon-md" aria-describedby="tooltip_gggrqlstux"><i class="la la-angle-down"></i></a>

                        </div>
                    </div>
                </div>
                <div class="kt-form ">
                    <div class="kt-portlet__body">
                        <div class="form-group row">
                            <label for="post_policy" class="col-12 col-lg-12 col-xl-3 col-form-label">Nội dung chính sách:</label>
                            <div class="col-12 col-lg-12 col-xl-9">
                                <textarea id="host_policy" name="host_policy" class="tox-target"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="kt-portlet kt-portlet--collapsed" data-ktportlet="true" id="kt_portlet_tools_convenient">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Tiện nghi khách sạn
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <div class="kt-portlet__head-group">
                            <a href="#" data-ktportlet-tool="toggle" class="btn btn-sm btn-icon btn-clean btn-icon-md" aria-describedby="tooltip_gggrqlstux"><i class="la la-angle-down"></i></a>

                        </div>
                    </div>
                </div>
                <div class="kt-form ">
                    <div class="kt-portlet__body">
                        <div class="form-group row">
                            <div class="col-12 col-sm-6 col-md-3 col-lg-4">
                                <h5 class="kt-font-primary">Dịch vụ khách sạn</h5>
                                <div class="kt-checkbox-list">

                                    @foreach($convenients['service_hotel'] as $key => $item)
                                    <label class="kt-checkbox kt-checkbox--tick kt-checkbox--success">
                                        <input type="checkbox" name="host_convenient[service_hotel][{{ $key }}]" value="{{ $item }}">{{ $item }}<span></span></label>
                                    @endforeach

                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-md-3 col-lg-4">
                                <h5 class="kt-font-primary">Ẩm thực</h5>
                                <div class="kt-checkbox-list">

                                    @foreach($convenients['foods'] as $key => $item)
                                    <label class="kt-checkbox kt-checkbox--tick kt-checkbox--success">
                                        <input type="checkbox" name="host_convenient[foods][{{ $key }}]" value="{{ $item }}">{{ $item }}<span></span></label>
                                    @endforeach
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-md-3 col-lg-4">
                                <h5 class="kt-font-primary">Tiện nghi phòng</h5>
                                <div class="kt-checkbox-list">

                                    @foreach($convenients['convenient_room'] as $key => $item)
                                    <label class="kt-checkbox kt-checkbox--tick kt-checkbox--success">
                                        <input type="checkbox" name="host_convenient[convenient_room][{{ $key }}]" value="{{ $item }}">{{ $item }}<span></span></label>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-12 col-sm-6 col-md-3 col-lg-4">
                                <h5 class="kt-font-primary">Tiện nghi chung</h5>
                                <div class="kt-checkbox-list">


                                    @foreach($convenients['convenient_general'] as $key => $item)
                                    <label class="kt-checkbox kt-checkbox--tick kt-checkbox--success">
                                        <input type="checkbox" name="host_convenient[convenient_general][{{ $key }}]" value="{{ $item }}">{{ $item }}<span></span></label>
                                    @endforeach
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-md-3 col-lg-4">
                                <h5 class="kt-font-primary">Tiện nghi công cộng</h5>
                                <div class="kt-checkbox-list">

                                    @foreach($convenients['convenient_public'] as $key => $item)
                                    <label class="kt-checkbox kt-checkbox--tick kt-checkbox--success">
                                        <input type="checkbox" name="host_convenient[convenient_public][{{ $key }}]" value="{{ $item }}">{{ $item }}<span></span></label>
                                    @endforeach
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-md-3 col-lg-4">
                                <h5 class="kt-font-primary">Tiện nghi văn phòng</h5>
                                <div class="kt-checkbox-list">

                                    @foreach($convenients['convenient_office'] as $key => $item)
                                    <label class="kt-checkbox kt-checkbox--tick kt-checkbox--success">
                                        <input type="checkbox" name="host_convenient[convenient_office][{{ $key }}]" value="{{ $item }}">{{ $item }}<span></span></label>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-12 col-sm-6 col-md-3 col-lg-4">
                                <h5 class="kt-font-primary">Các hoạt động</h5>
                                <div class="kt-checkbox-list">

                                    @foreach($convenients['convenient_activaty'] as $key => $item)
                                    <label class="kt-checkbox kt-checkbox--tick kt-checkbox--success">
                                        <input type="checkbox" name="host_convenient[convenient_activaty][{{ $key }}]" value="{{ $item }}">{{ $item }}<span></span></label>
                                    @endforeach
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-md-3 col-lg-4">
                                <h5 class="kt-font-primary">Vận chuyển</h5>
                                <div class="kt-checkbox-list">

                                    @foreach($convenients['convenient_transport'] as $key => $item)
                                    <label class="kt-checkbox kt-checkbox--tick kt-checkbox--success">
                                        <input type="checkbox" name="host_convenient[convenient_transport][{{ $key }}]" value="{{ $item }}">{{ $item }}<span></span></label>
                                    @endforeach
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-md-3 col-lg-4">
                                <h5 class="kt-font-primary">Hỗ trợ người khuyết tật</h5>
                                <div class="kt-checkbox-list">
                                    <label class="kt-checkbox kt-checkbox--tick kt-checkbox--success">
                                        <input type="checkbox" name="host_convenient[support_people][{{ $key }}]">Thuận tiện cho người khuyết tật
                                        <span></span>
                                    </label>

                                    @foreach($convenients['support_people'] as $key => $item)
                                    <label class="kt-checkbox kt-checkbox--tick kt-checkbox--success">
                                        <input type="checkbox" name="host_convenient[support_people][{{ $key }}]" value="{{ $item }}">{{ $item }}<span></span></label>
                                    @endforeach
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="kt-portlet" id="kt_portlet_tools_gallery">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Thư viện hình ảnh Khách Sạn
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <div class="kt-portlet__head-group">
                            <a href="#" data-ktportlet-tool="toggle" class="btn btn-sm btn-icon btn-clean btn-icon-md" aria-describedby="tooltip_gggrqlstux"><i class="la la-angle-down"></i></a>

                        </div>
                    </div>
                </div>
                <div class="kt-form ">
                    <div class="kt-portlet__body">
                        <div id="preview_gallery" class="preview_gallery sortable sortable-dragging sortable-placeholder row form-group">

                        </div>
                    </div>
                </div>
                <div class="kt-portlet__foot kt-align-right p-2">
                    <div>
                        <a data-src="@filemanager_get_resource(dialog.php)?type=1&field_id=host_gallery_select&lang=vi&akey=@filemanager_get_key()&fldr=khach-san" class="iframe-btn btn btn-primary" data-fancybox data-fancybox data-type="iframe" href="javascript:;">
                            Upload Image
                        </a>
                        <input type="hidden" id="host_gallery_select" name="host_gallery_select">
                    </div>
                </div>

            </div>
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Vị trí khách sạn
                        </h3>
                    </div>
                </div>
                <!--begin::Form-->
                <div class="kt-form ">
                    <div class="kt-portlet__body">
                        <div class="form-group row">
                            <label for="host_address" class="col-12 col-lg-12 col-xl-3 col-form-label">Địa chỉ</label>
                            <div class="col-12 col-lg-12 col-xl-9">
                                <input class="form-control" type="text" value="" id="host_address" name="host_address" placeholder="Nhập địa chỉ khách sạn của bạn">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="" class="col-12 col-lg-12 col-xl-3 col-form-label">Khu vực:</label>
                            <div class="col-12 col-lg-12 col-xl-3">
                                <select class="form-control kt-select2" id="kt_select_province" name="province_id" required>
                                    <option value="">Chọn Tỉnh/Thành Phố</option>
                                    @foreach($provinces as $province)
                                    <option value="{{ $province->id }}">{{ $province->_name }}</option>
                                    @endforeach
                                </select>
                                <input type="hidden" name="province_name" id="province_name" />
                            </div>

                            <div class="col-12 col-lg-12 col-xl-3">
                                <select class="form-control kt-select2" id="kt_select_district" name="district_id" required>
                                    <option value="">Chọn Quận/Huyện</option>
                                </select>
                                <input type="hidden" name="district_name" id="district_name" />
                            </div>
                            <div class="col-12 col-lg-12 col-xl-3">
                                <select class="form-control kt-select2" id="kt_select_ward" name="ward_id" required>
                                    <option value="">Chọn Phường/Xã</option>
                                </select>
                                <input type="hidden" name="ward_name" id="ward_name" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="" class="col-12 col-lg-12 col-xl-3 col-form-label">Vị trí:</label>
                            <div class="col-12 col-lg-12 col-xl-9">
                                <div id="kt_gmap_2" style="height:300px;"></div>
                                <br>
                                <p><span class="kt-font-danger">Click vào bản đồ để chọn tọa đô</span></p>

                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="host_lat" class="col-12 col-lg-12 col-xl-3 col-form-label">Kinh độ:</label>
                            <div class="col-12 col-lg-12 col-xl-9">
                                <input class="form-control" type="text" value="" id="host_lat" name="host_lat" placeholder="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="host_lng" class="col-12 col-lg-12 col-xl-3 col-form-label">Vĩ độ:</label>
                            <div class="col-12 col-lg-12 col-xl-9">
                                <input class="form-control" type="text" value="" id="host_lng" name="host_lng" placeholder="">
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
        <div class="col-md-4 col-lg-3">
            <div class="kt-portlet sticky">
                <div class="kt-portlet__body p-3">


                    <div class="form-group row mb-3">
                        <label for="created_at" class="col-4 col-form-label">Ngày tạo:</label>
                        <div class="col-8">
                            <input type="text" class="form-control" name="created_at" readonly="" value="{{ date('Y/m/d h:m') }}" placeholder="Chọn ngày" id="kt_datetimepicker_2">
                        </div>
                    </div>
                    <div class="form-group row mb-3">
                        <label for="host_status" class="col-4 col-form-label">Tình trạng:</label>
                        <div class="col-8">
                            <select class="form-control kt-select2" id="host_status" name="host_status">
                                <option value="draft">Ẩn</option>
                                <option value="pending">Chờ duyệt</option>
                                <option value="publish">Hiển thị</option>
                               
                            </select>

                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="language" class="col-4 col-form-label">Ngôn ngữ:</label>
                        <div class="col-8">
                            <select class="form-control kt-select2" disabled id="language" name="language">
                                @foreach($arrayLang as $key=>$lang)
                                    <option value="{{$key}}">{{$lang}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="kt-portlet__foot kt-align-right p-2">
                    <div>
                        <button type="submit" class="btn btn-primary"><i class="la la-save"></i> Lưu dữ liệu</button>
                    </div>
                </div>
            </div>
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Chọn Loại Cơ sở
                        </h3>
                    </div>
                </div>
                <!--begin::Form-->
                <div class="kt-form ">
                    <div class="kt-portlet__body">
                        <div class="form-group">

                            <div class="kt-radio-list">
                                <label class="kt-radio kt-radio--bold kt-radio--brand">
                                    <input type="radio" name="host_type" value="hotel" checked > Khách sạn
                                    <span></span>
                                </label>
                                <label class="kt-radio kt-radio--bold kt-radio--brand">
                                    <input type="radio" name="host_type" value="resort"> Resort
                                    <span></span>
                                </label>
                                <label class="kt-radio kt-radio--bold kt-radio--brand">
                                    <input type="radio" name="host_type" value="villa"> Biệt thự
                                    <span></span>
                                </label>
                                <label class="kt-radio kt-radio--bold kt-radio--brand">
                                    <input type="radio" name="host_type" value="homestay"> HomeStay
                                    <span></span>
                                </label>
                                <label class="kt-radio kt-radio--bold kt-radio--brand">
                                    <input type="radio" name="host_type" value="apartment"> Căn hộ, chung cư
                                    <span></span>
                                </label>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="kt-portlet">
                <div class="kt-portlet__body p-0">


                    <div class="form-group row mb-0">
                        <div class="col-12">
                            <a data-src="@filemanager_get_resource(dialog.php)?type=1&field_id=host_thumbnail&lang=vi&akey=@filemanager_get_key()" class="iframe-btn" data-fancybox data-fancybox data-type="iframe" href="javascript:;">
                                <img id="preview_thumbnail" class="img-fluid" src="admin/images/upload-thumbnail.png">
                            </a>
                            <input type="hidden" name="host_thumbnail" id="host_thumbnail" value="">
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>
</form>
@endsection
@section('vendor-script')
<script src="//maps.google.com/maps/api/js?key=AIzaSyAMMeGEeYSevUQYg9ZtSDC7PWuATS3fAVw" type="text/javascript"></script>
<script src="assets/plugins/custom/gmaps/gmaps.js" type="text/javascript"></script>
<script src="assets/plugins/custom/tinymce/tinymce.bundle.js" type="text/javascript"></script>
@endsection
@section('script')

<script src="assets/js/pages/crud/forms/widgets/bootstrap-datetimepicker.js" type="text/javascript"></script>

<script src="admin/plugins/fancybox/jquery.fancybox.min.js"></script>
<script type="text/javascript" src="admin/plugins/fancybox/jquery.observe_field.js"></script>
<script src="admin/plugins/sortable/sortable.min.js" type="text/javascript"></script>
<script src="admin/js/pages/hosts/add-host.js" type="text/javascript"></script>
<script>
    ! function(t) {
        var e = {};

        function n(i) {
            if (e[i]) return e[i].exports;
            var r = e[i] = {
                i: i,
                l: !1,
                exports: {}
            };
            return t[i].call(r.exports, r, r.exports, n), r.l = !0, r.exports
        }
        n.m = t, n.c = e, n.d = function(t, e, i) {
            n.o(t, e) || Object.defineProperty(t, e, {
                enumerable: !0,
                get: i
            })
        }, n.r = function(t) {
            "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(t, Symbol.toStringTag, {
                value: "Module"
            }), Object.defineProperty(t, "__esModule", {
                value: !0
            })
        }, n.t = function(t, e) {
            if (1 & e && (t = n(t)), 8 & e) return t;
            if (4 & e && "object" == typeof t && t && t.__esModule) return t;
            var i = Object.create(null);
            if (n.r(i), Object.defineProperty(i, "default", {
                    enumerable: !0,
                    value: t
                }), 2 & e && "string" != typeof t)
                for (var r in t) n.d(i, r, function(e) {
                    return t[e]
                }.bind(null, r));
            return i
        }, n.n = function(t) {
            var e = t && t.__esModule ? function() {
                return t.default
            } : function() {
                return t
            };
            return n.d(e, "a", e), e
        }, n.o = function(t, e) {
            return Object.prototype.hasOwnProperty.call(t, e)
        }, n.p = "", n(n.s = 677)
    }({
        677: function(t, e, n) {
            "use strict";
            var i = {
                init: function() {
                    tinymce.init({
                        selector: "#host_description",
                        toolbar: !1,
                        statusbar: !1,
                        height: 60,
                    }), tinymce.init({
                        selector: "#host_policy",
                        menubar: !1,
                        height: 60,
                    })
                }
            };
            jQuery(document).ready((function() {
                i.init()
            }))
        }
    });

    $('.iframe-btn').fancybox({
        // 'width': 900,
        // 'height': 600,
        // 'type': 'iframe',
        'iframe': {
            'css': {
                'width': '90%',
                'height': '90%',
            }
        },
        // 'autoScale': true
    });

    $(document).ready(function() {

        $("#host_thumbnail").observe_field(1, function() {
            // alert('Change observed! new value: ' + this.value );
            $('#preview_thumbnail').attr('src', this.value).show();

        });

        $("#host_gallery_select").observe_field(1, function() {
            // alert('Change observed! new value: ' + this.value );

            var list = this.value;
            if(list != '') {
                var n = list.includes("[");
                if (n) {
                    var array = JSON.parse(list);
                    console.log(array);
                    array.forEach((num, index) => {
                        console.log(num);
                        $('#preview_gallery').append('<div class="col-xl-2 col-lg-3 col-md-4 col-6"><img class="img-fluid img-thumbnail" src="' + num + '" alt=""><a class="remove_image" href="javascript:;" onClick="delete_image(this)"><i class="flaticon-circle"></i></a><input name="host_gallery[]" type="hidden" value="' + num + '" /></div>');

                    });
                } else {
                    $('#preview_gallery').append('<div class="col-xl-2 col-lg-3 col-md-4 col-6"><a class="remove_image" href="javascript:;" onClick="delete_image(this)"><i class="flaticon-circle"></i></a><img class="img-fluid img-thumbnail" src="' + list + '" alt=""><input name="host_gallery[]" type="hidden" value="' + list + '" /></div>');
                }

                $("#preview_gallery").sortable();
            }

        });

    });

    function delete_image(e) {
        e.parentNode.parentNode.removeChild(e.parentNode);
        document.getElementById("host_gallery_select").value = '';
    }
</script>
@endsection