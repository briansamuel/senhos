@extends('admin.index')
@section('page-header', 'Banner')
@section('page-sub_header', 'Thêm mới banner')
@section('style')
    <link rel="stylesheet" href="admin/plugins/fancybox/jquery.fancybox.min.css" />
@endsection
@section('content')
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

        <!-- begin:: Subheader -->
        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container  kt-container--fluid ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        Tài Khoản Banner </h3>
                </div>
            </div>
        </div>

        <!-- end:: Subheader -->

        <!-- begin:: Content -->
        <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
            <form id="kt_add_form">
                <div class="row">
                    <div class="col-md-8 col-lg-9">
                        <div class="kt-portlet">
                            <div class="kt-portlet__head kt-portlet__head--right">
                                <div class="kt-portlet__head-label ">
                                    <span class="kt-font-danger"><i class="fa fa-star"></i> Bắt buộc phải nhập / chọn nội dung</span>
                                </div>
                            </div>
                            <!--begin::Form-->
                            <div class="kt-form">
                                <div class="kt-portlet__body">
                                    <div class="form-group row">
                                        <label for="title" class="col-12 col-lg-12 col-xl-3 col-form-label">Tiêu đề:</label>
                                        <div class="col-12 col-lg-12 col-xl-9">
                                            {{csrf_field()}}
                                            <input class="form-control" type="text" value="" id="title" name="title" placeholder="Tiêu đề bắt buộc phải nhập nội dung">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="link" class="col-12 col-lg-12 col-xl-3 col-form-label">Đường dẫn:</label>
                                        <div class="col-12 col-lg-12 col-xl-9">
                                            <input class="form-control" type="text" value="" id="link" name="link" placeholder="Đường dẫn">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="description" class="col-12 col-lg-12 col-xl-3 col-form-label">Mô tả:</label>
                                        <div class="col-12 col-lg-12 col-xl-9">
                                            <textarea class="form-control" name="description" id="description" cols="30" rows="10"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="type" class="col-12 col-lg-12 col-xl-3 col-form-label">Type:</label>
                                        <div class="col-12 col-lg-12 col-xl-9">
                                            <select class="form-control kt-select2" id="type" name="type" onchange="changeType()">
                                                <option value="home">Trang chủ</option>
                                                <option value="login">Đăng nhập</option>
                                                <option value="project">Dự án</option>
                                                <option value="partner">Đối tác</option>
                                                <option value="gallery">Thư viện</option>
                                                <option value="page">Page</option>
                                                <option value="booking">Đặt phòng</option>
                                                <option value="search-hotel">Tìm phòng</option>
                                                <option value="news">Tin tức</option>
                                                <option value="recruitment">Tuyển dụng</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row" id="div_page" style="display: none">
                                        <label for="type_value" class="col-12 col-lg-12 col-xl-3 col-form-label">Chọn Page:</label>
                                        <div class="col-12 col-lg-12 col-xl-9">
                                            <select class="form-control kt-select2" id="type_value" name="type_value">
                                                @foreach($pages as $page)
                                                <option value="{{$page->page_slug}}">{{$page->page_title}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="caption" class="col-12 col-lg-12 col-xl-3 col-form-label">Chú thích:</label>
                                        <div class="col-12 col-lg-12 col-xl-9">
                                            <select class="form-control kt-select2" id="caption" name="caption" onchange="changeCaption()">
                                                <option value="1">Yes</option>
                                                <option value="0">No</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row" id="div_caption">
                                        <label for="caption_type" class="col-12 col-lg-12 col-xl-3 col-form-label">Vị trí chú thích:</label>
                                        <div class="col-12 col-lg-12 col-xl-9">
                                            <select class="form-control kt-select2" id="caption_type" name="caption_type">
                                                <option value="left">Trái</option>
                                                <option value="right">Phải</option>
                                                <option value="top">Trên</option>
                                                <option value="bottom">Dưới</option>
                                                <option value="center">Giữa</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="banner_type" class="col-12 col-lg-12 col-xl-3 col-form-label">Kiểu Banner:</label>
                                        <div class="col-12 col-lg-12 col-xl-9">
                                            <select class="form-control kt-select2" id="banner_type" name="banner_type" onchange="changeBannerType()">
                                                <option value="image">Hình ảnh</option>
                                                <option value="video">Video</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="active" class="col-12 col-lg-12 col-xl-3 col-form-label">Active:</label>
                                        <div class="col-12 col-lg-12 col-xl-9">
                                            <select class="form-control kt-select2" id="active" name="active">
                                                <option value="yes">Yes</option>
                                                <option value="no">No</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-3">
                        <div class="kt-portlet">
                            <div class="kt-portlet__body p-3">
                                <div class="form-group row mb-3">
                                    <label for="language" class="col-4 col-form-label">Ngôn ngữ:</label>
                                    <div class="col-8">
                                        <select class="form-control kt-select2" id="language" name="language">
                                            @foreach($arrayLang as $key=>$lang)
                                                <option value="{{$key}}">{{$lang}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row mb-3">
                                    <label for="language" class="col-4 col-form-label">Độ ưu tiên:</label>
                                    <div class="col-8">
                                        <input class="form-control" type="text" value="100" id="position" name="position" placeholder="Độ ưu tiên">
                                    </div>
                                </div>
                            </div>
                            <div class="kt-portlet__foot kt-align-right p-2">
                                <div>
                                    <button type="button" class="btn btn-primary" id="btn_add"><i class="la la-save"></i> Lưu dữ liệu</button>
                                </div>
                            </div>
                        </div>
                        <div class="kt-portlet">
                            <div class="kt-portlet__body p-0">


                                <div class="form-group row mb-0">
                                    <div class="col-12">
                                        <a style="display: none" id="div_video"
                                           data-src="@filemanager_get_resource(dialog.php)?type=3&field_id=image&lang=vi&akey=@filemanager_get_key()"
                                           class="iframe-btn" data-fancybox data-fancybox data-type="iframe"
                                           href="javascript:;">
                                                <video width="100%" height="100%" controls>
                                                    <source id="preview_thumbnail_video" src="" type="video/mp4">
                                                </video>
                                        </a>
                                        <a id="div_image"
                                           data-src="@filemanager_get_resource(dialog.php)?type=1&field_id=image&lang=vi&akey=@filemanager_get_key()"
                                           class="iframe-btn" data-fancybox data-fancybox data-type="iframe"
                                           href="javascript:;">
                                                <img id="preview_thumbnail" class="img-fluid" src="admin/images/upload-thumbnail.png">
                                        </a>
                                        <input type="hidden" name="image" id="image" value="">
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </form>
        </div>

        <!-- end:: Content -->
</div>
@endsection
@section('script')
    <!--end::Page Vendors -->
    <script src="assets/js/pages/crud/forms/widgets/bootstrap-datetimepicker.js" type="text/javascript"></script>
    <script src="admin/js/pages/banner/add-banner.js?v1.2" type="text/javascript"></script>
    <script src="assets/js/pages/crud/file-upload/dropzonejs.js?v2" type="text/javascript"></script>
    <script src="admin/plugins/fancybox/jquery.fancybox.min.js"></script>
    <script src="admin/plugins/fancybox/jquery.observe_field.js"></script>

    <script>
        function changeBannerType() {
            var banner_type = $("#banner_type").val();
            if (banner_type === 'image') {
                $("#div_image").show(100);
                $("#div_video").hide();
            } else {
                $("#div_image").hide();
                $("#div_video").show(100);
            }
        }

        function changeType() {
            var type = $("#type").val();
            if(type === 'page') {
                $("#div_page").show(100);
            } else {
                $("#div_page").hide();
            }
        }

        function changeCaption() {
            var caption = $("#caption").val();
            if(caption === '1') {
                $("#div_caption").show(100);
            } else {
                $("#div_caption").hide();
            }
        }

        $("#upload").dropzone({
            url: "/upload-image",
            paramName: "file",
            maxFiles: 1,
            maxFilesize: 5,
            addRemoveLinks: !0,
            sending: function(file, xhr, formData) {
                formData.append("_token", "{{ csrf_token() }}");
            },
            removedfile: function(file)
            {
                var name = $("#image").val();
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': "{{csrf_token()}}"
                    },
                    type: 'POST',
                    url: "destroy-image",
                    data: {filename: name},
                    success: function (data){
                        $("#image").val('');
                    },
                    error: function(e) {
                        console.log(e);
                    }});
                var fileRef;
                return (fileRef = file.previewElement) != null ?
                    fileRef.parentNode.removeChild(file.previewElement) : void 0;
            },
            success: function(file, response) {
                if(response.success) {
                    $("#image").val(response.url);
                } else {
                    alert('Có lỗi xảy ra, vui lòng thử lại sau');
                }
            },
            error: function(file, response) {
                alert('Có lỗi xảy ra, vui lòng thử lại sau');
            }
        });

        $('.iframe-btn').fancybox({
            'iframe': {
                'css': {
                    'width': '90%',
                    'height': '90%',
                }
            },
        });

        $(document).ready(function () {

            $("#image").observe_field(1, function () {
                // alert('Change observed! new value: ' + this.value );
                if ($("#banner_type").val() === 'video') {
                    $('#preview_thumbnail_video').attr('src', this.value).show();
                    $("video")[0].load();
                } else {
                    $('#preview_thumbnail').attr('src', this.value).show();
                }
            });

        });
    </script>


@endsection