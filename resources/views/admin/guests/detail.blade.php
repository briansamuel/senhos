@extends('admin.index')
@section('page-header', 'Khách Hàng')
@section('page-sub_header', 'Chi tiết khách hàng')
@section('style')

@endsection
@section('content')
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

        <!-- begin:: Subheader -->
        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container  kt-container--fluid ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        Tài Khoản Khách Hàng </h3>
                </div>
            </div>
        </div>

        <!-- end:: Subheader -->

        <!-- begin:: Content -->
        <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
            <div class="row">
                <div class="col-md-12">

                    <!--begin::Portlet-->
                    <div class="kt-portlet">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Xem Chi Tiết Đại Lý
                                </h3>
                            </div>
                        </div>

                        <!--begin::Form-->
                        <form class="kt-form" id="kt_user_edit_form">
                            <div class="kt-portlet__body">
                                <div class="form-group">
                                    <label>ID:</label>
                                    <input type="text" class="form-control" value="{{$guestInfo->id}}" disabled>
                                </div>
                                <div class="form-group">
                                    <label>Email:</label>
                                    <input type="text" class="form-control" value="{{$guestInfo->email}}" disabled>
                                </div>
                                <div class="form-group">
                                    <label>Username:</label>
                                    <input type="text" class="form-control" placeholder="Nhập username" name="username" value="{{$guestInfo->username}}" disabled>
                                </div>
                                <div class="form-group">
                                    <label>Họ và tên:</label>
                                    <input type="text" class="form-control" placeholder="Nhập họ và tên" name="full_name" value="{{$guestInfo->full_name}}" disabled>
                                </div>
                                <div class="form-group">
                                    <div class="kt-avatar kt-avatar--outline kt-avatar--circle" id="kt_user_edit_avatar">
                                        <label>Avatar:</label>
                                        <div class="kt-avatar__holder" style="background-image: url('{{$guestInfo->guest_avatar}}')"></div>
                                        <span class="kt-avatar__cancel" data-toggle="kt-tooltip" title="" data-original-title="Cancel avatar">
                                            <i class="fa fa-times"></i>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Status:</label>
                                    <input type="text" class="form-control" placeholder="Nhập họ và tên" name="full_name" value="{{ucfirst($guestInfo->status)}}" disabled>
                                </div>
                                <div class="form-group">
                                    <label>Địa chỉ:</label>
                                    <input type="text" class="form-control" placeholder="Nhập địa chỉ" name="guest_address" value="{{$guestInfo->guest_address}}" disabled>
                                </div>
                                <div class="form-group">
                                    <label>Số điện thoại:</label>
                                    <input type="text" class="form-control" placeholder="Nhập số điện thoại" name="guest_phone" value="{{$guestInfo->guest_phone}}" disabled>
                                </div>
                                <div class="form-group">
                                    <label>Ngày sinh:</label>
                                    <input type="text" class="form-control" id="kt_datepicker_1" name="guest_birthday" placeholder="Chọn ngày" value="{{date("m/d/Y", strtotime($guestInfo->guest_birthday))}}" disabled/>
                                </div>
                                <div class="form-group">
                                    <label>Ngày Kích Hoạt:</label>
                                    <input type="text" class="form-control" value="{{isset($guestInfo->email_verified_at) ? date("d-m-Y H:i:s", strtotime($guestInfo->email_verified_at)) : ''}}" disabled>
                                </div>
                                <div class="form-group">
                                    <label>Lần Truy Cập Cuối:</label>
                                    <input type="text" class="form-control" value="{{isset($guestInfo->last_visits) ? date("d-m-Y H:i:s", strtotime($guestInfo->last_visits)) : ''}}" disabled>
                                </div>
                                <div class="form-group">
                                    <label>Created At:</label>
                                    <input type="text" class="form-control" value="{{isset($guestInfo->created_at) ? date("d-m-Y H:i:s", strtotime($guestInfo->created_at)) : ''}}" disabled>
                                </div>
                                <div class="form-group">
                                    <label>Updated At:</label>
                                    <input type="text" class="form-control" value="{{isset($guestInfo->updated_at) ? date("d-m-Y H:i:s", strtotime($guestInfo->updated_at)) : ''}}" disabled>
                                </div>
                            </div>
                            <div class="kt-portlet__foot">
                                <div class="kt-form__actions">
                                    <a href="guest" class="btn btn-secondary">Quay lại</a>
                                </div>
                            </div>
                        </form>

                        <!--end::Form-->
                    </div>

                    <!--end::Portlet-->
                </div>
            </div>
        </div>

        <!-- end:: Content -->
    </div>
@endsection