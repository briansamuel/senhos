@extends('admin.index')
@section('page-header', 'Tin tức')
@section('page-sub_header', 'Chỉnh sửa bài viết')
@section('style')
<link rel="stylesheet" href="admin/plugins/fancybox/jquery.fancybox.min.css" />
@endsection
@section('content')
<form method="POST" action="{{ route('news.save')}}">
   {{ csrf_field()}}
   <input type="hidden" name="id" value="{{ $news->id }}" />
   <input type="hidden" value="news" name="post_type" />
   <div class="row">
      @if ($errors->any())
      @foreach ($errors->all() as $error)
      <div class="col-12">
         <div class="alert alert-solid-danger alert-bold" role="alert">
            <div class="alert-text">{{ $error }}</div>
            <div class="alert-close">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true"><i class="la la-close"></i></span>
            </button>
        </div>
         </div>
      </div>
      @endforeach
      @endif

      <div class="col-12">
         @include('admin.elements.alert_flash')
      </div>
   </div>
   <div class="row">

      <div class="col-md-8 col-lg-9">
         <div class="kt-portlet">
            <div class="kt-portlet__head kt-portlet__head--right">
               <div class="kt-portlet__head-label ">
                  <span class="kt-font-danger"><i class="fa fa-star"></i> Bắt buộc phải nhập / chọn nội dung</span>
               </div>
            </div>
            <!--begin::Form-->
            <div class="kt-form ">
               <div class="kt-portlet__body">
                  <div class="form-group row">
                     <label for="post_title" class="col-12 col-lg-12 col-xl-3">Tiêu đề:</label>
                     <div class="col-12 col-lg-12 col-xl-9">
                        <input class="form-control" type="text" value="{{ $news->post_title }}" id="post_title" name="post_title" placeholder="Tiêu đề bắt buộc phải nhập nội dung">
                     </div>
                  </div>
                  <div class="form-group row">
                     <label for="post_slug" class="col-12 col-lg-12 col-xl-3">Slug:</label>
                     <div class="col-12 col-lg-12 col-xl-9">
                        <input class="form-control" type="text" value="{{ $news->post_slug }}" id="post_slug" name="post_slug" placeholder="Nhập nội dung đường dẫn URL">
                     </div>
                  </div>
                  <div class="form-group row">
                     <label for="post_description" class="col-12 col-lg-12 col-xl-3">Tóm tắt bài viết:</label>
                     <div class="col-12 col-lg-12 col-xl-9">
                        <textarea id="post_description" rows="4" name="post_description" class="tox-target">
                            {{ $news->post_description }}
                        </textarea>
                     </div>
                  </div>
                  
                  <div class="form-group row">
                     <label for="post_content" class="col-12 col-lg-12 col-xl-3">Nội dung:</label>
                     <div class="col-12 col-lg-12 col-xl-9">
                        <textarea id="post_content" name="post_content" class="tox-target">
                        {{ $news->post_content }}
                     </textarea>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="kt-portlet">
            <div class="kt-portlet__head">
               <div class="kt-portlet__head-label">
                  <h3 class="kt-portlet__head-title">
                     SEO
                  </h3>
               </div>
            </div>
            <!--begin::Form-->
            <div class="kt-form ">
               <div class="kt-portlet__body">
                  <div class="form-group row">
                     <label for="post_seo_title" class="col-12 col-lg-12 col-xl-3">Tiêu đề:</label>
                     <div class="col-12 col-lg-12 col-xl-9">
                        <input class="form-control" type="text" value="{{ $news->post_seo_title }}" id="post_seo_title" name="post_seo_title" placeholder="Tiêu đề SEO">
                     </div>
                  </div>
                  <div class="form-group row">
                     <label for="post_seo_keyword" class="col-12 col-lg-12 col-xl-3">Từ khóa:</label>
                     <div class="col-12 col-lg-12 col-xl-9">
                        <input class="form-control" type="text" value="{{ $news->post_seo_keyword }}" id="post_seo_keyword" name="post_seo_keyword" placeholder="Nhập từ cách nhau bằng dấu phẩy">
                     </div>
                  </div>
                  <div class="form-group row">
                     <label for="post_seo_description" class="col-12 col-lg-12 col-xl-3">Nội dung:</label>
                     <div class="col-12 col-lg-12 col-xl-9">
                        <textarea class="form-control" type="text" value="{{ $news->post_seo_description }}" id="post_seo_description" name="post_seo_description" rows="6" placeholder="Nội dung SEO không quá 160 từ."></textarea>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="col-md-4 col-lg-3">
         <div class="kt-portlet">
            <div class="kt-portlet__body p-3">
               
               <div class="form-group row mb-0">
                  <div class="col-md-12">
                     <table class="table table-bordered table-striped">
                        <tbody>
                           <tr>
                              <td width="55%"></td>
                              <td width="15%" class="text-center">Dịch</td>
                              <td width="30%" class="text-center">Chi tiết</td>
                           </tr>
                           <tr>
                              <td class="va-middle">English</td>
                              <td class="text-center va-middle">
                                 <a href="#">
                                    <i class="fa fa-plus"></i>
                                 </a>
                              </td>
                              <td class="text-center va-middle"><a href="javascript:;">
                                    <i class="fa fa fa-link"></i>
                                 </a>
                              </td>
                           </tr>
                        </tbody>
                     </table>
                  </div>
               </div>
               <div class="form-group row mb-3">
                  <label for="language" class="col-4 col-form-label">Ngôn ngữ:</label>
                  <div class="col-8">
                     <select class="form-control kt-select2" id="language" name="language">
                        @foreach($arrayLang as $key=>$lang)
                           <option value="{{$key}}" <?= $news->language === $key ? 'selected' : ''; ?>>{{$lang}}</option>
                        @endforeach
                     </select>
                  </div>
               </div>
               <div class="form-group row mb-3">
                  <label for="created_at" class="col-4 col-form-label">Ngày công bố:</label>
                  <div class="col-8">
                     <input type="text" class="form-control" name="created_at" readonly="" value="{{ $news->created_at }}" placeholder="Chọn ngày" id="kt_datetimepicker_2">
                  </div>
               </div>
               <div class="form-group row mb-3">
                  <label for="post_status" class="col-4 col-form-label">Tình trạng:</label>
                  <div class="col-8">
                  <select class="form-control kt-select2" id="post_status" name="post_status">
                           <option value="draft" <?= $news->post_status === 'draft' ? 'selected' : ''; ?>>Bản nháp</option>
                           <option value="pending" <?= $news->post_status === 'pending' ? 'selected' : ''; ?>>Chờ duyệt</option>
                           <option value="publish" <?= $news->post_status === 'publish' ? 'selected' : ''; ?>>Xuất bản</option>
                     </select>

                  </div>
               </div>
               <div class="form-group row">
                  <label for="post_feature" class="col-4 col-form-label">Tinh nổi bật:</label>
                  <div class="col-8">
                  <select class="form-control kt-select2" id="post_feature" name="post_feature">
                        <option value="0" <?= $news->post_feature=== 0 ? 'selected' : ''; ?>>Không phải</option>
                        <option value="1" <?= $news->post_feature === 1 ? 'selected' : ''; ?>>Phải</option>
                     </select>
                  </div>
               </div>
            </div>
            <div class="kt-portlet__foot kt-align-right p-2">
               <div>
                  <button type="submit" class="btn btn-primary"><i class="la la-save"></i> Lưu dữ liệu</button>
               </div>
            </div>
         </div>
         <div class="kt-portlet">
            <div class="kt-portlet__head">
               <div class="kt-portlet__head-label">
                  <h3 class="kt-portlet__head-title">
                     Chọn Chuyên Mục
                  </h3>
               </div>
            </div>
            <!--begin::Form-->
            <div class="kt-form ">
               <div class="kt-portlet__body">
                  @include('admin.elements.categories_checkbox')
               </div>
            </div>
         </div>
         <div class="kt-portlet">
            <div class="kt-portlet__body p-0">


               <div class="form-group row mb-0">
                  <div class="col-12">
                  <a href="/filemanager/dialog.php?type=1&field_id=post_thumbnail&akey=@filemanager_get_key()" class="iframe-btn">
                        @if(isset($news->post_thumbnail) && $news->post_thumbnail != '')
                           <img id="preview_thumbnail" class="img-fluid" src="{{ $news->post_thumbnail }}" onerror="imgError(this);">
                        @else 
                           <img id="preview_thumbnail" class="img-fluid" src="admin/images/upload-thumbnail.png">
                        @endif
                     </a>
                     
                     <input type="hidden" name="post_thumbnail" id="post_thumbnail" value="{{ $news->post_thumbnail }}">
                  </div>
               </div>
            </div>

         </div>
      </div>

   </div>
</form>
@endsection
@section('vendor-script')
<script src="assets/plugins/custom/tinymce/tinymce.bundle.js" type="text/javascript"></script>
@endsection
@section('script')
<script src="assets/js/pages/crud/forms/widgets/bootstrap-datetimepicker.js" type="text/javascript"></script>
<script src="assets/js/pages/crud/forms/widgets/select2.js" type="text/javascript"></script>
<script src="admin/plugins/fancybox/jquery.fancybox.min.js"></script>
<script type="text/javascript" src="admin/plugins/fancybox/jquery.observe_field.js"></script>
<script src="admin/js/pages/news/add-news.js" type="text/javascript"></script>
<script>
   ! function(t) {
      var e = {};

      function n(i) {
         if (e[i]) return e[i].exports;
         var r = e[i] = {
            i: i,
            l: !1,
            exports: {}
         };
         return t[i].call(r.exports, r, r.exports, n), r.l = !0, r.exports
      }
      n.m = t, n.c = e, n.d = function(t, e, i) {
         n.o(t, e) || Object.defineProperty(t, e, {
            enumerable: !0,
            get: i
         })
      }, n.r = function(t) {
         "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(t, Symbol.toStringTag, {
            value: "Module"
         }), Object.defineProperty(t, "__esModule", {
            value: !0
         })
      }, n.t = function(t, e) {
         if (1 & e && (t = n(t)), 8 & e) return t;
         if (4 & e && "object" == typeof t && t && t.__esModule) return t;
         var i = Object.create(null);
         if (n.r(i), Object.defineProperty(i, "default", {
               enumerable: !0,
               value: t
            }), 2 & e && "string" != typeof t)
            for (var r in t) n.d(i, r, function(e) {
               return t[e]
            }.bind(null, r));
         return i
      }, n.n = function(t) {
         var e = t && t.__esModule ? function() {
            return t.default
         } : function() {
            return t
         };
         return n.d(e, "a", e), e
      }, n.o = function(t, e) {
         return Object.prototype.hasOwnProperty.call(t, e)
      }, n.p = "", n(n.s = 677)
   }({
      677: function(t, e, n) {
         "use strict";
         var i = {
            init: function() {
               tinymce.init({
                  selector: "#post_description",
                  toolbar: !1,
                  statusbar: !1,
                  height: 200,
               }), tinymce.init({
                  selector: "#post_content",
                  menubar: !1,
                  paste_data_images: true,
                  relative_urls: false, 
                  remove_script_host: false,
                  toolbar: ["styleselect fontselect fontsizeselect", "undo redo | cut copy paste | bold italic | link image | alignleft aligncenter alignright alignjustify", "bullist numlist | outdent indent | blockquote subscript superscript | advlist | autolink | lists charmap | print preview |  code"],
                  plugins: [
                     "advlist autolink link image lists charmap print preview hr anchor pagebreak",
                     "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
                     "table contextmenu directionality emoticons paste textcolor code"
                  ],
                  image_advtab: true,
                  image_advtab: true,
                  image_advtab: true,
                  filemanager_access_key: '@filemanager_get_key()',
                  filemanager_sort_by: '',
                  filemanager_descending: '',
                  filemanager_subfolder: '',
                  filemanager_crossdomain: '',
                  external_filemanager_path: '@filemanager_get_resource(dialog.php)',
                  filemanager_title: "Responsive Filemanager",
                  external_plugins: {
                     "filemanager": "http://admin.senhos.local/vendor/responsivefilemanager/plugin.min.js"
                  }
               })
            }
         };
         jQuery(document).ready((function() {
            i.init()
         }))
      }
   });

   $('.iframe-btn').fancybox({
      'width': 900,
      'height': 600,
      'type': 'iframe',
      'autoScale': false
   });

   $(document).ready(function() {

      $("#post_thumbnail").observe_field(1, function() {
         // alert('Change observed! new value: ' + this.value );
         $('#preview_thumbnail').attr('src', this.value).show();

      });

   });
</script>
@endsection