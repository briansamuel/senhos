@extends('admin.index')
@section('page-header', 'Top Deal')
@section('page-sub_header', 'Chi tiết top deal')
@section('style')

@endsection
@section('content')
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

        <!-- begin:: Subheader -->
        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container  kt-container--fluid ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        Chi tiết Top Deal </h3>
                </div>
            </div>
        </div>

        <!-- end:: Subheader -->

        <!-- begin:: Content -->
        <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
            <div class="row">
                <div class="col-md-12">

                    <!--begin::Portlet-->
                    <div class="kt-portlet">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Xem Chi Tiết Top Deal
                                </h3>
                            </div>
                        </div>

                        <!--begin::Form-->
                        <form class="kt-form" id="kt_user_edit_form">
                            <div class="kt-portlet__body">
                                <div class="form-group">
                                    <label>Title:</label>
                                    <input type="text" class="form-control" placeholder="Tiêu đề" name="title" value="{{$topDeal->title}}" disabled="">
                                </div>
                                <div class="form-group">
                                    <label>Label:</label>
                                    <input type="text" class="form-control" placeholder="Label" name="label" value="{{$topDeal->label}}" disabled="">
                                </div>
                                <div class="form-group">
                                    <label>Vị trí:</label>
                                    <input type="text" class="form-control" placeholder="Vị trí" name="location" value="{{$topDeal->location}}" disabled="">
                                </div>
                                <div class="form-group">
                                    <label>Deal Group:</label>
                                    <input type="text" class="form-control" placeholder="Deal group" name="deal_group" value="{{$topDeal->deal_group}}" disabled="">
                                </div>
                                <div class="form-group">
                                    <label>Start Time:</label>
                                    <input type="text" class="form-control" placeholder="Ngày bắt đầu" name="start_time" id="kt_datepicker_1" value="{{date("d-m-Y", $topDeal->start_time)}}" disabled="">
                                </div>
                                <div class="form-group">
                                    <label>End Time:</label>
                                    <input type="text" class="form-control" placeholder="Ngày kết thúc" name="end_time" id="kt_datepicker_2" value="{{date("d-m-Y", $topDeal->end_time)}}" disabled="">
                                </div>
                                <div class="form-group">
                                    <label>Giá gốc</label>
                                    <input type="text" class="form-control" placeholder="Giá gốc" name="regular_price" value="{{number_format($topDeal->regular_price)}}" disabled="">
                                </div>
                                <div class="form-group">
                                    <label>Giá sale</label>
                                    <input type="text" class="form-control" placeholder="Giá sale" name="sale_price" value="{{number_format($topDeal->sale_price)}}" disabled="">
                                </div>
                                <div class="form-group">
                                    <label>Link:</label>
                                    <input type="text" class="form-control" placeholder="Link" name="link" value="{{$topDeal->link}}" disabled="">
                                </div>
                                <div class="form-group">
                                    <label>Language:</label>
                                    <input type="text" class="form-control" placeholder="Link" name="link" value="{{$topDeal->language === 'vi' ? 'Việt Nam' : 'English'}}" disabled="">
                                </div>
                                <div class="form-group row">
                                    <label class="col-form-label col-lg-12 col-sm-12">Image Upload</label>
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <div class="dropzone dropzone-default" id="upload">
                                            <div class="dropzone-msg dz-message needsclick">
                                                <img height="300" data-dz-thumbnail src="{{$topDeal->image}}"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Description:</label>
                                    <textarea class="form-control" name="description" id="link" cols="20" rows="10">{{$topDeal->description}}</textarea>
                                </div>
                                <div class="form-group">
                                    <label>Độ ưu tiên:</label>
                                    <input type="number" class="form-control" name="position" value="{{$topDeal->position}}" disabled="">
                                </div>
                                <div class="form-group">
                                    <label>Ngày tạo:</label>
                                    <input type="text" class="form-control" name="created_at" value="{{date("d-m-Y H:i:s", strtotime($topDeal->created_at))}}" disabled="">
                                </div>
                                <div class="form-group">
                                    <label>Ngày cập nhập:</label>
                                    <input type="text" class="form-control" name="updated_at" value="{{date("d-m-Y H:i:s", strtotime($topDeal->updated_at))}}" disabled="">
                                </div>

                            </div>
                            <div class="kt-portlet__foot">
                                <div class="kt-form__actions">
                                    <a href="top-deal" class="btn btn-secondary">Quay lại</a>
                                </div>
                            </div>
                        </form>

                        <!--end::Form-->
                    </div>

                    <!--end::Portlet-->
                </div>
            </div>
        </div>

        <!-- end:: Content -->
    </div>
@endsection