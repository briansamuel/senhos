@extends('admin.index')
@section('page-header', 'KHÁCH SẠN')
@section('page-sub_header', 'THÊM PHÒNG')
@section('style')
<link rel="stylesheet" href="admin/plugins/fancybox/jquery.fancybox.min.css" />
<style type="text/css">
    .remove_image {
        position: absolute;
        right: 10%;
        top: 5%;
    }

    .remove_image i {
        font-size: 20px;
    }
</style>
@endsection
@section('content')
<form method="POST" action="{{ route('room.add.action')}}">
    {{ csrf_field()}}


    <div class="row">
        @if ($errors->any())
        @foreach ($errors->all() as $error)
        <div class="col-12">
            <div class="alert alert-solid-danger alert-bold" role="alert">
                <div class="alert-text">{{ $error }}</div>
                <div class="alert-close">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true"><i class="la la-close"></i></span>
                    </button>
                </div>
            </div>
        </div>
        @endforeach
        @endif

        <div class="col-12">
            @include('admin.elements.alert_flash')
        </div>
    </div>
    <div class="row" data-sticky-container>

        <div class="col-md-8 col-lg-9">
            <div class="kt-portlet">
                <div class="kt-portlet__head kt-portlet__head--right">
                    <div class="kt-portlet__head-label ">
                        <span class="kt-font-danger"><i class="fa fa-star"></i> Bắt buộc phải nhập / chọn nội dung</span>
                    </div>
                </div>
                <!--begin::Form-->
                <div class="kt-form ">
                    <div class="kt-portlet__body">
                        <div class="form-group row">
                            <label for="room_name" class="col-12 col-lg-12 col-xl-3 col-form-label">Thuộc khách sạn:</label>
                            <div class="col-12 col-lg-12 col-xl-9">
                                <select class="form-control kt-select2" id="kt_select_host" name="host_id" required>
                                    <option value="">Chọn Khách sạn, Resort</option>
                                    @foreach($hosts as $key => $host)
                                    <option value="{{ $host->id }}">{{ $host->host_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="room_name" class="col-12 col-lg-12 col-xl-3 col-form-label">Tên Phòng:</label>
                            <div class="col-12 col-lg-12 col-xl-9">
                                <input class="form-control" type="text" value="" id="room_name" name="room_name" placeholder="Tên phòng bắt buộc nhập">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="room_description" class="col-12 col-lg-12 col-xl-3 col-form-label">Mô tả:</label>
                            <div class="col-12 col-lg-12 col-xl-9">
                                <textarea id="room_description" name="room_description" class="tox-target">
                     </textarea>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
            <div class="kt-portlet" id="kt_portlet_info_room">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Thông tin phòng
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <div class="kt-portlet__head-group">
                            <a href="#" data-ktportlet-tool="toggle" class="btn btn-sm btn-icon btn-clean btn-icon-md" aria-describedby="tooltip_gggrqlstux"><i class="la la-angle-down"></i></a>

                        </div>
                    </div>
                </div>
                <div class="kt-form ">
                    <div class="kt-portlet__body">
                        <div class="form-group row">
                            <div class="col-12 col-lg-4 col-xl-4 ">
                                <label>Diện tích phòng:</label>
                                <input class="form-control" type="number" value="" id="room_area" min="1" name="room_area">
                                <span class="form-text text-muted">Nhập diện tích phòng</span>
                            </div>
                            <div class="col-12 col-lg-4 col-xl-4 ">
                                <label>Giá tiền mỗi đêm:</label>
                                <input class="form-control" type="number" value="" id="price_one_night" min="0" name="price_one_night">
                                <span class="form-text text-muted">Nhập tiền thuê mỗi đêm</span>
                            </div>
                            <div class="col-12 col-lg-4 col-xl-4 ">
                                <label>Số lượng khách:</label>
                                <input class="form-control" type="number" value="" id="guest_amount" name="guest_amount" min="0" max="100" value="1">
                                <span class="form-text text-muted">Nhập số khách cư trú tối đa</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-12 col-lg-4 col-xl-4 ">
                                <label>Số phòng còn trống:</label>
                                <input class="form-control" type="number" value="" id="room_amount_empty" min="1" name="room_amount_empty">
                                <span class="form-text text-muted">Nhập số phòng trống</span>
                            </div>
                            <div class="col-12 col-lg-4 col-xl-4 ">
                                <label>Số giường đơn</label>
                                <select class="form-control kt-select2" id="room_option[single_bed]" name="room_option[single_bed]">
                                    <option value="0" selected="selected">0</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                </select>
                                <span class="form-text text-muted">Chọn loại giường</span>
                            </div>
                            <div class="col-12 col-lg-4 col-xl-4 ">
                                <label>Số giường đôi</label>
                                <select class="form-control kt-select2" id="room_option[twin_beds]" name="room_option[twin_beds]">
                                    <option value="0" selected="selected">0</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                </select>
                                <span class="form-text text-muted">Chọn loại giường</span>
                            </div>

                        </div>
                    </div>
                </div>


            </div>
            <div class="kt-portlet" data-ktportlet="true" id="kt_portlet_tools_convenient">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Tiện nghi Phòng
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <div class="kt-portlet__head-group">
                            <a href="#" data-ktportlet-tool="toggle" class="btn btn-sm btn-icon btn-clean btn-icon-md" aria-describedby="tooltip_gggrqlstux"><i class="la la-angle-down"></i></a>

                        </div>
                    </div>
                </div>
                <div class="kt-form ">
                    <div class="kt-portlet__body">
                        <div class="form-group row">

                            <div class="col-12 col-sm-6 col-md-3 col-lg-4">
                                <h5 class="kt-font-primary">Phòng tắm và vật dụng phòng tắm</h5>

                                <div class="kt-checkbox-list">

                                    @foreach($convenients['bathroom_accessories'] as $key => $item)
                                    @php
                                    $item = (object) $item;

                                    @endphp
                                    <label class="kt-checkbox kt-checkbox--tick kt-checkbox--success">
                                        <input type="checkbox" name="room_convenient[bathroom_accessories][{{ $key }}]" value="{{ $item->icon }}">{{ $item->text }}<span></span></label>
                                    @endforeach
                                </div>
                            </div>

                            <div class="col-12 col-sm-6 col-md-3 col-lg-4">
                                <h5 class="kt-font-primary">Giải trí</h5>

                                <div class="kt-checkbox-list">

                                    @foreach($convenients['entertaiments'] as $key => $item)
                                    @php
                                    $item = (object) $item;

                                    @endphp
                                    <label class="kt-checkbox kt-checkbox--tick kt-checkbox--success">
                                        <input type="checkbox" name="room_convenient[entertaiments][{{ $key }}]" value="{{ $item->icon }}">{{ $item->text }}<span></span></label>
                                    @endforeach
                                </div>
                            </div>


                            <div class="col-12 col-sm-6 col-md-3 col-lg-4">
                                <h5 class="kt-font-primary">Tiện nghi</h5>

                                <div class="kt-checkbox-list">

                                    @foreach($convenients['convenients'] as $key => $item)
                                    @php
                                    $item = (object) $item;

                                    @endphp
                                    <label class="kt-checkbox kt-checkbox--tick kt-checkbox--success">
                                        <input type="checkbox" name="room_convenient[convenients][{{ $key }}]" value="{{ $item->icon }}">{{ $item->text }}
                                        <span></span>
                                    </label>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-12 col-sm-6 col-md-3 col-lg-4">
                                <h5 class="kt-font-primary">Ăn uống</h5>

                                <div class="kt-checkbox-list">

                                    @foreach($convenients['eating'] as $key => $item)
                                    @php
                                    $item = (object) $item;

                                    @endphp
                                    <label class="kt-checkbox kt-checkbox--tick kt-checkbox--success">
                                        <input type="checkbox" name="room_convenient[eating][{{ $key }}]" value="{{ $item->icon }}">{{ $item->text }}
                                        <span></span>
                                    </label>
                                    @endforeach
                                </div>
                            </div>


                            <div class="col-12 col-sm-6 col-md-3 col-lg-4">
                                <h5 class="kt-font-primary">Bố trí và nội thất</h5>

                                <div class="kt-checkbox-list">

                                    @foreach($convenients['layout_and_interio'] as $key => $item)
                                    @php
                                    $item = (object) $item;

                                    @endphp
                                    <label class="kt-checkbox kt-checkbox--tick kt-checkbox--success">
                                        <input type="checkbox" name="room_convenient[layout_and_interio][{{ $key }}]" value="{{ $item->icon }}">{{ $item->text }}
                                        <span></span>
                                    </label>
                                    @endforeach
                                </div>
                            </div>

                            <div class="col-12 col-sm-6 col-md-3 col-lg-4">
                                <h5 class="kt-font-primary">Quần áo và Giặt ủi</h5>

                                <div class="kt-checkbox-list">

                                    @foreach($convenients['clothes_and_laundry'] as $key => $item)
                                    @php
                                    $item = (object) $item;

                                    @endphp
                                    <label class="kt-checkbox kt-checkbox--tick kt-checkbox--success">
                                        <input type="checkbox" name="room_convenient[clothes_and_laundry][{{ $key }}]" value="{{ $item->icon }}">{{ $item->text }}
                                        <span></span>
                                    </label>
                                    @endforeach
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-12 col-sm-6 col-md-3 col-lg-4">
                                <h5 class="kt-font-primary">Vật dụng an toàn và an ninh</h5>

                                <div class="kt-checkbox-list">

                                    @foreach($convenients['security_item'] as $key => $item)
                                    @php
                                    $item = (object) $item;

                                    @endphp
                                    <label class="kt-checkbox kt-checkbox--tick kt-checkbox--success">
                                        <input type="checkbox" name="room_convenient[security_item][{{ $key }}]" value="{{ $item->icon }}">{{ $item->text }}
                                        <span></span>
                                    </label>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="kt-portlet" id="kt_portlet_tools_gallery">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Thư viện hình ảnh Phòng
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <div class="kt-portlet__head-group">
                            <a href="#" data-ktportlet-tool="toggle" class="btn btn-sm btn-icon btn-clean btn-icon-md" aria-describedby="tooltip_gggrqlstux"><i class="la la-angle-down"></i></a>

                        </div>
                    </div>
                </div>
                <div class="kt-form ">
                    <div class="kt-portlet__body">
                        <div id="preview_gallery" class="preview_gallery sortable sortable-dragging sortable-placeholder row form-group">

                        </div>
                    </div>
                </div>
                <div class="kt-portlet__foot kt-align-right p-2">
                    <div>
                        <a data-src="@filemanager_get_resource(dialog.php)?type=1&field_id=room_gallery_select&lang=vi&akey=@filemanager_get_key()&fldr=khach-san" class="iframe-btn btn btn-primary" data-fancybox data-fancybox data-type="iframe" href="javascript:;">
                            Tải ảnh lên
                        </a>
                        <input type="hidden" id="room_gallery_select" name="room_gallery_select">
                    </div>
                </div>

            </div>


        </div>
        <div class="col-md-4 col-lg-3">
            <div class="kt-portlet sticky">
                <div class="kt-portlet__body p-3">


                    <div class="form-group row">
                        <label for="language" class="col-4 col-form-label">Trạng thái:</label>
                        <div class="col-8">
                            <select class="form-control kt-select2" id="room_status" name="room_status">
                                <option value="available_room" selected="selected">Còn phòng</option>
                                <option value="no_vacancy">Hết phòng</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="kt-portlet__foot kt-align-right p-2">
                    <div>
                        <button type="submit" class="btn btn-primary"><i class="la la-save"></i> Lưu dữ liệu</button>
                    </div>
                </div>
            </div>



        </div>
    </div>
</form>
@endsection
@section('vendor-script')
<script src="//maps.google.com/maps/api/js?key=AIzaSyAMMeGEeYSevUQYg9ZtSDC7PWuATS3fAVw" type="text/javascript"></script>
<script src="assets/plugins/custom/gmaps/gmaps.js" type="text/javascript"></script>
<script src="assets/plugins/custom/tinymce/tinymce.bundle.js" type="text/javascript"></script>
@endsection
@section('script')

<script src="assets/js/pages/crud/forms/widgets/bootstrap-datetimepicker.js" type="text/javascript"></script>

<script src="admin/plugins/fancybox/jquery.fancybox.min.js"></script>
<script type="text/javascript" src="admin/plugins/fancybox/jquery.observe_field.js"></script>
<script src="admin/plugins/sortable/sortable.min.js" type="text/javascript"></script>
<script src="admin/js/pages/rooms/add-room.js" type="text/javascript"></script>
<script>
    ! function(t) {
        var e = {};

        function n(i) {
            if (e[i]) return e[i].exports;
            var r = e[i] = {
                i: i,
                l: !1,
                exports: {}
            };
            return t[i].call(r.exports, r, r.exports, n), r.l = !0, r.exports
        }
        n.m = t, n.c = e, n.d = function(t, e, i) {
            n.o(t, e) || Object.defineProperty(t, e, {
                enumerable: !0,
                get: i
            })
        }, n.r = function(t) {
            "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(t, Symbol.toStringTag, {
                value: "Module"
            }), Object.defineProperty(t, "__esModule", {
                value: !0
            })
        }, n.t = function(t, e) {
            if (1 & e && (t = n(t)), 8 & e) return t;
            if (4 & e && "object" == typeof t && t && t.__esModule) return t;
            var i = Object.create(null);
            if (n.r(i), Object.defineProperty(i, "default", {
                    enumerable: !0,
                    value: t
                }), 2 & e && "string" != typeof t)
                for (var r in t) n.d(i, r, function(e) {
                    return t[e]
                }.bind(null, r));
            return i
        }, n.n = function(t) {
            var e = t && t.__esModule ? function() {
                return t.default
            } : function() {
                return t
            };
            return n.d(e, "a", e), e
        }, n.o = function(t, e) {
            return Object.prototype.hasOwnProperty.call(t, e)
        }, n.p = "", n(n.s = 677)
    }({
        677: function(t, e, n) {
            "use strict";
            var i = {
                init: function() {
                    tinymce.init({
                        selector: "#room_description",
                        toolbar: !1,
                        statusbar: !1,
                        height: 60,
                    }), tinymce.init({
                        selector: "#room_policy",
                        menubar: !1,
                        height: 60,
                    })
                }
            };
            jQuery(document).ready((function() {
                i.init()
            }))
        }
    });

    $('.iframe-btn').fancybox({
        // 'width': 900,
        // 'height': 600,
        // 'type': 'iframe',
        'iframe': {
            'css': {
                'width': '90%',
                'height': '90%',
            }
        },
        // 'autoScale': true
    });

    $(document).ready(function() {

        $("#room_thumbnail").observe_field(1, function() {
            // alert('Change observed! new value: ' + this.value );
            $('#preview_thumbnail').attr('src', this.value).show();

        });

        $("#room_gallery_select").observe_field(1, function() {
            // alert('Change observed! new value: ' + this.value );

            var list = this.value;
            if (list != '') {
                var n = list.includes("[");
                if (n) {
                    var array = JSON.parse(list);
                    console.log(array);
                    array.forEach((num, index) => {
                        console.log(num);
                        $('#preview_gallery').append('<div class="col-xl-2 col-lg-3 col-md-4 col-6"><img class="img-fluid img-thumbnail" src="' + num + '" alt=""><a class="remove_image" href="javascript:;" onClick="delete_image(this)"><i class="flaticon-circle"></i></a><input name="room_gallery[]" type="hidden" value="' + num + '" /></div>');

                    });
                } else {
                    $('#preview_gallery').append('<div class="col-xl-2 col-lg-3 col-md-4 col-6"><a class="remove_image" href="javascript:;" onClick="delete_image(this)"><i class="flaticon-circle"></i></a><img class="img-fluid img-thumbnail" src="' + list + '" alt=""><input name="room_gallery[]" type="hidden" value="' + list + '" /></div>');
                }

                $("#preview_gallery").sortable();
            }

        });


    });

    function delete_image(e) {
        e.parentNode.parentNode.removeChild(e.parentNode);
        document.getElementById("room_gallery_select").value = '';
    }
</script>
@endsection