@extends('agent.profiles.index')
@section('page-header', 'Tài khoản')
@section('page-sub_header', 'Thông tin cá nhân')
@section('style')

@endsection
@section('content_profile')

<div class="kt-grid__item kt-grid__item--fluid kt-app__content">
    <div class="row">
        <div class="col-xl-12">
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">Thông Tin Cá Nhân
                            <small>cập nhập thông tin của bạn</small>
                        </h3>
                    </div>
                </div>
                <form class="kt-form kt-form--label-right">
                    <div class="kt-portlet__body">
                        <div class="kt-section kt-section--first" style="margin-bottom: 0px">
                            <div class="kt-section__body">
                                <div class="form-group row">
                                    {{ csrf_field() }}
                                    <label class="col-xl-3 col-lg-3 col-form-label">Avatar</label>
                                    <div class="col-lg-9 col-xl-6">
                                        <div class="kt-avatar kt-avatar--outline kt-avatar--circle" id="kt_user_avatar">
                                            <div class="kt-avatar__holder" style="background-image: url({{isset(Auth::guard('agent')->user()->agent_avatar) ? Auth::guard('agent')->user()->agent_avatar : ''}})"></div>
                                            <label class="kt-avatar__upload" data-toggle="kt-tooltip" title="" data-original-title="Change avatar">
                                                <i class="fa fa-pen"></i>
                                                <input type="file" name="avatar" accept="image/*">
                                            </label>
                                            <span class="kt-avatar__cancel" data-toggle="kt-tooltip" title="" data-original-title="Cancel avatar">
                                                <i class="fa fa-times"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-xl-3 col-lg-3 col-form-label">Tên Đầy Đủ</label>
                                    <div class="col-lg-9 col-xl-6">
                                        <input class="form-control" type="text" value="{{Auth::guard('agent')->user()->full_name}}" name="full_name" id="full_name">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <div class="row">
                                <div class="col-lg-3 col-xl-3">
                                </div>
                                <div class="col-lg-9 col-xl-9">
                                    <button id="btn_update_profile" type="submit" class="btn btn-success">Cập Nhập
                                    </button>&nbsp;
                                    <button type="reset" class="btn btn-secondary">Hủy Bỏ</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection
@section('script')
    <!--begin::Page Scripts(used by this page) -->
    <script src="agents/profile/profile.js?v1" type="text/javascript"></script>

@endsection