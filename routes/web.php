<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//Route::domain('senhos.local')->group(function () {

if (!defined('FM_USE_ACCESS_KEYS')) {
    define('FM_USE_ACCESS_KEYS', true); // TRUE or FALSE
}
Route::domain(Config::get('domain.web.domain'))->group(function () {
    Route::namespace('FrontSite')->group(function () {
        Route::get('/', ['uses' => 'HomeController@index'])->name('frontsite.home');

        // login
        Route::get('/dang-nhap', ['uses' => 'LoginController@login'])->name('frontsite.login');
        Route::post('/dang-nhap', ['uses' => 'LoginController@loginAction'])->name('frontsite.login.action');
        Route::post('/dang-ky', ['uses' => 'LoginController@registerAction'])->name('frontsite.register.action');
        Route::post('/quen-mat-khau', ['uses' => 'LoginController@sendMailResetPassword'])->name('frontsite.send_mail_reset_password.action');
        Route::get('/reset_password', ['uses' => 'LoginController@resetPassword'])->name('frontsite.reset_password');
        Route::post('/reset_password', ['uses' => 'LoginController@reset'])->name('frontsite.reset.action');
        Route::get('/dang-xuat', ['uses' => 'LoginController@logout'])->name('frontsite.logout');
        Route::get('/kich-hoat-tai-khoan', ['uses' => 'LoginController@activeGuest'])->name('activeGuest');

        Route::get('/auth/redirect/{provider}', 'LoginSocialController@redirect');
        Route::get('/auth/callback/{provider}', 'LoginSocialController@callback');

        // subcribe emails
        Route::post('/subcribe-email', ['uses' => 'SubcribeEmailsController@addAction'])->name('subcribe_email.addAction');

        // accounts
        Route::get('/account', ['uses' => 'AccountManagerController@info'])->name('account.info');
        Route::post('/account', ['uses' => 'AccountManagerController@infoAction'])->name('account.info.action');
        Route::get('/account/bookmark', ['uses' => 'AccountManagerController@bookmark'])->name('account.bookmark');
        Route::get('/account/change-pass', ['uses' => 'AccountManagerController@resetPassword'])->name('account.resetPassword');
        Route::post('/account/change-pass', ['uses' => 'AccountManagerController@resetPasswordAction'])->name('account.resetPasswordAction');
        Route::get('/account/update-pass', ['uses' => 'AccountManagerController@updatePassword'])->name('account.updatePassword');
        Route::post('/account/update-pass', ['uses' => 'AccountManagerController@updatePasswordAction'])->name('account.updatePasswordAction');
        Route::get('/account/history-booking', ['uses' => 'AccountManagerController@historyBooking'])->name('account.historyBooking');

        // autocomplete search
        Route::get('/ajax-search', ['uses' => 'NewsController@search'])->name('frontsite.ajax.search');

        // Đối tác route
        // Route::get('/lien-he', ['uses' => 'ContactController@contact'])->name('frontsite.contact');
        Route::post('/lien-he', ['uses' => 'ContactController@contactAction'])->name('frontsite.contactAction');

        // Đối tác route
        Route::get('/doi-tac/', ['uses' => 'PartnerController@index'])->name('frontsite.partner');
        Route::get('/doi-tac/{slug}.html', ['uses' => 'PartnerController@partnerDetail'])->name('frontsite.partnerDetail');

        // Dự án route
        Route::get('/du-an/', ['uses' => 'ProjectController@index'])->name('frontsite.project');
        Route::get('/du-an/{slug}.html', ['uses' => 'ProjectController@projectDetail'])->name('frontsite.projectDetail');

        // Tin tức
        Route::get('/tin-tuc/', ['uses' => 'NewsController@index'])->name('frontsite.news');
        Route::get('/{slug}.html', ['uses' => 'NewsController@newsDetail'])->name('frontsite.newsDetail');

        // Tin tức
        Route::get('/tin-du-lich/', ['uses' => 'TravelNewsController@index'])->name('frontsite.travel-news');
        Route::get('/du-lich/{slug}.html', ['uses' => 'TravelNewsController@newsDetail'])->name('frontsite.travel-newsDetail');

        // Tin tuyển dụng
        Route::get('/tuyen-dung/', ['uses' => 'RecruitmentController@index'])->name('frontsite.recruitment');
        Route::get('/tuyen-dung/{slug}.html', ['uses' => 'RecruitmentController@recruitmentDetail'])->name('frontsite.recruitmentDetail');

        // Thư viện
        // Thư viện
        Route::get('/thu-vien/', ['uses' => 'VideoController@index'])->name('frontsite.videos');
        Route::get('/thu-vien/{slug}.html', ['uses' => 'VideoController@videoDetail'])->name('frontsite.videoDetail');
        //Page
        Route::get('/{slug}/', ['uses' => 'PageController@index'])->name('frontsite.page');
        // CropImage Thumb
        Route::get('thumbnail/khach-san/{thumbnail_name}.{extension}', 'CropImageController@thumbnailHost')->name('thumbnailHost');
        
        // Đổi ngôn ngữ
        Route::get('lang/{locale}', 'LangController@changeLang')->name('lang');
        Route::get('test/config', 'HostController@updateMany')->name('testUpdate');
    });
});
Route::domain('booking.' . Config::get('domain.web.domain'))->group(function () {
    Route::namespace('FrontSite')->group(function () {
        // login
        Route::get('/dang-nhap', ['uses' => 'LoginController@login'])->name('frontsite.login');
        Route::post('/dang-nhap', ['uses' => 'LoginController@loginAction'])->name('frontsite.login.action');
        Route::post('/dang-ky', ['uses' => 'LoginController@registerAction'])->name('frontsite.register.action');
        Route::post('/quen-mat-khau', ['uses' => 'LoginController@sendMailResetPassword'])->name('frontsite.send_mail_reset_password.action');
        Route::get('/reset_password', ['uses' => 'LoginController@resetPassword'])->name('frontsite.reset_password');
        Route::post('/reset_password', ['uses' => 'LoginController@reset'])->name('frontsite.reset.action');
        Route::get('/dang-xuat', ['uses' => 'LoginController@logout'])->name('frontsite.logout');
        Route::get('/kich-hoat-tai-khoan', ['uses' => 'LoginController@activeGuest'])->name('activeGuest');

        Route::get('/auth/redirect/{provider}', 'LoginSocialController@redirect');
        Route::get('auth/callback/{provider}', 'LoginSocialController@callback');
        // Booking
        Route::get('/', ['uses' => 'BookingController@index'])->name('booking.index');
        Route::get('/tim-phong', ['uses' => 'HostController@search'])->name('hosts.search');
        Route::get('/thong-tin-phong.html', ['uses' => 'BookingController@infoHost'])->name('booking.infoHost');
        Route::get('/search-place', ['uses' => 'BookingController@searchAjax'])->name('booking.searchAjax');
        Route::get('/search-hotel', ['uses' => 'HostController@ajaxGetList'])->name('booking.ajaxGetList');
        Route::get('/search-near-hotel', ['uses' => 'HostController@nearHotel'])->name('booking.nearHotel');
        Route::get('/search-near-hotel-action', ['uses' => 'HostController@nearHotelAction'])->name('booking.nearHotelAction');
        Route::get('/fix-location', ['uses' => 'HostController@fixLocation'])->name('booking.fixLocation');
        // Thông tin Khách sạn
        Route::get('{type}/{slug}/{id}', 'HostController@detail')->name('host.info')->where('id', '[0-9]+');
       
        // Thông tin Phòng
        Route::get('room/{id}', 'HostController@detailRoom')->name('host.infoRoom')->where('id', '[0-9]+');
        // Reviews
        Route::post('add-review', 'ReviewController@addAction')->name('host.addReview');
       // Xác nhận phòng
       Route::get('comfirm', 'HostController@comfirmRoom')->name('host.comfirmRoom');
       Route::post('comfirm/',  ['uses' => 'HostController@comfirmRoomAction'])->name('host.comfirmRoomAction');
       Route::get('thannkyou', function(){
           return view('frontsite.pages.thankyou');
       })->name('pages.thankyou');
       

    });
});

Route::group(['middleware' => config('menu.middleware')], function () {
    //Route::get('wmenuindex', array('uses'=>'\Harimayco\Menu\Controllers\MenuController@wmenuindex'));
    $path = rtrim(config('menu.route_path'));
    Route::post($path . '/createnewmenu', array('as' => 'hcreatenewmenu', 'uses' => 'Admin\System\AdminMenuController@createnewmenu'));
});

Route::domain(Config::get('domain.web.admin'))->group(function () {
    Route::namespace('Admin')->group(function () {
        Route::get('denied-permission', function () {
            return view('admin.pages.permission_denied');
        });
        // login
        Route::get('/login', ['uses' => 'LoginController@login'])->name('login');
        Route::post('/login', ['uses' => 'LoginController@loginAction'])->name('loginAction');
        Route::get('/logout', ['uses' => 'LoginController@logout'])->name('logout');
        Route::get('/reset_password', ['uses' => 'LoginController@resetPassword'])->name('resetPassword');
        Route::get('/active-user', ['uses' => 'LoginController@activeUser'])->name('activeUser');
        Route::get('/active-agent', ['uses' => 'LoginController@activeAgent'])->name('activeAgent');
        Route::get('/kich-hoat-tai-khoan', ['uses' => 'LoginController@activeGuest'])->name('activeGuest');
    });




    Route::middleware(['auth'])->group(function () {


        Route::get('/dashboard', ['uses' => 'Admin\DashboardController@index'])->name('dash-board');
        Route::get('/welcome', ['uses' => 'Admin\WelcomeController@index'])->name('welcome');
        Route::get('/', ['uses' => 'Admin\WelcomeController@index'])->name('home');

        Route::namespace('Admin')->group(function () {
            // my profile
            Route::get('/my-profile', ['uses' => 'MyProfileController@profile'])->name('profile');
            Route::post('/my-profile', ['uses' => 'MyProfileController@updateProfile'])->name('profile.update');

            // change password
            Route::get('/change-password', ['uses' => 'MyProfileController@changePassword'])->name('profile.changePassword');
            Route::put('/change-password', ['uses' => 'MyProfileController@changePasswordAction'])->name('profile.changePasswordAction');

            Route::namespace('CMS')->group(function () {
                // Top Deal
                Route::get('/top-deal', ['uses' => 'TopDealsController@index'])->name('top_deal.list');
                Route::get('/top-deal/add', ['uses' => 'TopDealsController@add'])->name('top_deal.add');
                Route::post('/top-deal/add', ['uses' => 'TopDealsController@addAction'])->name('top_deal.add.action');
                Route::get('/top-deal/detail/{id}', ['uses' => 'TopDealsController@detail'])->name('top_deal.detail');
                Route::get('/top-deal/edit/{id}', ['uses' => 'TopDealsController@edit'])->name('top_deal.edit');
                Route::post('/top-deal/edit/{id}', ['uses' => 'TopDealsController@editAction'])->name('top_deal.edit.action');
                Route::get('/top-deal/delete/{id}', ['uses' => 'TopDealsController@delete'])->name('top_deal.delete');
                Route::get('/top-deal/ajax/get-list', ['uses' => 'TopDealsController@ajaxGetList'])->name('top_deal.ajax.getList');

                // Banner
                Route::get('/banner', ['uses' => 'BannersController@index'])->name('banner.list');
                Route::get('/banner/add', ['uses' => 'BannersController@add'])->name('banner.add');
                Route::post('/banner/add', ['uses' => 'BannersController@addAction'])->name('banner.add.action');
                Route::get('/banner/edit/{banner_id}', ['uses' => 'BannersController@edit'])->name('banner.edit');
                Route::post('/banner/edit/{banner_id}', ['uses' => 'BannersController@editAction'])->name('banner.edit.action');
                Route::get('/banner/delete/{banner_id}', ['uses' => 'BannersController@delete'])->name('banner.delete');
                Route::get('/banner/ajax/get-list', ['uses' => 'BannersController@ajaxGetList'])->name('banner.ajax.getList');

                // Page
                Route::get('/page', ['uses' => 'PageController@index'])->name('page.list');
                Route::get('/page/add', ['uses' => 'PageController@add'])->name('page.add');
                Route::post('/page/add', ['uses' => 'PageController@add'])->name('page.add.action');
                Route::get('/page/edit/{page_id}', ['uses' => 'PageController@add'])->name('page.edit');
                Route::post('/page/edit/{page_id}', ['uses' => 'PageController@add'])->name('page.edit.action');
                Route::post('/page/edit', ['uses' => 'PageController@editManyAction'])->name('page.edit.many.action');
                Route::get('/page/delete/{page_id}', ['uses' => 'PageController@delete'])->name('page.delete');
                Route::post('/page/delete', ['uses' => 'PageController@deletemany'])->name('page.delete.many');
                Route::post('/page/save', ['uses' => 'PageController@save'])->name('page.save');
                Route::get('/page/ajax/get-list', ['uses' => 'PageController@ajaxGetList'])->name('page.ajax.getList');

                // News
                Route::get('/news', ['uses' => 'NewsController@index'])->name('news.list');
                Route::get('/news/add', ['uses' => 'NewsController@add'])->name('news.add');
                Route::get('/news/edit/{news_id}', ['uses' => 'NewsController@add'])->name('news.edit');
                Route::post('/news/edit', ['uses' => 'NewsController@editManyAction'])->name('news.edit.many.action');
                Route::get('/news/delete/{news_id}', ['uses' => 'NewsController@delete'])->name('news.delete');
                Route::post('/news/delete', ['uses' => 'NewsController@deletemany'])->name('news.delete.many');
                Route::post('/news/save', ['uses' => 'NewsController@save'])->name('news.save');
                Route::get('/news/ajax/get-list', ['uses' => 'NewsController@ajaxGetList'])->name('news.ajax.getList');

                // Travel News
                Route::get('/travel', ['uses' => 'TravelNewsController@index'])->name('travel.list');
                Route::get('/travel/add', ['uses' => 'TravelNewsController@add'])->name('travel.add');
                Route::get('/travel/edit/{news_id}', ['uses' => 'TravelNewsController@add'])->name('travel.edit');
                Route::post('/travel/edit', ['uses' => 'TravelNewsController@editManyAction'])->name('travel.edit.many.action');
                Route::get('/travel/delete/{news_id}', ['uses' => 'TravelNewsController@delete'])->name('travel.delete');
                Route::post('/travel/delete', ['uses' => 'TravelNewsController@deletemany'])->name('travel.delete.many');
                Route::post('/travel/save', ['uses' => 'TravelNewsController@save'])->name('travel.save');
                Route::get('/travel/ajax/get-list', ['uses' => 'TravelNewsController@ajaxGetList'])->name('travel.ajax.getList');

                // subcribe Emails
                Route::get('/subcribe-emails', ['uses' => 'SubcribeEmailsController@index'])->name('subcribe_email.list');
                Route::get('/subcribe-emails/edit/{id}', ['uses' => 'SubcribeEmailsController@edit'])->name('subcribe_email.edit');
                Route::post('/subcribe-emails/edit/{id}', ['uses' => 'SubcribeEmailsController@editAction'])->name('subcribe_email.edit.action');
                Route::get('/subcribe-emails/delete/{id}', ['uses' => 'SubcribeEmailsController@delete'])->name('subcribe_email.delete');
                Route::get('/subcribe-emails/ajax/get-list', ['uses' => 'SubcribeEmailsController@ajaxGetList'])->name('subcribe_email.ajax.getList');

                // // Service
                // Route::get('/service', ['uses' => 'ServiceController@index'])->name('service.list');
                // Route::get('/service/add', ['uses' => 'ServiceController@add'])->name('service.add');
                // Route::post('/service/add', ['uses' => 'ServiceController@add'])->name('service.add.action');
                // Route::get('/service/edit/{service_id}', ['uses' => 'ServiceController@add'])->name('service.edit');
                // Route::post('/service/edit/{service_id}', ['uses' => 'ServiceController@add'])->name('service.edit.action');
                // Route::post('/service/edit', ['uses' => 'ServiceController@editManyAction'])->name('service.edit.many.action');
                // Route::get('/service/delete/{service_id}', ['uses' => 'ServiceController@delete'])->name('service.delete');
                // Route::post('/service/delete', ['uses' => 'ServiceController@deletemany'])->name('service.delete.many');
                // Route::post('/service/save', ['uses' => 'ServiceController@save'])->name('service.save');
                // Route::get('/service/ajax/get-list', ['uses' => 'ServiceController@ajaxGetList'])->name('service.ajax.getList');

                // Project
                Route::get('/project', ['uses' => 'ProjectController@index'])->name('project.list');
                Route::get('/project/add', ['uses' => 'ProjectController@add'])->name('project.add');
                Route::post('/project/add', ['uses' => 'ProjectController@addAction'])->name('project.add.action');
                Route::get('/project/edit/{edit_id}', ['uses' => 'ProjectController@edit'])->name('project.edit');
                Route::post('/project/edit/{edit_id}', ['uses' => 'ProjectController@editAction'])->name('project.edit.action');
                Route::post('/project/edit', ['uses' => 'ProjectController@editManyAction'])->name('project.edit.many.action');
                Route::get('/project/delete/{comment_id}', ['uses' => 'ProjectController@delete'])->name('project.delete');
                Route::post('/project/delete', ['uses' => 'ProjectController@deletemany'])->name('project.delete.many');
                Route::get('/project/ajax/get-list', ['uses' => 'ProjectController@ajaxGetList'])->name('project.ajax.getList');

                // Đối tác
                Route::get('/partner', ['uses' => 'PartnerController@index'])->name('partner.list');
                Route::get('/partner/add', ['uses' => 'PartnerController@add'])->name('partner.add');
                Route::get('/partner/edit/{news_id}', ['uses' => 'PartnerController@add'])->name('partner.edit');
                Route::post('/partner/edit', ['uses' => 'PartnerController@editManyAction'])->name('partner.edit.many.action');
                Route::get('/partner/delete/{news_id}', ['uses' => 'PartnerController@delete'])->name('partner.delete');
                Route::post('/partner/delete', ['uses' => 'PartnerController@deletemany'])->name('partner.delete.many');
                Route::post('/partner/save', ['uses' => 'PartnerController@save'])->name('partner.save');
                Route::get('/partner/ajax/get-list', ['uses' => 'PartnerController@ajaxGetList'])->name('partner.ajax.getList');

                // Tuyển dụng
                Route::get('/recruitment', ['uses' => 'RecruitmentController@index'])->name('recruitment.list');
                Route::get('/recruitment/add', ['uses' => 'RecruitmentController@add'])->name('recruitment.add');
                Route::get('/recruitment/edit/{news_id}', ['uses' => 'RecruitmentController@add'])->name('recruitment.edit');
                Route::post('/recruitment/edit', ['uses' => 'RecruitmentController@editManyAction'])->name('recruitment.edit.many.action');
                Route::get('/recruitment/delete/{news_id}', ['uses' => 'RecruitmentController@delete'])->name('recruitment.delete');
                Route::post('/recruitment/delete', ['uses' => 'RecruitmentController@deletemany'])->name('recruitment.delete.many');
                Route::post('/recruitment/save', ['uses' => 'RecruitmentController@save'])->name('recruitment.save');
                Route::get('/recruitment/ajax/get-list', ['uses' => 'RecruitmentController@ajaxGetList'])->name('recruitment.ajax.getList');

                // Thư viện Video
                Route::get('/video', ['uses' => 'VideoController@index'])->name('video.list');
                Route::get('/video/add', ['uses' => 'VideoController@add'])->name('video.add');
                Route::get('/video/edit/{news_id}', ['uses' => 'VideoController@add'])->name('video.edit');
                Route::post('/video/edit', ['uses' => 'VideoController@editManyAction'])->name('video.edit.many.action');
                Route::get('/video/delete/{news_id}', ['uses' => 'VideoController@delete'])->name('video.delete');
                Route::post('/video/delete', ['uses' => 'VideoController@deletemany'])->name('video.delete.many');
                Route::post('/video/save', ['uses' => 'VideoController@save'])->name('video.save');
                Route::get('/video/ajax/get-list', ['uses' => 'VideoController@ajaxGetList'])->name('video.ajax.getList');

                // Thư viện Video
                Route::get('/album', ['uses' => 'ImageController@index'])->name('album.list');
                Route::get('/album/add', ['uses' => 'ImageController@add'])->name('album.add');
                Route::get('/album/edit/{id}', ['uses' => 'ImageController@add'])->name('album.edit');
                Route::post('/album/edit', ['uses' => 'ImageController@editManyAction'])->name('album.edit.many.action');
                Route::get('/album/delete/{id}', ['uses' => 'ImageController@delete'])->name('album.delete');
                Route::post('/album/delete', ['uses' => 'ImageController@deletemany'])->name('album.delete.many');
                Route::post('/album/save', ['uses' => 'ImageController@save'])->name('video.save');
                Route::get('/album/ajax/get-list', ['uses' => 'ImageController@ajaxGetList'])->name('video.ajax.getList');
                // Brand
                 Route::get('/brand', ['uses' => 'BrandsController@index'])->name('brand.list');
                 Route::get('/brand/add', ['uses' => 'BrandsController@add'])->name('brand.add');
                 Route::post('/brand/add', ['uses' => 'BrandsController@addAction'])->name('brand.add.action');
                 Route::get('/brand/edit/{edit_id}', ['uses' => 'BrandsController@edit'])->name('brand.edit');
                 Route::post('/brand/edit/{edit_id}', ['uses' => 'BrandsController@editAction'])->name('brand.edit.action');
                 Route::post('/brand/edit', ['uses' => 'BrandsController@editManyAction'])->name('brand.edit.many.action');
                 Route::get('/brand/delete/{comment_id}', ['uses' => 'BrandsController@delete'])->name('brand.delete');
                 Route::post('/brand/delete', ['uses' => 'BrandsController@deletemany'])->name('brand.delete.many');
                 Route::get('/brand/ajax/get-list', ['uses' => 'BrandsController@ajaxGetList'])->name('brand.ajax.getList');

                // Comment
                Route::get('/comment', ['uses' => 'CommentController@index'])->name('comment.list');
                Route::get('/comment/add', ['uses' => 'CommentController@add'])->name('comment.add');
                Route::post('/comment/add', ['uses' => 'CommentController@addAction'])->name('comment.add.action');
                Route::get('/comment/edit/{comment_id}', ['uses' => 'CommentController@edit'])->name('comment.edit');
                Route::post('/comment/edit/{comment_id}', ['uses' => 'CommentController@editAction'])->name('comment.edit.action');
                Route::post('/comment/edit', ['uses' => 'CommentController@editManyAction'])->name('comment.edit.many.action');
                Route::get('/comment/delete/{comment_id}', ['uses' => 'CommentController@delete'])->name('comment.delete');
                Route::post('/comment/delete', ['uses' => 'CommentController@deletemany'])->name('comment.delete.many');
                Route::get('/comment/ajax/get-list', ['uses' => 'CommentController@ajaxGetList'])->name('comment.ajax.getList');

                // Category
                Route::get('/category', ['uses' => 'CategoryController@index'])->name('category.list');
                Route::post('/category/save', ['uses' => 'CategoryController@save'])->name('category.save');
                Route::get('/category/edit/{category_id}', ['uses' => 'CategoryController@edit'])->name('category.edit');
                Route::post('/category/edit', ['uses' => 'CategoryController@editManyAction'])->name('category.edit.many.action');
                Route::post('/category/edit/{category_id}', ['uses' => 'CategoryController@editAction'])->name('category.edit.action');
                Route::get('/category/delete/{category_id}', ['uses' => 'CategoryController@delete'])->name('category.delete');
                Route::post('/category/delete', ['uses' => 'CategoryController@deleteMany'])->name('category.deleteMany');
                Route::get('/category/ajax/get-list', ['uses' => 'CategoryController@ajaxGetList'])->name('category.ajax.getList');
                // User
                Route::get('/user', ['uses' => 'UsersController@index'])->name('user.list');
                Route::get('/user/add', ['uses' => 'UsersController@add'])->name('user.add');
                Route::post('/user/add', ['uses' => 'UsersController@addAction'])->name('user.add.action');
                Route::get('/user/detail/{user_id}', ['uses' => 'UsersController@detail'])->name('user.detail');
                Route::get('/user/edit/{user_id}', ['uses' => 'UsersController@edit'])->name('user.edit');
                Route::post('/user/edit', ['uses' => 'UsersController@editManyAction'])->name('user.edit.many.action');
                Route::post('/user/edit/{user_id}', ['uses' => 'UsersController@editAction'])->name('user.edit.action');
                Route::get('/user/delete', ['uses' => 'UsersController@deleteMany'])->name('user.delete.many');
                Route::get('/user/delete/{user_id}', ['uses' => 'UsersController@delete'])->name('user.delete');
                Route::post('/user/delete', ['uses' => 'UsersController@delete'])->name('user.delete');
                Route::get('/user/ajax/get-list', ['uses' => 'UsersController@ajaxGetList'])->name('user.ajax.getList');

                // Agent
                Route::get('/agent', ['uses' => 'AgentsController@index'])->name('agent.list');
                Route::get('/agent/add', ['uses' => 'AgentsController@add'])->name('agent.add');
                Route::post('/agent/add', ['uses' => 'AgentsController@addAction'])->name('agent.add.action');
                Route::get('/agent/detail/{agent_id}', ['uses' => 'AgentsController@detail'])->name('agent.detail');
                Route::get('/agent/edit/{agent_id}', ['uses' => 'AgentsController@edit'])->name('agent.edit');
                Route::post('/agent/edit', ['uses' => 'AgentsController@editManyAction'])->name('agent.edit.many.action');
                Route::post('/agent/edit/{agent_id}', ['uses' => 'AgentsController@editAction'])->name('agent.edit.action');
                Route::get('/agent/delete', ['uses' => 'AgentsController@deleteMany'])->name('agent.delete.many');
                Route::get('/agent/delete/{agent_id}', ['uses' => 'AgentsController@delete'])->name('agent.delete');
                Route::post('/agent/delete', ['uses' => 'AgentsController@delete'])->name('agent.delete');
                Route::get('/agent/ajax/get-list', ['uses' => 'AgentsController@ajaxGetList'])->name('agent.ajax.getList');

                // Agent
                Route::get('/guest', ['uses' => 'GuestsController@index'])->name('guest.list');
                Route::get('/guest/add', ['uses' => 'GuestsController@add'])->name('guest.add');
                Route::post('/guest/add', ['uses' => 'GuestsController@addAction'])->name('guest.add.action');
                Route::get('/guest/detail/{guest_id}', ['uses' => 'GuestsController@detail'])->name('guest.detail');
                Route::get('/guest/edit/{guest_id}', ['uses' => 'GuestsController@edit'])->name('guest.edit');
                Route::post('/guest/edit', ['uses' => 'GuestsController@editManyAction'])->name('guest.edit.many.action');
                Route::post('/guest/edit/{guest_id}', ['uses' => 'GuestsController@editAction'])->name('guest.edit.action');
                Route::get('/guest/delete', ['uses' => 'GuestsController@deleteMany'])->name('guest.delete.many');
                Route::get('/guest/delete/{guest_id}', ['uses' => 'GuestsController@delete'])->name('guest.delete');
                Route::post('/guest/delete', ['uses' => 'GuestsController@delete'])->name('guest.delete');
                Route::get('/guest/ajax/get-list', ['uses' => 'GuestsController@ajaxGetList'])->name('guest.ajax.getList');

                // Agent
                Route::get('/user-group', ['uses' => 'UserGroupController@index'])->name('user_group.list');
                Route::get('/user-group/add', ['uses' => 'UserGroupController@add'])->name('user_group.add');
                Route::post('/user-group/add', ['uses' => 'UserGroupController@addAction'])->name('user_group.add.action');
                Route::get('/user-group/detail/{guest_id}', ['uses' => 'UserGroupController@detail'])->name('user_group.detail');
                Route::get('/user-group/edit/{guest_id}', ['uses' => 'UserGroupController@edit'])->name('user_group.edit');
                Route::post('/user-group/edit', ['uses' => 'UserGroupController@editManyAction'])->name('user_group.edit.many.action');
                Route::post('/user-group/edit/{guest_id}', ['uses' => 'UserGroupController@editAction'])->name('user_group.edit.action');
                Route::get('/user-group/delete', ['uses' => 'UserGroupController@deleteMany'])->name('user_group.delete.many');
                Route::get('/user-group/delete/{guest_id}', ['uses' => 'UserGroupController@delete'])->name('user_group.delete');
                Route::post('/user-group/delete', ['uses' => 'UserGroupController@delete'])->name('user_group.delete');
                Route::get('/user-group/ajax/get-list', ['uses' => 'UserGroupController@ajaxGetList'])->name('user_group.ajax.getList');

                // Logs User
                Route::get('/logs-user', ['uses' => 'LogsUserController@index'])->name('logs_user.list');
                Route::get('/logs-user/detail/{id}', ['uses' => 'LogsUserController@detail'])->name('logs_user.detail');
                Route::get('/logs-user/ajax/get-list', ['uses' => 'LogsUserController@ajaxGetList'])->name('logs_user.ajax.getList');

                // Menu
                Route::get('/menus', ['uses' => 'MenuController@index'])->name('menu.index');

                // Custom Css
                Route::get('/theme-option', ['uses' => 'ThemeOptionsController@option'])->name('theme_option.index');
                Route::post('/theme-option', ['uses' => 'ThemeOptionsController@optionAction'])->name('theme_option.action');

                // Custom Css
                Route::get('/custom-css', ['uses' => 'CustomCssController@index'])->name('custom_css.index');
                Route::post('/custom-css', ['uses' => 'CustomCssController@editAction'])->name('custom_css.editAction');

                // Custom Template
                Route::get('/template', ['uses' => 'TemplateController@index'])->name('template.index');
                Route::post('/template', ['uses' => 'TemplateController@editAction'])->name('template.editAction');
                // Contacts
                Route::get('/contact', ['uses' => 'ContactController@index'])->name('contact.index');
                Route::get('/contact/ajax/get-list', ['uses' => 'ContactController@ajaxGetList'])->name('contact.ajax.getList');
                Route::get('/contact/edit/{id}', ['uses' => 'ContactController@edit'])->name('contact.edit');
                Route::post('/contact/edit', ['uses' => 'ContactController@editManyAction'])->name('contact.edit.many.action');
                Route::post('/contact/edit/{id}', ['uses' => 'ContactController@editAction'])->name('contact.edit.action');
                Route::get('/contact/delete', ['uses' => 'ContactController@deleteMany'])->name('contact.delete.many');
                Route::get('/contact/delete/{id}', ['uses' => 'ContactController@delete'])->name('contact.delete');
                Route::post('/contact/delete', ['uses' => 'ContactController@delete'])->name('contact.delete');
                Route::get('/contact/ajax/get-list', ['uses' => 'ContactController@ajaxGetList'])->name('contact.ajax.getList');
                Route::post('/contact/{id}/reply', ['uses' => 'ContactController@replyAction'])->name('contact.reply.action');

                // Setting
                Route::get('/settings-general', ['uses' => 'SettingController@general'])->name('setting.general');
                Route::post('/settings-general', ['uses' => 'SettingController@generalAction'])->name('setting.general.action');
                Route::post('/settings-login-social', ['uses' => 'SettingController@loginSocialAction'])->name('setting.login_social.action');
                Route::get('/settings-email', ['uses' => 'SettingController@email'])->name('setting.email');
                Route::get('/settings-social-login', ['uses' => 'SettingController@loginSocial'])->name('setting.login_social');
                Route::get('/settings-notification', ['uses' => 'SettingController@notification'])->name('setting.login_social');
                Route::post('/settings-notification', ['uses' => 'SettingController@notificationAction'])->name('setting.login_social');

                // Gallery
                Route::get('/gallery', ['uses' => 'GalleryController@index'])->name('gallery.index');
            });

            Route::namespace('Host')->group(function () {

                // Host
                Route::get('/host', ['uses' => 'HostController@index'])->name('host.list');
                Route::get('/host/add', ['uses' => 'HostController@add'])->name('host.add');
                Route::post('/host/add', ['uses' => 'HostController@addAction'])->name('host.add.action');
                Route::get('/host/edit/{host_id}', ['uses' => 'HostController@edit'])->name('host.edit');
                Route::post('/host/edit/{host_id}', ['uses' => 'HostController@editAction'])->name('host.edit.action');
                Route::post('/host/edit', ['uses' => 'HostController@editManyAction'])->name('host.edit.many.action');
                Route::get('/host/delete/{news_id}', ['uses' => 'HostController@delete'])->name('host.delete');
                Route::post('/host/delete', ['uses' => 'HostController@deletemany'])->name('host.delete.many');
                Route::get('/host/ajax/get-list', ['uses' => 'HostController@ajaxGetList'])->name('host.ajax.getList');
                Route::get('/host/ajax/get-district', ['uses' => 'HostController@ajaxGetDistrict'])->name('host.ajax.getDistrict');
                Route::get('/host/ajax/get-ward', ['uses' => 'HostController@ajaxGetWard'])->name('host.ajax.getWard');

                // Room
                Route::get('/room', ['uses' => 'RoomController@index'])->name('room.list');
                Route::get('/room/add', ['uses' => 'RoomController@add'])->name('room.add');
                Route::get('/room/add-popup', ['uses' => 'RoomController@addPopup'])->name('room.add.popup');
                Route::post('/room/add', ['uses' => 'RoomController@addAction'])->name('room.add.action');
                Route::get('/room/edit/{room_id}', ['uses' => 'RoomController@edit'])->name('room.edit');
                Route::get('/room/edit-popup/{room_id}', ['uses' => 'RoomController@editPopup'])->name('room.edit.popup');
                Route::post('/room/edit/{room_id}', ['uses' => 'RoomController@editAction'])->name('room.edit.action');
                Route::post('/room/edit', ['uses' => 'RoomController@editManyAction'])->name('room.edit.many.action');
                Route::get('/room/delete/{room_id}', ['uses' => 'RoomController@delete'])->name('room.delete');
                Route::post('/room/delete', ['uses' => 'RoomController@deletemany'])->name('room.delete.many');
                Route::get('/room/ajax/get-list', ['uses' => 'RoomController@ajaxGetList'])->name('room.ajax.getList');

                // Booking
                Route::get('/booking', ['uses' => 'BookingController@index'])->name('booking.list');
                Route::get('/booking/add', ['uses' => 'BookingController@add'])->name('booking.add');
                Route::post('/booking/add', ['uses' => 'BookingController@addAction'])->name('booking.add.action');
                Route::get('/booking/edit/{booking_id}', ['uses' => 'BookingController@edit'])->name('booking.edit');
                Route::post('/booking/edit/{booking_id}', ['uses' => 'BookingController@editAction'])->name('booking.edit.action');
                Route::post('/booking/edit', ['uses' => 'BookingController@editManyAction'])->name('booking.edit.many.action');
                Route::get('/booking/delete/{news_id}', ['uses' => 'BookingController@delete'])->name('booking.delete');
                Route::post('/booking/delete', ['uses' => 'BookingController@deletemany'])->name('booking.delete.many');
                Route::get('/booking/ajax/get-list', ['uses' => 'BookingController@ajaxGetList'])->name('booking.ajax.getList');
                Route::get('/booking/ajax/get-district', ['uses' => 'BookingController@ajaxGetDistrict'])->name('booking.ajax.getDistrict');
                Route::get('/booking/ajax/get-ward', ['uses' => 'BookingController@ajaxGetWard'])->name('booking.ajax.getWard');

                // Reviews 
                Route::get('/review', ['uses' => 'ReviewController@index'])->name('review.list');
                Route::get('/review/add', ['uses' => 'ReviewController@add'])->name('review.add');
                Route::post('/review/add', ['uses' => 'ReviewController@addAction'])->name('review.add.action');
                Route::get('/review/edit/{comment_id}', ['uses' => 'ReviewController@edit'])->name('review.edit');
                Route::post('/review/edit/{comment_id}', ['uses' => 'ReviewController@editAction'])->name('review.edit.action');
                Route::post('/review/edit', ['uses' => 'ReviewController@editManyAction'])->name('review.edit.many.action');
                Route::get('/review/delete/{comment_id}', ['uses' => 'ReviewController@delete'])->name('review.delete');
                Route::post('/review/delete', ['uses' => 'ReviewController@deletemany'])->name('review.delete.many');
                Route::get('/review/ajax/get-list', ['uses' => 'ReviewController@ajaxGetList'])->name('review.ajax.getList');
            });
        });
        Route::namespace('General')->group(function () {
            Route::post('/upload-image', ['uses' => 'UpLoadImageController@uploadImage'])->name('uploadImage');
            Route::post('/destroy-image', ['uses' => 'UpLoadImageController@imageDestroy'])->name('imageDestroy');
            Route::get('/language', ['uses' => 'MultiLanguageController@index'])->name('language.index');
        });
    });
});

Route::domain(Config::get('domain.web.agent'))->group(function () {
    Route::namespace('Admin')->group(function () {
        Route::get('denied-permission', function () {
            return view('admin.pages.permission_denied');
        });
        // login
        Route::get('/login', ['uses' => 'LoginController@login'])->name('agent.login');
        Route::post('/login', ['uses' => 'LoginController@loginAction'])->name('agent.loginAction');
        Route::get('/logout', ['uses' => 'LoginController@logout'])->name('agent.logout');
        Route::get('/reset_password', ['uses' => 'LoginController@resetPassword'])->name('agent.resetPassword');
    });

    Route::middleware(['agent_auth'])->group(function () {

        Route::get('/', ['uses' => 'Agent\DashboardController@index'])->name('agent.dash-board');
        Route::get('/welcome', ['uses' => 'Agent\DashboardController@index'])->name('agent.welcome');

        Route::namespace('Admin')->group(function () {
            // my profile
            Route::get('/my-profile', ['uses' => 'MyProfileController@profile'])->name('agent.profile');
            Route::post('/my-profile', ['uses' => 'MyProfileController@updateProfile'])->name('agent.profile.update');

            // change password
            Route::get('/change-password', ['uses' => 'MyProfileController@changePassword'])->name('agent.profile.changePassword');
            Route::put('/change-password', ['uses' => 'MyProfileController@changePasswordAction'])->name('agent.profile.changePasswordAction');

            Route::namespace('Host')->group(function () {
                // Booking
                Route::get('/booking', ['uses' => 'BookingController@index'])->name('agent.booking.list');
                Route::get('/booking/add', ['uses' => 'BookingController@add'])->name('agent.booking.add');
                Route::post('/booking/add', ['uses' => 'BookingController@addAction'])->name('agent.booking.add.action');
                Route::get('/booking/edit/{booking_id}', ['uses' => 'BookingController@edit'])->name('agent.booking.edit');
                Route::post('/booking/edit/{booking_id}', ['uses' => 'BookingController@editAction'])->name('agent.booking.edit.action');
                Route::post('/booking/edit', ['uses' => 'BookingController@editManyAction'])->name('agent.booking.edit.many.action');
                Route::get('/booking/delete/{news_id}', ['uses' => 'BookingController@delete'])->name('agent.booking.delete');
                Route::post('/booking/delete', ['uses' => 'BookingController@deletemany'])->name('agent.booking.delete.many');
                Route::get('/booking/ajax/get-list', ['uses' => 'BookingController@ajaxGetList'])->name('agent.booking.ajax.getList');
                Route::get('/booking/ajax/get-district', ['uses' => 'BookingController@ajaxGetDistrict'])->name('agent.booking.ajax.getDistrict');
                Route::get('/booking/ajax/get-ward', ['uses' => 'BookingController@ajaxGetWard'])->name('agent.booking.ajax.getWard');
            });
        });
    });
});
