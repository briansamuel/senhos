! function(t) {
    var e = {};

    function a(n) {
        if (e[n])
            return e[n].exports;
        var o = e[n] = {
            i: n,
            l: !1,
            exports: {}
        };
        return t[n].call(o.exports, o, o.exports, a),
            o.l = !0,
            o.exports
    }
    a.m = t,
        a.c = e,
        a.d = function(t, e, n) {
            a.o(t, e) || Object.defineProperty(t, e, {
                enumerable: !0,
                get: n
            })
        },
        a.r = function(t) {
            "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(t, Symbol.toStringTag, {
                    value: "Module"
                }),
                Object.defineProperty(t, "__esModule", {
                    value: !0
                })
        },
        a.t = function(t, e) {
            if (1 & e && (t = a(t)),
                8 & e)
                return t;
            if (4 & e && "object" == typeof t && t && t.__esModule)
                return t;
            var n = Object.create(null);
            if (a.r(n),
                Object.defineProperty(n, "default", {
                    enumerable: !0,
                    value: t
                }),
                2 & e && "string" != typeof t)
                for (var o in t)
                    a.d(n, o, function(e) {
                            return t[e]
                        }
                        .bind(null, o));
            return n
        },
        a.n = function(t) {
            var e = t && t.__esModule ? function() {
                    return t.default
                } :
                function() {
                    return t
                };
            return a.d(e, "a", e),
                e
        },
        a.o = function(t, e) {
            return Object.prototype.hasOwnProperty.call(t, e)
        },
        a.p = "",
        a(a.s = 703)
}({
    703: function(t, e, a) {
        "use strict";
        var n, o = (n = {
            data: {
                type: "remote",
               
                source: {
                    read: {
                        url: "travel/ajax/get-list",
                        method: 'get',
                        map: function (t) {
                            var e = t;
                            return void 0 !== t.data && (e = t.data), e
                        }
                    }
                },
                pageSize: 20,
                serverPaging: !0,
                serverFiltering: !0,
                serverSorting: !0
            },
            layout: {
                // scroll: !0,
                // height: 350,
                footer: !1
            },
            sortable: !0,
            pagination: !0,
            columns: [{
                field: "id",
                title: "#",
                sortable: !1,
                width: 20,
                selector: {
                    class: "kt-checkbox--solid"
                },
                textAlign: "center"
            }, {
                field: "post_title",
                title: "THÔNG TIN",
                width: 360,
                template: function(t) {
                    return "<a<strong>" + t.post_title + "</strong>"
                }
            }, {
                field: "post_author",
                title: "TẠO BỞI",
                template: function(t) {
                    return "<a<strong>" + t.post_author + "</strong>"
                }
            }, {
                field: "post_status",
                title: "TÌNH TRẠNG  ",
                width: 100,
                template: function(t) {
                    var e = {
                        'draft': {
                            title: "Bản nháp",
                            class: "kt-badge--brand"
                        },
                        'pending': {
                            title: "Đang chờ duyệt",
                            class: " kt-badge--danger"
                        },
                        'publish': {
                            title: "Đã xuất bản",
                            class: " kt-badge--success"
                        },
                        'trash': {
                            title: "Thùng rác",
                            class: " kt-badge--dark"
                        },
                        
                    };
                    return '<span class="kt-badge ' + e[t.post_status].class + ' kt-badge--inline kt-badge--pill">' + e[t.post_status].title + "</span>"
                }
            }, {
                field: "created_at",
                title: "NGÀY TẠO"
            }, {
                field: "Actions",
                title: "THAO TÁC",
                sortable: !1,
                width: 110,
                overflow: "visible",
                textAlign: "left",
                autoHide: !1,
                template: function(t) {
                    return '    <a target="_blank" href="#" class="btn btn-sm btn-clean btn-icon btn-icon-sm" title="Xem bài viết">                        <i class="flaticon-eye"></i>                    </a>                 <a href="travel/edit/' + t.id + '" class="btn btn-sm btn-clean btn-icon btn-icon-sm" title="Sửa bài viết">                        <i class="flaticon2-pen"></i>                    </a>                    <a href="travel/delete/' + t.id + '" class="btn btn-sm btn-clean btn-icon btn-icon-sm" title="Xóa bài viết" onclick="return confirm(\'Bạn có chắc muốn xóa bài viết này đi không ???\');">                        <i class="flaticon2-delete"></i>                    </a>                '
                }
            }]
        }, {
            init: function() {
                ! function() {
                    n.search = {
                        input: $("#post_title")
                    };
                }(),
                function() {
                    n.extensions = {
                            checkbox: {}
                        },
                        n.search = {
                            input: $("#post_title")
                        };
                    var t = $("#server_news_selection").KTDatatable(n);
                        $("#kt_datatable_delete_all").on("click", (function() {
                            var list_checked_id = t.checkbox().getSelectedId();
                            var total_checked_id = list_checked_id.length;
                            if(total_checked_id === 0) return false;
                            $.ajax({
                                url: "travel/delete",
                                method: 'post',
                                data: {
                                    _token: $("input[name='_token']").val(),
                                    ids: list_checked_id,
                                    total: total_checked_id
                                },
                                success: function(){
                                    window.location.reload();
                                },
                                error: function(errors) {
                                    errors = errors.responseJSON;
                                    var message = !errors.error ? 'Có lỗi xảy ra, vui lòng thử lại sau' : errors.error.message;
                                    KTApp.unprogress(e), swal.fire({
                                        title: "",
                                        text: message,
                                        type: 'error',
                                        confirmButtonClass: "btn btn-secondary"
                                    });
                                }
                            });
                        })),
                        $(".kt_datatable_update_status_all").on("click", (function() {
                            var status = $(this).attr('attr');
                            var list_checked_id = t.checkbox().getSelectedId();
                            var total_checked_id = list_checked_id.length;
                            if(total_checked_id === 0) return false;
                            $.ajax({
                                url: "travel/edit",
                                method: 'POST',
                                data: {
                                    _token: $("input[name='_token']").val(),
                                    ids: list_checked_id,
                                    total: total_checked_id,
                                    status: status
                                },
                                success: function(){
                                    window.location.reload();
                                },
                                error: function(errors) {
                                    errors = errors.responseJSON;
                                    var message = !errors.error ? 'Có lỗi xảy ra, vui lòng thử lại sau' : errors.error.message;
                                    KTApp.unprogress(e), swal.fire({
                                        title: "",
                                        text: message,
                                        type: 'error',
                                        confirmButtonClass: "btn btn-secondary"
                                    });
                                }
                            });
                        })),
                        $("#kt_form_status1").on("change", (function() {
                            t.search($(this).val().toLowerCase(), "status")
                        })),
                        $("#kt_form_type1").on("change", (function() {
                            t.search($(this).val().toLowerCase(), "Type")
                        })),
                        $("#kt_form_status1,#kt_form_type1").selectpicker(),
                        t.on("kt-datatable--on-click-checkbox kt-datatable--on-layout-updated", (function(e) {
                            var a = t.checkbox().getSelectedId().length;
                            $("#kt_datatable_selected_number1").html(a),
                                a > 0 ? $("#kt_datatable_group_action_form1").collapse("show") : $("#kt_datatable_group_action_form1").collapse("hide")
                        })),
                        $("#kt_modal_fetch_id_server").on("show.bs.modal", (function(e) {
                            for (var a = t.checkbox().getSelectedId(), n = document.createDocumentFragment(), o = 0; o < a.length; o++) {
                                var r = document.createElement("li");
                                r.setAttribute("data-id", a[o]),
                                    r.innerHTML = "Selected record ID: " + a[o],
                                    n.appendChild(r)
                            }
                            $(e.target).find(".kt-datatable_selected_ids").append(n)
                        })).on("hide.bs.modal", (function(t) {
                            $(t.target).find(".kt-datatable_selected_ids").empty()
                        }))
                }()
            }
        });
        jQuery(document).ready((function() {
            o.init()
        }))
    }
});