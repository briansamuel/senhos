! function(t) {
    var e = {};

    function a(n) {
        if (e[n])
            return e[n].exports;
        var o = e[n] = {
            i: n,
            l: !1,
            exports: {}
        };
        return t[n].call(o.exports, o, o.exports, a),
            o.l = !0,
            o.exports
    }
    a.m = t,
        a.c = e,
        a.d = function(t, e, n) {
            a.o(t, e) || Object.defineProperty(t, e, {
                enumerable: !0,
                get: n
            })
        },
        a.r = function(t) {
            "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(t, Symbol.toStringTag, {
                    value: "Module"
                }),
                Object.defineProperty(t, "__esModule", {
                    value: !0
                })
        },
        a.t = function(t, e) {
            if (1 & e && (t = a(t)),
                8 & e)
                return t;
            if (4 & e && "object" == typeof t && t && t.__esModule)
                return t;
            var n = Object.create(null);
            if (a.r(n),
                Object.defineProperty(n, "default", {
                    enumerable: !0,
                    value: t
                }),
                2 & e && "string" != typeof t)
                for (var o in t)
                    a.d(n, o, function(e) {
                            return t[e]
                        }
                        .bind(null, o));
            return n
        },
        a.n = function(t) {
            var e = t && t.__esModule ? function() {
                    return t.default
                } :
                function() {
                    return t
                };
            return a.d(e, "a", e),
                e
        },
        a.o = function(t, e) {
            return Object.prototype.hasOwnProperty.call(t, e)
        },
        a.p = "",
        a(a.s = 703)
}({
    703: function(t, e, a) {
        "use strict";
        var n, o = (n = {
            data: {
                type: "remote",
               
                source: {
                    read: {
                        url: "host/ajax/get-list",
                        method: 'get',
                        map: function (t) {
                            var e = t;
                            return void 0 !== t.data && (e = t.data), e
                        }
                    }
                },
                pageSize: 20,
                serverPaging: !0,
                serverFiltering: !0,
                serverSorting: !0
            },
            layout: {
                // scroll: !0,
                // height: 350,
                footer: !1
            },
            sortable: !0,
            pagination: !0,
            columns: [{
                field: "id",
                title: "#",
                sortable: !1,
                width: 20,
                selector: {
                    class: "kt-checkbox--solid"
                },
                textAlign: "center"
            }, {
                field: "host_name",
                title: "THÔNG TIN",
                width: 360,
                template: function(t) {
                    return "<strong>" + t.host_name + "</strong>"
                }
            }, {
                field: "province_name",
                title: "KHU VỰC",
                template: function(t) {
                    return "<strong>" + t.ward_name + ', ' + t.district_name + ', ' + t.province_name + "</strong>"
                }
            }, {
                field: "host_type",
                title: "Loại dịch vụ",
                width: 100,
                template: function(t) {
                    var e = {
                        'hotel': {
                            title: "Khách sạn",
                            class: "kt-badge--brand"
                        },
                        'villa': {
                            title: "Villa",
                            class: " kt-badge--danger"
                        },
                        'resort': {
                            title: "Khu nghỉ dưỡng",
                            class: " kt-badge--success"
                        },
                        'apartment': {
                            title: "Căn hộ, chung cư",
                            class: " kt-badge--info"
                        },
                        'homestay': {
                            title: "Home Stay",
                            class: " kt-badge--warning"
                        },
                        'campsite': {
                            title: "Cắm Trại",
                            class: " kt-badge--info"
                        },
                    };
                    return '<span class="kt-badge ' + e[t.host_type].class + ' kt-badge--inline kt-badge--pill">' + e[t.host_type].title + "</span>"
                }
            }, {
                field: "created_at",
                title: "NGÀY TẠO"
            }, {
                field: "Actions",
                title: "THAO TÁC",
                sortable: !1,
                width: 110,
                overflow: "visible",
                textAlign: "left",
                autoHide: !1,
                template: function(t) {
                    var site_main = $('#site-main').val();
                    return '    <a target="_blank" href="//booking.'+site_main+t.host_type+'/'+t.host_slug+'/'+t.id + '" class="btn btn-sm btn-clean btn-icon btn-icon-sm" title="Thông tin nơi lưu trú">                        <i class="flaticon-eye"></i>                    </a>                 <a href="host/edit/' + t.id + '" class="btn btn-sm btn-clean btn-icon btn-icon-sm" title="Sửa bài viết">                        <i class="flaticon2-pen"></i>                    </a>                    <a href="host/delete/' + t.id + '" class="btn btn-sm btn-clean btn-icon btn-icon-sm" title="Xóa bài viết" onclick="return confirm(\'Bạn có chắc muốn xóa bài viết này đi không ???\');">                        <i class="flaticon2-delete"></i>                    </a>                '
                }
            }]
        }, {
            init: function() {
                ! function() {
                    n.search = {
                        input: $("#host_name")
                    };
                }(),
                function() {
                    n.extensions = {
                            checkbox: {}
                        },
                        n.search = {
                            input: $("#host_name")
                        };
                    var t = $("#server_hosts_selection").KTDatatable(n);
                        $("#kt_datatable_delete_all").on("click", (function() {
                            var list_checked_id = t.checkbox().getSelectedId();
                            var total_checked_id = list_checked_id.length;
                            if(total_checked_id === 0) return false;
                            $.ajax({
                                url: "host/delete",
                                method: 'post',
                                data: {
                                    _token: $("input[name='_token']").val(),
                                    ids: list_checked_id,
                                    total: total_checked_id
                                },
                                success: function(){
                                    window.location.reload();
                                },
                                error: function(errors) {
                                    errors = errors.responseJSON;
                                    var message = !errors.error ? 'Có lỗi xảy ra, vui lòng thử lại sau' : errors.error.message;
                                    KTApp.unprogress(e), swal.fire({
                                        title: "",
                                        text: message,
                                        type: 'error',
                                        confirmButtonClass: "btn btn-secondary"
                                    });
                                }
                            });
                        })),
                        
                        $("#kt_form_type").on("change", (function() {
                            t.search($(this).val().toLowerCase(), "host_type")
                        })),
                        
                        $("#kt_form_type").selectpicker(),
                        t.on("kt-datatable--on-click-checkbox kt-datatable--on-layout-updated", (function(e) {
                            var a = t.checkbox().getSelectedId().length;
                            $("#kt_datatable_selected_number1").html(a),
                                a > 0 ? $("#kt_datatable_group_action_form1").collapse("show") : $("#kt_datatable_group_action_form1").collapse("hide")
                        })),
                        $("#kt_modal_fetch_id_server").on("show.bs.modal", (function(e) {
                            for (var a = t.checkbox().getSelectedId(), n = document.createDocumentFragment(), o = 0; o < a.length; o++) {
                                var r = document.createElement("li");
                                r.setAttribute("data-id", a[o]),
                                    r.innerHTML = "Selected record ID: " + a[o],
                                    n.appendChild(r)
                            }
                            $(e.target).find(".kt-datatable_selected_ids").append(n)
                        })).on("hide.bs.modal", (function(t) {
                            $(t.target).find(".kt-datatable_selected_ids").empty()
                        }))
                }()
            }
        });
        jQuery(document).ready((function() {
            o.init()
        }))
    }
});