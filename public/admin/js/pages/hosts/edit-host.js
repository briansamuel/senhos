"use strict";
var KTPortletTools = {
    init: function() {
        var e;
        toastr.options.showDuration = 1e3, (e = new KTPortlet("kt_portlet_tools_policy")),
            function() {
                var e = new KTPortlet("kt_portlet_tools_convenient");

            }(),
            function() {
                var e = new KTPortlet("kt_portlet_tools_gallery");

            }(),
            function() {
                var e = new KTPortlet("kt_portlet_tools_4");

            }(),
            function() {
                var e = new KTPortlet("kt_portlet_tools_5");

            }(),
            function() {
                var e = new KTPortlet("kt_portlet_tools_6");

            }()
    }
};
var objmap;
jQuery(document).ready(function() {
    KTPortletTools.init();
    var marker;
    var lat = $('#host_lat').val();
    var lng = $('#host_lng').val();
    objmap = new GMaps({
        el: "#kt_gmap_2",
        zoom: 16,
        lat: lat,
        lng: lng,
        dragend: function(t) {
            //console.log(t);
        }
    });
    objmap.addMarker({
        lat: lat,
        lng: lng,
        title: "Vị trí",
        infoWindow: {
            content: '<span style="color:#000">Vị trí khách sạn của bạn</span>'
        }
    });
    objmap.addListener('click', function(e) {
        //console.log(e);
        placeMarker(e);
    });

    function placeMarker(t) {

        objmap.removeMarkers();
        objmap.addMarker({
            position: t.latLng,
            title: "Vị trí",
            infoWindow: {
                content: '<span style="color:#000">Vị trí khách sạn của bạn</span>'
            }
        });
        $('#host_lat').val(t.latLng.lat());
        $('#host_lng').val(t.latLng.lng());
    }

    $('#kt_portlet_tools_convenient .kt-checkbox').each(function(index, element) {
        //let val_input = $(this).text();
        //console.log($(this).text());
        var list = index + ' => "' + $(this).text() + '",';
        $(this).children('input').val($(this).text());
        //$(this).val($(this).text()); 
        //console.log(list);
    });


});

var KTSelect2 = {
    init: function() {
        $("#kt_select_province, #kt_select2_1_validate").select2({
                placeholder: "Chọn tỉnh thành"
            }),
            $("#kt_select_district, #kt_select2_2_validate").select2({
                placeholder: "Chọn quận huyện",

            }),
            $("#kt_select_ward").select2({
                placeholder: "Chọn phường xã",

            }),
            $("#kt_select_host").select2({
                placeholder: "Chọn Khách sạn, Resort ...",

            }),
            $('#kt_select_province').on('select2:select', function(e) {

                var data = e.params.data;
                //console.log(data);// Do something
                console.log(data);
                $('#province_name').val(data.text);
                var districtSelect = $('#kt_select_district');
                districtSelect.select2('data', null);
                districtSelect.html('');
                $.ajax({
                    type: 'GET',
                    url: '/host/ajax/get-district?_province_id=' + data.id,
                }).then(function(data) {

                   
                    
                    var option = new Option('Chọn quận/huyện', 0, true, true);
                    districtSelect.append(option);
                    
                    data.forEach(function(i, index) {
                        var option = new Option(i._name, i.id, false, false);
                        districtSelect.append(option);
                    });
                    // create the option and append to Select2


                    // manually trigger the `select2:select` event

                    // districtSelect.trigger({
                    //     type: 'select2:select',
                    //     params: {
                    //         data: data
                    //     }
                    // });
                });
            }),
            $('#kt_select_district').on('select2:select', function(e) {

                var data = e.params.data;
                //console.log(data);// Do something
                $('#district_name').val(data.text);
                GMaps.geocode({
                    address: data.text+', '+$('#province_name'),
                    callback: function(results, status) {
                        console.log('TEST');
                      if (status == 'OK') {
                        var latlng = results[0].geometry.location;
                        objmap.setCenter(latlng.lat(), latlng.lng());
                        objmap.setZoom(14);
                      }
                    }
                });
                var wardSelect = $('#kt_select_ward');
                wardSelect.select2('data', null);
                wardSelect.html('');
                $.ajax({
                    type: 'GET',
                    url: '/host/ajax/get-ward?_district_id=' + data.id,
                }).then(function(data) {
                    
                   
                    var option = new Option('Chọn phường/xã', '', true, true);
                    wardSelect.append(option).trigger('change');
                    
                    data.forEach(function(i, index) {
                        var option = new Option(i._name, i.id, false, false);
                        wardSelect.append(option);
                    });
                    // create the option and append to Select2


                    // manually trigger the `select2:select` event
                    // wardSelect.trigger({
                    //     type: 'select2:select',
                    //     params: {
                    //         data: data
                    //     }
                    // });
                });
            }),
            $('#kt_select_ward').on('select2:select', function(e) {

                var data = e.params.data;
                //console.log(data);// Do something
                $('#ward_name').val(data.text);


            })
    }
}

;
jQuery(document).ready(function() {
        KTSelect2.init()
    }

);

! function(t) {
    var e = {};

    function a(n) {
        if (e[n])
            return e[n].exports;
        var o = e[n] = {
            i: n,
            l: !1,
            exports: {}
        };
        return t[n].call(o.exports, o, o.exports, a),
            o.l = !0,
            o.exports
    }
    a.m = t,
        a.c = e,
        a.d = function(t, e, n) {
            a.o(t, e) || Object.defineProperty(t, e, {
                enumerable: !0,
                get: n
            })
        },
        a.r = function(t) {
            "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(t, Symbol.toStringTag, {
                    value: "Module"
                }),
                Object.defineProperty(t, "__esModule", {
                    value: !0
                })
        },
        a.t = function(t, e) {
            if (1 & e && (t = a(t)),
                8 & e)
                return t;
            if (4 & e && "object" == typeof t && t && t.__esModule)
                return t;
            var n = Object.create(null);
            if (a.r(n),
                Object.defineProperty(n, "default", {
                    enumerable: !0,
                    value: t
                }),
                2 & e && "string" != typeof t)
                for (var o in t)
                    a.d(n, o, function(e) {
                            return t[e]
                        }
                        .bind(null, o));
            return n
        },
        a.n = function(t) {
            var e = t && t.__esModule ? function() {
                    return t.default
                } :
                function() {
                    return t
                };
            return a.d(e, "a", e),
                e
        },
        a.o = function(t, e) {
            return Object.prototype.hasOwnProperty.call(t, e)
        },
        a.p = "",
        a(a.s = 703)
}({
    703: function(t, e, a) {
        "use strict";
        var n, o = (n = {
            data: {
                type: "remote",
               
                source: {
                    read: {
                        url: "room/ajax/get-list?host_id="+ $('#host_id').val(),
                        method: 'get',
                        map: function (t) {
                            var e = t;
                            return void 0 !== t.data && (e = t.data), e
                        }
                    }
                },
                pageSize: 20,
                serverPaging: !0,
                serverFiltering: !0,
                serverSorting: !0
            },
            layout: {
                // scroll: !0,
                // height: 350,
                footer: !1,
                spinner: {
                    message: 'Đang tải ...',
                }
            },
            sortable: !0,
            pagination: !0,
            columns: [{
                field: "id",
                title: "#",
                sortable: !1,
                width: 20,
                selector: {
                    class: "kt-checkbox--solid"
                },
                textAlign: "center"
            }, {
                field: "room_name",
                title: "TÊN PHÒNG",
                width: 260,
                template: function(t) {
                    return "<strong>" + t.room_name + "</strong>"
                }
            }, {
                field: "price_one_night",
                title: "GIÁ PHÒNG",
                template: function(t) {

                    var price = new Intl.NumberFormat('vi-VN', { style: 'currency', currency: 'VND' }).format(t.price_one_night)
                    return "<strong>" + price + "</i></strong>"
                }
            }, {
                field: "room_status",
                title: "Trạng thái",
                width: 100,
                template: function(t) {
                    var e = {
                        'available_room': {
                            title: "Còn phòng",
                            class: "kt-badge--brand"
                        },
                        'no_vacancy': {
                            title: "Hết Phòng",
                            class: " kt-badge--danger"
                        },
                        
                        
                    };
                    return '<span class="kt-badge ' + e[t.room_status].class + ' kt-badge--inline kt-badge--pill">' + e[t.room_status].title + "</span>"
                }
            }, {
                field: "created_at",
                title: "NGÀY TẠO"
            }, {
                field: "Actions",
                title: "THAO TÁC",
                sortable: !1,
                width: 110,
                overflow: "visible",
                textAlign: "left",
                autoHide: !1,
                template: function(t) {
                    return '    <a data-src="room/edit-popup/'+t.id+'" class="popup-room btn btn-sm btn-clean btn-icon btn-icon-sm" data-fancybox data-fancybox data-type="iframe" href="javascript:;" title="Sửa thông tin phòng">                        <i class="flaticon2-pen"></i>                    </a>                    <a href="room/delete/' + t.id + '" class="btn btn-sm btn-clean btn-icon btn-icon-sm" title="Xóa phòng" onclick="return confirm(\'Bạn có chắc muốn xóa phòng này đi không ???\');">                        <i class="flaticon2-delete"></i>                    </a>                '
                }
            }]
        }, {
            init: function() {
                ! function() {
                    n.search = {
                        input: $("#post_title")
                    };
                }(),
                function() {
                    n.extensions = {
                            checkbox: {}
                        },
                        n.search = {
                            input: $("#post_title")
                        };
                    var t = $("#server_rooms_selection").KTDatatable(n);
                        $("#kt_datatable_delete_all").on("click", (function() {
                            var list_checked_id = t.checkbox().getSelectedId();
                            var total_checked_id = list_checked_id.length;
                            if(total_checked_id === 0) return false;
                            $.ajax({
                                url: "room/delete",
                                method: 'post',
                                data: {
                                    _token: $("input[name='_token']").val(),
                                    ids: list_checked_id,
                                    total: total_checked_id
                                },
                                success: function(){
                                    window.location.reload();
                                },
                                error: function(errors) {
                                    errors = errors.responseJSON;
                                    var message = !errors.error ? 'Có lỗi xảy ra, vui lòng thử lại sau' : errors.error.message;
                                    KTApp.unprogress(e), swal.fire({
                                        title: "",
                                        text: message,
                                        type: 'error',
                                        confirmButtonClass: "btn btn-secondary"
                                    });
                                }
                            });
                        })),
                        $(".kt_datatable_update_status_all").on("click", (function() {
                            var status = $(this).attr('attr');
                            var list_checked_id = t.checkbox().getSelectedId();
                            var total_checked_id = list_checked_id.length;
                            if(total_checked_id === 0) return false;
                            $.ajax({
                                url: "room/edit",
                                method: 'POST',
                                data: {
                                    _token: $("input[name='_token']").val(),
                                    ids: list_checked_id,
                                    total: total_checked_id,
                                    status: status
                                },
                                success: function(){
                                    window.location.reload();
                                },
                                error: function(errors) {
                                    errors = errors.responseJSON;
                                    var message = !errors.error ? 'Có lỗi xảy ra, vui lòng thử lại sau' : errors.error.message;
                                    KTApp.unprogress(e), swal.fire({
                                        title: "",
                                        text: message,
                                        type: 'error',
                                        confirmButtonClass: "btn btn-secondary"
                                    });
                                }
                            });
                        })),
                        $('#refresh-room').on("click", function() {
                            t.load();
                        }),
                        t.on("kt-datatable--on-click-checkbox kt-datatable--on-layout-updated", (function(e) {
                            var a = t.checkbox().getSelectedId().length;
                            $("#kt_datatable_selected_number1").html(a),
                                a > 0 ? $("#kt_datatable_group_action_form1").collapse("show") : $("#kt_datatable_group_action_form1").collapse("hide")
                        })),
                        $("#kt_modal_fetch_id_server").on("show.bs.modal", (function(e) {
                            for (var a = t.checkbox().getSelectedId(), n = document.createDocumentFragment(), o = 0; o < a.length; o++) {
                                var r = document.createElement("li");
                                r.setAttribute("data-id", a[o]),
                                    r.innerHTML = "Selected record ID: " + a[o],
                                    n.appendChild(r)
                            }
                            $(e.target).find(".kt-datatable_selected_ids").append(n)
                        })).on("hide.bs.modal", (function(t) {
                            $(t.target).find(".kt-datatable_selected_ids").empty()
                        }))
                }()
            }
        });
        jQuery(document).ready((function() {
            o.init()
        }))
    }
});