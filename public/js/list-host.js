$(document).ready(function() {
    // bacK to top JS
    var request = 'ToCancelPrevReq';
    var range = [];
    $('[data-toggle="tooltip"]').tooltip(); 
    let province =  $("input[name='province_id[]']").val() ? [$("input[name='province_id[]']").val()] : null;
    let district =  $("input[name='district_id[]']").val() ? [$("input[name='district_id[]']").val()] : null;
    let host =  $("input[name='host_id']").val() ? $("input[name='host_id']").val() : null;
    let checkin = $("input[name='checkin']").val();
    let checkout = $("input[name='checkout']").val();
    let amount = $("input[name='amount']").val();
    let type = $("input[name='host_type']").val() ? $("input[name='host_type']").val() : null;
    $.ajax({
        url: "/search-hotel",
        method: "GET",
        data: {
            _token: $("input[name='_token']").val(),
            province_id: province,
            district_id: district,
            host_id: host,
            host_type: type,
            checkin: checkin,
            checkout: checkout,
            amount: amount,
            page: 1, 
        },
        dataType: "html"
    }).done(function(msg) {
        $(".loader").hide("slow");
        $("#list-hotel").append(msg);
      
    });

    $("#filterType input").change(function() {
        
        $(".loader").show("slow");
        $("#list-hotel").html("");
        $("input[name='page']").val(1);
        ajaxLoadHotel();
    });
    $("#filterPlace input").change(function() {
        $(".loader").show("slow");
        $("#list-hotel").html("");
        $("input[name='page']").val(1);
        ajaxLoadHotel();
        
    });

    $("#filterStar input").change(function() {
        $(".loader").show("slow");
        $("#list-hotel").html("");
        $("input[name='page']").val(1);
        ajaxLoadHotel();
        
    });
    $('#loadMore .btn').click(function() {
        
        $('#loadMore .btn').prop('disabled', true);
        $(".loader").show("slow");
       
        ajaxLoadHotel();
    });

    $(".input-range").each(function() {
        var value = $(this).attr("data-slider-value");
        var separator = value.indexOf(",");
        if (separator !== -1) {
            value = value.split(",");
            value.forEach(function(item, i, arr) {
                arr[i] = parseFloat(item);
            });
        } else {
            value = parseFloat(value);
        }
        
        $(this).bootstrapSlider({
            formatter: function(value) {
                
                return Intl.NumberFormat("vn-VN", {
                    style: "currency",
                    currency: "VND"
                }).format(value);
            },
            min: parseFloat($(this).attr("data-slider-min")),
            max: parseFloat($(this).attr("data-slider-max")),
            range: $(this).attr("data-slider-range"),
            value: value,
            tooltip_split: $(this).attr("data-slider-tooltip_split"),
            tooltip: $(this).attr("data-slider-tooltip")
        });
        
    });
    
    $('.input-range').bootstrapSlider().on('slideStop', function(ev){
        range = $(this).data('bootstrapSlider').getValue();
        $(".loader").show("slow");
        $("#list-hotel").html("");
        $("input[name='page']").val(1);
        ajaxLoadHotel();
    });
    function ajaxLoadHotel() {
        let types = [],
            stars = [],
            province_ids = [],
            district_ids = [];
        let host_id = $("input[name='host_id']").val();
        let page = $("input[name='page']").val();
        let checkin = $("input[name='checkin']").val();
        let checkout = $("input[name='checkout']").val();
        let amount = $("input[name='amount']").val();
        $.each($("input[name='host_star']:checked"), function() {
            if ($(this).val() !== null) {
                stars.push($(this).val());
            }
        });
        $.each($("input[name='type']:checked"), function() {
            if ($(this).val() !== null) {
                types.push($(this).val());
            }
        });
        $.each($("input[name='province_id']:checked"), function() {
            if ($(this).val() !== null) {
                province_ids.push($(this).val());
            }
        });
        $.each($("input[name='district_id']:checked"), function() {
            if ($(this).val() !== null) {
                district_ids.push($(this).val());
            }
        });

        
        request = $.ajax({
            url: "/search-hotel",
            method: "GET",
            data: {
                _token: $("input[name='_token']").val(),
                province_id: province_ids,
                district_id: district_ids,
                host_id: host_id,
                type: types,
                stars: stars,
                range: range,
                checkin: checkin,
                checkout: checkout,
                amount: amount,
                page: page,
                
            },
            
            dataType: "html",
            beforeSend : function() {
                    if(request != 'ToCancelPrevReq' && request.readyState < 4) {
                        request.abort();
                    }
            }
        }).done(function(msg) {
            $(".loader").hide("slow");
            $('#loadMore .btn').prop('disabled', false);
            $("#list-hotel").append(msg);
            if(msg == '' || msg == null) {
                $('#loadMore').hide();
            }
            $("input[name='page']").val(parseInt(page)+1);
        });
    }
});
