$(document).ready(function() {
    

    $("#carousel-place-type").slick({
        dots: false,
        infinite: true,
        speed: 300,
        slidesToShow: 4,
        slidesToScroll: 4,
        margin: 10
    });

    $('input[name="checkin"]').daterangepicker(
        {
            locale: {
                format: "DD/MM/YYYY",
                applyLabel: "Xác nhận",
                cancelLabel: "Hủy",
                fromLabel: "Từ",
                toLabel: "Đến",
                monthNames: [
                    "Tháng 1",
                    "Tháng 2",
                    "Tháng 3",
                    "Tháng 4",
                    "Tháng 5",
                    "Tháng 6",
                    "Tháng 7",
                    "Tháng 8",
                    "Tháng 9",
                    "Tháng 10",
                    "Tháng 11",
                    "Tháng 12"
                ]
            },
            startDate: "07/02/2020",
            endDate: "07/03/2020",
            autoUpdateInput: false,
            opens: "center"
        },
        function(start, end, label) {
            console.log(
                "A new date selection was made: " +
                    start.format("DD/MM/YYYY") +
                    " to " +
                    end.format("DD/MM/YYYY")
            );
            $('input[name="checkin"]').val(start.format("DD/MM/YYYY"));
            $('input[name="checkout"]').val(end.format("DD/MM/YYYY"));
        }
    );
    $('input[name="checkout"]').daterangepicker(
        {
            locale: {
                format: "DD/MM/YYYY",
                applyLabel: "Xác nhận",
                cancelLabel: "Hủy",
                fromLabel: "Từ",
                toLabel: "Đến",
                monthNames: [
                    "Tháng 1",
                    "Tháng 2",
                    "Tháng 3",
                    "Tháng 4",
                    "Tháng 5",
                    "Tháng 6",
                    "Tháng 7",
                    "Tháng 8",
                    "Tháng 9",
                    "Tháng 10",
                    "Tháng 11",
                    "Tháng 12"
                ]
            },
            startDate: "07/02/2020",
            endDate: "07/03/2020",
            autoUpdateInput: false,
            opens: "center"
        },
        function(start, end, label) {
            $('input[name="checkin"]').val(start.format("DD/MM/YYYY"));
            $('input[name="checkout"]').val(end.format("DD/MM/YYYY"));

            console.log(
                "A new date selection was made: " +
                    start.format("DD/MM/YYYY") +
                    " to " +
                    end.format("DD/MM/YYYY")
            );
        }
    );
    $("#btn-search-hotel").click(function() {
        var keyword = $("#keyword").val();
        var checkin_date = $("#checkin_date").val();
        var checkin_out = $("#checkin_date").val();
        var amount_room = $("#checkin_date").val();
    });
    $("#keyword").select2({
        theme: "bootstrap4",

        placeholder: "Thành phố, Khách sạn, điểm đến",
        ajax: {
            delay: 100,
            url: "search-place/",
            dataType: "json",
            
            processResults: function (response) {
              return {
                 results: response
              };
            },
            cache: true,
            // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
        }
    });

    $('#keyword').on('select2:select', function (e) {
        var data = e.params.data;
        $('#type-query').attr('name', data.type);
        $('#type-query').val(data.id);
        console.log(data);
    });
});
