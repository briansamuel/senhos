<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // for ($i = 0; $i < 400; $i++) {
        //     //   $this->call(UsersTableSeeder::class);
        //     //   $this->call(PostsTableSeeder::class);
        //     //   $this->call(AgentsTableSeeder::class);
        //     // //   $this->call(CategoriesTableSeeder::class);
        //       $this->call(GuestsTableSeeder::class);
        //     // $this->call(HostsTableSeeder::class);
        //     // $this->call(RoomsTableSeeder::class);
        //     // $this->call(ReviewsTableSeeder::class);
        // }
        for ($i = 0; $i < 25; $i++) {
            //   $this->call(UsersTableSeeder::class);
            //   $this->call(PostsTableSeeder::class);
            //   $this->call(AgentsTableSeeder::class);
            // //   $this->call(CategoriesTableSeeder::class);
            //   $this->call(GuestsTableSeeder::class);
            // $this->call(HostsTableSeeder::class);
            // $this->call(RoomsTableSeeder::class);
            $this->call(ReviewsTableSeeder::class);
        }
        //  for($i = 0; $i < 100; $i++) {
        //     //  $this->call(LogsUserTableSeeder::class);
        //  }
        //  for($i = 0; $i < 15; $i++) {
        //     //  $this->call(UserGroupsTableSeeder::class);
        //  }
    }
}
