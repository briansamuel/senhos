<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
//       DB::table('users')->insert([
//           'username' => 'senhos',
//           'full_name' => 'SenHos',
//           'active_code' => Str::random(16),
//           'email' => 'senhos@gmail.com',
//           'password' => bcrypt('123456'),
//           'is_root' => true,
//           'status' =>  'active',
//           'group_id' => '',
//           'created_at' => date("Y-m-d H:i:s"),
//           'updated_at' => date("Y-m-d H:i:s")
//       ]);
//       DB::table('users')->insert([
//           'username' => 'dinhvanvu94@gmail.com',
//           'full_name' => 'Đinh Văn Vũ',
//           'active_code' => Str::random(16),
//           'email' => 'dinhvanvu94@gmail.com',
//           'password' => bcrypt('Nhocmiss@23'),
//           'is_root' => true,
//           'status' =>  'active',
//           'group_id' => '',
//           'created_at' => date("Y-m-d H:i:s"),
//           'updated_at' => date("Y-m-d H:i:s")
//       ]);
         DB::table('users')->insert([
             'username' => Str::random(40),
             'full_name' => Str::random(40),
             'active_code' => Str::random(16),
             'email' =>  Str::random(40).'@gmail.com',
             'password' => bcrypt('123456'),
             'is_root' => false,
             'status' =>  'active',
             'group_id' => '',
             'created_at' => date("Y-m-d H:i:s"),
             'updated_at' => date("Y-m-d H:i:s")
         ]);
        
    }
}
