<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBannersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banners', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('image', 200);
            $table->string('title', 200);
            $table->longText('description');
            $table->string('link');
            $table->tinyInteger('caption')->default(1);
            $table->enum('caption_position', ['left', 'right', 'top', 'bottom', 'center']);
            $table->string('type', 100);
            $table->enum('banner_type', ['image', 'video']);
            $table->string('type_value');
            $table->enum('language', ['vi', 'en'])->default('vi');
            $table->enum('active', ['yes', 'no'])->default('yes');
            $table->string('position')->default(100);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banners');
    }
}
