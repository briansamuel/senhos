<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('project_title');
            $table->string('project_slug');
            $table->text('project_description');
            $table->longText('project_content');
            $table->string('project_location');
            $table->string('project_seo_title')->nullable();
            $table->string('project_seo_keyword')->nullable();
            $table->string('project_seo_description')->nullable();
            $table->string('project_thumbnail')->nullable();
            $table->string('project_author', 100);
            $table->enum('project_status', ['trash', 'pending', 'draft', 'publish']);
            $table->string('language', 10);
            $table->bigInteger('created_by_user')->default(0)->unsigned();
            $table->bigInteger('updated_by_user')->default(0)->unsigned();
            $table->foreign('created_by_user')->references('id')->on('users');
            $table->foreign('updated_by_user')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project');
    }
}
