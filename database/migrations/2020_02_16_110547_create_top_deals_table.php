<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTopDealsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('top_deals', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title', 200);
            $table->string('image', 200);
            $table->text('description');
            $table->string('label', 100);
            $table->string('location', 100);
            $table->integer('start_time');
            $table->integer('end_time');
            $table->integer('regular_price');
            $table->integer('sale_price');
            $table->string('link', 200);
            $table->integer('position');
            $table->enum('language', ['vi', 'en']);
            $table->string('deal_group', 200);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('top_deals');
    }
}
