<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Schema::table('my_table', function (Blueprint $table) {
        //     //
        // });

        // Schema::table('posts', function (Blueprint $table) {
        //     $table->enum('post_status', ['trash', 'pending', 'draft', 'publish'])->change();
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('my_table', function (Blueprint $table) {
            //
        });
    }
}
