<?php

namespace App\Http\Controllers\FrontSite;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\PostService;
use App\Services\SettingService;
use App\Services\BannerService;
class VideoController extends Controller
{
    //
    //
    protected $request;
    protected $postService;

    function __construct(Request $request, PostService $postService)
    {
        $this->request = $request;
        $this->postService = $postService;
    }

    /**
     * ======================
     * Method:: View List video
     * ======================
     */

    public function index()
    {
        $post_type = $this->request->input('type_gallery', 'video');
        $filter = array('status' => 'publish', 'post_type' => $post_type);
        $videos = $this->postService->takeNew(SettingService::get('theme_option::post::pagination_post', 10), $filter);
        $banners = BannerService::getForFrontEndByTypeValue('gallery');
        if ($this->request->ajax()) {
            return view('frontsite.videos.elements.load', ['videos' => $videos])->render();
        }

        return view('frontsite.videos.index', compact('videos', 'banners'));
        
    }
    /**
     * ======================
     * Method:: View video Detail
     * ======================
     */

    public function videoDetail($slug)
    {
        $video = $this->postService->findBySlug($slug);
        if (!$video) {
            abort(404);
        }
        $breadcrumbs[] = (object) array('slug' => 'thu-vien-video', 'title' => __('seo.video.title'));
        $filter_2 = array('status' => 'publish', 'post_type' => 'video', 'exclude' => $video->id);
        $lasted_video =  $this->postService->takeNew(4, $filter_2);
        $data['video'] = $video;
        $data['breadcrumbs'] = $breadcrumbs;
        $data['lasted_video'] = $lasted_video;
        return view('frontsite.videos.video_detail', $data);
        
    }
}
