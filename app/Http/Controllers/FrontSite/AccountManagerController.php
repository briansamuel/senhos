<?php

namespace App\Http\Controllers\FrontSite;

use App\Helpers\ArrayHelper;
use App\Helpers\Message;
use App\Services\Frontsite\AuthService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Services\GuestService;

class AccountManagerController extends Controller
{
    protected $request;
    protected $guestService;

    function __construct(Request $request, GuestService $guestService)
    {
        $this->request = $request;
        $this->guestService = $guestService;

        $this->middleware(function ($request, $next) {
            if (!Auth::guard('user')->check()) {
                return redirect('/dang-nhap');
            }

            return $next($request);
        });
    }


    public function info()
    {

        $account = Auth::guard('user')->user();

        return view('frontsite.accounts.index', compact('account'));
    }


    public function infoAction()
    {
        $params = $this->request->only(['full_name', 'guest_phone', 'email']);
        $params = ArrayHelper::removeArrayNull($params);
        $id = Auth::guard('user')->user()->id;
        if (!isset($params['email']) || !isset($params['full_name']) || !isset($params['guest_phone'])) {
            return response()->json(Message::get(1, $lang = '', []));
        }

        if (GuestService::checkEmailExist($params['email'], $id)) {
            return response()->json(Message::get(32, $lang = '', []));
        }


        $edit = $this->guestService->edit($id, $params);
        if (!$edit) {
            return response()->json(Message::get(13, $lang = '', []), 400);
        }

        $data['success'] = true;
        $data['message'] = "Bạn đã cập nhập thông tin thành công";
        return response()->json($data);
    }

    public function resetPassword()
    {
        return view('frontsite.accounts.changepass');
    }

    public function resetPasswordAction()
    {
        $params = $this->request->only(['old_password', 'new_password', 'confirm_password']);
        $params = ArrayHelper::removeArrayNull($params);
        $id = Auth::guard('user')->user()->id;
        if (!isset($params['old_password']) || !isset($params['new_password']) || !isset($params['confirm_password'])) {
            return response()->json(Message::get(1, $lang = '', []));
        }
        if ($params['new_password'] !== $params['confirm_password']) {
            return response()->json(Message::get(21, $lang = '', []));
        }

        if (!AuthService::checkCurrentPassword($params['old_password'])) {
            return response()->json(Message::get(22, $lang = '', []));
        }

        $password = bcrypt($params['new_password']);

        $edit = $this->guestService->edit($id, ['password' => $password]);
        if (!$edit) {
            return response()->json(Message::get(13, $lang = '', []), 400);
        }

        $data['success'] = true;
        $data['message'] = "Bạn đã đổi mật khẩu thành công";
        return response()->json($data);
    }

    public function updatePassword()
    {
        if (Auth::guard('user')->user()->password !== '') {
            return redirect('/');
        }

        return view('frontsite.accounts.updatepass');
    }

    public function updatePasswordAction()
    {
        if (Auth::guard('user')->user()->password !== '') {
            return redirect('/');
        }

        $params = $this->request->only(['password', 'confirm_password']);
        $params = ArrayHelper::removeArrayNull($params);
        $id = Auth::guard('user')->user()->id;
        if (!isset($params['password']) || !isset($params['confirm_password'])) {
            return response()->json(Message::get(1, $lang = '', []));
        }
        if ($params['password'] !== $params['confirm_password']) {
            return response()->json(Message::get(21, $lang = '', []));
        }

        $password = bcrypt($params['password']);

        $edit = $this->guestService->edit($id, ['password' => $password]);
        if (!$edit) {
            return response()->json(Message::get(13, $lang = '', []), 400);
        }

        $data['success'] = true;
        $data['message'] = "Bạn đã cập nhập mật khẩu thành công";
        return response()->json($data);
    }

    public function bookmark()
    {
        return view('frontsite.accounts.bookmark');
    }


    public function historyBooking()
    {
        return view('frontsite.accounts.historybooking');
    }


}
