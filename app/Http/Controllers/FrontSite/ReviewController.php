<?php 

namespace App\Http\Controllers\FrontSite;

use App;
use App\Helpers\ArrayHelper;
use App\Helpers\Message;
use App\Http\Controllers\Controller;

use App\Services\ReviewService;
use App\Services\LogsUserService;
use App\Services\ValidationService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class ReviewController extends Controller
{
    protected $request;
    protected $locale;
    protected $reviewService;
    protected $validator;

    public function __construct(Request $request, ReviewService $reviewService, ValidationService $validator)
    {
        $this->request = $request;
        $this->validator = $validator;
        $this->reviewService = $reviewService;
        $this->locale = App::getLocale();
    }

    public function index()
    {

        return view('admin.reviews.index');
    }

    public function addAction() {

       
        try {
            
            $params = $this->request->only(['review_title', 'review_content', 'rating_review', 'host_id']);
            $user = Auth::guard('user')->user();
            
            if(!$user) {
               return response()->json( array('message' => __('message.guest.denied-permission'), 'status' => 'denied'));
            }
            
            $params['created_by_guest'] = $user->id;
            $params['name_guest'] = $user->full_name;
            $params['language'] = $this->locale;
           
            $validator = $this->validator->make($params, 'add_review_fields');
            if ($validator->fails()) {
                return response()->json( array('message' => $validator->errors()->all(), 'status' => 'fails'));
            }

            $add = $this->reviewService->insert($params);
            if ($add) {
                
                return response()->json( array('message' => __('message.add_review.success'), 'status' => 'success'));

            } else {

                return response()->json( array('message' => __('message.add_review.fails'), 'status' => 'fails'));

            }
        } catch(Exception $e) {
            return response()->json( array('message' => $e, 'status' => 'error'));
        }
        
    }

}