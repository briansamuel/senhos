<?php

namespace App\Http\Controllers\FrontSite;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\PostService;
use App\Services\BannerService;
use Exception;

class PartnerController extends Controller
{
    //
    protected $request;
    protected $postService;
    
    function __construct(Request $request, PostService $postService)
    {
        $this->request = $request;

        $this->postService = $postService;
    }


    /**
     * ======================
     * Method:: View List partner
     * ======================
     */

    public function index()
    {
        $filter = array('status' => 'publish', 'post_type' => 'partner');
        $partners = $this->postService->takeNew(6, $filter);
        $data['partners'] = $partners;
        $data['banners'] = BannerService::getForFrontEnd('partner');
        return view('frontsite.partners.list-partner', $data);
        
    }

    /**
     * ======================
     * Method:: View List partner
     * ======================
     */

    public function partnerDetail($slug) {
        
        try {
            $partner = $this->postService->findBySlug($slug);
            if (!$partner) {
                abort(404);
            }
            $filter = array('status' => 'publish', 'exclude' => $partner->id);
            $partners = $this->postService->takeNew(3, $filter);
            $breadcrumbs[] = (object) array('slug' => 'du-an', 'title' => __('frontsite.breadcrumb.partner_text'));
            $filter = array('status' => 'publish', 'post_type' => 'news');
            $lasted_news =  $this->postService->takeNew(4, $filter);
            $data['partner'] = $partner;
            $data['partners'] = $partners;
            $data['lasted_news'] = $lasted_news;
            $data['breadcrumbs'] = $breadcrumbs;
            
            return view('frontsite.partners.partner_detail', $data);
        } catch(Exception $e) {
            dd($e);
        }
        
    }
     

}
