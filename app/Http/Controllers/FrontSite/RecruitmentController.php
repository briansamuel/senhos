<?php

namespace App\Http\Controllers\FrontSite;

use App\Services\BannerService;
use App\Services\SettingService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\PostService;

class RecruitmentController extends Controller
{
    //
    //
    protected $request;
    protected $postService;

    function __construct(Request $request, PostService $postService)
    {
        $this->request = $request;
        $this->postService = $postService;
    }

    /**
     * ======================
     * Method:: View List recruitment
     * ======================
     */

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filter = array('status' => 'publish', 'post_type' => 'recruitment');
        $recruitments = $this->postService->takeNew(SettingService::get('theme_option::post::pagination_recruitment', 10), $filter);
        $hot_news =  $this->postService->takeHotNew(5, $filter);

        if ($request->ajax()) {
            return view('frontsite.recruitments.elements.load', ['recruitments' => $recruitments])->render();
        }
        $banners = BannerService::getForFrontEnd('recruitment');
        $filter = array('status' => 'publish', 'post_type' => 'video');
        $lasted_video =  $this->postService->takeNew(7, $filter);
        return view('frontsite.recruitments.list_recruitment', compact('recruitments','hot_news', 'banners', 'lasted_video'));
    }

    /**
     * ======================
     * Method:: View recruitment Detail
     * ======================
     */

    public function recruitmentDetail($slug)
    {
        $recruitment = $this->postService->findBySlug($slug);
        if (!$recruitment) {
            abort(404);
        }

        $breadcrumbs[] = (object) array('slug' => 'tuyen-dung', 'title' => __('seo.recruitment.title'));
        $filter_2 = array('status' => 'publish', 'post_type' => 'recruitment');
        $filter = array('status' => 'publish', 'post_type' => 'video');
        $lasted_video =  $this->postService->takeNew(7, $filter);
        $data['lasted_news'] =  $this->postService->takeNew(4, $filter_2);
        $data['related'] =  $this->postService->takeNew(7, $filter_2);
        $data['recruitment'] = $recruitment;
        $data['breadcrumbs'] = $breadcrumbs;
        $data['lasted_video'] = $lasted_video;
        $data['banners'] = BannerService::getForFrontEnd('recruitment');
        // update view total
        $this->postService->updateView($recruitment->id);
        return view('frontsite.recruitments.recruitment_detail', $data);
        
    }
}
