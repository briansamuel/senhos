<?php

namespace App\Http\Controllers\FrontSite;

use App\Http\Controllers\Controller;
use App\Services\BannerService;
use App\Services\ProjectService;
use Illuminate\Http\Request;
use App\Services\PostService;
use View;
use App;

class HomeController extends Controller
{
    //
    protected $request;
    protected $projectService;
    protected $postService;

    function __construct(Request $request, ProjectService $projectService, PostService $postService)
    {
        $this->request = $request;
        $this->projectService = $projectService;
        $this->postService = $postService;
    }


    /**
     * ======================
     * Method:: View Home
     * ======================
     */

    public function index()
    {
        $locale = App::getLocale();
        $filter = array('status' => 'publish', 'language' => $locale);
        $projects = $this->projectService->takeNew(3, $filter);
        $filter_2 = array('status' => 'publish', 'post_type' => 'video', 'language' => $locale);
        $lasted_video =  $this->postService->takeNew(3, $filter_2);
        $data['lasted_video'] = $lasted_video;
        $data['projects'] = $projects;
        $data['banners'] = BannerService::getForFrontEnd('home');

        return view('frontsite.home.index', $data);
    }
}
