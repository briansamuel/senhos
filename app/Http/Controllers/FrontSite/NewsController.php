<?php

namespace App\Http\Controllers\FrontSite;

use App;
use App\Services\BannerService;
use App\Services\SettingService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\PostService;
use App\Services\CategoryPostService;
use App\Services\CategoryService;
class NewsController extends Controller
{
    //
    //
    protected $request;
    protected $postService;
    protected $categoryPostService;

    function __construct(
        Request $request, 
        PostService $postService, 
        CategoryPostService 
        $categoryPostService, 
        CategoryService $categoryService
    )
    {
        $this->request = $request;
        $this->postService = $postService;
        $this->categoryPostService = $categoryPostService;
        $this->categoryService = $categoryService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $search = $this->request->input('search', '');
        
        if($search || $search !== '') {
            $filter['post_title'] = $search;
        }
        
        $locale = App::getLocale();
        $filter = array('category_parent' => 0, 'category_type' => 'category_of_news', 'language' => $locale);
        $categories = $this->categoryService->getAllByKey(['id', 'category_name'], $filter);
        $default_cate = 11;
        if(!empty($categories)) {
            $default_cate = $categories[0]->id;
        }
        $filter_p = array();
        $category_id = $this->request->input('category_id', $default_cate);
        if($category_id || $category_id != '') {
            $filter_p['category_id'] = $category_id;
        } else {
            $filter_p['category_id'] = $default_cate;
        }
        
        $posts_ids = $this->categoryPostService->getAllByKey(['post_id'], $filter_p, false);
        $ids = collect($posts_ids)->map(function ($post) {
            return $post->post_id;
        })->toArray();
        $filter = array('status' => 'publish', 'post_type' => 'news', 'ids' => $ids);

        $news = $this->postService->takeNew(SettingService::get('theme_option::post::pagination_post', 10), $filter);
        
        $filter = array('status' => 'publish', 'post_type' => 'news');
        $hot_news =  $this->postService->takeHotNew(5, $filter);
        
        
        if ($this->request->ajax()) {
            return view('frontsite.news.elements.load', ['news' => $news])->render();
        }
        
        $banners = BannerService::getForFrontEnd('news');
        $filter = array('status' => 'publish', 'post_type' => 'video');
        $lasted_video =  $this->postService->takeNew(7, $filter);
        return view('frontsite.news.list_news', compact('news','hot_news', 'top_deals', 'banners', 'categories', 'lasted_video'));
    }

    public function search(){
        $search = $this->request->input('search', '');
        $list = [];
        if(!$search || $search === '') {
            return [];
        }
        // check if ajax request is coming or not
        if($this->request->ajax()) {
            // select country name from database
            $data = $this->postService->searchNews($this->request->search);

            foreach($data as $item) {
                $list[] = array('title' => $item->post_title, 'url' => '//'.config('domain.web.domain').'/'.$item->post_slug.'.html');
            }
        }

        return $list;
    }

    /**
     * ======================
     * Method:: View News Detail
     * ======================
     */

    public function newsDetail($slug)
    {
        
        $news = $this->postService->findByKey('post_slug', $slug);
        if(!$news) {
            return abort(404);
        }
        $breadcrumbs[] = (object) array('slug' => 'tin-tuc', 'title' => __('seo.news.title'));
        $filter = array('status' => 'publish', 'post_type' => 'news');
        $data['hot_news'] =  $this->postService->takeHotNew(4, $filter);
        $filter['exclude'] = $news->id;
        $data['related_news'] =  $this->postService->takeNew(7, $filter);
        $filter = array('status' => 'publish', 'post_type' => 'video');
        $data['lasted_video'] =  $this->postService->takeNew(7, $filter);
        $data['news'] = $news;
        $data['breadcrumbs'] = $breadcrumbs;
        $data['banners'] = BannerService::getForFrontEnd('new');

        // update view
        $this->postService->updateView($news->id);
        return view('frontsite.news.news_detail', $data);
        
    }
}
