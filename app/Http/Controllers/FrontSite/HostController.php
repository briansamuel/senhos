<?php

namespace App\Http\Controllers\FrontSite;

use App;
use App\Http\Controllers\Controller;
use App\Services\HostService;
use App\Services\RoomService;
use App\Services\ReviewService;
use App\Services\BookingService;
use App\Services\ProvinceService;
use App\Services\DistrictService;
use App\Services\ValidationService;
use Illuminate\Http\Request;
use App\Helpers\Message;
use Exception;
use Illuminate\Support\Facades\Auth;

use function GuzzleHttp\json_decode;

class HostController extends Controller
{
    //
    protected $request;
    protected $locale;
    protected $hostService;
    protected $roomService;
    protected $reviewService;
    protected $provinceService;
    protected $validationService;

    function __construct(
        Request $request,
        HostService $hostService,
        RoomService $roomService,
        ReviewService $reviewService,
        ProvinceService $provinceService,
        DistrictService $districtService,
        ValidationService $validationService
    ) {
        $this->request = $request;
        $this->hostService = $hostService;
        $this->roomService = $roomService;
        $this->reviewService = $reviewService;
        $this->provinceService = $provinceService;
        $this->districtService = $districtService;
        $this->validationService = $validationService;
        $this->locale = App::getLocale();
    }



    public function search()
    {
        $params = $this->request->all();
        $query =  __('frontsite.header.site_name');
        if (isset($params['province_id'])) {
            $province = $this->provinceService->findByKey('id', $params['province_id']);
            if ($province) {
                $query = $province->_name;
            }
        }

        if (isset($params['district_id'])) {
            $district = $this->districtService->findByKey('id', $params['district_id']);
            if ($district) {
                $query = $district->_name;
            }
        }

        if (isset($params['host_type'])) {
            $query = __('frontsite.booking.'.$params['host_type']);
        }
        $type_count = $this->hostService->countGroupBy('host_type');

        $total_hotel = $this->hostService->totalRows($params);
        $data['total_host'] = $total_hotel;
        $data['query'] = $query;
        $data['type_count'] = $type_count;
        // Type
        return view('frontsite.booking.list-hotel', $data);
    }

    public function updateMany()
    {
        $params = array(
            'pagination' => array(
                'perpage' => 500,
                'page' => 1,
            ),

            'sort' => array(
                'field' => 'id',
                'sort' => 'DESC',
            ),
        );

        $result = $this->hostService->filterList($params);  
        $hosts = $result['data'];

        foreach ($hosts as $index => $host) {

            $host->host_star = rand(1, 5);
            $host = (array) $host;
            $this->hostService->update($host, $host);
        }
    }

    public function detail($type, $slug, $id)
    {

        try {

            if ($this->request->ajax()) {
                $filter =  array('host_id' => $id, 'review_status' => 'publish', 'language' => $this->locale);
                $reviews = $this->reviewService->takeNew(10, $filter);
                return view('frontsite.hosts.elements.reviews', ['reviews' => $reviews])->render();
            }

            $host = $this->hostService->findByKey('id', $id);

            if (!$host) {
                return redirect()->route('hosts.search');
            }

            if ($slug != $host->host_slug) {
                return redirect()->route('hosts.search');
            }



            $host->host_gallery = json_decode($host->host_gallery);
            $host->host_type = $this->typePlace_text($host->host_type);
            $convenient = json_decode($host->host_convenient);
            $reviews = $this->reviewService->getAll(['rating_review'], array('host_id' => $host->id, 'review_status' => 'publish', 'language' => $this->locale));
            $user_rating = $reviews->count() == 0 ? 1 : $reviews->count();
           
            $host->total_rating = $reviews->sum('rating_review');
            $host->rating =  (float) ($reviews->sum('rating_review') / $user_rating);
            $host->user_rating = $reviews->count() == 0 ? 1 : $reviews->count();
            $host->rating_text = $reviews->count() == 0 ? __('frontsite.booking.no_rating') : $this->rating_text($host->rating);
            // Rating Review Collect
            $host->great = $reviews->filter(function ($value, $key) {
                return $value->rating_review === 5;
            });
            $host->very_good = $reviews->filter(function ($value, $key) {
                return $value->rating_review === 4;
            });
            $host->good = $reviews->filter(function ($value, $key) {
                return $value->rating_review === 3;
            });
            $host->bad = $reviews->filter(function ($value, $key) {
                return $value->rating_review === 2;
            });
            $host->very_bad = $reviews->filter(function ($value, $key) {
                return $value->rating_review === 1;
            });
            // 
            $rooms = $this->roomService->getMany(
                array(
                    'page' => 1,
                    'perpage' => 30,
                ),
                array(
                    'field' => 'price_one_night',
                    'sort' => 'ASC'
                ),
                array(
                    'host_id' => $host->id
                )
            );
            $data['host'] = $host;
            $data['rooms'] = $rooms;
            $data['reviews'] = $reviews;
            $data['convenient'] = $convenient;
            
            return view('frontsite.hosts.host_detail', $data);
        } catch (Exception $e) {
            return redirect()->route('hosts.search');
        }
    }

    public function detailRoom($id)
    {
        if ($this->request->ajax()) {
            $room = $this->roomService->findByKey('id', $id, ['*']);
            $convenients = json_decode($room->room_convenient);
            $room->room_option = json_decode($room->room_option);
            $room->room_gallery = json_decode($room->room_gallery);
            
            return view('frontsite.hosts.elements.room-popup', ['room' => $room, 'convenients' => $convenients])->render();
        }
        
    }
    public function comfirmRoom()
    {
        $user = Auth::guard('user')->user();
        $data['user'] = $user;
        $params = $this->request->all('room_id');
        if(isset($params['room_id'])) {
            $column = ['id', 'host_id','room_name', 'price_one_night', 'sale_for_room', 'room_option', 'guest_amount'];
            $room = $this->roomService->findByKey('id', $params['room_id'], $column);

            if($room) {
                $room->room_option = json_decode($room->room_option);
      
                $host = $this->hostService->findByKey('id', $room->host_id);
                $data['room'] =  $room;
                $data['host'] = $host;
            }
            
            return view('frontsite.hosts.comfirm', $data);
        }
        return redirect()->back();

    }

    public function comfirmRoomAction() {
        try {
            
            $params = $this->request->all();
            
            $user = Auth::guard('user')->user();
     
            if(!$user) {
               return response()->json( array('message' => __('message.guest.denied-permission'), 'status' => 'denied'));
            }
            $params['guest_id'] = $user->id;
            $params['guest_info'] = $user->full_name;
            $params['created_by_agent'] = 0;
            $params['bed_type'] = json_encode($params['bed_type']);
            $params['checkin_date'] = strtotime($params['checkin_date']);
            $params['checkin_date'] = date('Y-m-d',$params['checkin_date']);
            $params['checkout_date'] = strtotime($params['checkout_date']);
            $params['checkout_date'] = date('Y-m-d',$params['checkout_date']);
            $validator = $this->validationService->make($params, 'add_booking_fields');
            if ($validator->fails()) {
                return response()->json( $validator->errors()->all());     
            }
            
            $add =BookingService::insert($params);
            if ($add) {
                
                return redirect()->route('pages.thankyou');
                //return response()->json( array('message' => __('message.add_booking.success'), 'status' => 'success'));

            } else {

                Message::alertFlash( __('message.add_booking.fails'), 'danger');

                return redirect()->back()->withInput();
                

            }
        } catch(Exception $e) {
            dd($e);
            return response()->json( array('message' => $e, 'status' => 'error'));
        }
 
        
    }
    
    public function ajaxGetList()
    {
        try {
            $input = $this->request->only(['province_id', 'district_id', 'host_id','stars', 'type', 'range', 'page', 'host_type']);
            $query = $this->request->only(['checkin', 'checkout', 'amount']);
            $text_query = '?';
            foreach($query as $key => $q) {
                if(isset($q)) {
                    $text_query .= $key.'='.$q.'&';
                }
            }
            $params = array(
                'pagination' => array(
                    'perpage' => 20,
                    'page' => $input['page'],
                ),
                'query' => $input,
                'sort' => array(
                    'field' => 'id',
                    'sort' => 'DESC',
                ),
                'columns' => ['id', 'host_name', 'host_slug', 'host_description',  'host_address', 'host_type', 'host_thumbnail', 'lowest_room_rates', 'province_name', 'district_name', 'ward_name']
            );
           
            $result = $this->hostService->filterList($params);
            
            $hosts = $result['data'];
            $sort =  array(
                'field' => 'price_one_night',
                'sort' => 'ASC',
            );
            $columns = ['price_one_night', 'sale_for_room'];
            foreach ($hosts as $index => $host) {

                $room = $this->roomService->findRoomSort('host_id', $host->id, $columns, $sort);
                $reviews = $this->reviewService->getAll(['rating_review'], array('host_id' => $host->id));
                $user_rating = $reviews->count() == 0 ? 1 : $reviews->count();
                $host->rating =  (float) ($reviews->sum('rating_review') / $user_rating);
                $host->user_rating = $reviews->count();
                $host->rating_text = $reviews->count() == 0 ? __('frontsite.booking.no_rating') : $this->rating_text($host->rating);
                $host->host_query = $text_query;
                $host->room = $room;
            }
            return view('frontsite.booking.elements.ajax-hosts', ['hosts' => $hosts])->render();
        } catch (Exception $e) {
            return response()->json($e);
        }
    }

    public function ajaxGetPlace()
    {
        $input = $this->request->only(['q']);
        $params = array(
            'pagination' => array(
                'perpage' => 20,
                'page' => 0,
            ),
            'query' => $input,
        );
        $hosts = $this->hostService->getList($params);
        $data['hosts'] = $hosts;

        return view('frontsite.booking.list-hotel', $data);
    }

    public function typePlace_text($type)
    {
        switch ($type) {
            case $type == 'hotel':
                return __('frontsite.booking.hotel');
                break;
            case $type == 'resort':
                return __('frontsite.booking.resort');
                break;
            case $type == 'apartment':
                return __('frontsite.booking.apartment');
                break;
            case $type == 'homestay':
                return __('frontsite.booking.homestay');
                break;
            case $type == 'villa':
                return __('frontsite.booking.villa');
                break;
            case $type == 'campsite':
                return __('frontsite.booking.campsite');
                break;
            default:
                return __('frontsite.booking.campsite');
                break;
        }
    }
    public function rating_text(float $score)
    {

        switch ($score) {

            case $score == 0:

                return __('frontsite.booking.no_rating');
                break;
            case $score <= 1:
                return __('frontsite.booking.very_bad');
                break;
            case $score > 1 && $score <= 2:
                return __('frontsite.booking.bad');
                break;
            case $score > 2 && $score <= 3:
                return __('frontsite.booking.good');
                break;
            case $score > 3 && $score <= 4:
                return __('frontsite.booking.very_good');
                break;
            case $score > 4 && $score <= 5:
                return __('frontsite.booking.great');
                break;
            default:
                return __('frontsite.booking.no_rating');
                break;
        }
    }

    public function nearHotel()
    {

        return view('frontsite.hosts.near_host');
    }

    public function nearHotelAction()
    {
        $lat = $this->request->input('lat');
        $lng = $this->request->input('lng');
        $radius = $this->request->input('radius', 3);
        $data = $this->hostService->getShopNear($lat, $lng, $radius, 20);

        return response()->json($data, 200);
    }

    public function fixLocation()
    {
        $this->hostService->fixLocation();

        return ['success' => true];
    }
}
