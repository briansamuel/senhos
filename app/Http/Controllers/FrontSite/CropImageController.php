<?php

namespace App\Http\Controllers\FrontSite;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;


class CropImageController extends Controller
{
    //
    protected $request;



    function __construct(Request $request)
    {
        $this->request = $request;
    }


    

    /**
     * METHOD index - Ajax Get List story
     *
     * @return void
     */

    public function thumbnailHost($thumbnail_name, $extension) {
        
        $this->cropImage($thumbnail_name, $extension, 350, 350);
    }

    public function cropImage($thumbnail_name, $extension, $width, $height ) {
        $filename = 'uploads/files/khach-san/'.$thumbnail_name.'.'.$extension;
        $image = imagecreatefromjpeg($filename);
        

        $thumb_width = 350;
        $thumb_height = 350;

        $width = imagesx($image);
        $height = imagesy($image);

        $original_aspect = $width / $height;
        $thumb_aspect = $thumb_width / $thumb_height;

        if ( $original_aspect >= $thumb_aspect )
        {
        // If image is wider than thumbnail (in aspect ratio sense)
        $new_height = $thumb_height;
        $new_width = $width / ($height / $thumb_height);
        }
        else
        {
        // If the thumbnail is wider than the image
        $new_width = $thumb_width;
        $new_height = $height / ($width / $thumb_width);
        }

        $thumb = imagecreatetruecolor( $thumb_width, $thumb_height );

        // Resize and crop
        imagecopyresampled($thumb,
                        $image,
                        0 - ($new_width - $thumb_width) / 2, // Center the image horizontally
                        0 - ($new_height - $thumb_height) / 2, // Center the image vertically
                        0, 0,
                        $new_width, $new_height,
                        $width, $height);
        header("Content-Type: image/jpeg");                      
        imagejpeg($thumb);
        imagedestroy($thumb);
        exit();
    }
    
}
