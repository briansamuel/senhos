<?php

namespace App\Http\Controllers\FrontSite;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\ProjectService;
use App\Services\PostService;
use App\Services\BannerService;
use Exception;

class ProjectController extends Controller
{
    //
    protected $request;
    protected $projectService;
    protected $postService;

    function __construct(Request $request, ProjectService $projectService, PostService $postService)
    {
        $this->request = $request;
        $this->projectService = $projectService;
        $this->postService = $postService;
    }


    /**
     * ======================
     * Method:: View List Project
     * ======================
     */

    public function index()
    {
        $filter = array('status' => 'publish');
        $projects = $this->projectService->takeNew(6, $filter);
        $data['projects'] = $projects;
        $data['banners'] = BannerService::getForFrontEnd('project');
        return view('frontsite.projects.list-project', $data);
        
    }

    /**
     * ======================
     * Method:: View List Project
     * ======================
     */

    public function projectDetail($slug) {
        
        try {
            $project = $this->projectService->findByKey('project_slug', $slug);
            $filter = array('status' => 'publish', 'exclude' => $project->id);
            $projects = $this->projectService->takeNew(3, $filter);
            $breadcrumbs[] = (object) array('slug' => 'du-an', 'title' => __('frontsite.breadcrumb.project_text'));
            $filter_2 = array('status' => 'publish', 'post_type' => 'video');
            $lasted_video =  $this->postService->takeNew(4, $filter_2);
            $data['project'] = $project;
            $data['projects'] = $projects;
            $data['lasted_video'] = $lasted_video;
            $data['breadcrumbs'] = $breadcrumbs;
            return view('frontsite.projects.project_detail', $data);
        } catch(Exception $e) {
            dd($e);
        }
        
    }
     

}
