<?php

namespace App\Http\Controllers\Admin\CMS;

use App\Helpers\ArrayHelper;
use App\Helpers\Message;
use App\Http\Controllers\Controller;
use App\Services\Auth\AuthPermissionService;
use App\Services\LogsUserService;
use App\Services\ValidationService;
use App\Services\UserGroupService;
use Illuminate\Http\Request;
use Session;

class UserGroupController extends Controller
{
    protected $request;
    protected $userGroupService;
    protected $validator;

    function __construct(Request $request, ValidationService $validator, UserGroupService $userGroupService)
    {
        $this->request = $request;
        $this->validator = $validator;
        $this->userGroupService = $userGroupService;
    }


    /**
     * METHOD index - View List News
     *
     * @return void
     */

    public function index()
    {
        return view('admin.user_group.index');
    }


    /**
     * METHOD index - View List News
     *
     * @return void
     */

    public function ajaxGetList()
    {
        $params = $this->request->all();

        $result = $this->userGroupService->getList($params);

        return response()->json($result);
    }

    /**
     * METHOD viewInsert - VIEW ADD, EDIT NEWS
     *
     * @return void
     */

    public function add()
    {
        $Auth = new AuthPermissionService($this->request);
        $data['controllers'] = $Auth->listController();

        return view('admin.user_group.add', ['data' => $data]);
    }

    public function addAction()
    {
        $params = $this->request->all();
        $params = ArrayHelper::removeArrayNull($params);
        $validator = $this->validator->make($params, 'add_user_group_fields');
        if ($validator->fails()) {
            return response()->json(Message::get(1, $lang = '', $validator->errors()->all()), 400);
        }

        $checkGroupName = UserGroupService::checkExistsName($params['group_name']);
        if ($checkGroupName) {
            $message = "Đã tồn tại User Group có Name = $checkGroupName";
            return response()->json(Message::getArray(0, $message, []), 400);
        }

        $Auth = new AuthPermissionService($this->request);
        $encode_permission = $Auth->getListPermission();

        $insertData = array(
            'group_name' => $params['group_name'],
            'short_description' => $params['short_description'],
            'enabled' => $params['enabled'],
            'permission' => $encode_permission
        );
        $add = $this->userGroupService->add($insertData);
        if($add) {
            //add log
            $log['action'] = "Thêm mới 1 phân quyền có id = " . $add;
            $log['content'] = json_encode($insertData);
            $log['ip'] = $this->request->ip();
            LogsUserService::add($log);

            $data['success'] = true;
            $data['message'] = "Thêm mới phân quyền thành công !!!";
        } else {
            $data['message'] = "Lỗi khi thêm mới phân quyền !";
        }

        return response()->json($data);
    }

    public function deleteMany()
    {
        $params = $this->request->only('ids', 'total');
        if (!isset($params['ids'])) {
            return response()->json(Message::get(26, $lang = '', []), 400);
        }
        $delete = $this->userGroupService->deleteMany($params['ids']);
        if (!$delete) {
            return response()->json(Message::get(12, $lang = '', []), 400);
        }

        Message::alertFlash("Bạn đã xóa tổng cộng " . $params['total'] . " phân quyền thành công !!!", 'success');

        //add log
        $log['action'] = "Xóa " . $params['total'] . " phân quyền thành công";
        $log['content'] = json_encode($params);
        $log['ip'] = $this->request->ip();
        LogsUserService::add($log);

        $data['success'] = true;
        $data['message'] = "Bạn đã xóa tổng cộng " . $params['total'] . " phân quyền thành công !!!";
        return response()->json($data);
    }

    public function delete($id)
    {
        $delete = $this->userGroupService->delete($id);
        if($delete) {
            $detail = $this->userGroupService->detail($id);
            //add log
            $log['action'] = "Xóa phân quyền thành công có ID = " . $id;
            $log['content'] = json_encode($detail);
            $log['ip'] = $this->request->ip();
            LogsUserService::add($log);
            Message::alertFlash('Bạn đã xóa phân quyền thành công', 'success');
        } else {
            Message::alertFlash('Bạn đã xóa phân quyền không thành công', 'danger');
        }

        return redirect("user-group");
    }

    public function detail($id)
    {
        $Auth = new AuthPermissionService($this->request);
        $data['controllers'] = $Auth->listController();

        $data['userGroup'] = $this->userGroupService->detail($id);
        $data['permissions'] = json_decode($data['userGroup']->permission);
        if($data['permissions'] === null) {
            $data['permissions'] = [];
        }

        return view('admin.user_group.detail', $data);
    }

    public function edit($id)
    {
        $Auth = new AuthPermissionService($this->request);
        $data['controllers'] = $Auth->listController();

        $data['userGroup'] = $this->userGroupService->detail($id);
        $data['permissions'] = json_decode($data['userGroup']->permission);
        if($data['permissions'] === null) {
            $data['permissions'] = [];
        }

        return view('admin.user_group.edit', $data);
    }

    public function editAction($id)
    {
        $params = $this->request->all();
        $params = ArrayHelper::removeArrayNull($params);
        $validator = $this->validator->make($params, 'edit_user_group_fields');
        if ($validator->fails()) {
            return response()->json(Message::get(1, $lang = '', $validator->errors()->all(), 400));
        }

        $checkGroupName = UserGroupService::checkExistsName($params['group_name'], $id);
        if ($checkGroupName) {
            $message = "Đã tồn tại User Group có Name = $checkGroupName";
            return response()->json(Message::getArray(0, $message, []), 400);
        }

        $Auth = new AuthPermissionService($this->request);
        $encode_permission = $Auth->getListPermission();

        $updateData = array(
            'group_name' => $params['group_name'],
            'short_description' => $params['short_description'],
            'enabled' => $params['enabled'],
            'permission' => $encode_permission
        );
        $edit = $this->userGroupService->edit($id, $updateData);
        if (!$edit) {
            return response()->json(Message::get(13, $lang = '', []), 400);
        }
        //add log
        $log['action'] = "Cập nhập 1 phân quyền có id = " . $id;
        $log['content'] = json_encode($updateData);
        $log['ip'] = $this->request->ip();
        LogsUserService::add($log);

        $data['success'] = true;
        $data['message'] = "Cập nhập phân quyền thành công !!!";

        return response()->json($data);
    }

    public function editManyAction()
    {
        $params = $this->request->only(['enabled', 'ids', 'total']);
        $params = ArrayHelper::removeArrayNull($params);
        if (!isset($params['ids'])) {
            return response()->json(Message::get(26, $lang = '', []), 400);
        }
        $update = $this->userGroupService->updateMany($params['ids'], ['enabled' => $params['enabled']]);
        if (!$update) {
            return response()->json(Message::get(12, $lang = '', []), 400);
        }

        Message::alertFlash("Bạn đã cập nhập tổng cộng " . $params['total'] . " phân quyền thành công !!!", 'success');

        //add log
        $log['action'] = "Cập nhập nhiều phân quyền thành công";
        $log['content'] = json_encode($params);
        $log['ip'] = $this->request->ip();
        LogsUserService::add($log);

        $data['success'] = true;
        $data['message'] = "Bạn đã cập nhập tổng cộng " . $params['total'] . " phân quyền thành công !!!";
        return response()->json($data);
    }

}
