<?php

namespace App\Http\Controllers\Admin\Host;

use App\Http\Controllers\Controller;
use App\Services\ValidationService;
use App\Services\HostService;
use App\Services\ProvinceService;
use App\Services\DistrictService;
use App\Services\WardService;
use App\Services\LogsUserService;
use Illuminate\Http\Request;
use App\Helpers\Message;
use App\Helpers\ArrayHelper;
use App\Helpers\ConvenientHelper;
class HostController extends Controller
{
    //
    protected $request;
    protected $validator;
    protected $hostService;
    
    function __construct(Request $request, ValidationService $validator, HostService $hostService)
    {
        $this->request = $request;
        $this->validator = $validator;
        $this->hostService = $hostService;
        session()->start();
        session()->put('RF.subfolder', 'khach-san');
        session()->put('RF.thumbnailWidth', 350);
        session()->put('RF.thumbnailHeight', 350);
    }

    /**
     * METHOD index - View List Hosts
     *
     * @return void
     */

    public function index()
    {
        
        return view('admin.hosts.index');
    }

    /**
     * METHOD index - Ajax Get List Host
     *
     * @return void
     */

    public function ajaxGetList()
    {
        $params = $this->request->all();
        $result = $this->hostService->getList($params);
        
        return response()->json($result);
    }

    /**
     * METHOD index - View Add Host
     *
     * @return void
     */

    public function add() 
    {   
        
        $convenients = ConvenientHelper::getConvenient();
        $provinces = ProvinceService::getMany(['id', '_name'], null);
        return view('admin.hosts.add', ['provinces' => $provinces, 'convenients' => $convenients]);
    }

    /**
     * METHOD index - Action Add Host
     *
     * @return void
     */

    public function addAction() 
    {
        $params = $this->request->only('host_name', 'host_slug', 'host_description', 'host_policy', 'host_convenient', 'host_gallery', 'host_status', 'host_type', 'host_thumbnail', 'host_address', 'host_lat', 'host_lng', 'province_id', 'district_id', 'ward_id', 'province_name', 'district_name', 'ward_name', 'language');
        $params = ArrayHelper::removeArrayNull($params);
        $params['created_by_agent'] = 1;
        $params['updated_by_agent'] = 1;
        $params['host_convenient'] = isset($params['host_convenient']) ? json_encode($params['host_convenient']) : '';
        $params['host_gallery'] = isset($params['host_gallery']) ? json_encode($params['host_gallery']) : '';
        $validator = $this->validator->make($params, 'add_host_fields');
        if ($validator->fails()) {
            return redirect()->back()->withErrors(['error' => $validator->errors()->all()]);     
        }
        
        $add = $this->hostService->insert($params);
        if ($add) {
            //add log

            Message::alertFlash('Bạn đã thêm khách sạn thành công', 'success');

            $log['action'] = "Thêm mới 1 Khách sạn có id = " . $add;
            $log['content'] = json_encode($params);
            $log['ip'] = $this->request->ip();
            LogsUserService::add($log);


        } else {

            Message::alertFlash('Đã xảy ra lỗi khi thêm khách sạn, vui lòng liên hệ quản trị viên!', 'danger');

        }

        // TEST CI
        return redirect()->back()->withInput();
        
    }
    
    /**
     * METHOD index - View Edit Host
     *
     * @return void
     */

    public function edit($id = 0) 
    {
       
        $host = $this->hostService->findByKey('id', $id);
        if(!$host) {
            return redirect('denied-permission');
        }
        $convenients = ConvenientHelper::getConvenient();
        $provinces = ProvinceService::getMany(['id', '_name'], array('perpage' => 100));
        $districts = DistrictService::getMany(['id', '_name'], array('_province_id' => $host->province_id, 'perpage' => 100));
        $wards = WardService::getMany(['id', '_name'], array('_district_id' => $host->district_id, 'perpage' => 100));
        $host->host_gallery = json_decode($host->host_gallery);
        
        //$host->host_convenient = json_decode($host->host_convenient);
        $convenient = json_decode($host->host_convenient);
        
        $convenient = $this->convenients_array($convenient);
        foreach($convenients as $type => $array) {
            
            foreach($array as $key => $item) {
                if(in_array($item, $convenient)) {
                    $convenients[$type][$key] = ['text' => $convenients[$type][$key], 'value' => 'checked' ];
                    //dd($convenients[$type][$key]);
                } else {
                    $convenients[$type][$key] = ['text' => $convenients[$type][$key], 'value' => '' ];
                }
                
            }
            
        }
        
        //$convenients['service_hotel'][0] = [ 'text' => $convenients['service_hotel'][0], 'value' => 'selected' ];
        
        $data['provinces'] = $provinces;
        $data['districts'] = $districts;
        $data['wards'] = $wards;
        $data['convenients'] = $convenients;
        $data['host'] = $host;
        
        return view('admin.hosts.edit', $data);
    }

     /**
     * METHOD index - Action Edit Host
     *
     * @return void
     */

    public function editAction($id = 0) 
    {

        $params = $this->request->only('host_name', 'host_slug', 'host_description', 'host_policy', 'host_convenient', 'host_gallery', 'host_status', 'host_type', 'host_thumbnail', 'host_address', 'host_lat', 'host_lng', 'province_id', 'district_id', 'ward_id', 'province_name', 'district_name', 'ward_name', 'language');
        $params = ArrayHelper::removeArrayNull($params);
        $params['created_by_agent'] = 0;
        $params['updated_by_agent'] = 0;
        $params['host_convenient'] = isset($params['host_convenient']) ? json_encode($params['host_convenient']) : '';
        $params['host_gallery'] = isset($params['host_gallery']) ? json_encode($params['host_gallery']) : '[]';
        $validator = $this->validator->make($params, 'edit_host_fields');
        if ($validator->fails()) {
            return redirect()->back()->withErrors(['error' => $validator->errors()->all()]);     
        }


        $edit = $this->hostService->update($id, $params);
        if (!$edit) {
            Message::alertFlash('Đã xảy ra lỗi khi cập nhật khách sạn, vui lòng liên hệ quản trị viên!', 'danger');
            return redirect()->back()->withInput();
        }
        
        Message::alertFlash('Cập nhật khách sạn thành công !', 'success');
        //add log
        $log['action'] = "Cập nhập Khách sạn thành công có ID = " . $id;
        $log['content'] = json_encode($params);
        $log['ip'] = $this->request->ip();
        LogsUserService::add($log);

        return redirect()->back()->withInput();
    }

    public function deleteMany()
    {
        $params = $this->request->only('ids', 'total');
        if (!isset($params['ids'])) {
            return response()->json(Message::get(26, $lang = '', []), 400);
        }
        $delete = $this->hostService->deleteMany($params['ids']);
        if (!$delete) {
            return response()->json(Message::get(12, $lang = '', []), 400);
        }

        Message::alertFlash("Bạn đã xóa tổng cộng " . $params['total'] . " Khách sạn thành công !!!", 'success');

        //add log
        $log['action'] = "Xóa " . $params['total'] . " User thành công";
        $log['content'] = json_encode($params);
        $log['ip'] = $this->request->ip();
        LogsUserService::add($log);

        $data['success'] = true;
        $data['message'] = "Bạn đã xóa tổng cộng " . $params['total'] . " Khách sạn thành công !!!";
        return response()->json($data);
    }

    public function delete($id)
    {
        
        $delete = $this->hostService->delete($id);
        if($delete) {
            //add log
            $log['action'] = "Xóa Khách sạn thành công có ID = " . $id;
            $log['content'] = json_encode($delete);
            $log['ip'] = $this->request->ip();
            LogsUserService::add($log);

            Message::alertFlash('Bạn đã xóa Khách sạn thành công', 'success');
        } else {
            Message::alertFlash('Bạn đã xóa Khách sạn không thành công', 'danger');
        }

        return redirect("host");
    }

    public function ajaxGetDistrict() {
        $params = $this->request->only(['_province_id']);
        $params['_province_id'] = isset($params['_province_id']) ? $params['_province_id'] : 0;
        $districts = DistrictService::getMany(['id', '_name'], $params);
        return response()->json($districts);
    }

    public function ajaxGetWard() {
        $params = $this->request->only(['_district_id']);
        $params['_district_id'] = isset($params['_district_id']) ? $params['_district_id'] : 0;
        $wards = WardService::getMany(['id', '_name'], $params);
        return response()->json($wards);
    }

    public function convenients_array($convenient_obj = array()) {
        $array_con = array();
        if(is_object($convenient_obj)) { 
            foreach($convenient_obj as $array) {
                $array = (array) $array;
                if(is_array($array)) {
                    foreach($array as $key => $item) {
                        array_push($array_con, $item);
                    }
                }
                
            }
        }
        return $array_con;
    }
}
