<?php

namespace App\Http\Controllers\Admin\Host;

use App\Http\Controllers\Controller;
use App\Services\ValidationService;
use App\Services\HostService;
use App\Services\BookingService;
use App\Services\LogsUserService;
use Illuminate\Http\Request;
use App\Helpers\Message;
use App\Helpers\ArrayHelper;
use App\Helpers\ConvenientHelper;

class BookingController extends Controller
{
    //

    function __construct(Request $request, ValidationService $validator, BookingService $bookingService)
    {
        $this->request = $request;
        $this->validator = $validator;
        $this->bookingService = $bookingService;
    }

    /**
     * METHOD index - View List Hosts
     *
     * @return void
     */

    public function index()
    {
        
        return view('admin.bookings.index');
    }

    /**
     * METHOD index - Ajax Get List Host
     *
     * @return void
     */

    public function ajaxGetList()
    {
        $params = $this->request->all();
        $result = $this->bookingService->getList($params);

        return response()->json($result);
    }

    /**
     * METHOD index - View Add Host
     *
     * @return void
     */

    public function add() 
    {
        $convenients = ConvenientHelper::getConvenient();
        
        return view('admin.rooms.add', ['convenients' => $convenients]);
    }

    /**
     * METHOD index - Action Add Host
     *
     * @return void
     */

    public function addAction() 
    {
        $params = $this->request->only('room_name', 'room_description', 'room_gallery', 'room_area', 'room_convenient', 'room_option', 'price_one_night', 'guest_amount', 'room_amount_empty', 'room_status', 'language');
        $params = ArrayHelper::removeArrayNull($params);

        $params['created_by_agent'] = 0;
        $params['updated_by_agent'] = 0;
        $params['host_convenient'] = isset($params['host_convenient']) ? json_encode($params['host_convenient']) : '';
        $params['host_gallery'] = isset($params['host_gallery']) ? json_encode($params['host_gallery']) : '';
        $validator = $this->validator->make($params, 'add_room_fields');
        if ($validator->fails()) {
            return redirect()->back()->withErrors(['error' => $validator->errors()->all()]);     
        }
        
        $add = $this->roomService->insert($params);
        if ($add) {
            //add log

            Message::alertFlash('Bạn đã thêm phòng thành công', 'success');

            $log['action'] = "Thêm mới 1 phòng có id = " . $add;
            $log['content'] = json_encode($params);
            $log['ip'] = $this->request->ip();
            LogsUserService::add($log);


        } else {

            Message::alertFlash('Đã xảy ra lỗi khi thêm phòng, vui lòng liên hệ quản trị viên!', 'danger');

        }

        // TEST CI
        return redirect()->back()->withInput();
        
    }

    /**
     * METHOD index - View Edit Host
     *
     * @return void
     */

    public function edit($id = 0) 
    {
        
    }

     /**
     * METHOD index - Action Edit Host
     *
     * @return void
     */

    public function editAction($id = 0) 
    {

        $params = $this->request->only('host_name', 'host_slug', 'host_description', 'host_policy', 'host_convenient', 'host_gallery', 'host_status', 'host_type', 'host_thumbnail', 'host_address', 'host_lat', 'host_lng', 'province_id', 'district_id', 'ward_id', 'province_name', 'district_name', 'ward_name', 'language');
        $params = ArrayHelper::removeArrayNull($params);
        $params['created_by_agent'] = 0;
        $params['updated_by_agent'] = 0;
        $params['host_convenient'] = isset($params['host_convenient']) ? json_encode($params['host_convenient']) : '';
        $params['host_gallery'] = isset($params['host_gallery']) ? json_encode($params['host_gallery']) : json_encode('');
        $validator = $this->validator->make($params, 'edit_host_fields');
        if ($validator->fails()) {
            return redirect()->back()->withErrors(['error' => $validator->errors()->all()]);     
        }


        $edit = $this->roomService->update($id, $params);
        if (!$edit) {
            Message::alertFlash('Đã xảy ra lỗi khi cập nhật khách sạn, vui lòng liên hệ quản trị viên!', 'danger');
            return redirect()->back()->withInput();
        }
        
        Message::alertFlash('Cập nhật khách sạn thành công !', 'success');
        //add log
        $log['action'] = "Cập nhập Khách sạn thành công có ID = " . $id;
        $log['content'] = json_encode($params);
        $log['ip'] = $this->request->ip();
        LogsUserService::add($log);

        return redirect()->back()->withInput();
    }

    public function deleteMany()
    {
        $params = $this->request->only('ids', 'total');
        if (!isset($params['ids'])) {
            return response()->json(Message::get(26, $lang = '', []), 400);
        }
        $delete = $this->roomService->deleteMany($params['ids']);
        if (!$delete) {
            return response()->json(Message::get(12, $lang = '', []), 400);
        }

        Message::alertFlash("Bạn đã xóa tổng cộng " . $params['total'] . " Khách sạn thành công !!!", 'success');

        //add log
        $log['action'] = "Xóa " . $params['total'] . " User thành công";
        $log['content'] = json_encode($params);
        $log['ip'] = $this->request->ip();
        LogsUserService::add($log);

        $data['success'] = true;
        $data['message'] = "Bạn đã xóa tổng cộng " . $params['total'] . " Khách sạn thành công !!!";
        return response()->json($data);
    }

    public function delete($id)
    {
        
        $delete = $this->roomService->delete($id);
        if($delete) {
            //add log
            $log['action'] = "Xóa Khách sạn thành công có ID = " . $id;
            $log['content'] = json_encode($delete);
            $log['ip'] = $this->request->ip();
            LogsUserService::add($log);

            Message::alertFlash('Bạn đã xóa Khách sạn thành công', 'success');
        } else {
            Message::alertFlash('Bạn đã xóa Khách sạn không thành công', 'danger');
        }

        return redirect("host");
    }
}