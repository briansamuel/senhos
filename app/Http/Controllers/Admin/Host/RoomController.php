<?php

namespace App\Http\Controllers\Admin\Host;

use App\Http\Controllers\Controller;
use App\Services\ValidationService;
use App\Services\HostService;
use App\Services\RoomService;
use App\Services\ProvinceService;
use App\Services\DistrictService;
use App\Services\WardService;
use App\Services\LogsUserService;
use Illuminate\Http\Request;
use App\Helpers\Message;
use App\Helpers\ArrayHelper;
use App\Helpers\ConvenientHelper;

class RoomController extends Controller
{
    //

    function __construct(Request $request, ValidationService $validator, RoomService $roomService)
    {
        $this->request = $request;
        $this->validator = $validator;
        $this->roomService = $roomService;
        session()->start();
        session()->put('RF.subfolder', 'khach-san');
        session()->put('RF.thumbnailWidth', 350);
        session()->put('RF.thumbnailHeight', 350);
    }

    /**
     * METHOD index - View List Hosts
     *
     * @return void
     */

    public function index()
    {
        $hosts = HostService::getAll(['id', 'host_name'], null);
        return view('admin.hosts.index', ['hosts' => $hosts]);
    }

    /**
     * METHOD index - Ajax Get List Host
     *
     * @return void
     */

    public function ajaxGetList()
    {
        $params = $this->request->all();
        if(isset($params['host_id'])) {
            $params['query']['host_id'] = $params['host_id'];
        }
        $result = $this->roomService->getList($params);

        return response()->json($result);
    }

    /**
     * METHOD index - View Add Room
     *
     * @return void
     */

    public function add() 
    {
        $convenients = ConvenientHelper::getConvenientRoom();
        $hosts = HostService::getAll(['id', 'host_name'], null);
        return view('admin.rooms.add', ['convenients' => $convenients, 'hosts' => $hosts]);
    }

    /**
     * METHOD index - View Add Room
     *
     * @return void
     */

    public function addPopup() 
    {
        $convenients = ConvenientHelper::getConvenientRoom();
        // $hosts = HostService::getAll(['id', 'host_name'], null);
        $params = $this->request->only('host_id');
        $host_id = isset($params['host_id']) ? $params['host_id'] : 0;
        return view('admin.rooms.add-popup', ['convenients' => $convenients, 'host_id' => $host_id]);
    }

    /**
     * METHOD index - Action Add Host
     *
     * @return void
     */

    public function addAction() 
    {
        $params = $this->request->only('room_name', 'room_description', 'room_gallery', 'room_area', 'room_convenient', 'room_option', 'price_one_night', 'guest_amount', 'room_amount_empty', 'room_status', 'host_id');
        $params = ArrayHelper::removeArrayNull($params);

        $params['created_by_agent'] = 1;
        $params['updated_by_agent'] = 1;
        $params['room_convenient'] = isset($params['room_convenient']) ? json_encode($params['room_convenient']) : '';
        
        $params['room_gallery'] = isset($params['room_gallery']) ? json_encode($params['room_gallery']) : '';
        $params['room_option'] = isset($params['room_option']) ? json_encode($params['room_option']) : '';
        $host = HostService::findByKey('id', $params['host_id']);
        if(!$host) {
            return redirect()->back()->withInput();
            Message::alertFlash('Nơi lưu trú không tồn tại, vui lòng liên hệ quản trị viên !', 'danger');
        }
        $validator = $this->validator->make($params, 'add_room_fields');
        if ($validator->fails()) {
            return redirect()->back()->withErrors(['error' => $validator->errors()->all()]);     
        }
        //dd($params);
        $add = $this->roomService->insert($params);
        if ($add) {
            //add log

            Message::alertFlash('Bạn đã thêm phòng thành công', 'success');

            $log['action'] = "Thêm mới 1 phòng có id = " . $add;
            $log['content'] = json_encode($params);
            $log['ip'] = $this->request->ip();
            LogsUserService::add($log);


        } else {

            Message::alertFlash('Đã xảy ra lỗi khi thêm phòng, vui lòng liên hệ quản trị viên!', 'danger');

        }

        // TEST CI
        return redirect()->back()->withInput();
        
    }

    /**
     * METHOD index - View Edit Room
     *
     * @return void
     */

    public function edit($id = 0) 
    {
        $room = $this->roomService->findByKey('id', $id, '*');
        if(!$room) {
            return redirect('denied-permission');
        }
        $room->room_gallery = json_decode($room->room_gallery);
        $room->room_option = json_decode($room->room_option);
        $convenients = ConvenientHelper::getConvenientRoom();
        $convenient = json_decode($room->room_convenient);
        $convenient = $this->convenients_array($convenient);
        foreach($convenients as $type => $array) {
            
            foreach($array as $key => $item) {
                if(in_array($key, $convenient)) {
                    $convenients[$type][$key] = ['icon' => $convenients[$type][$key]['icon'], 'text' => $convenients[$type][$key], 'value' => 'checked' ];
                    //dd($convenients[$type][$key]);
                } else {
                    $convenients[$type][$key] = ['icon' => $convenients[$type][$key]['icon'],'text' => $convenients[$type][$key]['text'], 'value' => '' ];
                }
                
            }
            
        }
        //$convenients['service_hotel'][0] = [ 'text' => $convenients['service_hotel'][0], 'value' => 'selected' ];
        //dd($convenients['convenient_room']);
        
        return view('admin.rooms.edit', [ 'room' => $room, 'convenients' => $convenients]);
    }

    /**
     * METHOD index - View Edit Room Popup
     *
     * @return void
     */

    public function editPopup($id = 0) 
    {
        $room = $this->roomService->findByKey('id', $id, '*');
        
        $room->room_gallery = json_decode($room->room_gallery);
        $room->room_option = json_decode($room->room_option);
        $convenients = ConvenientHelper::getConvenientRoom();
        $convenient = json_decode($room->room_convenient);
        $convenient = $this->convenients_array($convenient);
        foreach($convenients as $type => $array) {
            
            foreach($array as $key => $item) {
                if(in_array($key, $convenient)) {
                    $convenients[$type][$key] = ['icon' => $convenients[$type][$key]['icon'], 'text' => $convenients[$type][$key]['text'], 'value' => 'checked' ];
                    //dd($convenients[$type][$key]);
                } else {
                    $convenients[$type][$key] = ['icon' => $convenients[$type][$key]['icon'],'text' => $convenients[$type][$key]['text'], 'value' => '' ];
                }
                
            }
            
        }
        //$convenients['service_hotel'][0] = [ 'text' => $convenients['service_hotel'][0], 'value' => 'selected' ];
       
        
        return view('admin.rooms.edit-popup', [ 'room' => $room, 'convenients' => $convenients]);
    }
     /**
     * METHOD index - Action Edit Host
     *
     * @return void
     */

    public function editAction($id = 0) 
    {

        $params = $this->request->only('room_name', 'room_description', 'room_convenient', 'room_gallery', 'room_area', 'room_convenient', 'room_option', 'price_one_night', 'guest_amount', 'room_amount_empty', 'room_status', 'language');
        
        $params = ArrayHelper::removeArrayNull($params);
        $params['created_by_agent'] = 0;
        $params['updated_by_agent'] = 0;
        $params['room_convenient'] = isset($params['room_convenient']) ? json_encode($params['room_convenient']) : '';
        $params['room_gallery'] = isset($params['room_gallery']) ? json_encode($params['room_gallery']) : json_encode('');
        $params['room_option'] = isset($params['room_option']) ? json_encode($params['room_option']) : '';
        
        $validator = $this->validator->make($params, 'edit_room_fields');
        if ($validator->fails()) {
            return redirect()->back()->withErrors(['error' => $validator->errors()->all()]);     
        }


        $edit = $this->roomService->update($id, $params);
        if (!$edit) {
            Message::alertFlash('Đã xảy ra lỗi khi cập nhật phòng, vui lòng liên hệ quản trị viên!', 'danger');
            return redirect()->back()->withInput();
        }
        
        Message::alertFlash('Cập nhật phòng thành công !', 'success');
        //add log
        $log['action'] = "Cập nhập phòng thành công có ID = " . $id;
        $log['content'] = json_encode($params);
        $log['ip'] = $this->request->ip();
        LogsUserService::add($log);

        return redirect()->back()->withInput();
    }

    public function deleteMany()
    {
        $params = $this->request->only('ids', 'total');
        if (!isset($params['ids'])) {
            return response()->json(Message::get(26, $lang = '', []), 400);
        }
        $delete = $this->roomService->deleteMany($params['ids']);
        if (!$delete) {
            return response()->json(Message::get(12, $lang = '', []), 400);
        }

        Message::alertFlash("Bạn đã xóa tổng cộng " . $params['total'] . " phòng thành công !!!", 'success');

        //add log
        $log['action'] = "Xóa " . $params['total'] . " User thành công";
        $log['content'] = json_encode($params);
        $log['ip'] = $this->request->ip();
        LogsUserService::add($log);

        $data['success'] = true;
        $data['message'] = "Bạn đã xóa tổng cộng " . $params['total'] . " phòng thành công !!!";
        return response()->json($data);
    }

    public function delete($id)
    {
        
        $delete = $this->roomService->delete($id);
        if($delete) {
            //add log
            $log['action'] = "Xóa phòng thành công có ID = " . $id;
            $log['content'] = json_encode($delete);
            $log['ip'] = $this->request->ip();
            LogsUserService::add($log);

            Message::alertFlash('Bạn đã xóa phòng thành công', 'success');
        } else {
            Message::alertFlash('Bạn đã xóa phòng không thành công', 'danger');
        }

        return redirect()->back();
    }

    

    public function convenients_array($convenient_obj = array()) {
        $array_con = array();
        if(is_object($convenient_obj)) { 
            foreach($convenient_obj as $array) {
                $array = (array) $array;
                if(is_array($array)) {
                    foreach($array as $key => $item) {
                        array_push($array_con, $key);
                    }
                }
                
            }
        }
        return $array_con;
    }
}