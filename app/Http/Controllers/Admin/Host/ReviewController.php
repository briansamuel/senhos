<?php 

namespace App\Http\Controllers\Admin\Host;

use App\Helpers\ArrayHelper;
use App\Helpers\Message;
use App\Http\Controllers\Controller;

use App\Services\ReviewService;
use App\Services\LogsUserService;
use App\Services\ValidationService;
use Illuminate\Http\Request;



class ReviewController extends Controller
{
    protected $request;
    protected $reviewService;
    protected $validator;

    public function __construct(Request $request, ReviewService $reviewService, ValidationService $validator)
    {
        $this->request = $request;
        $this->validator = $validator;
        $this->reviewService = $reviewService;
    }

    public function index()
    {

        return view('admin.reviews.index');
    }

    /**
     * METHOD index - View List News
     *
     * @return void
     */

    public function ajaxGetList()
    {
        $params = $this->request->all();
        $params['columns'] = ['*'];
        $result = $this->reviewService->getList($params);

        return response()->json($result);
    }

    public function deleteMany()
    {
        $params = $this->request->only('ids', 'total');
        if (!isset($params['ids'])) {
            return response()->json(Message::get(26, $lang = '', []), 400);
        }
        $delete = $this->reviewService->deleteMany($params['ids']);
        if (!$delete) {
            return response()->json(Message::get(12, $lang = '', []), 400);
        }

        Message::alertFlash("Bạn đã xóa tổng cộng " . $params['total'] . " đánh giá thành công !!!", 'success');

        //add log
        $log['action'] = "Xóa " . $params['total'] . " đánh giá thành công";
        $log['content'] = json_encode($params);
        $log['ip'] = $this->request->ip();
        LogsUserService::add($log);

        $data['success'] = true;
        $data['message'] = "Bạn đã xóa tổng cộng " . $params['total'] . " đánh giá thành công !!!";
        return response()->json($data);
    }

    public function delete($id)
    {
        $detail = $this->reviewService->detail($id);
        $delete = $this->reviewService->delete($id);
        if($delete) {
            //add log
            $log['action'] = "Xóa đánh giá thành công có ID = " . $id;
            $log['content'] = json_encode($detail);
            $log['ip'] = $this->request->ip();
            LogsUserService::add($log);

            Message::alertFlash('Bạn đã xóa đánh giá thành công', 'success');
        } else {
            Message::alertFlash('Bạn đã xóa đánh giá không thành công', 'danger');
        }

        return redirect("comment");
    }

    public function edit($id)
    {

        $data['reviewInfo'] = $this->reviewService->detail($id);
        

        return view('admin.reviews.edit', $data);

       
    }

    public function editAction($id)
    {
        $params = $this->request->only(['review_status']);
        $params = ArrayHelper::removeArrayNull($params);
        $validator = $this->validator->make($params, 'edit_review_fields');
        if ($validator->fails()) {
            return response()->json(Message::get(1, $lang = '', $validator->errors()->all(), 400));
        }

        $edit = $this->reviewService->edit($id, $params);
        if (!$edit) {
            return response()->json(Message::get(13, $lang = '', []), 400);
        }

        //add log
        $log['action'] = "Cập nhập đánh giá thành công có ID = " . $id;
        $log['content'] = json_encode($params);
        $log['ip'] = $this->request->ip();
        LogsUserService::add($log);

        $data['success'] = true;
        $data['message'] = "Cập nhập đánh giá thành công !!!";
        return response()->json($data);
    }

    public function editManyAction()
    {
        $params = $this->request->only(['status', 'ids', 'total']);
        $params = ArrayHelper::removeArrayNull($params);
        if (!isset($params['ids'])) {
            return response()->json(Message::get(26, $lang = '', []), 400);
        }
        $update = $this->reviewService->updateMany($params['ids'], ['review_status' => $params['status']]);
        if (!$update) {
            return response()->json(Message::get(12, $lang = '', []), 400);
        }

        Message::alertFlash("Bạn đã cập nhập tổng cộng " . $params['total'] . " đánh giá thành công !!!", 'success');

        //add log
        $log['action'] = "Cập nhập nhiều đánh giá thành công";
        $log['content'] = json_encode($params);
        $log['ip'] = $this->request->ip();
        LogsUserService::add($log);

        $data['success'] = true;
        $data['message'] = "Bạn đã cập nhập tổng cộng " . $params['total'] . " đánh giá thành công !!!";
        return response()->json($data);
    }

}