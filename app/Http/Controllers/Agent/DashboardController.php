<?php

namespace App\Http\Controllers\Agent;

use App\Services\DashboardService;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    

    public function __construct()
    {

    }

    /** 
    * ======================
    * Method:: INDEX
    * ======================
    */

    public function index()
    {
        // take 5 new user
        $data['listGuest'] = DashboardService::takeNewGuest(5);
        $data['listContact'] = DashboardService::takeNewContact(5);
        $data['listNews'] = DashboardService::takeNewNews(7);

        $data['totalGuest'] = DashboardService::totalGuest();
        $data['totalContact'] = DashboardService::totalContact();
        $data['totalNews'] = DashboardService::totalNews();
        $data['totalService'] = DashboardService::totalService();

        return view('agent.dash-board', $data);
    }
}
