<?php

namespace App\Http\Controllers\Agent;

use App\Helpers\Message;
use App\Http\Controllers\Controller;
use App\Models\AgentModel;
use App\Models\PasswordReset;
use App\Services\AgentService;
use App\Services\Auth\AgentAuthService;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    protected $request;
    protected $agentService;

    function __construct(Request $request, AgentService $agentService)
    {
        $this->request = $request;
        $this->agentService = $agentService;
    }

    /**
     * ======================
     * Method:: login
     * ======================
     */

    public function login()
    {
        //check login
        $checkLogin = AgentAuthService::checkLogin();
        if ($checkLogin) {
            return redirect('/welcome');
        }

        return view('agent.pages.login');
    }

    public function loginAction(Request $request)
    {
        $loginAdmin = AgentAuthService::loginAdmin($request);

        return $loginAdmin;
    }

    /*
    * function logout
    */
    public function logout()
    {
        AgentAuthService::logout();

        return redirect('/login');
    }

    public function resetPassword(Request $request)
    {
        $token = $request->input('token');
        $passwordReset = PasswordReset::where('token', $token)->first();
        if (!$passwordReset) {
            return view('agent.pages.error404');
        }
        return view('agent.pages.reset_password', ['token' => $token]);
    }

    public function activeAgent(Request $request)
    {
        $active_code = $request->input('active_code');
        if ($active_code == '') {
            return view('agent.pages.error404');
        }
        $agentInfo = AgentModel::where('active_code', $active_code)->first();
        if (!$agentInfo || $agentInfo->status !== 'inactive') {
            return view('agent.pages.error404');
        }

        $active = $this->agentService->active($agentInfo->id);
        if ($active) {
            Message::alertFlash('Bạn đã kích hoạt tài khoản thành công, vui lòng đăng nhập vào hệ thống!', 'success');
        } else {
            Message::alertFlash('Bạn đã kích hoạt tài khoản không thành công!', 'danger');
        }


        return redirect()->route('agent-login');
    }

}
