<?php

namespace App\Providers;

use App\Services\BrandService;
use App\Services\SettingService;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use Harimayco\Menu\Facades\Menu;
use App\Helpers\ArrayHelper;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $brands = BrandService::getForFrontEnd();
        $brands_partner = BrandService::getForFrontEndPartner();
        View::share('arrayLang', ArrayHelper::arrayLang());
        View::share('brands', $brands);
        View::share('brands_partner', $brands_partner);
        View::share('setting', new SettingService());
        View::share( 'menus', Menu::getByName('Main Menu'));
        View::share( 'menus_footer', Menu::getByName('Footer Menu'));
        Schema::defaultStringLength(191);
    }
}
