<?php

namespace App\Helpers;
use File;

class Common
{
    public static function saveFileData($path, $data, $json = true)
    {
        if ($json) {
            $data = json_encode_prettify($data);
        }
        if (!File::isDirectory(dirname($path))) {
            File::makeDirectory(dirname($path), 493, true);
        }
        File::put($path, $data);

        return true;
    }

    public static function getFileData($file, $convert_to_array = true)
    {
        $file = File::get($file);
        if (!empty($file)) {
            if ($convert_to_array) {
                return json_decode($file, true);
            } else {
                return $file;
            }
        }
        if (!$convert_to_array) {
            return null;
        }
        return [];
    }

    
}
