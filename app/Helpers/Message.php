<?php

namespace App\Helpers;

use Session;

class Message
{
    /*
     * function get notice with message
     */
    public static function get($error_code = '16', $lang = '', $errors = array())
    {
//        $lang =  $lang ? $lang : (app('translator')->getLocale());
        $lang =  'vi';
        $messages = Message::getMessage($error_code);
        return Message::getArray($error_code, $messages[$lang], $errors);
    }

    /*
     * function get message by error code
     */
    public static function getMessage($error_code)
    {
        $errors = [
            1 => [
                'vi' => 'Thông tin yêu cầu thiếu hoặc không hợp lệ.',
                'en' => 'Parameters are Missing or Invalid.'
            ],
            10 => [
                'vi'    => 'Insert không thành công',
                'en'    => 'Insert Unsuccessful'
            ],

            11 => [
                'vi'    => 'Update không thành công',
                'en'    => 'Update Unsuccessful'
            ],

            12 => [
                'vi'    => 'Xóa không thành công',
                'en'    => 'Delete Unsuccessful'
            ],

            13 => [
                'vi'    => 'Cập nhập không thành công',
                'en'    => 'Upload Unsuccessful'
            ],

            20 => [
                'vi' => 'Mật khẩu không hợp lệ',
                'en' => 'Password not valid'
            ],

            21 => [
                'vi' => 'Xác nhận mật khẩu không khớp',
                'en' => 'Confirm password does not match'
            ],

            22 => [
                'vi' => 'Mật khẩu không đúng',
                'en' => 'Password is valid'
            ],

            30 => [
                'vi' => 'Email đã tồn tại trong hệ thống',
                'en' => 'Email was exist'
            ],

            31 => [
                'vi' => 'User đã tồn tại trong hệ thống',
                'en' => 'User was exist'
            ],

            32 => [
                'vi' => 'Email này đã tồn tại trong hệ thống',
                'en' => 'Email was exist'
            ],

            33 => [
                'vi' => 'Email này đã đăng ký nhận ưu đãi trước đó rồi',
                'en' => 'Email này đã đăng ký nhận ưu đãi trước đó rồi'
            ],

        ];

        return $errors[$error_code];
    }


    /*
     * function get error object
     */
    public static function getArray($error_code, $message, $errors)
    {
        return [
            'error' => [
                'code' => $error_code,
                'message' => $message,
                'errors' => $errors
            ]
        ];
    }

    public static function alertFlash($message, $flash)
    {
        Session::flash('message', $message);
        Session::flash('alert-class', $flash);
    }

}
