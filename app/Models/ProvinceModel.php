<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ProvinceModel
{
    //
    protected static $table = 'province';

    public static function getMany($columns = ['*'], $filter)
    {
        $filter['perpage']  = isset($filter['perpage']) ? $filter['perpage'] : 20;
        $filter['page']  = isset($filter['page']) ? $filter['page'] : 1;
        $offset = ($filter['page'] - 1) * $filter['perpage'];
        $query = DB::table(self::$table)->select($columns)->skip($offset)->take($filter['perpage']); 

        if(isset($filter['_name']) && $filter['_name'] != ""){
            $query->whereRaw('LOWER(_name) like BINARY LOWER("%'.$filter['_name'].'%")');    
        }



        return $query->get();
    }

    public static function totalRows($filter) {

        $query = DB::table(self::$table);

        if(isset($filter['_name']) && $filter['_name'] != ""){
            $query->where('_name', 'like', "%".$filter['_name']."%");
        }
        
        return $query->count();

    }

    public static function findByKey($key, $value, $columns = ['*'], $with = [])
    {
        $data = DB::table(self::$table)->select($columns)->where($key, $value)->first();
        return $data ? $data : [];
    }
    public static function insert($params)
    {
        return DB::table(self::$table)->insertGetId($params);

    }

    public static function update($id, $params)
    {
        return DB::table(self::$table)->where('id', $id)->update($params);

    }

    public static function updateManyPost($ids, $data)
    {
        return DB::table(self::$table)->whereIn('id', $ids)->update($data);
    }

    public static function delete($id)
    {
        return DB::table(self::$table)->where('id', $id)->delete();

    }

    public static function deleteManyPost($ids)
    {
        return DB::table(self::$table)->whereIn('id', $ids)->delete();
    }
}
