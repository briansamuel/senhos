<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class WardModel
{
    //
    protected static $table = 'ward';

    public static function getMany($columns = ['*'], $filter)
    {

        $query = DB::table(self::$table)->select($columns);

        if(isset($filter['_name']) && $filter['_name'] != ""){
            $query->where('_name', 'like', "%".$filter['_name']."%");
        }

        if(isset($filter['_district_id']) && $filter['_district_id'] != ""){
            $query->where('_district_id', '=', $filter['_district_id']);
        }

        return $query->get();
    }

    public static function totalRows($filter) {

        $query = DB::table(self::$table);

        if(isset($filter['_name']) && $filter['_name'] != ""){
            $query->where('_name', 'like', "%".$filter['_name']."%");
        }
        
        if(isset($filter['_district_id']) && $filter['_district_id'] != ""){
            $query->where('_district_id', '=', $filter['_district_id']);
        }
        
        return $query->count();

    }

    public static function findByKey($key, $value, $columns = ['*'], $with = [])
    {
        $data = DB::table(self::$table)->select($columns)->where($key, $value)->first();
        return $data ? $data : [];
    }
    public static function insert($params)
    {
        return DB::table(self::$table)->insertGetId($params);

    }

    public static function update($id, $params)
    {
        return DB::table(self::$table)->where('id', $id)->update($params);

    }

    public static function updateManyPost($ids, $data)
    {
        return DB::table(self::$table)->whereIn('id', $ids)->update($data);
    }

    public static function delete($id)
    {
        return DB::table(self::$table)->where('id', $id)->delete();

    }

    public static function deleteManyPost($ids)
    {
        return DB::table(self::$table)->whereIn('id', $ids)->delete();
    }
}
