<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;

class TopDealModel
{

    protected static $table = 'top_deals';

    public static function getAll()
    {
        $result = DB::table(self::$table)->get();
        
        return $result ? $result : [];
    }

    public static function getMany($pagination, $sort, $filter)
    {
        $pagination['perpage']  = isset($pagination['perpage']) ? $pagination['perpage'] : 20;
        $offset = ($pagination['page'] - 1) * $pagination['perpage'];
        $query = DB::table(self::$table)->skip($offset)->take($pagination['perpage']);

        if(isset($filter['language']) && $filter['language'] != ""){
            $query->where('language', $filter['language']);
        }

        if(isset($sort['field']) && $sort['field'] === "created_at"){
            $query->orderBy('created_at', $sort['sort']);
        }
        return $query->get();
    }

    public static function getForFrontEnd($limit = 2)
    {
        return DB::table(self::$table)
                    ->where('start_time', '<=', time())
                    ->where('end_time', '>=', time())
                    ->limit($limit)->get();
    }

    public static function totalRows() {
        $result = DB::table(self::$table)->count();
        return $result;
    }

    public static function findByKey($key, $value, $columns = ['*'])
    {
        $result = DB::table(self::$table)->select($columns)->where($key, $value)->first();
        return $result ? $result : [];
    }

    public static function findById($id, $columns = ['*'])
    {
        $result = DB::table(self::$table)->select($columns)->where('id', $id)->first();
        return $result ? $result : [];
    }

    public static function edit($id, $data)
    {
        return DB::table(self::$table)->where('id', $id)->update($data);
    }

    public static function add($data)
    {
        return DB::table(self::$table)->insert($data);
    }

    public static function delete($ids)
    {
        return DB::table(self::$table)->where('id', $ids)->delete();
    }

}
