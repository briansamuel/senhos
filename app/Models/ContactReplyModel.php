<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;

class ContactReplyModel
{

    protected static $table = 'contact_replies';

    public static function getAll()
    {
        $result = DB::table(self::$table)::get();
        return $result ? $result : [];
    }

    public static function getByKey($key, $value, $columns = ['*'])
    {
        $result = DB::table(self::$table)->select($columns)->where($key, $value)->get();
        return $result ? $result : [];
    }


    public static function findByKey($key, $value, $columns = ['*'])
    {
        $result = DB::table(self::$table)->select($columns)->where($key, $value)->first();
        return $result ? $result : [];
    }

    public static function findById($id, $columns = ['*'])
    {
        $result = DB::table(self::$table)->select($columns)->where('id', $id)->first();
        return $result ? $result : [];
    }

    public static function update($id, $data)
    {
        return DB::table(self::$table)->where('id', $id)->update($data);
    }

    public static function deleteMany($ids)
    {
        return DB::table(self::$table)->whereIn('id', $ids)->delete();
    }

    public static function updateMany($ids, $data)
    {
        return DB::table(self::$table)->whereIn('id', $ids)->update($data);
    }

    public static function delete($id)
    {
        return DB::table(self::$table)->where('id', $id)->delete();
    }

    public static function add($data)
    {
        return DB::table(self::$table)->insertGetId($data);
    }
}
