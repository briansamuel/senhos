<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class GuestModel extends Authenticatable
{
    use Notifiable;

    protected $table = 'guests';

    public static function getAll()
    {
        $result = GuestModel::get();
        return $result ? $result : [];
    }

    public static function getMany($pagination, $sort, $filter)
    {
        $pagination['perpage'] = isset($pagination['perpage']) ? $pagination['perpage'] : 20;
        $offset = ($pagination['page'] - 1) * $pagination['perpage'];
        $query = GuestModel::skip($offset)->take($pagination['perpage']);
        if (isset($filter['status']) && $filter['status'] != "") {
            $query->where('status', '=', $filter['status']);
        }
        if (isset($filter['email']) && $filter['email'] != "") {
            $query->where('email', 'like', "%" . $filter['email'] . "%");
        }
        if (isset($filter['username']) && $filter['username'] != "") {
            $query->where('username', 'like', "%" . $filter['username'] . "%");
        }
        if (isset($filter['full_name']) && $filter['full_name'] != "") {
            $query->where('full_name', 'like', "%" . $filter['full_name'] . "%");
        }
        if (isset($filter['created_at']) && $filter['created_at'] != "") {
            $time_filter = explode(" - ", $filter['created_at']);
            $start_time = date("Y-m-d 00:00:00", strtotime($time_filter[0]));
            $end_time = date("Y-m-d 23:59:59", strtotime($time_filter[1]));

            $query->where('created_at', '>=', $start_time);
            $query->where('created_at', '<', $end_time);
        }
        if (isset($sort['field']) && $sort['field'] === "created_at") {
            $query->orderBy('created_at', $sort['sort']);
        }
        return $query->get();
    }

    public static function totalRows()
    {
        $result = GuestModel::count();
        return $result;
    }

    public static function findByKey($key, $value, $columns = ['*'])
    {
        $result = GuestModel::select($columns)->where($key, $value)->first();
        return $result ? $result : [];
    }

    public static function findById($id, $columns = ['*'])
    {
        $result = GuestModel::select($columns)->where('id', $id)->first();
        return $result ? $result : [];
    }

    public static function updateGuest($id, $data)
    {
        return GuestModel::where('id', $id)->update($data);
    }

    public static function deleteMany($ids)
    {
        return GuestModel::whereIn('id', $ids)->delete();
    }

    public static function updateMany($ids, $data)
    {
        return GuestModel::whereIn('id', $ids)->update($data);
    }

    public static function deleteGuest($id)
    {
        return GuestModel::where('id', $id)->delete();
    }

    public static function checkEmailExist($email, $id = '')
    {
        if ($id) {
            return GuestModel::where('email', $email)->where('id', '!=', $id)->exists();
        } else {
            return GuestModel::where('email', $email)->exists();
        }
    }

    public static function checkProviderExist($provider_id)
    {
        return GuestModel::where('provider_id', $provider_id)->exists();
    }

    public static function getByProviderId($provider_id)
    {
        return GuestModel::where('provider_id', $provider_id)->first();
    }

    public static function takeNew($quantity)
    {
        return GuestModel::orderBy('id', 'DESC')->limit($quantity)->get();
    }
}
