<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;

class ReviewModel
{

    protected static $table = 'reviews';

    public static function getAll($columns, $filter)
    {
        $query = DB::table(self::$table)->select($columns);
        if(isset($filter['host_id']) && $filter['host_id'] != ""){
            $query->where('host_id', '=', $filter['host_id']);
        }

        if(isset($filter['language']) && $filter['language'] != ""){
            $query->where('language', $filter['language']);
        }
        
        $result = $query->get();
        return $result ? $result : [];
    }


    public static function getMany($columns = ['*'], $pagination, $sort, $filter)
    {
        $pagination['perpage']  = isset($pagination['perpage']) ? $pagination['perpage'] : 20;
        $offset = ($pagination['page'] - 1) * $pagination['perpage'];
        $query = DB::table(self::$table)->select($columns)->skip($offset)->take($pagination['perpage']);
        if(isset($filter['review_status']) && $filter['review_status'] != ""){
            $query->where('review_status', '=', $filter['review_status']);
        }
        if(isset($filter['language']) && $filter['language'] != ""){
            $query->where('language', '=', $filter['language']);
        }

        if(isset($filter['host_id']) && $filter['host_id'] != ""){
            $query->where('host_id', $filter['host_id']);
        }

        

        if (isset($filter['created_at']) && $filter['created_at'] != "") {
            $time_filter = explode(" - ", $filter['created_at']);
            $start_time = date("Y-m-d 00:00:00", strtotime($time_filter[0]));
            $end_time = date("Y-m-d 23:59:59", strtotime($time_filter[1]));

            $query->where('created_at', '>=', $start_time);
            $query->where('created_at', '<', $end_time);
        }
        if(isset($sort['field']) && $sort['field'] === "created_at"){
            $query->orderBy('created_at', $sort['sort']);
        }
        return $query->get();
    }

    public static function totalRows() {
        $result = DB::table(self::$table)->count();
        return $result;
    }

    public static function findByKey($key, $value, $columns = ['*'])
    {
        $result = DB::table(self::$table)->select($columns)->where($key, $value)->first();
        return $result ? $result : [];
    }

    public static function findById($id, $columns = ['*'])
    {
        $result = DB::table(self::$table)->select($columns)->where('id', $id)->first();
        return $result ? $result : [];
    }

    public static function insert($params)
    {
        return DB::table(self::$table)->insertGetId($params);

    }

    public static function update($id, $params)
    {
        return DB::table(self::$table)->where('id', $id)->update($params);

    }

    public static function deleteMany($ids)
    {
        return DB::table(self::$table)->whereIn('id', $ids)->delete();
    }

    public static function updateMany($ids, $data)
    {
        return DB::table(self::$table)->whereIn('id', $ids)->update($data);
    }

    public static function delete($id)
    {
        return DB::table(self::$table)->where('id', $id)->delete();
    }

    public static function add($data)
    {
        return DB::table(self::$table)->insert($data);
    }

    public static function takeNew($quantity, $filter)
    {
        $query = DB::table(self::$table);

        if(isset($filter['status']) && $filter['status'] != ""){
            $query->where('review_status', '=', $filter['status']);
        }
        if(isset($filter['review_title']) && $filter['review_title'] != ""){
            $query->where('review_title', 'like', "%".$filter['review_title']."%");
        }
        
        if(isset($filter['host_id']) && $filter['host_id'] != ""){
            $query->where('host_id', '=', $filter['host_id']);
        }
        
        if(isset($filter['exclude']) && $filter['exclude'] != ""){
            $query->where('id', '!=', $filter['exclude']);
        }

        if(isset($filter['language']) && $filter['language'] != ""){
            $query->where('language', $filter['language']);
        }
        
        return $query->latest('created_at')->paginate($quantity);
    }
}
