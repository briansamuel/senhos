<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;
class HostModel
{
    //
    protected static $table = 'hosts';

    public static function getMany($pagination, $sort, $filter)
    {
        $pagination['perpage']  = isset($pagination['perpage']) ? $pagination['perpage'] : 20;
        $offset = ($pagination['page'] - 1) * $pagination['perpage'];
        $query = DB::table(self::$table)->skip($offset)->take($pagination['perpage']);
        if(isset($filter['status']) && $filter['status'] != ""){
            $query->where('host_status', '=', $filter['status']);
        }
        if(isset($filter['host_name']) && $filter['host_name'] != ""){
            $query->where('host_name', 'like BINARY', "%".$filter['host_name']."%");
        }

        if(isset($filter['host_type']) && $filter['host_type'] != ""){
            $query->where('host_type', '=', $filter['host_type']);
        }

        if(isset($filter['province_name']) && $filter['province_name'] != ""){
            $query->where('province_name', '=', $filter['province_name']);
        }

        if(isset($filter['province_name']) && $filter['province_name'] != ""){
            $query->where('province_name', '=', $filter['province_name']);
        }

        if(isset($filter['province_id']) && $filter['province_id'] != ""){
            $query->where('province_id', '=', $filter['province_id']);
        }

        if(isset($filter['district_id']) && $filter['district_id'] != ""){
            $query->where('district_id', '=', $filter['district_id']);
        }
        
        if(isset($filter['ward_id']) && $filter['ward_id'] != ""){
            $query->where('ward_id', '=', $filter['ward_id']);
        }

        if(!empty($filter['type'])){
            $query->whereIn('host_type', $filter['type']);
        }

        if(isset($filter['language']) && $filter['language'] != ""){
            $query->where('language', $filter['language']);
        }

        if (isset($filter['created_at']) && $filter['created_at'] != "") {
            $time_filter = explode(" - ", $filter['created_at']);
            $start_time = date("Y-m-d 00:00:00", strtotime($time_filter[0]));
            $end_time = date("Y-m-d 23:59:59", strtotime($time_filter[1]));

            $query->where('created_at', '>=', $start_time);
            $query->where('created_at', '<', $end_time);
        }
        if(isset($sort['field']) && $sort['field'] != ""){
            $query->orderBy($sort['field'], $sort['sort']);
        }

        return $query->get();
    }

    public static function filterHost($columns = ['*'], $pagination, $sort, $filter) {
        
        
        $pagination['perpage']  = isset($pagination['perpage']) ? $pagination['perpage'] : 20;
        $offset = ($pagination['page'] - 1) * $pagination['perpage'];
        $query = DB::table(self::$table)->select($columns)->skip($offset)->take($pagination['perpage']);
        if(isset($filter['status']) && $filter['status'] != ""){
            $query->where('host_status', '=', $filter['status']);
        }
        if(isset($filter['host_name']) && $filter['host_name'] != ""){
            $query->where('host_name', 'like BINARY', "%".$filter['host_name']."%");
        }

        if(isset($filter['host_id']) && $filter['host_id'] != ""){
            $query->where('id', '=', $filter['host_id']);
        }

        if(isset($filter['host_type']) && $filter['host_type'] != ""){
            $query->where('host_type', '=', $filter['host_type']);
        }

        if(isset($filter['province_name']) && $filter['province_name'] != ""){
            $query->where('province_name', '=', $filter['province_name']);
        }

        if(isset($filter['language']) && $filter['language'] != ""){
            $query->where('language', $filter['language']);
        }

        $query->where(function ($q) use ($filter) {
            

            if(!empty($filter['province_id'])){
                $q->whereIn('province_id',  $filter['province_id'], 'or');
            }

            if(!empty($filter['district_id'])){
                $q->whereIn('district_id',  $filter['district_id'], 'or');
            }
            
            if(!empty($filter['ward_id'])){
                $q->whereIn('ward_id', $filter['ward_id'], 'or');
            }
        });
        if(!empty($filter['type'])){
            $query->whereIn('host_type', $filter['type']);
        }

        if(!empty($filter['stars'])){
            $query->whereIn('host_star', $filter['stars']);
        }

        if(!empty($filter['range'])){
            $query->where('lowest_room_rates', '>=' ,$filter['range'][0]);
            $query->where('lowest_room_rates', '<=' ,$filter['range'][1]);
        }

        if (isset($filter['created_at']) && $filter['created_at'] != "") {
            $time_filter = explode(" - ", $filter['created_at']);
            $start_time = date("Y-m-d 00:00:00", strtotime($time_filter[0]));
            $end_time = date("Y-m-d 23:59:59", strtotime($time_filter[1]));

            $query->where('created_at', '>=', $start_time);
            $query->where('created_at', '<', $end_time);
        }
        if(isset($sort['field']) && $sort['field'] != ""){
            $query->orderBy($sort['field'], $sort['sort']);
        }

        return $query->get();
    }
    public static function totalRows($filter) {

        $query = DB::table(self::$table);
        if(isset($filter['status']) && $filter['status'] != ""){
            $query->where('host_status', '=', $filter['status']);
        }
        if(isset($filter['host_name']) && $filter['host_name'] != ""){
            $query->where('host_name', 'like BINARY', "%".$filter['host_name']."%");
        }

        if(isset($filter['host_id']) && $filter['host_id'] != ""){
            $query->where('id', '=', $filter['host_id']);
        }

        if(isset($filter['host_type']) && $filter['host_type'] != ""){
            $query->where('host_type', '=', $filter['host_type']);
        }

        if(isset($filter['province_id']) && $filter['province_id'] != ""){
            $query->where('province_id', '=', $filter['province_id']);
        }

        if(isset($filter['district_id']) && $filter['district_id'] != ""){
            $query->where('district_id', '=', $filter['district_id']);
        }

        if(isset($filter['language']) && $filter['language'] != ""){
            $query->where('language', $filter['language']);
        }
        
        if (isset($filter['created_at']) && $filter['created_at'] != "") {
            $time_filter = explode(" - ", $filter['created_at']);
            $start_time = date("Y-m-d 00:00:00", strtotime($time_filter[0]));
            $end_time = date("Y-m-d 23:59:59", strtotime($time_filter[1]));

            $query->where('created_at', '>=', $start_time);
            $query->where('created_at', '<', $end_time);
        }
        return $query->count();

    }

    public static function getAll($columns = ['*'], $filter)
    {
        $query = DB::table(self::$table)->select($columns);
        if(isset($filter['status']) && $filter['status'] != ""){
            $query->where('host_status', '=', $filter['status']);
        }
        if(isset($filter['host_name']) && $filter['host_name'] != ""){
            $query->whereRaw('LOWER(host_name) like BINARY LOWER("%'.$filter['host_name'].'%")');    
        }

        if(isset($filter['host_type']) && $filter['host_type'] != ""){
            $query->where('host_type', '=', $filter['host_type']);
        }

        if(isset($filter['province_name']) && $filter['province_name'] != ""){
            $query->where('province_name', 'like', "%".$filter['province_name']."%");
        }

        if(isset($filter['district_name']) && $filter['district_name'] != ""){
            $query->where('district_name', 'like', "%".$filter['district_name']."%");
        }

        if(isset($filter['district_name']) && $filter['district_name'] != ""){
            $query->where('district_name', 'like', "%".$filter['district_name']."%");
        }
        
        if (isset($filter['created_at']) && $filter['created_at'] != "") {
            $time_filter = explode(" - ", $filter['created_at']);
            $start_time = date("Y-m-d 00:00:00", strtotime($time_filter[0]));
            $end_time = date("Y-m-d 23:59:59", strtotime($time_filter[1]));

            $query->where('created_at', '>=', $start_time);
            $query->where('created_at', '<', $end_time);
        }
        

        return $query->get();
    }

    public static function findByKey($key, $value, $columns = ['*'], $with = [])
    {
        $data = DB::table(self::$table)->select($columns)->where($key, $value)->first();
        return $data ? $data : [];
    }
    public static function insert($params)
    {
        return DB::table(self::$table)->insertGetId($params);

    }

    public static function update($id, $params)
    {
        return DB::table(self::$table)->where('id', $id)->update($params);

    }

    public static function updateManyHost($ids, $data)
    {
        return DB::table(self::$table)->whereIn('id', $ids)->update($data);
    }

    public static function delete($id)
    {
        return DB::table(self::$table)->where('id', $id)->delete();

    }

    public static function deleteManyHost($ids)
    {
        return DB::table(self::$table)->whereIn('id', $ids)->delete();
    }

    public static function getShopNear($lat, $long, $radius, $limit)
    {
        return DB::table(self::$table)->selectRaw('id, host_name, host_slug, host_thumbnail, host_address, host_lat, host_lng, host_star, created_at, ( 6371 * acos( cos( radians(' . $lat . ') ) * cos( radians( host_lat ) ) * cos( radians( host_lng ) - radians(' . $long . ') ) + sin( radians(' . $lat . ') ) * sin( radians( host_lat ) ) ) ) AS distance')->havingRaw("distance <= ?", [$radius])
            ->where('host_status', 'publish')->orderBy('distance', 'asc')->take($limit)->get();


    }

    public static function countGroupBy($column = 'id') {
        return DB::table(self::$table)
                 ->select($column, DB::raw('count(*) as total'))
                 ->groupBy($column)
                 ->get();
    }
}
