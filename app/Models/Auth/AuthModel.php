<?php

namespace App\Models\Auth;

use DB;

class AuthModel
{

    /*
     * function hash password
     */
    public static function hash($password)
    {
        $options = array('cost' => 8);
        return password_hash($password, PASSWORD_BCRYPT, $options);
    }

    /*
    * funciton verify password
     * truyền thẳng vào chuỗi user nhập  và chuỗi mã hóa lưu ở db
    */
    public static function verify($plainPass, $hashPass)
    {

        return password_verify($plainPass, $hashPass);
    }
}
