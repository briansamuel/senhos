<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GuestPasswordReset extends Model
{

    protected $fillable = [
        'email',
        'token'
    ];
}
