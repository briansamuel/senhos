<?php

namespace App\Models;
use Illuminate\Support\Facades\DB;
class BookingModel
{
    //
    //
    protected static $table = 'bookings';

    public static function getMany($pagination, $sort, $filter)
    {
        $pagination['perpage']  = isset($pagination['perpage']) ? $pagination['perpage'] : 20;
        $offset = ($pagination['page'] - 1) * $pagination['perpage'];
        $query = DB::table(self::$table)->skip($offset)->take($pagination['perpage']);
        if(isset($filter['status']) && $filter['status'] != ""){
            $query->where('booking_status', '=', $filter['status']);
        }

        if(isset($filter['host_id']) && $filter['host_id'] != ""){
            $query->where('host_id', '=', $filter['host_id']);
        }

        if(isset($filter['room_id']) && $filter['room_id'] != ""){
            $query->where('room_id', '=', $filter['room_id']);
        }

        if(isset($filter['checkin_date']) && $filter['checkin_date'] != ""){
            $time_filter = explode(" - ", $filter['checkin_date']);
            $start_time = date("Y-m-d 00:00:00", strtotime($time_filter[0]));
            $end_time = date("Y-m-d 23:59:59", strtotime($time_filter[1]));

            $query->where('checkin_date', '>=', $start_time);
            $query->where('checkin_date', '<', $end_time);
        }

        if(isset($filter['checkout_date']) && $filter['checkout_date'] != ""){
            $time_filter = explode(" - ", $filter['checkout_date']);
            $start_time = date("Y-m-d 00:00:00", strtotime($time_filter[0]));
            $end_time = date("Y-m-d 23:59:59", strtotime($time_filter[1]));

            $query->where('checkout_date', '>=', $start_time);
            $query->where('checkout_date', '<', $end_time);
        }

        if(isset($filter['guest_name']) && $filter['guest_name'] != ""){
            $query->where('guest_info->guest_name', 'like', "%".$filter['guest_name']."%");
        }

        if(isset($filter['guest_phone']) && $filter['guest_phone'] != ""){
            $query->where('guest_phone->guest_name', 'like', "%".$filter['guest_phone']."%");
        }
        

        
        if (isset($filter['created_at']) && $filter['created_at'] != "") {
            $time_filter = explode(" - ", $filter['created_at']);
            $start_time = date("Y-m-d 00:00:00", strtotime($time_filter[0]));
            $end_time = date("Y-m-d 23:59:59", strtotime($time_filter[1]));

            $query->where('created_at', '>=', $start_time);
            $query->where('created_at', '<', $end_time);
        }
        if(isset($sort['field']) && $sort['field'] != ""){
            $query->orderBy($sort['field'], $sort['sort']);
        }

        return $query->get();
    }

    public static function totalRows($filter) {

        $query = DB::table(self::$table);

        if(isset($filter['status']) && $filter['status'] != ""){
            $query->where('booking_status', '=', $filter['status']);
        }

        if(isset($filter['host_id']) && $filter['host_id'] != ""){
            $query->where('host_id', '=', $filter['host_id']);
        }

        if(isset($filter['Booking_id']) && $filter['Booking_id'] != ""){
            $query->where('Booking_id', '=', $filter['Booking_id']);
        }

        if(isset($filter['checkin_date']) && $filter['checkin_date'] != ""){
            $time_filter = explode(" - ", $filter['checkin_date']);
            $start_time = date("Y-m-d 00:00:00", strtotime($time_filter[0]));
            $end_time = date("Y-m-d 23:59:59", strtotime($time_filter[1]));

            $query->where('checkin_date', '>=', $start_time);
            $query->where('checkin_date', '<', $end_time);
        }

        if(isset($filter['checkout_date']) && $filter['checkout_date'] != ""){
            $time_filter = explode(" - ", $filter['checkout_date']);
            $start_time = date("Y-m-d 00:00:00", strtotime($time_filter[0]));
            $end_time = date("Y-m-d 23:59:59", strtotime($time_filter[1]));

            $query->where('checkout_date', '>=', $start_time);
            $query->where('checkout_date', '<', $end_time);
        }

        if(isset($filter['guest_name']) && $filter['guest_name'] != ""){
            $query->where('guest_info->guest_name', 'like', "%".$filter['guest_name']."%");
        }

        if(isset($filter['guest_phone']) && $filter['guest_phone'] != ""){
            $query->where('guest_phone->guest_name', 'like', "%".$filter['guest_phone']."%");
        }

        if (isset($filter['created_at']) && $filter['created_at'] != "") {
            $time_filter = explode(" - ", $filter['created_at']);
            $start_time = date("Y-m-d 00:00:00", strtotime($time_filter[0]));
            $end_time = date("Y-m-d 23:59:59", strtotime($time_filter[1]));

            $query->where('created_at', '>=', $start_time);
            $query->where('created_at', '<', $end_time);
        }
        return $query->count();

    }

    public static function findByKey($key, $value, $columns = ['*'], $with = [])
    {
        $data = DB::table(self::$table)->select($columns)->where($key, $value)->first();
        return $data ? $data : [];
    }
    public static function insert($params)
    {
        return DB::table(self::$table)->insertGetId($params);

    }

    public static function update($id, $params)
    {
        return DB::table(self::$table)->where('id', $id)->update($params);

    }

    public static function updateManyBooking($ids, $data)
    {
        return DB::table(self::$table)->whereIn('id', $ids)->update($data);
    }

    public static function delete($id)
    {
        return DB::table(self::$table)->where('id', $id)->delete();

    }

    public static function deleteManyBooking($ids)
    {
        return DB::table(self::$table)->whereIn('id', $ids)->delete();
    }
}
