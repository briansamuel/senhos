<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Auth\User as Authenticatable;

class AgentModel extends Authenticatable
{
    use Notifiable;

    protected $table = 'agents';
    protected $guarded = 'agent';

    public static function getAll()
    {
        $result = AgentModel::get();
        return $result ? $result : [];
    }

    public static function getMany($pagination, $sort, $filter)
    {
        $pagination['perpage']  = isset($pagination['perpage']) ? $pagination['perpage'] : 20;
        $offset = ($pagination['page'] - 1) * $pagination['perpage'];
        $query = AgentModel::skip($offset)->take($pagination['perpage']);
        if(isset($filter['status']) && $filter['status'] != ""){
            $query->where('status', '=', $filter['status']);
        }
        if(isset($filter['email']) && $filter['email'] != ""){
            $query->where('email', 'like', "%".$filter['email']."%");
        }
        if(isset($filter['username']) && $filter['username'] != ""){
            $query->where('username', 'like', "%".$filter['username']."%");
        }
        if(isset($filter['full_name']) && $filter['full_name'] != ""){
            $query->where('full_name', 'like', "%".$filter['full_name']."%");
        }
        if (isset($filter['created_at']) && $filter['created_at'] != "") {
            $time_filter = explode(" - ", $filter['created_at']);
            $start_time = date("Y-m-d 00:00:00", strtotime($time_filter[0]));
            $end_time = date("Y-m-d 23:59:59", strtotime($time_filter[1]));

            $query->where('created_at', '>=', $start_time);
            $query->where('created_at', '<', $end_time);
        }
        if(isset($sort['field']) && $sort['field'] === "created_at"){
            $query->orderBy('created_at', $sort['sort']);
        }
        return $query->get();
    }

    public static function totalRows() {
        $result = AgentModel::count();
        return $result;
    }

    public static function findByKey($key, $value, $columns = ['*'])
    {
        $result = AgentModel::select($columns)->where($key, $value)->first();
        return $result ? $result : [];
    }

    public static function findById($id, $columns = ['*'])
    {
        $result = AgentModel::select($columns)->where('id', $id)->first();
        return $result ? $result : [];
    }

    public static function updateAgent($id, $data)
    {
        return AgentModel::where('id', $id)->update($data);
    }

    public static function deleteManyUser($ids)
    {
        return AgentModel::whereIn('id', $ids)->delete();
    }

    public static function updateManyUser($ids, $data)
    {
        return AgentModel::whereIn('id', $ids)->update($data);
    }

    public static function deleteUser($id)
    {
        return AgentModel::where('id', $id)->delete();
    }

    public static function checkEmailExist($email)
    {
        return AgentModel::where('email', $email)->exists();
    }

    public static function updateUser($id, $data)
    {
        return AgentModel::where('id', $id)->update($data);
    }
}
