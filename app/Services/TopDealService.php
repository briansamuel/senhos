<?php

namespace App\Services;

use App\Models\TopDealModel;
use App\Transformers\TopDealTransformer;

class TopDealService
{

    public static function totalRows() {
        $result = TopDealModel::totalRows();
        return $result;
    }

    public static function add($params)
    {
        $params['created_at'] = date("Y-m-d H:i:s");
        $params['updated_at'] = date("Y-m-d H:i:s");
        return TopDealModel::add($params);
    }

    public function edit($id, $params)
    {
        $params['updated_at'] = date("Y-m-d H:i:s");
        return TopDealModel::edit($id, $params);
    }

    public function delete($ids)
    {
        return TopDealModel::delete($ids);
    }

    public function detail($id)
    {
        return TopDealModel::findById($id);
    }

    public function getList(array $params)
    {
        $total = self::totalRows();
        $pagination = $params['pagination'];
        $sort = isset($params['sort']) ? $params['sort'] : [];
        $query = isset($params['query']) ? $params['query'] : [];

        $result = TopDealModel::getMany($pagination, $sort, $query);
        $result = TopDealTransformer::transformCollection($result);

        $data['data'] = $result;
        $data['meta']['page'] = isset($pagination['page']) ? $pagination['page'] : 1;
        $data['meta']['perpage'] = isset($pagination['perpage']) ? $pagination['perpage'] : 20;
        $data['meta']['total'] = $total;
        $data['meta']['pages'] = ceil($total / $data['meta']['perpage']);
        $data['meta']['rowIds'] = self::getListIDs($result);

        return $data;
    }

    public static function getForFrontEnd($limit = 2)
    {
        $result = TopDealModel::getForFrontEnd($limit);

        return $result;
    }

    public function getListIDs($data) {

        $ids = array();

        foreach($data as $row) {
            array_push($ids, $row->id);
        }

        return $ids;
    }

}
