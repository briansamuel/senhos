<?php

namespace App\Services;

class DashboardService
{

    public static function takeNewGuest($quantity) {
        $result = GuestService::takeNew($quantity);
        return $result;
    }

    public static function takeNewContact($quantity) {
        $result = ContactService::takeNew($quantity);
        return $result;
    }

    public static function takeNewNews($quantity) {

        $params = array('post_type' => 'news');
        $result = PostService::takeNew($quantity, $params);
        return $result;
    }

    public static function takeNewService($quantity) {

        $params = array('post_type' => 'service');
        $result = PostService::takeNew($quantity, $params);
        return $result;
    }

    public static function totalGuest() {
        $result = GuestService::totalRows();
        return $result;
    }

    public static function totalContact() {
        $result = ContactService::totalRows();
        return $result;
    }

    public static function totalNews() {

        $params = array('post_type' => 'news');
        $result = PostService::totalRows($params);
        return $result;
    }

    public static function totalService() {

        $params = array('post_type' => 'service');
        $result = PostService::totalRows($params);
        return $result;
    }

}
