<?php
namespace App\Services;

use App\Models\BookingModel;

use Illuminate\Support\Str;

class BookingService
{
	public static function totalRows($params) {
        $result = BookingModel::totalRows($params);
        return $result;
	}
	
	public function getMany($limit, $offset, $filter)
	{
		$result = BookingModel::getMany($limit, $offset, $filter);
        return $result ? $result : [];
	}
	public function findByKey($key, $value)
	{
        $result = BookingModel::findByKey($key, $value);
        return $result ? $result : [];
	}
	public static function insert($params)
	{
		
		$insert['host_id'] =isset($params['host_id']) ? $params['host_id'] : 0; 
        $insert['room_id'] =isset($params['room_id']) ? $params['room_id'] : 0; 
        $insert['guest_id'] =isset($params['guest_id']) ? $params['guest_id'] : 0;
        $insert['checkin_date'] =isset($params['checkin_date']) ? $params['checkin_date'] : date("Y-m-d H:i:s");
        $insert['checkout_date'] =isset($params['checkout_date']) ? $params['checkout_date'] : date("Y-m-d H:i:s");
        $insert['night_booking'] =isset($params['night_booking']) ? $params['night_booking'] : 1;
        $insert['room_amount'] =isset($params['room_amount']) ? $params['room_amount'] : 1; 
        $insert['room_type'] =isset($params['room_type']) ? $params['room_type'] : ''; 
        $insert['bed_type'] =isset($params['bed_type']) ? $params['bed_type'] : '';
        $insert['guest_amount'] =isset($params['guest_amount']) ? $params['guest_amount'] : 1;
        $insert['payment_method'] =isset($params['guest_payment_methodamount']) ? $params['payment_method'] : '';
        $insert['guest_info'] =isset($params['guest_info']) ? $params['guest_info'] : '';
        $insert['booking_price'] =isset($params['booking_price']) ? $params['booking_price'] : 0;
        $insert['booking_status'] =isset($params['booking_status']) ? $params['booking_status'] : 'pending';
		$insert['created_by_agent'] = isset($params['created_by_agent']) ? $params['created_by_agent'] : 0; 
		$insert['created_at'] = date("Y-m-d H:i:s"); 
		$insert['updated_at'] = date("Y-m-d H:i:s"); 
		return BookingModel::insert($insert);		
	}
	public function update($id, $params)
	{
		$update['host_id'] =isset($params['host_id']) ? $params['host_id'] : 0; 
        $update['room_id'] =isset($params['room_id']) ? $params['room_id'] : 0; 
        $update['guest_id'] =isset($params['guest_id']) ? $params['guest_id'] : 0;
        $update['checkin_date'] =isset($params['checkin_date']) ? $params['checkin_date'] : date("Y-m-d H:i:s");
        $update['checkout_date'] =isset($params['checkout_date']) ? $params['checkout_date'] : date("Y-m-d H:i:s");
        $update['night_booking'] =isset($params['night_booking']) ? $params['night_booking'] : 1;
        $update['room_amount'] =isset($params['room_amount']) ? $params['room_amount'] : 1; 
        $update['room_type'] =isset($params['room_type']) ? $params['room_type'] : ''; 
        $update['bed_type'] =isset($params['bed_type']) ? $params['bed_type'] : '';
        $update['guest_amount'] =isset($params['guest_amount']) ? $params['guest_amount'] : 1;
        $update['payment_method'] =isset($params['guest_payment_methodamount']) ? $params['payment_method'] : '';
        $update['guest_info'] =isset($params['guest_info']) ? $params['guest_info'] : '';
        $update['booking_status'] =isset($params['booking_status']) ? $params['booking_status'] : '';
		$update['updated_at'] = date("Y-m-d H:i:s"); 
		return BookingModel::update($id, $update);		
	}

	public function updateMany($ids, $data)
    {
        return BookingModel::updateManyBooking($ids, $data);
	}
	
	public function deleteMany($ids)
    {
        return BookingModel::deleteManyBooking($ids);
	}
	
	public function delete($id)
	{
		return BookingModel::delete($id);		
	}

	public function getList(array $params)
    {
        $total = self::totalRows($params['query']);
        $pagination = $params['pagination'];
        $sort = isset($params['sort']) ? $params['sort'] : [];
        $query = isset($params['query']) ? $params['query'] : [];

        $result = BookingModel::getMany($pagination, $sort, $query);

        $data['data'] = $result;
        $data['meta']['page'] = isset($pagination['page']) ? $pagination['page'] : 1;
        $data['meta']['perpage'] = isset($pagination['perpage']) ? $pagination['perpage'] : 20;
        $data['meta']['total'] = $total;
        $data['meta']['pages'] = ceil($total / $data['meta']['perpage']);
		$data['meta']['rowIds'] = self::getListIDs($result);
        return $data;
	}
	
	public function getListIDs($data) {

		$ids = array();

		foreach($data as $row) {
			array_push($ids, $row->id);
		}

		return $ids;
	}
}