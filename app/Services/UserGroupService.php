<?php

namespace App\Services;

use App\Models\UserGroupModel;

class UserGroupService
{
    /*
    * lấy danh sách quyền admin
    */
    public static function getGroupActive()
    {

        return UserGroupModel::getGroupActive();
    }

    /*
    * lấy danh sách quyền admin
    */
    public static function getGroupById($ids)
    {
        if (!isset($ids) || !$ids || empty($ids)) {
            return [];
        }

        return UserGroupModel::getById($ids);
    }

    public function getList(array $params)
    {
        $total = self::totalRows();
        $pagination = $params['pagination'];
        $sort = isset($params['sort']) ? $params['sort'] : [];
        $query = isset($params['query']) ? $params['query'] : [];

        $result = UserGroupModel::getMany($pagination, $sort, $query);

        $data['data'] = $result;
        $data['meta']['page'] = isset($pagination['page']) ? $pagination['page'] : 1;
        $data['meta']['perpage'] = isset($pagination['perpage']) ? $pagination['perpage'] : 20;
        $data['meta']['total'] = $total;
        $data['meta']['pages'] = ceil($total / $data['meta']['perpage']);
        $data['meta']['rowIds'] = self::getListIDs($result);

        return $data;
    }

    public function getListIDs($data) {

        $ids = array();

        foreach($data as $row) {
            array_push($ids, $row->id);
        }

        return $ids;
    }

    public static function totalRows() {
        $result = UserGroupModel::totalRows();
        return $result;
    }

    public static function checkExistsGroup($permission, $id = ''){
        if(!isset($permission) || !$permission || $permission == ""){
            return false;
        }
        $userGroupInfo =  UserGroupModel::findExistByKey('permission', $permission, $id);
        if($userGroupInfo){
            return $userGroupInfo->id;
        }else{
            return false;
        }
    }

    public static function checkExistsName($group_name, $id = ''){
        if(!isset($group_name) || !$group_name || $group_name == ""){
            return false;
        }
        $userGroupInfo =  UserGroupModel::findExistByKey('group_name', $group_name, $id);
        if($userGroupInfo){
            return $userGroupInfo->group_name;
        }else{
            return false;
        }
    }

    public static function add($params)
    {
        $params['created_at'] = date("Y-m-d H:i:s");
        $params['updated_at'] = date("Y-m-d H:i:s");
        return UserGroupModel::add($params);
    }

    public static function edit($id, $data)
    {
        $data['updated_at'] = date("Y-m-d H:i:s");
        return UserGroupModel::edit($id, $data);
    }

    public function detail($id)
    {
        return UserGroupModel::findById($id);
    }

    public function delete($ids)
    {
        return UserGroupModel::delete($ids);
    }

    public function updateMany($ids, $data)
    {
        return UserGroupModel::updateMany($ids, $data);
    }

    public function deleteMany($ids)
    {
        return UserGroupModel::deleteMany($ids);
    }
}
