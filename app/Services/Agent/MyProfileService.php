<?php
namespace App\Services\Agent;

use App\Exceptions\CheckException;
use App\Services\Auth\AgentAuthService;
use App\Services\AgentService;

class MyProfileService
{
    public static function changePassword(string $password, string $new_password)
    {
        if ( !AgentAuthService::checkCurrentPassword($password)) {
            throw new CheckException('password_not_valid', 20, []);
        }

        $update = AgentService::updatePassword($new_password);
        if ($update) {
            $result = [
                'success' => true,
                'message' => 'Thay đổi mật khẩu thành công!'
            ];
            return $result;
        }

        throw new CheckException('update_password_not_success', 11, ['res' => $update]);
    }
}